package cz.agents.cycleplanner.citydatastorage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.log4j.Logger;

public class CycleCityDataLoader {

	private static final Logger log = Logger.getLogger(CycleCityDataLoader.class);
	
	public static Collection<InputStream> getCycleCitiesDataAsStream() throws URISyntaxException, IOException {

		final Collection<InputStream> streams = new ArrayList<>();
		final File jarFile = new File(CycleCityDataLoader.class.getProtectionDomain().getCodeSource().getLocation()
				.getPath());
		log.info("Resource directory (jar archive): " + jarFile);

		if (jarFile.isFile()) { // Run with JAR file
			final JarFile jar = new JarFile(jarFile);
			// gives ALL entries in jar
			final Enumeration<JarEntry> entries = jar.entries(); 

			while (entries.hasMoreElements()) {
				final String name = entries.nextElement().getName();

				// filter only serialized city data (files with suffix ser)
				if (name.matches(".*.ser")) { 
					log.info("city data file: " + name);
					streams.add(CycleCityDataLoader.class.getResourceAsStream("/" + name));
				}
			}

			jar.close();

		} else { // Run in IDE
			for (File file : jarFile.listFiles()) {
				// filter only serialized city data (files with suffix ser)
				if (file.getName().matches(".*.ser")) {
					log.info("city data file: " + file.getName());
					streams.add(CycleCityDataLoader.class.getResourceAsStream("/" + file.getName()));
				}

			}

		}

		return streams;
	}

}
