package cz.agents.cycleplanner.citydatastorage;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;

public class CycleCityDataLoaderTest {

	@Test
	public void test() throws URISyntaxException, IOException {
		Collection<InputStream> streams = CycleCityDataLoader.getCycleCitiesDataAsStream();
		
		Assert.assertNotNull(streams);
		Assert.assertFalse(streams.isEmpty());
	}

}
