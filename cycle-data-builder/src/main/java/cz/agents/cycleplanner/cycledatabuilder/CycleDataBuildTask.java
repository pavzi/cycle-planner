package cz.agents.cycleplanner.cycledatabuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.apache.log4j.Logger;

import cz.agents.basestructures.BoundingBox;
import cz.agents.basestructures.Graph;
import cz.agents.basestructures.Zone;
import cz.agents.cycleplanner.cyclegraphbuilder.CycleGraphBuilder;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

public class CycleDataBuildTask {

	private static final Logger log = Logger.getLogger(CycleDataBuildTask.class);

	public static void main(String[] args) {
		CycleDataBuilderCli cityBuilderCli = new CycleDataBuilderCli(args);
		
		CycleGraphBuilder cycleGraphBuilder = new CycleGraphBuilder(cityBuilderCli);
		
		int srid = 2065;
		Graph<CycleNode, CycleEdge> cycleGraph = cycleGraphBuilder.build();
		BoundingBox boundingBox = new BoundingBox(cityBuilderCli.getLeftE6(),
				cityBuilderCli.getBottomE6(), cityBuilderCli.getRightE6(),
				cityBuilderCli.getTopE6());
		Zone cycleZone = new Zone(cityBuilderCli.getCityName(), srid, cycleGraph, boundingBox);

		log.info("Saving cycle city data...");

		try {
			File outputFile = cityBuilderCli.getOutputFile();
			FileOutputStream fos = new FileOutputStream(outputFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(cycleZone);
			oos.close();
		} catch (IOException ex) {
			log.error(ex.getMessage());
		}

		log.info("cycle city data saved in file " + cityBuilderCli.getOutputFile().getName());
	}

}
