package cz.agents.cycleplanner.cycledatabuilder;

import java.io.File;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

public class CycleDataBuilderCli {

	private static final Logger log = Logger.getLogger(CycleDataBuilderCli.class);

	private int topE6;
	private int leftE6;
	private int rightE6;
	private int bottomE6;
	private String cityName;
	private File osmFile;
	private File outputFile;
	
	private boolean junctionExtensionAllowed = false;

	public CycleDataBuilderCli(String[] args) {
		Options options = new Options();

		// TODO add description of command line options
		options.addOption("top", true, "description");
		// options.addOption("topE6", true, "description");
		options.addOption("left", true, "description");
		// options.addOption("leftE6", true, "description");
		options.addOption("bottom", true, "description");
		// options.addOption("bottomE6", true, "description");
		options.addOption("right", true, "description");
		// options.addOption("rightE6", true, "description");
		options.addOption("cityName", true, "description");
		options.addOption("osmFile", true, "description");
		options.addOption("outputFile", true, "description");
		
		options.addOption("junction", false, "Specifies, whether building process should extend junctions.");

		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {

			HelpFormatter formatter = new HelpFormatter();
			log.error("Wrong arguments!");
			formatter.printHelp("Please, use following arguments", options);
			System.exit(-1);
		}

		if (cmd.hasOption("top") && cmd.hasOption("left") && cmd.hasOption("bottom") && cmd.hasOption("right")
				&& cmd.hasOption("cityName") && cmd.hasOption("osmFile")) {
			topE6 = (int) (Double.parseDouble(cmd.getOptionValue("top")) * 1E6);
			leftE6 = (int) (Double.parseDouble(cmd.getOptionValue("left")) * 1E6);
			rightE6 = (int) (Double.parseDouble(cmd.getOptionValue("right")) * 1E6);
			bottomE6 = (int) (Double.parseDouble(cmd.getOptionValue("bottom")) * 1E6);
			cityName = cmd.getOptionValue("cityName");
			osmFile = new File(cmd.getOptionValue("osmFile"));
			outputFile = new File(cmd.getOptionValue("outputFile"));
		} else {
			HelpFormatter formatter = new HelpFormatter();
			log.error("You did not specified any of the arguments!");
			formatter.printHelp("Please, all following arguments are mandatory", options);
			System.exit(-1);
		}
		
		if (cmd.hasOption("junction")) {
			junctionExtensionAllowed = true;
		}
	}

	public int getTopE6() {
		return topE6;
	}

	public int getLeftE6() {
		return leftE6;
	}

	public int getRightE6() {
		return rightE6;
	}

	public int getBottomE6() {
		return bottomE6;
	}

	public String getCityName() {
		return cityName;
	}

	public File getOsmFile() {
		return osmFile;
	}

	public File getOutputFile() {
		return outputFile;
	}

	public boolean isJunctionExtensionAllowed() {
		return junctionExtensionAllowed;
	}
}
