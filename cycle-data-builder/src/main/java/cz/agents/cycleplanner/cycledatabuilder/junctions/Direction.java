package cz.agents.cycleplanner.cycledatabuilder.junctions;

public enum Direction {
	STRAIGHT, LEFT, RIGHT, U_TURN;
}
