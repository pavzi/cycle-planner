package cz.agents.cycleplanner.cycledatabuilder.junctions;

import org.apache.log4j.Logger;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.EPSGProjection;
import cz.agents.geotools.EdgeUtil;

public class JunctionCoordinatesUtil {

	private final static Logger log = Logger.getLogger(JunctionCoordinatesUtil.class);

	public final static double RADIUS = 1d;
	public final static double NEW_NODE_ANGLE = Math.toRadians(10d);
	private final static double COS = Math.cos(NEW_NODE_ANGLE);
	private final static double SIN = Math.sin(NEW_NODE_ANGLE);
	private static EPSGProjection projection;

	static {
		try {
			projection = new EPSGProjection(2065);
		} catch (FactoryException | TransformException e) {
			log.error(e.getMessage(), e.fillInStackTrace());
		}
	}

	/**
	 * x3=x1+((x2-x1)*r*cosθ)/l+((y2-y1)*r*sinθ)/l
	 * y3=y1+((y2-y1)*r*cosθ)/l-((x2-x1)*r*sinθ)/l
	 * 
	 * @param junction
	 * @param outsideJunction
	 * @return
	 */
	public static GPSLocation deriveCoordinatesJunctionOutgoingNode(GPSLocation junction,
			GPSLocation outsideJunction) {

		double length = EdgeUtil.computeEuclideanDistance(junction, outsideJunction);

		double xJunction = junction.lonProjected;
		double yJunction = junction.latProjected;

		double xNode = outsideJunction.lonProjected;
		double yNode = outsideJunction.latProjected;

		int x = (int) Math.round(xJunction + ((xNode - xJunction) * RADIUS * COS) / length
				+ ((yNode - yJunction) * RADIUS * SIN) / length);
		int y = (int) Math.round(yJunction + ((yNode - yJunction) * RADIUS * COS) / length
				- ((xNode - xJunction) * RADIUS * SIN) / length);

		// log.debug("Junction: " + xJunction + " " + yJunction);
		// log.debug("Junction: " + junction.getLongitudeE6() + " " +
		// junction.getLatitudeE6());
		// log.debug("Old outcoming node: " + xNode + " " + yNode);
		// log.debug("Old outcoming node: " + node.getLongitudeE6() + " " +
		// node.getLatitudeE6());
		// log.debug("New outcomming node: " + x + " " + y);

		return projection.getSphericalGPSLocation(y, x, junction.elevation);
	}

	/**
	 * x3=x1+((x2-x1)*r*cosθ)/l-((y2-y1)*r*sinθ)/l
	 * y3=y1+((y2-y1)*r*cosθ)/l+((x2-x1)*r*sinθ)/l
	 * 
	 * @param junction
	 * @param outsideJunction
	 * @return
	 */
	public static GPSLocation deriveCoordinatesJunctionIncomingNode(GPSLocation junction,
			GPSLocation outsideJunction) {

		double length = EdgeUtil.computeEuclideanDistance(junction, outsideJunction);

		double xJunction = junction.lonProjected;
		double yJunction = junction.latProjected;

		double xNode = outsideJunction.lonProjected;
		double yNode = outsideJunction.latProjected;

		int x = (int) Math.round(xJunction + ((xNode - xJunction) * RADIUS * COS) / length
				- ((yNode - yJunction) * RADIUS * SIN) / length);
		int y = (int) Math.round(yJunction + ((yNode - yJunction) * RADIUS * COS) / length
				+ ((xNode - xJunction) * RADIUS * SIN) / length);

		// log.debug("Junction: " + xJunction + " " + yJunction);
		// log.debug("Junction: " + junction.getLongitudeE6() + " " +
		// junction.getLatitudeE6());
		// log.debug("Old incoming node: " + xNode + " " + yNode);
		// log.debug("Old outcoming node: " + node.getLongitudeE6() + " " +
		// node.getLatitudeE6());
		// log.debug("New incomming node: " + x + " " + y);

		return projection.getSphericalGPSLocation(y, x, junction.elevation);
	}
}
