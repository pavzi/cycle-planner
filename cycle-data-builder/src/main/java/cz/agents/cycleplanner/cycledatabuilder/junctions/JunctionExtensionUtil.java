package cz.agents.cycleplanner.cycledatabuilder.junctions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import cz.agents.basestructures.GPSLocation;
import cz.agents.cycleplanner.cyclegraphbuilder.NewNodeIdUtil;
import cz.agents.geotools.AngleUtil;
import cz.agents.tns.structures.TnsEdge;
import cz.agents.tns.structures.TnsGraph;
import cz.agents.tns.structures.TnsNode;

public class JunctionExtensionUtil {
	
	private static final Pattern PERMITED_EDGES = Pattern.compile("way::highway::living_street|"
			+ "way::highway::primary|"
			+ "way::highway::primary_link|"
			+ "way::highway::residential|"
			+ "way::highway::secondary|"
			+ "way::highway::secondary_link|"
			+ "way::highway::tertiary|"
			+ "way::highway::tertiary_link|"
			+ "way::highway::service|"
			+ "way::highway::unclassified|"
			+ "way::highway::road");
	
	private static final Pattern MOTORWAY = Pattern.compile("way::highway::motroway|way::highway::motroway_link");
	private static final Pattern TRUNK = Pattern.compile("way::highway::trunk|way::highway::trunk_link");
	private static final Pattern PRIMARY = Pattern.compile("way::highway::primary|way::highway::primary_link");
	private static final Pattern SECONDARY = Pattern.compile("way::highway::secondary|way::highway::secondary_link");
	private static final Pattern TERTIARY = Pattern.compile("way::highway::tertiary|way::highway::tertiary_link");
	private static final Pattern ROAD = Pattern.compile("way::highway::road");
	private static final Pattern UNCLASSIFIED = Pattern.compile("way::highway::unclassified");
	private static final Pattern RESIDENTIAL = Pattern.compile("way::highway::residential");
	private static final Pattern LIVING_STREET = Pattern.compile("way::highway::living_street");
	private static final Pattern SERVICE = Pattern.compile("way::highway::service");

	private static final Pattern TRAFFIC_SIGNALS = Pattern.compile("node::highway::traffic_signals|"
			+ "node::crossing::traffic_signals");
	
	private static final Logger log = Logger.getLogger(JunctionExtensionUtil.class);
		
	public static void extend(TnsGraph generalGraphBuilder) {
		
		Collection<TnsNode> potentionalJunctions = new ArrayList<>(
				generalGraphBuilder.getAllNodes());

		for (Iterator<TnsNode> junctionIterator = potentionalJunctions.iterator(); junctionIterator
				.hasNext();) {
			TnsNode node = junctionIterator.next();
			
			Collection<TnsEdge> incomingEdges = new HashSet<TnsEdge>(
					generalGraphBuilder.getAllNodeIncomingEdges(node.id));
			Collection<TnsEdge> outgoingEdges = new HashSet<TnsEdge>(
					generalGraphBuilder.getAllNodeOutgoingEdges(node.id));
						
//			removeNotPermittedEdges(incomingEdges);
//			removeNotPermittedEdges(outgoingEdges);
			
			if (!areAllEdgesPermited(incomingEdges) || !areAllEdgesPermited(outgoingEdges)) {
				junctionIterator.remove();
				continue;
			}
			
			Set<Long> neighbors = getNeighbors(node.id, incomingEdges);
			
			neighbors.addAll(getNeighbors(node.id, outgoingEdges));
			int numberOfNeighbors = neighbors.size();
			
			if (numberOfNeighbors <= 2) {
				junctionIterator.remove();
			}
		}
		
		log.info("Number of junctions: " + potentionalJunctions.size());
		
		for (TnsNode node : potentionalJunctions) {
			
			Collection<TnsEdge> incomingEdges = new HashSet<TnsEdge>(
					generalGraphBuilder.getAllNodeIncomingEdges(node.id));
			Collection<TnsEdge> outgoingEdges = new HashSet<TnsEdge>(
					generalGraphBuilder.getAllNodeOutgoingEdges(node.id));
			
			// TODO assign value
			boolean trafficLightsPresent = false;
			
			// vymaz hrany z generalGraphBuilder
			for (TnsEdge generalEdge : incomingEdges) {
				generalGraphBuilder.removeEdge(generalEdge);
			}
			for (TnsEdge generalEdge : outgoingEdges) {
				generalGraphBuilder.removeEdge(generalEdge);
			}				
			
			// vytvor in nodes a pridaj do generalGraphBuilder
			// vytvor in edges a pridaj do generalGraphBuilder
			
			FlowIntensity mainTrafficFlow = FlowIntensity.SMALL;
			
			Collection<TnsEdge> newIncomingEdges = new ArrayList<TnsEdge>();
			
			for (TnsEdge incomingEdge : incomingEdges) {
				
				if (!trafficLightsPresent && trafficLightPresent(incomingEdge)) {
					trafficLightsPresent = true;
				}
				
				FlowIntensity edgeTrafficFlow = assignFlowIntensity(incomingEdge.getTags());

				if (edgeTrafficFlow.getValue() > mainTrafficFlow.getValue()) {
					mainTrafficFlow = edgeTrafficFlow;
				}
				
				TnsNode incomingEdgeFromNode = generalGraphBuilder.getNode(incomingEdge.fromId);
				TnsNode inNode = createInNode(node, incomingEdgeFromNode);
				TnsEdge inEdge = createInEdge(inNode, incomingEdge);
				
				generalGraphBuilder.addNode(inNode);
				generalGraphBuilder.addEdge(inEdge);
				
				newIncomingEdges.add(inEdge);
			}
			
			// vytvor out nodes a pridaj do generalGraphBuilder
			// vytvor out edges a pridaj do generalGraphBuilder
			Collection<TnsEdge> newOutgoingEdges = new ArrayList<TnsEdge>();
			
			for (TnsEdge outgoingEdge : outgoingEdges) {
				
				if (!trafficLightsPresent && trafficLightPresent(outgoingEdge)) {
					trafficLightsPresent = true;
				}
				
				FlowIntensity edgeTrafficFlow = assignFlowIntensity(outgoingEdge.getTags());

				if (edgeTrafficFlow.getValue() > mainTrafficFlow.getValue()) {
					mainTrafficFlow = edgeTrafficFlow;
				}
				
				TnsNode outogingEdgeToNode = generalGraphBuilder.getNode(outgoingEdge.toId);
				TnsNode outNode = createOutNode(node, outogingEdgeToNode);
				TnsEdge outEdge = createOutEdge(outNode, outgoingEdge);
				
				generalGraphBuilder.addNode(outNode);
				generalGraphBuilder.addEdge(outEdge);
				
				newOutgoingEdges.add(outEdge);
			}
				
			// inner edge
			boolean moreThanOneMainTrafficFlow = isMoreThanOneMainTrafficFlow(newIncomingEdges, newOutgoingEdges,
					mainTrafficFlow);

			for (TnsEdge newIncomingEdge : newIncomingEdges) {
				for (TnsEdge newOutgoingEdge : newOutgoingEdges) {

					// Uhol pocitam medzi zaciatocnym vrcholom vstupnej hrany, krizovatkovym
					// vrcholom a koncovym vrcholov vystupnej hrany
					GPSLocation from = generalGraphBuilder.getNode(newIncomingEdge.fromId);
					GPSLocation to = generalGraphBuilder.getNode(newOutgoingEdge.toId);
					double angle = AngleUtil.getAngle(from, node, to);

					FlowIntensity flowIncoming = assignFlowIntensity(newIncomingEdge.getTags());
					FlowIntensity flowOutgoing = assignFlowIntensity(newOutgoingEdge.getTags());

					// Parallel - ked idem z vacsej intenzity na nizsiu
					// Cross - ked idem z nizsej intenzity na vyssiu
					// Ak sa incoming a outcoming rovnaju a rovnaju sa aj najvysiej hodnote
					// trafficflow v krizovatke tak

					Traffic traffic;
					if (flowIncoming == mainTrafficFlow && mainTrafficFlow == flowOutgoing) {
						traffic = moreThanOneMainTrafficFlow ? Traffic.CROSS : Traffic.PARALLEL;
					} else if (flowIncoming.getValue() > flowOutgoing.getValue()) {
						traffic = Traffic.PARALLEL;
					} else {
						traffic = Traffic.CROSS;
					}
					
					String innerEdgeTag = getInnerEdgeTag(trafficLightsPresent, angle, mainTrafficFlow, traffic);
					Collection<String> innerEdgeTags = new HashSet<String>();
					
					innerEdgeTags.add(innerEdgeTag);
					
					TnsEdge innerEdge = new TnsEdge(newIncomingEdge.toId, new ArrayList<TnsNode>(),
							newOutgoingEdge.fromId,
							new HashSet<Long>(), innerEdgeTags);
					
					generalGraphBuilder.addEdge(innerEdge);
				}

			}
		}
	}
	
	private static void removeNotPermittedEdges(Collection<TnsEdge> edges) {
		for (Iterator<TnsEdge> edgesIterator = edges.iterator(); edgesIterator.hasNext();) {
			TnsEdge generalEdge = edgesIterator.next();
			boolean isPermitted = false;
			
			for (String	tag : generalEdge.getTags()) {
				if (PERMITED_EDGES.matcher(tag).matches()) {
					isPermitted = true;
					break;
				}
			}
			
			if (!isPermitted) {
				edgesIterator.remove();
			}
		}
	}
	
	private static boolean areAllEdgesPermited(Collection<TnsEdge> edges) {

		for (TnsEdge generalEdge : edges) {
			boolean isPermitted = false;

			for (String tag : generalEdge.getTags()) {
				if (PERMITED_EDGES.matcher(tag).matches()) {
					isPermitted = true;
					break;
				}
			}

			if (!isPermitted) {
				return false;
			}
		}

		return true;
	}
	
	private static Set<Long> getNeighbors(long id, Collection<TnsEdge> edges) {
		Set<Long> neighbors = new HashSet<Long>();
		
		for (TnsEdge edge : edges) {
			if (edge.fromId != id) {
				neighbors.add(edge.fromId);
			}
			
			if (edge.toId != id) {
				neighbors.add(edge.toId);
			}
		}
		
		return neighbors;
	}
	
	/**
	 * If at least one node contains traffic_signals tag, then traffic lights
	 * are present in junction.
	 */
	private static boolean trafficLightPresent(TnsEdge edge) {
		for (String tag : edge.getTags()) {
			if (TRAFFIC_SIGNALS.matcher(tag).matches()) {
				return true;
			}
		}

		return false;
	}
	
	private static boolean isMoreThanOneMainTrafficFlow(Collection<TnsEdge> incomingEdges,
			Collection<TnsEdge> outgoingEdges, FlowIntensity mainTrafficFlow) {

		Set<Long> inNodes = new HashSet<>();

		for (TnsEdge edge : incomingEdges) {
			if (assignFlowIntensity(edge.getTags()) == mainTrafficFlow) {
				inNodes.add(edge.fromId);
			}
		}
		
		Set<Long> outNodes = new HashSet<>();
		
		for (TnsEdge edge : outgoingEdges) {
			if (assignFlowIntensity(edge.getTags()) == mainTrafficFlow) {
				outNodes.add(edge.toId);
			}
		}		

		return inNodes.size() > 2 || outNodes.size() > 2;
	}
		
	private static FlowIntensity assignFlowIntensity(Collection<String> tags) {
		for (String tag : tags) {

			if (MOTORWAY.matcher(tag).matches() || TRUNK.matcher(tag).matches() 
					|| PRIMARY.matcher(tag).matches()) {

				return FlowIntensity.HUGE;
			} else if (SECONDARY.matcher(tag).matches()) {

				return FlowIntensity.BIG;
			} else if (TERTIARY.matcher(tag).matches()) {

				return FlowIntensity.MEDIUM;
			} else if (ROAD.matcher(tag).matches() || UNCLASSIFIED.matcher(tag).matches()
					|| RESIDENTIAL.matcher(tag).matches() || LIVING_STREET.matcher(tag).matches()
					|| SERVICE.matcher(tag).matches()) {

				return FlowIntensity.SMALL;
			}
		}

		return FlowIntensity.SMALL;
	}
	
	private static TnsNode createInNode(TnsNode junction, TnsNode outsideJunction) {
		Collection<String> tags = new HashSet<String>(junction.getTags());
		tags.add("node::junction::in");

		GPSLocation gpsLocation = JunctionCoordinatesUtil.deriveCoordinatesJunctionIncomingNode(
				junction, outsideJunction);
		TnsNode newInNode = new TnsNode(NewNodeIdUtil.getNewNodeId(), gpsLocation.latE6,
				gpsLocation.lonE6, gpsLocation.latProjected, gpsLocation.lonProjected,
				gpsLocation.elevation, tags);

		return newInNode;
	}
	
	private static TnsNode createOutNode(TnsNode junction, TnsNode outsideJunction) {
		Collection<String> tags = new HashSet<String>(junction.getTags());
		tags.add("node::junction::out");

		GPSLocation gpsLocation = JunctionCoordinatesUtil.deriveCoordinatesJunctionOutgoingNode(
				junction, outsideJunction);
		TnsNode newInNode = new TnsNode(NewNodeIdUtil.getNewNodeId(), gpsLocation.latE6,
				gpsLocation.lonE6, gpsLocation.latProjected, gpsLocation.lonProjected,
				gpsLocation.elevation, tags);

		return newInNode;
	}
		
	private static TnsEdge createInEdge(TnsNode newToNode, TnsEdge oldIncomingEdge) {
		List<TnsNode> via = new ArrayList<>(oldIncomingEdge.getVia());
		Collection<String> newTags = new HashSet<String>(oldIncomingEdge.getTags());
		Collection<Long> wayIds = new HashSet<Long>(oldIncomingEdge.getWayIds());

		return new TnsEdge(oldIncomingEdge.fromId, via, newToNode.id, wayIds, newTags);
	}
	
	private static TnsEdge createOutEdge(TnsNode newFromNode, TnsEdge oldOutgoingEdge) {
		List<TnsNode> via = new ArrayList<>(oldOutgoingEdge.getVia());
		Collection<String> newTags = new HashSet<String>(oldOutgoingEdge.getTags());
		Collection<Long> wayIds = new HashSet<Long>(oldOutgoingEdge.getWayIds());

		return new TnsEdge(newFromNode.id, via, oldOutgoingEdge.toId, wayIds, newTags);
	}
	
	private static String getInnerEdgeTag(boolean trafficLightsPresent, double angle, FlowIntensity flowIntensity, Traffic traffic) {
		StringBuilder tagBuilder = new StringBuilder();
		tagBuilder.append("way::turn::");

		if (trafficLightsPresent) {
			tagBuilder.append("traffic_lights_");
		}

		Direction direction = assignDirection(angle);
		tagBuilder.append(direction.toString().toLowerCase());
		tagBuilder.append("_");


		tagBuilder.append(String.valueOf(flowIntensity.getValue()));

		tagBuilder.append(traffic.toString().toLowerCase());

		return tagBuilder.toString();
	}
	
	private static Direction assignDirection(double angle) {
		if (angle <= 15 || angle >= (360 - 15)) {
			return Direction.U_TURN;
		} else if (angle > 15 && angle < (180 - 15)) {
			return Direction.RIGHT;
		} else if (angle >= (180 - 15) && angle <= (180 + 15)) {
			return Direction.STRAIGHT;
		}

		return Direction.LEFT;
	}
	
}
