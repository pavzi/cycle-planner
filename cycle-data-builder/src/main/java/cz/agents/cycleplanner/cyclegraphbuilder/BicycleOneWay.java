package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.regex.Pattern;

public class BicycleOneWay extends OneWay {

	private static final Pattern bicycleOneWay = Pattern
			.compile("way::bicycle:backward::no");

	private static final Pattern bicycleNoOneWay = Pattern
			.compile("way::cycleway::opposite|"
					+ "way::cycleway::opposite_lane|"
					+ "way::cycleway::opposite_track|"
					+ "way::oneway:bicycle::no|"
					+ "way::bicycle:oneway::no|"
					+ "way::bicycle:backward::yes");
	
	/**
	 * TODO javadoc -- use super and also check if it equals for bicycles
	 * {@inheritDoc}
	 */
	@Override
	protected boolean checkOneWay(String tag) {
		return super.checkOneWay(tag) || bicycleOneWay.matcher(tag).matches();
	}

	/**
	 * TODO javadoc -- use super and also check if it equals for bicycles
	 * {@inheritDoc}
	 */
	@Override
	protected boolean checkNoOneWay(String tag) {
		return super.checkNoOneWay(tag) || bicycleNoOneWay.matcher(tag).matches();
	}

}
