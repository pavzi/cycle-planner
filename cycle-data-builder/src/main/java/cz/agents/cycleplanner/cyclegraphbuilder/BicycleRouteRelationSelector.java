package cz.agents.cycleplanner.cyclegraphbuilder;

import cz.agents.tns.selector.Conjunction;
import cz.agents.tns.selector.Selector;

public class BicycleRouteRelationSelector extends Selector {

	public BicycleRouteRelationSelector() {
		super();
	}

	@Override
	public void init() {
		include.add(new Conjunction(false, "relation::route::bicycle"));
	}

}
