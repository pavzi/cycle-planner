package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.Collection;
import java.util.List;

import cz.agents.tns.structures.TnsEdge;
import cz.agents.tns.structures.TnsGraph;
import cz.agents.tns.structures.TnsNode;
import cz.agents.tns.structures.TnsRelation;
import cz.agents.tns.structures.TnsRelationMember;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class BicycleRouteTnsRelation extends TnsRelation {

	// private static final Logger log =
	// Logger.getLogger(BicycleRouteRelationWrapper.class);

	public BicycleRouteTnsRelation(long relationId, String relationType,
			List<TnsRelationMember> members, Collection<String> tags) {
		super(relationId, relationType, members, tags);
	}

	// TODO take care about role::forward/backward
	// If a route should be followed in only one direction for some or
	// all of its length, the "role" can indicate this for some or all
	// of the constituent ways. "Forward" means the route follows this
	// way only in the direction of the way, and "backward" means the
	// route runs only against the direction of the way.
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void apply(TnsGraph generalGraph) {

		for (TnsRelationMember generalRelationMember : members) {

			long memberId = generalRelationMember.getId();

			if (generalRelationMember.getType().equals("node")) {
				// log.info("Relation member of type NODE: ");

				applyNode(memberId, generalGraph);

			} else if (generalRelationMember.getType().equals("way")) {
				// log.info("Relation member of type WAY: ");

				applyWay(memberId, generalGraph);

			} else if (generalRelationMember.getType().equals("relation")) {
				// log.info("Relation member of type RELATION: ");

				applyRelation(memberId, generalGraph);
			}
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param meberId
	 */
	private void applyNode(long memberId, TnsGraph generalGraph) {

		if (generalGraph.containsNode(memberId)) {

			TnsNode generalNode = generalGraph.getNode(memberId);

			// log.info("- before adding tags: " +
			// generalNode.getTags().toString());

			generalNode.addTags(tags);

			// log.info("-  after adding tags: " +
			// generalNode.getTags().toString());
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param memberId
	 */
	private void applyWay(long memberId, TnsGraph generalGraph) {
		if (generalGraph.containsWay(memberId)) {

			Collection<TnsEdge> generalEdges = generalGraph.getAllEdgesOfWay(memberId);

			for (TnsEdge generalEdge : generalEdges) {
				// log.info("Before adding tags from relation: " +
				// generalEdge.getTags().toString());

				generalEdge.addTags(tags);

				// log.info("New tags from relation: " +
				// generalEdge.getTags().toString());
			}
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param meberId
	 */
	private void applyRelation(long memberId, TnsGraph generalGraph) {
		if (generalGraph.containsRelation(memberId)) {

			TnsRelation relation = generalGraph.getRelation(memberId);

			// log.info("- before adding tags: " +
			// relation.getTags().toString());

			relation.addTags(tags);

			// log.info("-  after adding tags: " +
			// relation.getTags().toString());

			// Apply changed relation, we don't know the execution order of
			// relations. It may happened, that child relation was already
			// process,
			relation.apply(generalGraph);
		}
	}

}
