package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.Collection;
import java.util.List;

import cz.agents.basestructures.GPSLocation;
import cz.agents.cyclestructures.CycleNode;
import cz.agents.geotools.EdgeUtil;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class CostVectorFactory {

	public static final int COST_VECTOR_LENGTH = 6;

	public static final int TRAVEL_TIME = 0;
	public static final int COMFORT = 1;
	public static final int QUIETNESS = 2;
	public static final int ELEVATION_GAIN = 3;
	public static final int PHYSICAL_EFFORT = 4;
	public static final int STRESS = 5;

	public static final double MAXIMUM_DOWNHILL_SPEED_MULTIPLIER = 2.5; // s_dmax
	public static final double CRITICAL_DOWNHILL_GRADE = 0.1; // d'c
	public static final double UPHILL_MULTIPLIER = 8; // a_l
	public static final double PERCEPTION_UPHILL_MULTIPLIER = 13; // a_p
	public static final double AVERAGE_CRUISING_SPEED_METERS_PER_SECOND = 4.2;

	/* rolling coefficient */
	private final static double ROLLING_COEFFICIENT = 0.005;
	/* gravitational acceleration */
	private final static double GRAVITATIONAL_ACCELERATION = 9.81;
	/* weight rider+bike+load (kg) */
	private final static double OVERALL_WEIGHT = 100;
	/* frontal area (m^2) */
	private final static double FRONTAL_AREA = .4;
	/* air density (kg/m^3) */
	private final static double AIR_DENSITY = 1.247;
	/* drag coefficient */
	private final static double DRAG_COEFFICIENT = 1d;

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public static short[] getCostVector(CycleNode from, CycleNode to, List<CycleNode> via,
			Collection<String> tags) {

		EdgeConstants edgeConstants = new EdgeConstants(tags);

		double totalTravelTime = 0d;
		double totalFlatnessTravelTime = 0d;
		double totalPhysicalEffort = 0d;

		GPSLocation actual = from;
		via.add(to);

		for (GPSLocation next : via) {

			double elevation = next.elevation - actual.elevation;
			double drops = (elevation > 0) ? 0 : -elevation;
			double rises = (elevation < 0) ? 0 : elevation;
			double lengthInMeters = EdgeUtil.computeEuclideanDistance(actual, next);

			double travelTime = getTravelTime(lengthInMeters, rises, drops, edgeConstants,
					AVERAGE_CRUISING_SPEED_METERS_PER_SECOND);
			double flatnessTravelTime = getElevationGain(rises);
			double physicalEffort = getPhysicalEffort(lengthInMeters, rises,
					AVERAGE_CRUISING_SPEED_METERS_PER_SECOND);

			totalTravelTime += travelTime;
			totalFlatnessTravelTime += flatnessTravelTime;
			totalPhysicalEffort += physicalEffort;

			actual = next;
		}

		// remove to node from list
		via.remove(via.size() - 1);

		short[] costVector = new short[COST_VECTOR_LENGTH];

		costVector[TRAVEL_TIME] = (short) Math.ceil(totalTravelTime);
		costVector[COMFORT] = (short) Math.ceil(getComfort(totalTravelTime, edgeConstants));
		costVector[QUIETNESS] = (short) Math.ceil(getQuietness(totalTravelTime, edgeConstants));
		costVector[ELEVATION_GAIN] = (short) Math.ceil(totalFlatnessTravelTime);
		costVector[PHYSICAL_EFFORT] = (short) Math.ceil(totalPhysicalEffort);
		costVector[STRESS] = 0;

		return costVector;
	}

	private static double getTravelTime(double lengthInMeters, double rises, double drops,
			EdgeConstants edgeConstants, double speedInMetersPerSecond) {
		double numerator = lengthInMeters + UPHILL_MULTIPLIER * rises;
		double denominator = speedInMetersPerSecond
				* getDownhillSpeedMultiplier(drops, lengthInMeters)
				* edgeConstants.minTravelTimeMultiplier;
		double travelTime = (numerator / denominator) + edgeConstants.maxTraveTimeConstant;

		return travelTime;
	}

	private static double getDownhillSpeedMultiplier(double drop, double length) {
		double downhillGrade = (length == 0d) ? 0 : drop / length;
		if (downhillGrade > CRITICAL_DOWNHILL_GRADE)
			return MAXIMUM_DOWNHILL_SPEED_MULTIPLIER;
		else
			return (((downhillGrade / CRITICAL_DOWNHILL_GRADE) * (MAXIMUM_DOWNHILL_SPEED_MULTIPLIER - 1)) + 1);
	}

	private static double getComfort(double travelTime, EdgeConstants edgeConstants) {
		return travelTime * edgeConstants.maxComfortMultiplier
				+ (edgeConstants.prolongationConstant / AVERAGE_CRUISING_SPEED_METERS_PER_SECOND);
	}

	private static double getQuietness(double travelTime, EdgeConstants edgeConstants) {
		return travelTime * edgeConstants.quietnessAvgBaseValue;
	}

	private static double getElevationGain(double rises) {
		return (rises * PERCEPTION_UPHILL_MULTIPLIER) / AVERAGE_CRUISING_SPEED_METERS_PER_SECOND;
	}

	/**
	 * Compute energy consumption for given edge in Joules
	 * 
	 * @param edge
	 * @param averageSpeedMetersPerSecond
	 * @return energy in Joules
	 */
	private static double getPhysicalEffort(double lengthInMeters, double rises,
			double averageSpeedMetersPerSecond) {

		double powerRollingResistance = ROLLING_COEFFICIENT * GRAVITATIONAL_ACCELERATION
				* OVERALL_WEIGHT;
		// Pdrag = 0.5 * Cd * D * A * (vg + vw)^2
		double powerDrag = 0.5 * DRAG_COEFFICIENT * AIR_DENSITY * FRONTAL_AREA
				* averageSpeedMetersPerSecond * averageSpeedMetersPerSecond;

		double slope = rises / lengthInMeters;
		double powerClimb = GRAVITATIONAL_ACCELERATION * slope * OVERALL_WEIGHT;

		return lengthInMeters * (powerRollingResistance + powerDrag + powerClimb);
	}
}
