package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import cz.agents.basestructures.GPSLocation;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;
import cz.agents.cyclestructures.RoadType;
import cz.agents.cyclestructures.Surface;
import cz.agents.geotools.EdgeUtil;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class CycleEdgeFactory {

	/**
	 * 
	 */
	private static final Pattern SURFACE_PAVED_SMOOTH = Pattern
			.compile("way::surface::(paved|asphalt|concrete:.*|metal|wood)");
	private static final Pattern SURFACE_PAVED_COBBLESTONE_LIKE = Pattern
			.compile("way::surface::(cobblestone:.*|paving_stone:.*|sett)");
	private static final Pattern SURFACE_UNPAVED = Pattern
			.compile("way::surface::(unpaved|compacted|dirt|earth|fine_gravel|grass|gravel|ground|mud|pebblestone|sand)");

	/**
	 * TODO javadoc
	 */
	private static final Pattern HIGHWAY = Pattern.compile("way::highway::.*");
	private static final Pattern PRIMARY = Pattern
			.compile("way::highway::primary|way::highway::primary_link");
	private static final Pattern SECONDARY = Pattern
			.compile("way::highway::secondary|way::highway::secondary_link");
	private static final Pattern TERTIARY = Pattern
			.compile("way::highway::tertiary|way::highway::tertiary_link");
	private static final Pattern CYCLEWAY = Pattern.compile(".*cycleway.*");
	private static final Pattern FOOTWAY = Pattern.compile(".*footway.*|.*pedestrian.*");
	private static final Pattern STEPS = Pattern
			.compile("way::highway::steps|node::highway::elevator|node::highway::steps");

	/**
	 * TODO javadoc
	 */
	private static final Pattern STREET_NAME = Pattern.compile("way::name::.*");

	/**
	 * TODO javadoc
	 */
	private static final Pattern BICYCLE_ROUTE_NAME = Pattern.compile("relation::ref::.*");

	private CycleEdgeFactory() {
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param from
	 * @param to
	 * @param via
	 * @param tags
	 * @return
	 */
	public static CycleEdge getCycleEdge(CycleNode from, CycleNode to, List<CycleNode> via,
			Collection<String> tags) {

		short[] costVector = CostVectorFactory.getCostVector(from, to, via, tags);
		int length = getCycleEdgeLength(from, to, via);

		return new CycleEdge(from.id, to.id, length, via, costVector, getSurface(tags),
				getRoadType(tags), getStreetName(tags), getBicycleRouteName(tags));
	}

	/**
	 * 
	 * Returns total direct length for <code>CycleEdge</code> consisting of from
	 * <code>CycleNode</code>, <code>List</code> of via <code>CycleNode</code>
	 * and to <code>CycleNode</code>
	 * 
	 * @param from
	 *            <code>CycleNode</code>
	 * @param to
	 *            <code>CycleNode</code>
	 * @param via
	 *            <code>List</code> of <code>CycleNode</code>
	 * @return direct length in meters
	 */
	private static int getCycleEdgeLength(CycleNode from, CycleNode to, List<CycleNode> via) {

		double totalLengthInMeters = 0d;

		GPSLocation actual = from;
		via.add(to);

		for (GPSLocation next : via) {

			double lengthInMeters = EdgeUtil.computeEuclideanDistance(actual, next);

			totalLengthInMeters += lengthInMeters;
			actual = next;
		}

		// remove to node from list
		via.remove(via.size() - 1);

		return (int) Math.ceil(totalLengthInMeters);
	}

	/**
	 * 
	 * Detects surface type from tags
	 * 
	 * @param tags
	 *            <code>Collection</code> of <code>String</code> tags
	 * @return surface type if surface tags is define in <code>Collection</code>
	 *         of tags, otherwise null
	 */
	private static Surface getSurface(Collection<String> tags) {
		for (String tag : tags) {
			if (SURFACE_PAVED_SMOOTH.matcher(tag).matches()) {
				return Surface.PAVED_SMOOTH;
			}

			if (SURFACE_PAVED_COBBLESTONE_LIKE.matcher(tag).matches()) {
				return Surface.PAVED_COBBLESTONE;
			}
			if (SURFACE_UNPAVED.matcher(tag).matches()) {
				return Surface.UNPAVED;
			}
		}
		return null;
	}

	/**
	 * 
	 * Detects road type from tags
	 * 
	 * @param tags
	 *            <code>Collection</code> of <code>String</code> tags
	 * @return road type if it is define in <code>Collection</code> of tags,
	 *         otherwise null
	 */
	private static RoadType getRoadType(Collection<String> tags) {
		for (String tag : tags) {

			if (FOOTWAY.matcher(tag).matches()) {

				return RoadType.FOOTWAY;
			} else if (CYCLEWAY.matcher(tag).matches()) {

				return RoadType.CYCLEWAY;
			} else if (HIGHWAY.matcher(tag).matches()) {

				if (STEPS.matcher(tag).matches()) {
					return RoadType.STEPS;
				} else if (PRIMARY.matcher(tag).matches()) {
					return RoadType.PRIMARY;
				} else if (SECONDARY.matcher(tag).matches()) {
					return RoadType.SECONDARY;
				} else if (TERTIARY.matcher(tag).matches()) {
					return RoadType.TERTIARY;
				}

				return RoadType.ROAD;
			}
		}
		return null;
	}

	/**
	 * 
	 * Detects street name from tags
	 * 
	 * @param tags
	 *            <code>Collection</code> of <code>String</code> tags
	 * @return street name if it is define in <code>Collection</code> of tags,
	 *         otherwise null
	 */
	private static String getStreetName(Collection<String> tags) {
		for (String tag : tags) {
			if (STREET_NAME.matcher(tag).matches()) {
				return tag.replaceFirst("way::name::", "");
			}
		}
		return null;
	}

	/**
	 * 
	 * Detects bicycle route name from tags
	 * 
	 * @param tags
	 *            <code>Collection</code> of <code>String</code> tags
	 * @return bicycle route name if it is define in <code>Collection</code> of
	 *         tags, otherwise null
	 */
	private static String getBicycleRouteName(Collection<String> tags) {
		for (String tag : tags) {
			if (BICYCLE_ROUTE_NAME.matcher(tag).matches()) {
				return tag.replaceFirst("relation::ref::", "");
			}
		}
		return null;
	}
}
