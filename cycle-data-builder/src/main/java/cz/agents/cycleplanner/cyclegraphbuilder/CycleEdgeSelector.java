package cz.agents.cycleplanner.cyclegraphbuilder;

import cz.agents.tns.selector.Conjunction;
import cz.agents.tns.selector.Selector;


/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class CycleEdgeSelector extends Selector {

	public CycleEdgeSelector() {
		super();
	}

	@Override
	public void init() {
		include.add(new Conjunction(false, "way::highway::trunk"));
		include.add(new Conjunction(false, "way::highway::primary"));
		include.add(new Conjunction(false, "way::highway::secondary"));
		include.add(new Conjunction(false, "way::highway::tertiary"));
		include.add(new Conjunction(false, "way::highway::unclassified"));
		include.add(new Conjunction(false, "way::highway::residential"));
		include.add(new Conjunction(false, "way::highway::service"));
		include.add(new Conjunction(false, "way::highway::*_link"));
		include.add(new Conjunction(false, "way::highway::living_street"));
		include.add(new Conjunction(false, "way::highway::road"));
		include.add(new Conjunction(false, "way::highway::track"));
		include.add(new Conjunction(false, "way::highway::path"));
		include.add(new Conjunction(false, "way::highway::bridleway"));

		include.add(new Conjunction(false, "way::highway::pedestrian").and(false,
				"way::bicycle::yes"));
		include.add(new Conjunction(false, "way::highway::pedestrian").and(false,
				"way::bicycle::designated"));
		include.add(new Conjunction(false, "way::highway::footway"));
		include.add(new Conjunction(false, "way::highway::steps"));
		include.add(new Conjunction(false, "way::highway::cycleway"));

		exclude.add(new Conjunction(false, "way::motorroad::yes").and(true, "way::highway!=*_link"));
		exclude.add(new Conjunction(false, "way::area::yes"));
		exclude.add(new Conjunction(false, "way::route::ferry"));
		exclude.add(new Conjunction(false, "way::bicycle::no"));
		exclude.add(new Conjunction(false, "way::access::customers"));
		exclude.add(new Conjunction(false, "way::access::private").and(true, "way::bicycle::yes")
				.and(true, "way::bicycle::permissive").and(true, "way::bicycle::dismount"));
		exclude.add(new Conjunction(false, "way::access::no").and(true, "way::bicycle::yes")
				.and(true, "way::bicycle::permissive").and(true, "way::bicycle::dismount"));

		// TODO ( highway=steps and tunnel=yes and layer<0 ) or
		exclude.add(new Conjunction(false, "way::highway::steps").and(false, "way::tunnel::yes")
				.and(false, "way::layer::-1"));
		exclude.add(new Conjunction(false, "way::highway::steps").and(false, "way::tunnel::yes")
				.and(false, "way::layer::-2"));

		// TODO ( highway=service and layer<0 ) or
		exclude.add(new Conjunction(false, "way::highway::service").and(false, "way::layer::-1"));
		exclude.add(new Conjunction(false, "way::highway::service").and(false, "way::layer::-2"));
	}
}
