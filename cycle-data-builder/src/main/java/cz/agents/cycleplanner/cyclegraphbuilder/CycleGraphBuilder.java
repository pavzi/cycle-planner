package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import cz.agents.basestructures.Graph;
import cz.agents.cycleplanner.cycledatabuilder.CycleDataBuilderCli;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;
import cz.agents.tns.structures.TnsEdge;
import cz.agents.tns.structures.TnsGraph;
import cz.agents.tns.structures.TnsNode;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class CycleGraphBuilder {

	private static final Logger log = Logger.getLogger(CycleGraphBuilder.class);

	private CycleDataBuilderCli cli;

	public CycleGraphBuilder(CycleDataBuilderCli cli) {
		this.cli = cli;
	}

	public Graph<CycleNode, CycleEdge> build() {

		log.info("Importing general graph...");

		TnsGraph tnsGraph = Tns2TnsGraph.run();

		TnsGraphUtils.deleteEdgesWithSameFromAndToNode(tnsGraph);

		TnsGraphUtils.moveHighwayTrafficSignalsTagFromNodeBeforeJunction(tnsGraph);

		// TODO presun tagy traffic_signals na spravne hrany podla tagu
		// trafic_signals_direction

		TnsGraphUtils.moveImportantNodeTagsToEdges(tnsGraph);

		// TnsGraphUtils.interpolate(generalGraph);

		// TnsGraphUtils.simplify(generalGraph);

		TnsGraphUtils.createEdgesInOppositeDirection(tnsGraph);

		// try {
		// generalGraph.serializeToGeoJson("prague_graph.geojson");
		// } catch (FileNotFoundException e) {
		// log.error(e.getMessage(), e);
		// }

		// Counts number of nodes with tag 'traffic_signals:direction'
		int countTrafficSignalDirectionTag = 0;

		for (TnsNode node : tnsGraph.getAllNodes()) {
			if (node.getTags().toString().matches(".*traffic_signals:direction.*")) {
				countTrafficSignalDirectionTag++;
			}
		}
		log.info("# of nodes containing tag traffic_signals:direction = "
				+ countTrafficSignalDirectionTag);

		// if (cli.isJunctionExtensionAllowed()) {
		// log.info("Before junction extension we have " +
		// generalGraph.getAllNodes().size() + " nodes and "
		// + generalGraph.getAllEdges().size() + " edges.");
		//
		// // extend junctions
		// JunctionExtensionUtil.extend(generalGraph);
		// log.info("After junction extension we have " +
		// generalGraph.getAllNodes().size() + " nodes and "
		// + generalGraph.getAllEdges().size() + " edges.");
		// }


		// Rebuild CycleGraph form TnsGraph
		GraphBuilder<CycleNode, CycleEdge> graphBuilder = new GraphBuilder<CycleNode, CycleEdge>();
		int nodeId = 0;

		for (TnsNode generalNode : tnsGraph.getAllNodes()) {
			CycleNode cycleNode = new CycleNode(nodeId++, generalNode.id, generalNode.latE6,
					generalNode.lonE6, generalNode.latProjected, generalNode.lonProjected,
					generalNode.elevation);

			graphBuilder.addNode(cycleNode);
		}

		for (TnsEdge generalEdge : tnsGraph.getAllEdges()) {
			CycleNode fromCycleNode = graphBuilder.getNodeByNodeId(graphBuilder
					.getIntIdForOsmId(generalEdge.fromId));
			CycleNode toCycleNode = graphBuilder.getNodeByNodeId(graphBuilder
					.getIntIdForOsmId(generalEdge.toId));
			List<CycleNode> via = new ArrayList<CycleNode>();

			for (TnsNode generalNode : generalEdge.getVia()) {
				CycleNode viaCycleNode = new CycleNode(nodeId++, generalNode.id, generalNode.latE6,
						generalNode.lonE6, generalNode.latProjected, generalNode.lonProjected,
						generalNode.elevation);

				via.add(viaCycleNode);
				log.info("Via node: " + viaCycleNode);
			}

			CycleEdge cycleEdge = CycleEdgeFactory.getCycleEdge(fromCycleNode, toCycleNode, via,
					generalEdge.getTags());

			graphBuilder.addEdge(cycleEdge);
		}

		Graph<CycleNode, CycleEdge> cycleGraph = graphBuilder.createGraph();

		// Find largest strongly connected component
		// log.info("Creating strongly connected components...");
		// long time = System.currentTimeMillis();
		//
		// ArrayList<StronglyConnectedComponent> stronglyConnectedComponents =
		// SCCFinder
		// .getAllStronglyConnectedComponentsSortedBySize(cycleGraph,
		// ModeOfTransport.BIKE);
		// log.info("Strongly connected component contains "
		// + stronglyConnectedComponents.get(0).getSize() + " nodes.");
		//
		// StronglyConnectedComponent largestStronglyConnectedComponent =
		// stronglyConnectedComponents
		// .get(0);
		// // graphBuilder = new GraphBuilder<>();
		// HashSet<CycleNode> nodesOfLargestStronglyConnectedComponent =
		// largestStronglyConnectedComponent
		// .getNodes();
		//
		// nodeId = 0;
		//
		// for (CycleNode cycleNode : nodesOfLargestStronglyConnectedComponent)
		// {
		// CycleNode newCycleNode = new CycleNode(nodeId++, cycleNode.osmId,
		// cycleNode.latE6,
		// cycleNode.lonE6, cycleNode.latProjected, cycleNode.lonProjected,
		// cycleNode.elevation);
		//
		// // log.debug(newCycleNode);
		//
		// graphBuilder.addNode(newCycleNode);
		// }
		//
		// // graphBuilder.addNodes(nodesOfLargestStronglyConnectedComponent);
		//
		// for (CycleNode node : nodesOfLargestStronglyConnectedComponent) {
		//
		// for (CycleEdge edge : cycleGraph.getNodeIncomingEdges(node.id)) {
		// CycleNode fromNode = cycleGraph.getNodeByNodeId(edge.fromId);
		// CycleNode toNode = cycleGraph.getNodeByNodeId(edge.toId);
		//
		// if (nodesOfLargestStronglyConnectedComponent.contains(fromNode)
		// && nodesOfLargestStronglyConnectedComponent.contains(toNode)) {
		//
		// int fromId = graphBuilder.getIntIdForOsmId(fromNode.osmId);
		// int toId = graphBuilder.getIntIdForOsmId(toNode.osmId);
		//
		// if (graphBuilder.containsEdge(fromId, toId))
		// continue;
		//
		// CycleEdge newCycleEdge = new CycleEdge(fromId, toId, edge.length,
		// edge.getViaNodes(), edge.getCostVector(), edge.getSurface(),
		// edge.getRoadType(), edge.getStreetName(),
		// edge.getBicycleRouteNumber());
		//
		// graphBuilder.addEdge(newCycleEdge);
		// }
		// }
		//
		// for (CycleEdge edge : cycleGraph.getNodeOutcomingEdges(node.id)) {
		// CycleNode fromNode = cycleGraph.getNodeByNodeId(edge.fromId);
		// CycleNode toNode = cycleGraph.getNodeByNodeId(edge.toId);
		//
		// if (nodesOfLargestStronglyConnectedComponent.contains(fromNode)
		// && nodesOfLargestStronglyConnectedComponent.contains(toNode)) {
		//
		// int fromId = graphBuilder.getIntIdForOsmId(fromNode.osmId);
		// int toId = graphBuilder.getIntIdForOsmId(toNode.osmId);
		//
		// if (graphBuilder.containsEdge(fromId, toId))
		// continue;
		//
		// CycleEdge newCycleEdge = new CycleEdge(fromId, toId, edge.length,
		// edge.getViaNodes(), edge.getCostVector(), edge.getSurface(),
		// edge.getRoadType(), edge.getStreetName(),
		// edge.getBicycleRouteNumber());
		//
		// graphBuilder.addEdge(newCycleEdge);
		// }
		// }
		// }
		//
		// cycleGraph = graphBuilder.createGraph();
		//
		// log.info("Getting the largest strongly connected component took "
		// + ((System.currentTimeMillis() - time) / 1000) + " s");
		log.info("Final cycle graph has " + cycleGraph.getAllNodes().size() + " nodes and "
				+ cycleGraph.getAllEdges().size() + " edges");

		return cycleGraph;
		// return null;
	}

}
