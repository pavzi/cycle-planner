package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.Collection;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class EdgeConstants {

	// TODO private
	double maxTraveTimeConstant = 0d;

	double minTravelTimeMultiplier = 1d;

	double maxComfortMultiplier = -1d;

	double maxQuietnessMultiplier = -1d;

	double prolongationConstant = 0d;

	double quietnessAvgBaseValue = 0d;

	public EdgeConstants(Collection<String> tags) {
		TagsParameters parameters = TagsParameters.INSTANCE;
		double countQuietnessTags = 0;

		if (tags != null) {
			for (String edgeTag : tags) {

				if (parameters.contains(edgeTag)) {

					double multiplier = parameters.getTravelTimeMultiplier(edgeTag);
					if (minTravelTimeMultiplier > multiplier)
						minTravelTimeMultiplier = multiplier;

					double constant = parameters.getTravelTimeSlowdownConstant(edgeTag);
					if (constant > maxTraveTimeConstant)
						maxTraveTimeConstant = constant;

					multiplier = parameters.getComfortMultiplier(edgeTag);
					if (multiplier > maxComfortMultiplier)
						maxComfortMultiplier = multiplier;

					multiplier = parameters.getQuietnessMultiplier(edgeTag);
					if (multiplier > maxQuietnessMultiplier)
						maxQuietnessMultiplier = multiplier;

					constant = parameters.getProlongationConstant(edgeTag);
					prolongationConstant = (constant > prolongationConstant) ? constant
							: prolongationConstant;

					if (parameters.getQuietnessMultiplier(edgeTag) != -1) {
						quietnessAvgBaseValue += parameters.getQuietnessMultiplier(edgeTag);
						countQuietnessTags++;
					}
				}
			}
		}

		maxComfortMultiplier = (maxComfortMultiplier == -1) ? 1 : maxComfortMultiplier;
		maxQuietnessMultiplier = (maxQuietnessMultiplier == -1) ? 1 : maxQuietnessMultiplier;

		quietnessAvgBaseValue = (countQuietnessTags == 0) ? 1 : (Math
				.round((quietnessAvgBaseValue / countQuietnessTags) * 10) / 10d);
	}
}
