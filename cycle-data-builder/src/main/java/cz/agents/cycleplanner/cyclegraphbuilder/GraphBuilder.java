package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import cz.agents.basestructures.Edge;
import cz.agents.basestructures.Graph;
import cz.agents.basestructures.Node;

/**
 * Builder for a graph
 *
 * @author Jan Hrncir (CVUT)
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 * @param <TNode>
 *            Type of roadNodes
 * @param <TEdge>
 *            Type of edges
 */
public class GraphBuilder<TNode extends Node, TEdge extends Edge> {

	private Map<Long, Integer> longIdToIntId = new HashMap<>();

	private Map<Integer, TNode> nodesByNodeId = new LinkedHashMap<>();
	private Map<EdgeId, TEdge> edgeByFromToNodeIds = new LinkedHashMap<>();
	private Map<Integer, List<TEdge>> nodeOutgoingEdges = new HashMap<>();
	private Map<Integer, List<TEdge>> nodeIncomingEdges = new HashMap<>();

	public GraphBuilder() {
	}

	public int getIntIdForOsmId(long osmId) {
		return longIdToIntId.get(osmId);
	}

	public Map<Long, Integer> getLongIdToIntId() {
		return longIdToIntId;
	}

	/**
	 * Add node to graph
	 *
	 * @param node
	 *            Node
	 *
	 * @return returns int id of added node
	 */
	public void addNode(TNode node) {

		if (nodesByNodeId.containsKey(node.id)) {
			throw new IllegalArgumentException(
					"Node with this int id already present! Can not import node");
		}

		nodesByNodeId.put(node.id, node);
		nodeOutgoingEdges.put(node.id, new ArrayList<TEdge>());
		nodeIncomingEdges.put(node.id, new ArrayList<TEdge>());

		longIdToIntId.put(node.osmId, node.id);

	}

	/**
	 * Add edge to graph
	 *
	 * @param edge
	 *            Edge
	 */
	public void addEdge(TEdge edge) { // we may think about "creating edges"
										// inside graph builder because of ids

		assert nodesByNodeId.get(edge.fromId) != null && nodesByNodeId.get(edge.toId) != null : "Node has to be in graph builder before inserting edge";

		EdgeId edgeId = new EdgeId(edge.fromId, edge.toId);

		assert !edgeByFromToNodeIds.containsKey(edgeId) : "Edge has not to exist yet";

		List<TEdge> outcomingEdgesFromNode = nodeOutgoingEdges.get(edge.fromId);
		List<TEdge> incomingEdgesToNode = nodeIncomingEdges.get(edge.toId);

		outcomingEdgesFromNode.add(edge);
		incomingEdgesToNode.add(edge);

		edgeByFromToNodeIds.put(edgeId, edge);
		nodeOutgoingEdges.put(edge.fromId, outcomingEdgesFromNode);
		nodeIncomingEdges.put(edge.toId, incomingEdgesToNode);
	}

	/**
	 * Create a final graph
	 *
	 * @return Final graph
	 */
	public Graph<TNode, TEdge> createGraph() {

		Graph<TNode, TEdge> graph = dumpCurrentGraph();

		this.nodesByNodeId = new HashMap<>();
		this.edgeByFromToNodeIds = new HashMap<>();
		this.nodeOutgoingEdges = new HashMap<>();
		this.nodeIncomingEdges = new HashMap<>();
		this.longIdToIntId = new HashMap<>();

		return graph;
	}

	/**
	 * does not deletes all the structures inside builder, so you can continue
	 * working on graph creation
	 *
	 * @return
	 */
	public Graph<TNode, TEdge> dumpCurrentGraph() {

		// check posloupnosti nodeu
		for (int i = 0; i < nodesByNodeId.keySet().size(); i++) {

			TNode node = nodesByNodeId.get(i);
			if (node == null) {
				throw new NoSuchElementException(
						" Node with id "
								+ i
								+ " not present! The sequence of node id must start with 0 and end with 'numOfNodes-1'");
			}

		}

		ArrayList<TNode> nodesByNodeIdList = new ArrayList<>(nodesByNodeId.keySet().size());
		for (int i = 0; i < nodesByNodeId.keySet().size(); i++) {
			nodesByNodeIdList.add(nodesByNodeId.get(i));
		}

		int[] outgoingPositions = new int[nodesByNodeId.keySet().size() + 1];
		ArrayList<TEdge> outgoingEdges = new ArrayList<>(nodesByNodeId.keySet().size());
		int[] incomingPositions = new int[nodesByNodeId.keySet().size() + 1];
		ArrayList<TEdge> incomingEdges = new ArrayList<>(nodesByNodeId.keySet().size());

		// OUTGOING EDGES
		// iterate over all roadNodes
		int j = 0; // outgoing edges id
		for (int k = 0; k < nodesByNodeIdList.size(); k++) {

			// log.debug("---------");
			// log.debug("NODE: " + allNodesByNodeId[k]);

			// assign outgoing position
			outgoingPositions[k] = j;

			// iterate over outgoing edges
			for (TEdge edge : nodeOutgoingEdges.get(k)) {

				outgoingEdges.add(edge);
				j++;
			}
		}
		// set numberOfNodes+1 of outgoing positions to current edge id as
		// indentation
		outgoingPositions[nodesByNodeId.keySet().size()] = j;

		// INCOMING EDGES
		// iterate over all roadNodes
		int l = 0; // outgoing edges id
		for (int k = 0; k < nodesByNodeIdList.size(); k++) {

			// assign incoming position
			incomingPositions[k] = l;

			// iterate over incoming edges
			for (TEdge edge : nodeIncomingEdges.get(k)) {

				incomingEdges.add(edge);
				l++;
			}
		}
		// set numberOfNodes+1 of incoming positions to current edge id as
		// indentation
		incomingPositions[nodesByNodeId.keySet().size()] = l;

		return new Graph<TNode, TEdge>(nodesByNodeIdList, outgoingPositions, outgoingEdges,
				incomingPositions, incomingEdges);

	}

	/**
	 * 
	 * Returns true when the graph contains edge with given edgeId
	 * 
	 * @param fromId
	 * @param toId
	 * @return True when the graph contains edge with given edgeId
	 */
	public boolean containsEdge(int fromId, int toId) {
		EdgeId edgeId = new EdgeId(fromId, toId);
		return edgeByFromToNodeIds.containsKey(edgeId);
	}

	public TEdge getEdge(int fromId, int toId) {
		EdgeId edgeId = new EdgeId(fromId, toId);
		return edgeByFromToNodeIds.get(edgeId);
	}

	/**
	 * Get all roadNodes that are so far in the graph builder
	 *
	 * @return All roadNodes
	 */
	public Collection<TNode> getAllNodes() {
		return nodesByNodeId.values();
	}

	/**
	 * Get node by given ID
	 *
	 * @param nodeId
	 *            Node ID
	 * @return Node
	 */
	public TNode getNodeByNodeId(int nodeId) {
		return nodesByNodeId.get(nodeId);
	}

	/**
	 * Provide current collection of edges
	 *
	 * @return Collection of edges
	 */
	public Collection<TEdge> getAllEdges() {
		return edgeByFromToNodeIds.values();
	}

	/**
	 * Add a collection of roadNodes to the graph
	 *
	 * @param nodes
	 *            Nodes
	 */
	public void addNodes(Collection<TNode> nodes) {
		for (TNode node : nodes) {
			addNode(node);
		}
	}

	public List<TEdge> getNodeOutcomingEdges(int nodeId) {
		return nodeOutgoingEdges.get(nodeId);
	}

	/**
	 * Add a collection of edges to the graph
	 *
	 * @param edges
	 *            Edges
	 */
	public void addEdges(Collection<TEdge> edges) {
		for (TEdge edge : edges) {
			if (!containsEdge(edge.fromId, edge.toId)) {
				addEdge(edge);
			}
		}
	}

	private class EdgeId {
		public final long fromNodeId;
		public final long toNodeId;

		public EdgeId(long fromNodeId, long toNodeId) {
			this.fromNodeId = fromNodeId;
			this.toNodeId = toNodeId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + (int) (fromNodeId ^ (fromNodeId >>> 32));
			result = prime * result + (int) (toNodeId ^ (toNodeId >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			@SuppressWarnings("unchecked")
			EdgeId other = (EdgeId) obj;
			if (fromNodeId != other.fromNodeId)
				return false;
			return toNodeId == other.toNodeId;
		}

		private GraphBuilder<TNode, TEdge> getOuterType() {
			return GraphBuilder.this;
		}
	}
}
