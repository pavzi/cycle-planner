package cz.agents.cycleplanner.cyclegraphbuilder;

public class NewNodeIdUtil {
	private static long newNodeId = -2;
	
	private NewNodeIdUtil(){
	}
	
	public static long getNewNodeId() {
		return newNodeId--;
	}
}	
