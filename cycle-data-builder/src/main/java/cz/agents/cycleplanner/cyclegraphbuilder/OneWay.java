package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Pattern;

import cz.agents.tns.structures.TnsEdge;


public class OneWay {

	private static final Pattern oneWay = Pattern.compile("way::oneway::yes|" + "way::oneway::true|"
			+ "way::oneway::1|" + "way::junction::roundabout|" + "way::highway::motorway|"
			+ "way::highway::motorway_link");

	// driving on this edges is allowed in opposite direction
	private static final Pattern oneWayInOppositeDirection = Pattern.compile("way::oneway::-1|"
			+ "way::oneway::reverse");

	private static final Pattern noOneWay = Pattern.compile("way::oneway::no|" + "way::oneway::false|"
			+ "way::oneway::0|" + "way::oneway::reversible");

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 * @return
	 */
	public boolean isOneWay(TnsEdge edge) {
		for (String tag : edge.getTags()) {

			if (checkOneWay(tag)) {

				for (String tag2 : edge.getTags()) {

					if (checkNoOneWay(tag2)) {
						return false;
					}
				}

				return true;
			}

		}

		return false;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 * @return
	 */
	public boolean isOneWayInOppositeDirection(TnsEdge edge) {

		for (String tag : edge.getTags()) {

			if (checkReverseOneWay(tag)) {

				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tag
	 * @return
	 */
	protected boolean checkOneWay(String tag) {
		return oneWay.matcher(tag).matches();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tag
	 * @return
	 */
	protected boolean checkReverseOneWay(String tag) {
		return oneWayInOppositeDirection.matcher(tag).matches();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tag
	 * @return
	 */
	protected boolean checkNoOneWay(String tag) {
		return noOneWay.matcher(tag).matches();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tags
	 * @return
	 */
	public Collection<String> removeOneWayTags(Collection<String> tags) {
		Pattern oneWayKey = Pattern.compile(".*oneway.*");

		for (Iterator<String> it = tags.iterator(); it.hasNext();) {
			String tag = it.next();

			if (oneWayKey.matcher(tag).matches()) {
				it.remove();
			}
		}

		return tags;
	}

}
