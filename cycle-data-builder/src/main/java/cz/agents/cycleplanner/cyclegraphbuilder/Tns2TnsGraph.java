package cz.agents.cycleplanner.cyclegraphbuilder;

import org.apache.log4j.Logger;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.EPSGProjection;
import cz.agents.tns.TnsEdgeResultSet;
import cz.agents.tns.TnsNodeResultSet;
import cz.agents.tns.TnsReader;
import cz.agents.tns.TnsRelationResultSet;
import cz.agents.tns.psql.PostgresqlTnsReader;
import cz.agents.tns.structures.TnsEdge;
import cz.agents.tns.structures.TnsEdgeFactory;
import cz.agents.tns.structures.TnsGraph;
import cz.agents.tns.structures.TnsNode;
import cz.agents.tns.structures.TnsRelation;



public class Tns2TnsGraph {

	private static final Logger log = Logger.getLogger(Tns2TnsGraph.class);

	private static EPSGProjection projection;

	static {
		try {
			projection = new EPSGProjection(2065);
		} catch (FactoryException | TransformException e) {
			log.error(e.getMessage(), e.fillInStackTrace());
		}
	}

	private Tns2TnsGraph() {
	}

	public static TnsGraph run() {

		TnsGraph generalGraph = new TnsGraph();

		try {
			TnsReader reader = new PostgresqlTnsReader();
			TnsNodeResultSet tnsNodeResultSet = reader.getAllNodes();

			while (tnsNodeResultSet.next()) {

				int latE6 = tnsNodeResultSet.getLatE6();
				int lonE6 = tnsNodeResultSet.getLonE6();
				GPSLocation projLocation = projection.getProjectedGPSLocation(latE6, lonE6, 0);

				generalGraph.addNode(new TnsNode(tnsNodeResultSet.getNodeOsmId(), latE6, lonE6,
						projLocation.latProjected, projLocation.lonProjected, tnsNodeResultSet
								.getElevation(), tnsNodeResultSet.getTags()));
			}

			log.info("Number of loaded nodes: " + generalGraph.getAllNodes().size());

			TnsEdgeResultSet tnsEdgeResultSet = reader.getAllEdges();
			CycleEdgeSelector filter = new CycleEdgeSelector();

			while (tnsEdgeResultSet.next()) {
				TnsEdge generalEdge = TnsEdgeFactory.getTnsEdge(
						tnsEdgeResultSet.getFromNodeId(), tnsEdgeResultSet.getToNodeId(),
						tnsEdgeResultSet.getWayId(), tnsEdgeResultSet.getTags());

				if (filter.isAllowed(generalEdge.getTags())) {
					generalGraph.addEdge(generalEdge);
				}

			}

			log.info("Number of general edges: " + generalGraph.getAllEdges().size());

			TnsRelationResultSet tnsRelationResultSet = reader.getRelations("route");
			BicycleRouteRelationSelector bicycleRouteRelationFilter = new BicycleRouteRelationSelector();

			while (tnsRelationResultSet.next()) {
				TnsRelation generalRelation = new BicycleRouteTnsRelation(
						tnsRelationResultSet.getRelationId(), tnsRelationResultSet.getType(),
						tnsRelationResultSet.getMembers(), tnsRelationResultSet.getTags());

				if (bicycleRouteRelationFilter.isAllowed(tnsRelationResultSet.getTags())) {
					generalGraph.addRelation(generalRelation);
				}

				// TODO add other relations
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			System.exit(-1);
		}

		return generalGraph;
	}
}
