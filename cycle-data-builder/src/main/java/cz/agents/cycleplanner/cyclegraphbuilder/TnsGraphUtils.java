package cz.agents.cycleplanner.cyclegraphbuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import cz.agents.basestructures.GPSLocation;
import cz.agents.geotools.EPSGProjection;
import cz.agents.geotools.EdgeUtil;
import cz.agents.tns.structures.TnsEdge;
import cz.agents.tns.structures.TnsEdgeFactory;
import cz.agents.tns.structures.TnsGraph;
import cz.agents.tns.structures.TnsNode;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class TnsGraphUtils {

	private static final Logger log = Logger.getLogger(TnsGraphUtils.class);

	private static EPSGProjection projection;

	static {
		try {
			projection = new EPSGProjection(2065);
		} catch (FactoryException | TransformException e) {
			log.error(e.getMessage(), e.fillInStackTrace());
		}
	}

	private TnsGraphUtils() {
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * Detect malformed edges with nodes with same id or same coordinates
	 * 
	 * @param generalGraphBuilder
	 */
	public static void deleteEdgesWithSameFromAndToNode(TnsGraph generalGraphBuilder) {
		TnsEdge malformedEdge;
		int countMalformedEdges = 0;
		do {
			malformedEdge = null;

			for (TnsEdge generalEdge : generalGraphBuilder.getAllEdges()) {
				if (generalEdge.fromId == generalEdge.toId) {
					// log.info("Found edge with same id for from and to node.");
					countMalformedEdges++;

					malformedEdge = generalEdge;
					break;
				}

				if (generalGraphBuilder.getNode(generalEdge.fromId).latE6 == generalGraphBuilder
						.getNode(generalEdge.toId).latE6
						&& generalGraphBuilder.getNode(generalEdge.fromId).lonE6 == generalGraphBuilder
								.getNode(generalEdge.toId).lonE6) {

					// log.info("Found edge with same coordinates for from and to node.");
					countMalformedEdges++;

					malformedEdge = generalEdge;
					break;
				}
			}

			if (malformedEdge != null) {
				generalGraphBuilder.shrinkEdgeToNode(malformedEdge);
			}

		} while (malformedEdge != null);

		log.info("Number of malformed edges: " + countMalformedEdges);
	}

	/**
	 * Move highway traffic signals tag from node before junction to the node
	 * closer to junction and with tags crossing traffic signals
	 * 
	 * TODO javadoc
	 * 
	 * In a lot of cases there exists node in OSM representing traffic signals,
	 * it has one in edge and one out edge. We want to eliminate such a nodes
	 * and create more compact junction
	 * 
	 * @param generalGraphBuilder
	 */
	public static void moveHighwayTrafficSignalsTagFromNodeBeforeJunction(
			TnsGraph generalGraphBuilder) {
		int count = 0;

		for (TnsNode node : generalGraphBuilder.getAllNodes()) {
			if (node.getTags().toString().matches(".*node::highway::traffic_signals.*")) {

				Collection<TnsEdge> incomingEdges = generalGraphBuilder
						.getAllNodeIncomingEdges(node.id);
				Collection<TnsEdge> outgoingEdges = generalGraphBuilder
						.getAllNodeOutgoingEdges(node.id);

				if (incomingEdges.size() == 1 && outgoingEdges.size() == 1) {

					boolean tagOnIncoming = false;
					boolean tagOnOutgoing = false;
					TnsEdge incomingEdge = incomingEdges.iterator().next();
					TnsEdge outgoingEdge = outgoingEdges.iterator().next();
					TnsNode incomingEdgeFromNode = generalGraphBuilder.getNode(incomingEdge.fromId);
					TnsNode outgoingEdgeToNode = generalGraphBuilder.getNode(outgoingEdge.toId);

					if (incomingEdgeFromNode.getTags().toString()
							.matches(".*node::crossing::traffic_signals.*")) {
						count++;
						tagOnIncoming = true;
					}

					if (outgoingEdgeToNode.getTags().toString()
							.matches(".*node::crossing::traffic_signals.*")) {
						count++;
						tagOnOutgoing = true;
					}

					if (tagOnIncoming && tagOnOutgoing) {
						double incomingEdgeDistance = EdgeUtil.computeEuclideanDistance(
								incomingEdgeFromNode, node);
						double outgoingEdgeDistance = EdgeUtil.computeEuclideanDistance(node,
								outgoingEdgeToNode);

						if (incomingEdgeDistance <= outgoingEdgeDistance) {
							moveHighwayTrafficSignalTag(node, incomingEdgeFromNode);
						} else {
							moveHighwayTrafficSignalTag(node, outgoingEdgeToNode);
						}

					} else if (tagOnIncoming) {
						moveHighwayTrafficSignalTag(node, incomingEdgeFromNode);
					} else if (tagOnOutgoing) {
						moveHighwayTrafficSignalTag(node, outgoingEdgeToNode);
					}
				}
			}
		}

		log.info("Number of nodes with tag highway::traffic_signals with in/out degree 1 is "
				+ count);
	}

	private static void moveHighwayTrafficSignalTag(TnsNode fromNode, TnsNode toNode) {
		toNode.addTag("node::highway::traffic_signals");

		if (fromNode.getTags().toString().matches(".*node::traffic_signals:direction::backward.*")) {
			toNode.addTag("node::traffic_signals:direction::backward");
			fromNode.removeTag("node::traffic_signals:direction::backward");
		} else if (fromNode.getTags().toString()
				.matches(".*node::traffic_signals:direction::forward.*")) {
			toNode.addTag("node::traffic_signals:direction::forward");
			fromNode.removeTag("node::traffic_signals:direction::forward");
		}

		fromNode.removeTag("node::highway::traffic_signals");
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalGraphBuilder
	 */
	public static void moveImportantNodeTagsToEdges(TnsGraph generalGraphBuilder) {
		Pattern crossing = Pattern
				.compile("node::crossing::(island|uncontrolled|unmarked|yes|zebra)|"
						+ "node::highway::crossing|node::railway::crossing");
		Pattern pedestrianTrafficSignals = Pattern.compile("node::crossing::traffic_signals");
		Pattern roadTrafficSignalsAndStopSign = Pattern
				.compile("node::highway::traffic_signals|node::highway::stop");
		Pattern roads = Pattern.compile(".*highway::(primary|secondary|tertiary|"
				+ "unclassified|residential|service|" + "motorway_link|trunk_link|primary_link|"
				+ "secondary_link|tertiary_link|living_street|" + "track|road).*");
		Pattern obstacles = Pattern
				.compile("node::highway::elevator|node::highway::steps|"
						+ "node::barrier::(yes|block|chain|rope|cycle_barrier|motorcycle_barrier|gate|lift_gate|swing_gate)|"
						+ "node::traffic_calming::(yes|bump)");
		Pattern trafficCalmings = Pattern.compile("node::traffic_calming::(yes|bump)");

		for (TnsNode generalNode : generalGraphBuilder.getAllNodes()) {
			Collection<TnsEdge> edgesNotDesignedForVehicles = new ArrayList<TnsEdge>();
			Collection<TnsEdge> edgesDesignedForVehicles = new ArrayList<TnsEdge>();
			Collection<TnsEdge> edges = new ArrayList<TnsEdge>(
					generalGraphBuilder.getAllNodeOutgoingEdges(generalNode.id));

			edges.addAll(generalGraphBuilder.getAllNodeIncomingEdges(generalNode.id));

			for (TnsEdge edge : edges) {
				Collection<String> tagsCollection = edge.getTags();
				String tags = tagsCollection.toString();

				if (roads.matcher(tags).matches()) {
					edgesDesignedForVehicles.add(edge);
				} else {
					edgesNotDesignedForVehicles.add(edge);
				}
			}

			Collection<String> tagsToMoveToAllEdges = new ArrayList<String>();
			Collection<String> tagsToMoveToEdgesDesignedForVehicles = new ArrayList<String>();
			Collection<String> tagsToMoveToEdgesNotDesignedForVehicles = new ArrayList<String>();

			for (String tag : generalNode.getTags()) {
				if (crossing.matcher(tag).matches()) {
					tagsToMoveToEdgesNotDesignedForVehicles.add(tag);
				}

				if (pedestrianTrafficSignals.matcher(tag).matches()) {
					tagsToMoveToEdgesNotDesignedForVehicles.add(tag);
				}

				if (roadTrafficSignalsAndStopSign.matcher(tag).matches()) {
					tagsToMoveToEdgesDesignedForVehicles.add(tag);
				}

				if (obstacles.matcher(tag).matches()) {
					tagsToMoveToAllEdges.add(tag);
				}

				if (trafficCalmings.matcher(tag).matches()) {
					tagsToMoveToEdgesDesignedForVehicles.add(tag);
				}
			}

			moveTagsFromNodeToEdges(tagsToMoveToAllEdges, generalNode, edges);
			moveTagsFromNodeToEdges(tagsToMoveToEdgesNotDesignedForVehicles, generalNode,
					edgesNotDesignedForVehicles);
			moveTagsFromNodeToEdges(tagsToMoveToEdgesDesignedForVehicles, generalNode,
					edgesDesignedForVehicles);

		}
	}

	public static void moveTagsFromNodeToEdges(Collection<String> tags, TnsNode from,
			Collection<TnsEdge> to) {
		for (String tag : tags) {
			moveTagFromNodeToEdges(tag, from, to);
		}
	}

	public static void moveTagFromNodeToEdges(String tag, TnsNode from, Collection<TnsEdge> to) {
		from.removeTag(tag);

		for (TnsEdge generalEdge : to) {
			generalEdge.addTag(tag);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalGraphBuilder
	 */
	public static void interpolate(TnsGraph generalGraphBuilder) {

		final int minDistanceIncrement = 25;
		Collection<TnsEdge> edges = new HashSet<TnsEdge>(generalGraphBuilder.getAllEdges());

		for (TnsEdge edge : edges) {
			TnsNode fromNode = generalGraphBuilder.getNode(edge.fromId);
			TnsNode toNode = generalGraphBuilder.getNode(edge.toId);
			double distance = EdgeUtil.computeEuclideanDistance(fromNode, toNode);

			int numberOfInterpolatedNodes = (int) Math.floor(distance / minDistanceIncrement) - 1;

			if (numberOfInterpolatedNodes > 0) {
				TnsNode[] interpolatedNodes = new TnsNode[numberOfInterpolatedNodes + 1];

				// x=longitude=[0], y=latitude=[1], z=elevation[2]
				double[] fromPoint = { fromNode.lonProjected, fromNode.latProjected,
						fromNode.elevation };
				double[] fromToVector = { toNode.lonProjected - fromPoint[0],
						toNode.latProjected - fromPoint[1], toNode.elevation - fromPoint[2] };
				double fromToVectorSize = Math.sqrt(fromToVector[0] * fromToVector[0]
						+ fromToVector[1] * fromToVector[1] + fromToVector[2] * fromToVector[2]);
				double[] normalizedFromToVector = { fromToVector[0] / fromToVectorSize,
						fromToVector[1] / fromToVectorSize, fromToVector[2] / fromToVectorSize };
				double distanceIncrement = distance / (numberOfInterpolatedNodes + 1);
				// log.info("Distance: " + distance+", distance increment: " +
				// distanceIncrement +", # of interpolated nodes: " +
				// numberOfInterpolatedNodes);

				for (int i = 0; i < numberOfInterpolatedNodes; i++) {
					double distanceFromFromPoint = distanceIncrement * (i + 1);

					TnsNode interpolatedNode = getInterpolatedTnsNode(fromPoint[1]
							+ normalizedFromToVector[1] * distanceFromFromPoint, fromPoint[0]
							+ normalizedFromToVector[0] * distanceFromFromPoint, fromPoint[2]
							+ normalizedFromToVector[2] * distanceFromFromPoint);

					interpolatedNodes[i] = interpolatedNode;
					generalGraphBuilder.addNode(interpolatedNode);
				}

				interpolatedNodes[numberOfInterpolatedNodes] = toNode;

				for (TnsNode node : interpolatedNodes) {
					List<TnsNode> via = Collections.emptyList();
					Set<String> tags = new HashSet<String>(edge.getTags());
					Collection<Long> wayIds = new HashSet<Long>(edge.getWayIds());
					TnsEdge newEdge = new TnsEdge(fromNode.id, via, node.id, wayIds, tags);

					generalGraphBuilder.addEdge(newEdge);

					fromNode = node;
				}

				generalGraphBuilder.removeEdge(edge);
			}

		}
	}

	private static TnsNode getInterpolatedTnsNode(double projectedLatitude,
			double projectedLongitude, double elevation) {
		GPSLocation location = projection.getSphericalGPSLocation(
				(int) Math.round(projectedLatitude), (int) Math.round(projectedLongitude),
				(int) Math.round(elevation));

		TnsNode interpolatedNode = new TnsNode(NewNodeIdUtil.getNewNodeId(), location.latE6,
				location.lonE6, location.latProjected, location.lonProjected, location.elevation,
				new HashSet<String>());
		return interpolatedNode;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * If node in and out degree is equal to 1, and in and out edge have same
	 * tags, then this node is discarded and new edge is created containing
	 * discarded node in order to preserve the correct geographical shape of
	 * newly created edge
	 * 
	 * This method should be used before introducing edges in opposite
	 * direction.
	 * 
	 * Simplify while there are only edges in one direction
	 * 
	 * Disable nodes that are not intersections
	 * 
	 * @param generalGraphBuilder
	 */
	// TODO rename
	public static void simplify(TnsGraph generalGraphBuilder) {
		// Simplify while there are only edges in one direction
		int count = 0;
		int count2 = 0;
		Set<TnsNode> mergedNodes = new HashSet<TnsNode>();

		for (TnsNode node : generalGraphBuilder.getAllNodes()) {
			Collection<TnsEdge> inEdges = generalGraphBuilder.getAllNodeIncomingEdges(node.id);
			Collection<TnsEdge> outEdges = generalGraphBuilder.getAllNodeOutgoingEdges(node.id);

			if (inEdges.size() == 1 && outEdges.size() == 1) {
				TnsEdge inEdge = inEdges.iterator().next();
				TnsEdge outEdge = outEdges.iterator().next();

				// We do not want to create self loop
				if (outEdge.toId == inEdge.fromId) {
					continue;
				}

				// if
				// (inEdge.getTags().toString().equals(outEdge.getTags().toString()))
				// {
				if (areSetOfTagsEqual(inEdge.getTags(), outEdge.getTags())
						&& !generalGraphBuilder.containsEdge(inEdge.fromId, outEdge.toId)) {
					count2++;

					List<TnsNode> via = new ArrayList<>(inEdge.getVia());
					via.add(node);
					via.addAll(outEdge.getVia());

					Set<String> tags = new HashSet<String>(inEdge.getTags());

					Collection<Long> wayIds = new HashSet<Long>(inEdge.getWayIds());
					wayIds.addAll(outEdge.getWayIds());

					TnsEdge newEdge = new TnsEdge(inEdge.fromId, via, outEdge.toId, wayIds, tags);

					// log.debug(count2 + " - merged two edges! new Edge " +
					// newEdge.toString() + ", original ones: "
					// + inEdge.toString() + ", " + outEdge.toString());

					mergedNodes.add(node);
					generalGraphBuilder.removeEdge(inEdge);
					generalGraphBuilder.removeEdge(outEdge);
					generalGraphBuilder.addEdge(newEdge);

				}
				count++;
			}
		}

		log.info("# nodes with inDegree and outDegree equals to 1: " + count);
		log.info("# nodes with inDegree and outDegree equals to 1 and its tags are the same: "
				+ count2);
	}

	private static boolean areSetOfTagsEqual(Collection<String> tags1, Collection<String> tags2) {
		if (tags1.size() != tags2.size()) {
			return false;
		}
		for (String tag : tags1) {
			if (!tags2.contains(tag)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalGraphBuilder
	 */
	public static void createEdgesInOppositeDirection(TnsGraph generalGraphBuilder) {
		// Process one-way streets
		// TODO use information from route=bicycle role=backward/forward?
		// TODO spracuj takisto ways v TnsGraph
		OneWay oneWayChecker = new BicycleOneWay();
		Collection<TnsEdge> newTnsEdges = new ArrayList<TnsEdge>();
		Collection<TnsEdge> generalEdgesToRemove = new ArrayList<TnsEdge>();
		Pattern onlyOneWayEdges = Pattern
				.compile(".*highway::(motorway_link|trunk_link|primary|primary_link|secondary|secondary_link).*");

		for (TnsEdge generalEdge : generalGraphBuilder.getAllEdges()) {

			if (!oneWayChecker.isOneWay(generalEdge)) {
				TnsEdge oppositeTnsEdge = TnsEdgeFactory.getOppositeTnsEdge(generalEdge);

				newTnsEdges.add(oppositeTnsEdge);

				if (oneWayChecker.isOneWayInOppositeDirection(generalEdge)) {
					generalEdgesToRemove.add(generalEdge);

					// String edgeJointTags = generalEdge.getTags().toString();
					//
					// if (!onlyOneWayEdges.matcher(edgeJointTags).matches()) {
					// oppositeTnsEdge.addTag("way::oneway::opposite");
					// newTnsEdges.add(oppositeTnsEdge);
					// }
				}
			} else {
				// Is oneway, creates opposite edge if it does not contain tag
				// highway::motorway_link|trunk_link|primary|primary_link|secondary|secondary_link
				// String edgeJointTags = generalEdge.getTags().toString();
				//
				// if (!onlyOneWayEdges.matcher(edgeJointTags).matches()) {
				// TnsEdge oppositeTnsEdge = TnsEdgeFactory
				// .getOppositeTnsEdge(generalEdge);
				//
				// oppositeTnsEdge.addTag("way::oneway::opposite");
				// newTnsEdges.add(oppositeTnsEdge);
				// }
			}
		}

		log.info("How many edges before taking care of oneway? "
				+ generalGraphBuilder.getAllEdges().size());

		log.info("How many edges are we deleting? " + generalEdgesToRemove.size());
		generalGraphBuilder.removeEdges(generalEdgesToRemove);

		log.info("How many edges are we adding? " + newTnsEdges.size());
		generalGraphBuilder.addEdges(newTnsEdges);

		log.info("So far we have how many edges? " + generalGraphBuilder.getAllEdges().size());
		// Cisla nesedia -- v grafu uz existovali niektore hrany, ktore sme
		// vytvorily pocas spracovavanie smerov hran

		// Remove one-way tags
		// for (TnsEdge generalEdge : generalGraphBuilder.getAllEdges()) {
		//
		// Collection<String> tags = generalEdge.getTags();
		// log.debug("tags pred vymazanim: " + tags);
		// oneWayChecker.removeOneWayTags(tags);
		// log.debug("tags po vymazani tagov: " + tags);
		// }
	}
}
