package cz.agents.cycleplanner.scc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

import org.apache.log4j.Logger;

import cz.agents.basestructures.Graph;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

/**
 * Created by najkl on 17/12/14.
 */
// TODO refactor use GENERALEDGE and GENERALNODE
public class SCCFinder {

	private static Logger log = Logger.getLogger(SCCFinder.class);

	// for SCC
	private static ArrayList<CycleNode> points;
	private static HashSet<CycleNode> alreadyInOrdering;
	private static Stack<CycleNode> ordering;
	private static HashSet<CycleNode> open;

	public static ArrayList<StronglyConnectedComponent> getAllStronglyConnectedComponentsSortedBySize(
			Graph<CycleNode, CycleEdge> graph) {

		// log.info("current mode: " + mode.toString());

		open = new HashSet<>();
		points = new ArrayList<>(graph.getAllNodes());
		alreadyInOrdering = new HashSet<>();
		ordering = new Stack<>();

		int index = 0;
		while (alreadyInOrdering.size() < points.size()) {

			while (index < points.size()
					&& (alreadyInOrdering.contains(points.get(index)) || !supportsOutgoingMode(
							graph, points.get(index))))
				index++; // &&!PlanningGraphUtil.checkOutgoingFeasibility(graph,points.get(index).getId(),JourneyPlanTemplate.getFirstModeOfTransport(mode))
			if (index == points.size())
				break;
			DFSextwalk(points.get(index), graph);
		}

		HashSet<CycleNode> alreadyInSomeRegion = new HashSet<CycleNode>();
		Stack<CycleNode> stack2 = new Stack<CycleNode>();
		open = new HashSet<CycleNode>();

		ArrayList<StronglyConnectedComponent> components = new ArrayList<>();

		StronglyConnectedComponent component;

		while (!ordering.isEmpty()) {

			boolean empty = false;
			CycleNode currentStarting = null;
			do {
				if (ordering.isEmpty()) {
					empty = true;
					break;
				} else {
					currentStarting = ordering.pop();
				}

				/**/
			} while (alreadyInSomeRegion.contains(currentStarting));
			if (empty) {
				break;
			}
			component = new StronglyConnectedComponent();
			components.add(component);

			stack2.add(currentStarting);

			while (!stack2.isEmpty()) {
				CycleNode current = stack2.pop();
				component.addNode(current);
				alreadyInSomeRegion.add(current);

				ArrayList<CycleNode> succ = new ArrayList<>();

				for (CycleEdge edge : graph.getNodeIncomingEdges(current.id)) {
					// if(edge.checkFeasibility(mode)) {
					succ.add(graph.getNodeByNodeId(edge.fromId));
					// }

				}

				for (int i = 0; i < succ.size(); i++) {
					if (!alreadyInSomeRegion.contains(succ.get(i)) && !open.contains(succ.get(i))) {
						stack2.push(succ.get(i));
						open.add(succ.get(i));
					}
				}
			}
		}

		// System.out.println("num of components: "+components.size());
		int singleComp = 0;
		int smallComp = 0;
		int biggestSize = Integer.MIN_VALUE;
		StronglyConnectedComponent bestComponent = null;

		for (StronglyConnectedComponent stronglyConnectedComponent : components) {
			int size = stronglyConnectedComponent.getSize();
			if (size > biggestSize) {
				bestComponent = stronglyConnectedComponent;
				biggestSize = size;
			}
			/*
			 * if(size==1) { singleComp++; } else if(size<20) { smallComp++; }
			 * else { //
			 * System.out.println(stronglyConnectedComponent.getSize()); }
			 */
		}
		if (!components.isEmpty()) {
			// log.info("the biggest component contains " +
			// bestComponent.getNodes().size() + " nodes");
		}

		// System.out.println("num of single components "+singleComp+", num of small components "+smallComp);

		Collections.sort(components, new Comparator<StronglyConnectedComponent>() {
			@Override
			public int compare(StronglyConnectedComponent o1, StronglyConnectedComponent o2) {
				return o2.getSize() - o1.getSize();
			}
		});

		return components;

	}

	public static boolean supportsOutgoingMode(Graph<CycleNode, CycleEdge> graph, CycleNode node) {
		for (CycleEdge edge : graph.getNodeOutcomingEdges(node.id)) {
			// if(edge.checkFeasibility(mode)) {
			return true;
			// }
		}

		return false;
	}

	public static boolean minedOutgoingMode(Graph<CycleNode, CycleEdge> graph, CycleNode node) {
		for (CycleEdge edge : graph.getNodeOutcomingEdges(node.id)) {
			// if(edge.checkFeasibility(mode)) {
			return true;
			// }
		}

		return false;
	}

	private static void DFSextwalk(CycleNode root, Graph<CycleNode, CycleEdge> graph) {
		// System.out.println("current point: "+current.id);
		open.add(root);
		// System.out.println("starting walk with: "+root.getId());

		Stack<CycleNode> stack = new Stack<>();
		HashMap<CycleNode, ArrayList<CycleNode>> successors = new HashMap<>();

		stack.add(root);

		while (!stack.isEmpty()) {

			CycleNode current = stack.peek();
			open.add(current);

			if (!successors.containsKey(current)) {
				ArrayList<CycleNode> succ = new ArrayList<>();

				for (CycleEdge edge : graph.getNodeOutcomingEdges(current.id)) {
					// if(edge.checkFeasibility(mode)) {

					succ.add(graph.getNodeByNodeId(edge.toId));
					// }
				}

				successors.put(current, succ);

			}

			if (!successors.get(current).isEmpty()) {
				CycleNode next = successors.get(current).remove(0);
				if (!open.contains(next)) {
					stack.push(next);
				}

			} else {
				stack.pop();
				ordering.push(current);
				alreadyInOrdering.add(current);

			}
		}
	}

}
