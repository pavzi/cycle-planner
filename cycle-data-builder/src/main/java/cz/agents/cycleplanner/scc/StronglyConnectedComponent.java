package cz.agents.cycleplanner.scc;

import java.util.HashSet;

import cz.agents.cyclestructures.CycleNode;


/**
 * Created by najkl on 17/12/14.
 */
public class StronglyConnectedComponent {

    private HashSet<CycleNode> nodesInSCC;

    public StronglyConnectedComponent() {
        this.nodesInSCC = new HashSet<>();
    }

    public boolean addNode(CycleNode node) {
        return nodesInSCC.add(node);
    }

    public int getSize() {
        return nodesInSCC.size();
    }

    public HashSet<CycleNode> getNodes() {
        return nodesInSCC;
    }

}
