package cz.agents.cycleplanner.cycledatabuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.log4j.Logger;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.Zone;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

public class DeserializationTest {

	private static final Logger log = Logger.getLogger(DeserializationTest.class);

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		log.info("Running deserialization of test cycle city data...");
		try {
			File f = new File("test");
			FileInputStream fileInputStream = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fileInputStream);

			Zone zone = (Zone) ois.readObject();
			Graph<CycleNode, CycleEdge> cycleGraph = (Graph<CycleNode, CycleEdge>) zone.graph;

			log.info("# of nodes: " + cycleGraph.getAllNodes().size());
			log.info("# of edges: " + cycleGraph.getAllEdges().size());
			log.info("City name: " + zone.name);
			log.info("Bounding box: " + zone.boundingBox.toString());

			ois.close();

		} catch (FileNotFoundException e) {
			log.error("Could not find a file! " + e.getMessage(), e);
		} catch (IOException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
		}

	}

}
