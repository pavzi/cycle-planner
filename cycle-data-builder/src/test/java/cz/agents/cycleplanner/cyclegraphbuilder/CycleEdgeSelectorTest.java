package cz.agents.cycleplanner.cyclegraphbuilder;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

public class CycleEdgeSelectorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPedestrian() {
		CycleEdgeSelector filter = new CycleEdgeSelector();
		Collection<String> tags = new HashSet<String>();

		tags.add("highway::pedestrian");
		tags.add("bicycle::yes");
		tags.add("jozko::mrkvicka");

		assertTrue(filter.isAllowed(tags));
	}

	@Test
	public void testPrivateAccess() {
		CycleEdgeSelector filter = new CycleEdgeSelector();
		Collection<String> tags = new HashSet<String>();

		tags.add("highway::primary");
		tags.add("access::private");
		assertFalse(filter.isAllowed(tags));

		tags.add("bicycle::yes");
		tags.add("tagkey::tagvalue");
		assertTrue(filter.isAllowed(tags));

		tags.remove("bicycle::yes");
		tags.add("bicycle::permissive");
		assertTrue(filter.isAllowed(tags));

		tags.remove("bicycle::permissive");
		tags.add("bicycle::dismount");
		assertTrue(filter.isAllowed(tags));
	}

	@Test
	public void testUndergroundSteps() {
		CycleEdgeSelector filter = new CycleEdgeSelector();
		Collection<String> tags = new HashSet<String>();

		tags.add("highway::steps");
		tags.add("tunnel::yes");
		tags.add("layer::-1");
		assertFalse(filter.isAllowed(tags));

		tags.remove("layer::-1");
		tags.add("layer::-2");
		assertFalse(filter.isAllowed(tags));
	}
}
