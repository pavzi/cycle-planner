package cz.agents.cycleplanner.core.criteria2;

import eu.superhub.wp5.graphcommon.graph.utils.EdgeUtil;
import eu.superhub.wp5.wp5common.location.Location;

public class CriteriaBuilder {

	public static double MAXIMUM_DOWNHILL_SPEED_MULTIPLIER = 2.5; // s_dmax
	public static double CRITICAL_DOWNHILL_GRADE = 0.1; // d'c
	public static double UPHILL_MULTIPLIER = 8; // a_l
	public static double PERCEPTION_UPHILL_MULTIPLIER = 13; // a_p
	public static double AVERAGE_CRUISING_SPEED_METERS_PER_SECOND = 4.2;

	private final Location from;
	private final Location to;
	private double elevation;
	private double drops;
	private double rises;
	private double lengthInMeters;

	public CriteriaBuilder(Location from, Location to) {
		this.from = from;
		this.to = to;

		this.elevation = computeElevation();
		this.drops = (elevation > 0) ? 0 : -elevation;
		this.rises = (elevation < 0) ? 0 : elevation;
		this.lengthInMeters = EdgeUtil.computeDirectDistanceInM(from, to);
	}

	public double getTravelTime(double travelTimeBaseValue, double travelTimeConstant, double speedInMetersPerSecond) {
		double numerator = lengthInMeters + UPHILL_MULTIPLIER * rises;
		double denominator = speedInMetersPerSecond * getDownhillSpeedMultiplier(drops, lengthInMeters)
				* travelTimeBaseValue;
		double travelTime = (numerator / denominator) + travelTimeConstant;

		return travelTime;
	}

	private double getDownhillSpeedMultiplier(double drop, double length) {
		double downhillGrade = (length == 0d) ? 0 : drop / length;
		if (downhillGrade > CRITICAL_DOWNHILL_GRADE)
			return MAXIMUM_DOWNHILL_SPEED_MULTIPLIER;
		else
			return (((downhillGrade / CRITICAL_DOWNHILL_GRADE) * (MAXIMUM_DOWNHILL_SPEED_MULTIPLIER - 1)) + 1);
	}

	private double computeElevation() {
		if (to.hasElevation() && !from.hasElevation()) {
			return to.getElevation();

		} else if (!to.hasElevation() && from.hasElevation()) {
			return from.getElevation();

		} else if (!to.hasElevation() && !from.hasElevation()) {

			return 0d;
		}
		return to.getElevation() - from.getElevation();
	}

	public double getLengthInMeters() {
		return lengthInMeters;
	}

	public double getDrops() {
		return drops;
	}

	public double getRises() {
		return rises;
	}
}
