package cz.agents.cycleplanner.core.datastructures;

import java.io.Serializable;

import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 * @param <TNode>
 * @param <TEdge>
 */
public class CityData<TNode extends Node, TEdge extends Edge> implements Serializable {

	private static final long serialVersionUID = -6878712050675602570L;

	/**
	 * 
	 */
	private final String cityName;

	/**
	 * 
	 */
	private final int topE6;

	/**
	 * 
	 */
	private final int leftE6;

	/**
	 * 
	 */
	private final int rightE6;

	/**
	 * 
	 */
	private final int bottomE6;

	/**
	 * 
	 */
	private final Graph<TNode, TEdge> graph;

	public CityData(String cityName, int topE6, int leftE6, int rightE6, int bottomE6, Graph<TNode, TEdge> graph) {
		super();
		this.cityName = cityName;
		this.topE6 = topE6;
		this.leftE6 = leftE6;
		this.rightE6 = rightE6;
		this.bottomE6 = bottomE6;
		this.graph = graph;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCityName() {
		return cityName;
	}

	public int getTopE6() {
		return topE6;
	}

	public int getLeftE6() {
		return leftE6;
	}

	public int getRightE6() {
		return rightE6;
	}

	public int getBottomE6() {
		return bottomE6;
	}

	public Graph<TNode, TEdge> getGraph() {
		return graph;
	}

	@Override
	public String toString() {
		return "CityData [cityName=" + cityName + ", topE6=" + topE6 + ", leftE6=" + leftE6 + ", rightE6=" + rightE6
				+ ", bottomE6=" + bottomE6 + ", graph=" + graph + "]";
	}

}
