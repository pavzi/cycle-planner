package cz.agents.cycleplanner.core.datastructures;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.ReadableDuration;
import org.joda.time.ReadableInstant;

import eu.superhub.wp5.plannercore.structures.base.TimeDependentEdge;
import eu.superhub.wp5.wp5common.modes.JourneyPlanTemplate;
import eu.superhub.wp5.wp5common.modes.ModeOfTransport;

@SuppressWarnings("serial")
public class CycleEdge2 extends TimeDependentEdge {

	private final List<CycleNode2> via;

	/**
	 * 
	 */
	// TODO unite to criteria
	private final int[] costs;
	private final double traveTimeConstant;
	private final double travelTimeBaseValue;
	private final double comfortBaseValue;
	private final double quietnessBaseValue;
	private final double prolongationConstant;

	/**
	 * 
	 */
	private final int drops;

	/**
	 * 
	 */
	private final int rises;

	/**
	 * 
	 */
	private final Surface surface;

	/**
	 * 
	 */
	private final RoadType roadType;

	/**
	 * 
	 */
	private final String streetName;

	/**
	 * Bicycle route number
	 */
	private final String bicycleRouteNumber;

	// TODO remove
	private final Collection<String> tags;

	public CycleEdge2(long fromNodeId, long toNodeId, double lengthInMeters, List<CycleNode2> via, int[] costs,
			double traveTimeConstant, double travelTimeBaseValue, double comfortBaseValue, double quietnessBaseValue,
			double prolongationConstant, int drops, int rises, Surface surface, RoadType roadType, String streetName,
			String bicycleRouteNumber, Collection<String> tags) {
		super(fromNodeId, toNodeId, lengthInMeters);

		this.via = via;
		this.costs = costs;
		this.traveTimeConstant = traveTimeConstant;
		this.travelTimeBaseValue = travelTimeBaseValue;
		this.comfortBaseValue = comfortBaseValue;
		this.quietnessBaseValue = quietnessBaseValue;
		this.prolongationConstant = prolongationConstant;
		this.drops = drops;
		this.rises = rises;
		this.surface = surface;
		this.roadType = roadType;
		this.streetName = streetName;
		this.bicycleRouteNumber = bicycleRouteNumber;
		this.tags = tags;
	}

	public List<CycleNode2> getVia() {
		return via;
	}

	public int[] getCosts() {
		return costs;
	}

	public double getTraveTimeConstant() {
		return traveTimeConstant;
	}

	public double getTravelTimeBaseValue() {
		return travelTimeBaseValue;
	}

	public double getComfortBaseValue() {
		return comfortBaseValue;
	}

	public double getQuietnessBaseValue() {
		return quietnessBaseValue;
	}

	public double getProlongationConstant() {
		return prolongationConstant;
	}

	public int getDrops() {
		return drops;
	}

	public int getRises() {
		return rises;
	}

	public Surface getSurface() {
		return surface;
	}

	public RoadType getRoadType() {
		return roadType;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getBicycleRouteNumber() {
		return bicycleRouteNumber;
	}

	public Collection<String> getTags() {
		return tags;
	}

	@Override
	public ReadableInstant findNearestArrivalTimeAtTerminalNode(ReadableInstant timeAtInitialNode,
			double cruisingSpeedInKmph) {
		return new DateTime(timeAtInitialNode).plusMillis(costs[0] * 1000);
	}

	@Override
	public ReadableDuration computeAverageTravelTime(double cruisingSpeedInKmph) {
		return new Duration(costs[0] * 1000);
	}

	@Override
	public boolean checkFeasibility(ModeOfTransport currentModeOfTransport) {
		return currentModeOfTransport.equals(ModeOfTransport.BIKE);
	}

	@Override
	public boolean checkFeasibility(JourneyPlanTemplate journeyPlanTemplate) {
		return JourneyPlanTemplate.getModesOfTransport(journeyPlanTemplate).contains(ModeOfTransport.BIKE);
	}

	@Override
	public boolean checkFeasibility(Set<ModeOfTransport> allowedModesOfTransport) {
		return allowedModesOfTransport.contains(ModeOfTransport.BIKE);
	}

}
