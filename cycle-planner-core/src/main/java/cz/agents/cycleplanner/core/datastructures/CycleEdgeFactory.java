package cz.agents.cycleplanner.core.datastructures;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import cz.agents.cycleplanner.core.criteria2.CriteriaBuilder;
import cz.agents.cycleplanner.core.evaluate.ParametersOfTags;
import eu.superhub.wp5.wp5common.location.Location;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class CycleEdgeFactory {

	public static double MAXIMUM_DOWNHILL_SPEED_MULTIPLIER = 2.5; // s_dmax
	public static double CRITICAL_DOWNHILL_GRADE = 0.1; // d'c
	public static double UPHILL_MULTIPLIER = 8; // a_l
	public static double PERCEPTION_UPHILL_MULTIPLIER = 13; // a_p
	public static double AVERAGE_CRUISING_SPEED_METERS_PER_SECOND = 4.2;

	private static final Pattern SURFACE_PAVED_SMOOTH = Pattern
			.compile("way::surface::(paved|asphalt|concrete:.*|metal|wood)");
	private static final Pattern SURFACE_PAVED_COBBLESTONE_LIKE = Pattern
			.compile("way::surface::(cobblestone:.*|paving_stone:.*|sett)");
	private static final Pattern SURFACE_UNPAVED = Pattern
			.compile("way::surface::(unpaved|compacted|dirt|earth|fine_gravel|grass|gravel|ground|mud|pebblestone|sand)");
	private static final Pattern HIGHWAY = Pattern.compile("way::highway::.*");
	private static final Pattern PRIMARY = Pattern.compile("way::highway::primary|way::highway::primary_link");
	private static final Pattern SECONDARY = Pattern.compile("way::highway::secondary|way::highway::secondary_link");
	private static final Pattern TERTIARY = Pattern.compile("way::highway::tertiary|way::highway::tertiary_link");
	private static final Pattern CYCLEWAY = Pattern.compile(".*cycleway.*");
	private static final Pattern FOOTWAY = Pattern.compile(".*footway.*|.*pedestrian.*");
	private static final Pattern STEPS = Pattern
			.compile("way::highway::steps|node::highway::elevator|node::highway::steps");
	private static final Pattern STREET_NAME = Pattern.compile("way::name::.*");
	private static final Pattern BICYCLE_ROUTE_NAME = Pattern.compile("relation::ref::.*");

	private CycleEdgeFactory() {
	}

	public static CycleEdge2 getCycleEdge2(CycleNode2 from, CycleNode2 to, List<CycleNode2> via, Collection<String> tags) {

		ParametersOfTags parameters = ParametersOfTags.INSTANCE;

		double maxTraveTimeConstant = 0d;
		double minTravelTimeMultiplier = 1d;
		double maxComfortMultiplier = -1d;
		double maxQuietnessMultiplier = -1d;
		double prolongationConstant = 0d;
		double quietnessAvgBaseValue = 0d;
		double countQuietnessTags = 0;

		if (tags != null) {
			for (String edgeTag : tags) {

				if (parameters.contains(edgeTag)) {

					double multiplier = parameters.getTravelTimeMultiplier(edgeTag);
					if (minTravelTimeMultiplier > multiplier)
						minTravelTimeMultiplier = multiplier;

					double constant = parameters.getTravelTimeSlowdownConstant(edgeTag);
					if (constant > maxTraveTimeConstant)
						maxTraveTimeConstant = constant;

					multiplier = parameters.getComfortMultiplier(edgeTag);
					if (multiplier > maxComfortMultiplier)
						maxComfortMultiplier = multiplier;

					multiplier = parameters.getQuietnessMultiplier(edgeTag);
					if (multiplier > maxQuietnessMultiplier)
						maxQuietnessMultiplier = multiplier;

					constant = parameters.getProlongationConstant(edgeTag);
					prolongationConstant = (constant > prolongationConstant) ? constant : prolongationConstant;

					if (parameters.getQuietnessMultiplier(edgeTag) != -1) {
						quietnessAvgBaseValue += parameters.getQuietnessMultiplier(edgeTag);
						countQuietnessTags++;
					}
				}
			}
		}

		maxComfortMultiplier = (maxComfortMultiplier == -1) ? 1 : maxComfortMultiplier;
		maxQuietnessMultiplier = (maxQuietnessMultiplier == -1) ? 1 : maxQuietnessMultiplier;

		quietnessAvgBaseValue = (countQuietnessTags == 0) ? 1 : (Math
				.round((quietnessAvgBaseValue / countQuietnessTags) * 10) / 10d);

		double totalLengthInMeters = 0d;
		double totalTravelTime = 0d;
		double totalRises = 0d;
		double totalDrops = 0d;
		double totalFlatnessTravelTime = 0d;

		Location actual = from;
		via.add(to);

		for (Location next : via) {
			
			CriteriaBuilder criteriaBuilder = new CriteriaBuilder(actual, next);
			double lengthInMeters = criteriaBuilder.getLengthInMeters();
			double travelTime = criteriaBuilder.getTravelTime(minTravelTimeMultiplier, maxTraveTimeConstant,
					AVERAGE_CRUISING_SPEED_METERS_PER_SECOND);
			
			double flatnessTravelTime = (criteriaBuilder.getRises() * PERCEPTION_UPHILL_MULTIPLIER)
					/ AVERAGE_CRUISING_SPEED_METERS_PER_SECOND;

			totalLengthInMeters += lengthInMeters;
			totalTravelTime += travelTime;
			totalRises += criteriaBuilder.getRises();
			totalDrops += criteriaBuilder.getDrops();
			totalFlatnessTravelTime += flatnessTravelTime;

			actual = next;
		}

		// remove to node from list
		via.remove(via.size() - 1);

		int comfort = (int) Math.ceil(totalTravelTime * maxComfortMultiplier
				+ (prolongationConstant / AVERAGE_CRUISING_SPEED_METERS_PER_SECOND));
		int[] costs = { (int) Math.ceil(totalTravelTime), comfort,
				(int) Math.ceil(totalTravelTime * quietnessAvgBaseValue), (int) Math.ceil(totalFlatnessTravelTime) };

		return new CycleEdge2(from.getId(), to.getId(), totalLengthInMeters, via, costs, maxTraveTimeConstant,
				minTravelTimeMultiplier, maxComfortMultiplier, quietnessAvgBaseValue, prolongationConstant,
				(int) Math.round(totalDrops), (int) Math.round(totalRises), getSurface(tags), getRoadType(tags),
				getStreetName(tags), getBicycleRouteName(tags), tags);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tags
	 * @return
	 */
	private static Surface getSurface(Collection<String> tags) {
		for (String tag : tags) {
			if (SURFACE_PAVED_SMOOTH.matcher(tag).matches()) {
				return Surface.PAVED_SMOOTH;
			}

			if (SURFACE_PAVED_COBBLESTONE_LIKE.matcher(tag).matches()) {
				return Surface.PAVED_COBBLESTONE;
			}
			if (SURFACE_UNPAVED.matcher(tag).matches()) {
				return Surface.UNPAVED;
			}
		}
		return null;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tags
	 * @return
	 */
	private static RoadType getRoadType(Collection<String> tags) {
		for (String tag : tags) {

			if (FOOTWAY.matcher(tag).matches()) {

				return RoadType.FOOTWAY;
			} else if (CYCLEWAY.matcher(tag).matches()) {

				return RoadType.CYCLEWAY;
			} else if (HIGHWAY.matcher(tag).matches()) {

				if (STEPS.matcher(tag).matches()) {
					return RoadType.STEPS;
				} else if (PRIMARY.matcher(tag).matches()) {
					return RoadType.PRIMARY;
				} else if (SECONDARY.matcher(tag).matches()) {
					return RoadType.SECONDARY;
				} else if (TERTIARY.matcher(tag).matches()) {
					return RoadType.TERTIARY;
				}

				return RoadType.ROAD;
			}
		}
		return null;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tags
	 * @return
	 */
	private static String getStreetName(Collection<String> tags) {
		for (String tag : tags) {
			if (STREET_NAME.matcher(tag).matches()) {
				return tag.replaceFirst("way::name::", "");
			}
		}
		return null;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tags
	 * @return
	 */
	private static String getBicycleRouteName(Collection<String> tags) {
		for (String tag : tags) {
			if (BICYCLE_ROUTE_NAME.matcher(tag).matches()) {
				return tag.replaceFirst("relation::ref::", "");
			}
		}
		return null;
	}
}
