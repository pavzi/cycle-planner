package cz.agents.cycleplanner.core.datastructures;

import java.util.Collection;

import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.wp5common.location.GPSLocation;

@SuppressWarnings("serial")
public class CycleNode2 extends Node {

	// private CycleNode2() {
	// }

	private final Collection<String> tags;

	public CycleNode2(long id, GPSLocation gpsLocation, String description, Collection<String> tags) {
		super(id, gpsLocation, description);
		this.tags = tags;
	}

	public Collection<String> getTags() {
		return tags;
	}
}
