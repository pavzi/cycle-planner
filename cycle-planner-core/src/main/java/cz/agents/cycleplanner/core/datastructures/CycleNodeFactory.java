package cz.agents.cycleplanner.core.datastructures;

import java.util.Collection;

import eu.superhub.wp5.wp5common.location.GPSLocation;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class CycleNodeFactory {

	private CycleNodeFactory() {
	}

	public static CycleNode2 getCycleNode2(long id, GPSLocation gpsLocation, Collection<String> tags) {
		String description = "CycleNode";
		CycleNode2 node = new CycleNode2(id, gpsLocation, description, tags);

		return node;
	}
}
