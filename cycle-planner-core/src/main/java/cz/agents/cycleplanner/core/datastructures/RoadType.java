package cz.agents.cycleplanner.core.datastructures;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum RoadType {
	CYCLEWAY, FOOTWAY, STEPS, PRIMARY, SECONDARY, TERTIARY, ROAD;
}
