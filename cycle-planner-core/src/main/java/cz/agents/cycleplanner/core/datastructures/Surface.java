package cz.agents.cycleplanner.core.datastructures;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum Surface {
	PAVED_SMOOTH, // paved smooth surface, e.g. asphalt, concrete
	PAVED_COBBLESTONE, // paved surface but rough, e.g. cobble stones, setts,...
	UNPAVED; // unpaved surface -- dirt, grass, mud,...
}
