package cz.agents.cycleplanner.core.criteria2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import eu.superhub.wp5.wp5common.location.GPSLocation;
import eu.superhub.wp5.wp5common.location.Location;

public class CriteriaBuilderTest {

	private CriteriaBuilder criteriaBuilder;
	
	@Before
	public void setUp() throws Exception {
		Location from = new GPSLocation(1d, 1d, 1d, 1d, 20d);
		Location to = new GPSLocation(1, 10, 1, 10, 30d);
		criteriaBuilder = new CriteriaBuilder(from, to);
	}

	@Test
	public void testRises() {
		assertEquals(10d, criteriaBuilder.getRises(), 0d);
	}
	@Test
	public void testDrops() {
		assertEquals(0d, criteriaBuilder.getDrops(), 0d);
	}
	@Test
	public void testLengthInMeters() {
		assertEquals(9d, criteriaBuilder.getLengthInMeters(), 0d);
	}
	
	@Test
	public void testTravelTime() {
		double expectedTravelTime = 52.25d;
		double actualTravelTime = criteriaBuilder.getTravelTime(1, 30, 4);
		assertEquals(expectedTravelTime, actualTravelTime, 0d);		
	}
	
}
