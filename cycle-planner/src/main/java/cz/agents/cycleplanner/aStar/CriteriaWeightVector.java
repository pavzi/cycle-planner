package cz.agents.cycleplanner.aStar;

import cz.agents.cycleplanner.api.datamodel.v2.Profile;

/**
 * 
 * TODO javadoc
 * 
 * elements of vector sum up to one
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class CriteriaWeightVector {

	public static final CriteriaWeightVector FAST = new CriteriaWeightVector(1, 0, 0, 0);
	public static final CriteriaWeightVector FLAT = new CriteriaWeightVector(0, 1, 1, 8);
	public static final CriteriaWeightVector COMMUTING = new CriteriaWeightVector(3, 5, 1, 1);
	public static final CriteriaWeightVector BIKE_FRIENDLY = new CriteriaWeightVector(0, 2, 6, 2);

	
	private final double[] weights;

	public CriteriaWeightVector(Profile profile) {
		this(profile.getTravelTimeWeight(), profile.getComfortWeight(), profile.getQuietnessWeight(), profile
				.getFlatnessWeight());
	}

	public CriteriaWeightVector(double travelTimeWeight, double comfortWeight, double quietnessWeight,
			double flatnessWeight) {
		
		weights = new double[4];

		double sum = travelTimeWeight + comfortWeight + quietnessWeight + flatnessWeight;

		if (travelTimeWeight > 0.001) {
			weights[0] = travelTimeWeight / sum;
		}

		if (comfortWeight > 0.001) {
			weights[1] = comfortWeight / sum;
		}

		if (quietnessWeight > 0.001) {
			weights[2] = quietnessWeight / sum;
		}

		if (flatnessWeight > 0.001) {
			weights[3] = flatnessWeight / sum;
		}
	}
	
	public double[] getWeights() {
		return weights;
	}
}
