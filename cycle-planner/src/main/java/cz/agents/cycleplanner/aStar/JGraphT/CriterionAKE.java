package cz.agents.cycleplanner.aStar.JGraphT;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.plannercore.structures.base.TimeDependentEdge;
import eu.superhub.wp5.plannercore.structures.evaluators.additionalkeysevaluators.AdditionalKeyEvaluator;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

/**
 * @author Jan Nykl(CVUT)
 * @author Pavol Zilecky pavol.zilecky@agents.felk.cvut.cz
 */
public class CriterionAKE extends AdditionalKeyEvaluator<TimedNode>{

    private CriteriaWeightVector weightVector;

    public CriterionAKE(CriteriaWeightVector profile) {

        this.weightVector = profile;
    }


    @Override
    public double computeAdditionalKey(TimedNode current, Node successor, TimeDependentEdge timeDependentEdge) {
        CycleEdge2 edge = (CycleEdge2) timeDependentEdge;
        double currentCost = 0;
        
        for (int i = 0; i < edge.getCosts().length; i++) {
			currentCost += edge.getCosts()[i] * weightVector.getWeights()[i]; 
		}
        
        return current.getAdditionalKey() + currentCost;
    }
}
