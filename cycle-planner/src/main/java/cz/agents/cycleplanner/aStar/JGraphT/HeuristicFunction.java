package cz.agents.cycleplanner.aStar.JGraphT;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.graphcommon.graph.utils.EdgeUtil;
import eu.superhub.wp5.plannercore.algorithms.heuristics.Heuristic;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

/**
 * TODO UPDATE TO NEW CYCLENODE AND CYCLEEDGE 
 * TODO javadoc
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class HeuristicFunction implements Heuristic<TimedNode> {
	
	public static double MAXIMUM_DOWNHILL_SPEED_MULTIPLIER = 2.5; // s_dmax
	public static double CRITICAL_DOWNHILL_GRADE = 0.1; // d'c
	public static double UPHILL_MULTIPLIER = 8; // a_l
	public static double PERCEPTION_UPHILL_MULTIPLIER = 13; // a_p
	public static double AVERAGE_CRUISING_SPEED_METERS_PER_SECOND = 4.2; 
	
	private Node goal;
	private double oneOverTravelTimeDenominator;
	private double flatnessBase;
	private CriteriaWeightVector weightVector;

	public HeuristicFunction(Node goal, CriteriaWeightVector weightVector) {

		this.goal = goal;
		
		this.oneOverTravelTimeDenominator = 1 / (AVERAGE_CRUISING_SPEED_METERS_PER_SECOND * MAXIMUM_DOWNHILL_SPEED_MULTIPLIER);
		this.weightVector = weightVector;
		this.flatnessBase = PERCEPTION_UPHILL_MULTIPLIER / AVERAGE_CRUISING_SPEED_METERS_PER_SECOND;
	}

	// TODO Implement Heuristic function
	@Override
	public double getCostToGoalEstimate(TimedNode current) {

		double directDistance = EdgeUtil.computeDirectDistanceInM(current, goal);

		double traveTimeHeuristic = directDistance * oneOverTravelTimeDenominator;
		double result = 0;

//		List<Criterion> criteria = weightVector.getCriteria();
//
//		for (Criterion criterion : criteria) {
//			if (criterion instanceof FlatnessCriterion) {
//				result += flatnessBase * computeRisesToDestination(current) * criterion.getWeightedHeuristicValue();
//			} else {
//				result += traveTimeHeuristic * criterion.getWeightedHeuristicValue();
//			}
//		}

		return result;
	}

	private double computeRisesToDestination(TimedNode node) {
		double elevation = goal.getElevation() - node.getElevation();

		if (elevation > 0)
			return elevation;
		return 0;
	}

}
