package cz.agents.cycleplanner.api;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.api.datamodel.v2.Coordinate;
import cz.agents.cycleplanner.api.datamodel.v2.Feedback;
import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;
import cz.agents.cycleplanner.api.datamodel.v2.Plan;
import cz.agents.cycleplanner.api.datamodel.v2.Profile;
import cz.agents.cycleplanner.api.datamodel.v2.Request;
import cz.agents.cycleplanner.api.datamodel.v2.Response;
import cz.agents.cycleplanner.api.datamodel.v2.ResponseStatus;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.exceptions.PlanNotFoundException;
import cz.agents.cycleplanner.exceptions.PlannerException;
import cz.agents.cycleplanner.mongodb.BicycleJourneyPlanStorage;
import cz.agents.cycleplanner.mongodb.StorageMongoDbConnectionProvider;
import cz.agents.cycleplanner.multicriteria.MCCycleCostFunction2;
import cz.agents.cycleplanner.multicriteria.ParetoSet;
import cz.agents.cycleplanner.planner.MultipleGoalDestinationBicyclePlanner;
import cz.agents.cycleplanner.planner.mc.MultiCriteriaBicyclePlanner;
import cz.agents.cycleplanner.planner.mc.SupportedMCAlgorithms;
import cz.agents.cycleplanner.routingService.FirstLastMileGraph;
import cz.agents.cycleplanner.routingService.RoutingService3;
import cz.agents.cycleplanner.util.JSONUtils;
import eu.superhub.wp5.graphcommon.graph.EdgeId;
import eu.superhub.wp5.graphcommon.graph.Graph;

/**
 * 
 * @author Pavol Zilecky <pavol.zilecky@agents.fel.cvut.cz>
 * 
 */
@Path("/api/v2")
public class BicycleJourneyPlanning implements ServletContextListener {

	@Context
	private UriInfo context;

	private static final String DATABASE_PROPERTIES_FILE_NAME = "mongodb.properties";

	private static final Logger log = Logger.getLogger(BicycleJourneyPlanning.class);

	private static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

	private static PlanningInstanceBuilder planningInstanceBuilder;
	private static BicycleScalarizedMultiCriteriaRequestHandler requestHandler;
	private static BicycleJourneyPlanStorage bicycleJourneyPlanStorage;

	/**
	 * Creates a new instance of BicycleJourneyPlanning
	 */
	public BicycleJourneyPlanning() {
		super();
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		log.info("Call contextInitialized(...) method.");
		try {
			StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile(DATABASE_PROPERTIES_FILE_NAME);
		} catch (IOException e) {
			log.error("Unable to connect to database! ", e);
			throw new RuntimeException("Unable to connect to database! ");
		}
		bicycleJourneyPlanStorage = BicycleJourneyPlanStorage.getStorage();

		planningInstanceBuilder = new PlanningInstanceBuilder();
		requestHandler = new BicycleScalarizedMultiCriteriaRequestHandler(new MultipleGoalDestinationBicyclePlanner());
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		requestHandler.close();
	}

	/**
	 * Store planned journey in database and returns location to that journey.
	 * 
	 * If profile is specified in request, a response will contains only one
	 * plan for given profile. If no profile is specified, a response contains
	 * four plans for predefined profiles (
	 * <code>Commuting, Bike friendly, Flat and Fast</code>)
	 * 
	 * @param requestJson
	 *            is <code>Request</code> class in JSON format and defined in
	 *            package <code>cz.agents.cycleplanner.api.datamodel</code>
	 * @return location to journey if it was successfully stored to database,
	 *         otherwise HTTP 404 response
	 */
	@POST
	@Path("/journeys")
	@Consumes(MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response planJourney(String requestJson) {

		Request request = JSONUtils.jsonToJavaObject(requestJson, Request.class);

		if (request == null) {
			log.warn("Incompatible JSON request format!");

			return javax.ws.rs.core.Response.status(400).entity("Incompatible JSON request format!").type("text/plain")
					.build();
		}

		long responseId = bicycleJourneyPlanStorage.getNewId(BicycleJourneyPlanStorage.LAST_RESPONSE_ID_COLLECTION,
				BicycleJourneyPlanStorage.LAST_RESPONSE_ID_KEY, BicycleJourneyPlanStorage.RESPONSES_COLLECTION,
				BicycleJourneyPlanStorage.RESPONSES_ID_KEY);
		Response response = createResponse(responseId, request);
		boolean success = bicycleJourneyPlanStorage.storeCycleplannerResponse(response);

		if (!success) {
			log.error("Unable to store response to database!");
			return javax.ws.rs.core.Response.status(404).entity("Unable to store response to database!")
					.type("text/plain").build();
			// TODO What to do when plan is not store to database, we can
			// shutdown application, or we have to set up some watchdog, that
			// warns us when something is wrong with DB
			// throw new
			// RuntimeException("Unable to store response to database!");
		}

		URI responseLocation;

		try {
			responseLocation = new URI(Long.toString(responseId));
		} catch (URISyntaxException e) {
			log.error(request, e);
			return javax.ws.rs.core.Response.status(404).entity("Unable to create link to response!")
					.type("text/plain").build();
		}

		return javax.ws.rs.core.Response.created(responseLocation).build();
	}

	/**
	 * 
	 * Returns bicycle journey plan for predefined query. This method serves to
	 * test whether planner is functional.
	 * 
	 * @return bicycle journey plan for predefined query in JSON format
	 */
	@GET
	@Path("/journeys/test")
	@Produces("application/json")
	public javax.ws.rs.core.Response planTestJourney() {
		Coordinate origin = new Coordinate(50072600, 14391800);
		Coordinate destination = new Coordinate(50073200, 14441600);
		double averageSpeedKMpH = 20;
		Profile profile = new Profile(1, 3, 5, 2);
		Request request = new Request("test", origin, null, destination, averageSpeedKMpH, "praha", profile);
		log.info("Request handler in planTestJourney method: " + requestHandler);
		Response response = createResponse(Long.MAX_VALUE, request);

		String responseJson = JSONUtils.javaObjectToJson(response);

		return javax.ws.rs.core.Response.ok(responseJson, MediaType.APPLICATION_JSON).build();
	}

	/**
	 * 
	 * Uses multi-label correcting algorithm to search Pareto set of bicycle
	 * journeys between origin and destination.
	 * 
	 * Returns set of edges together with number of journeys from Pareto set
	 * that includes it. Information is represented as suggested width and color
	 * for the edge.
	 * 
	 * @param startLat
	 *            latitude of origin
	 * @param startLon
	 *            longitude of origin
	 * @param endLat
	 *            latitude of destination
	 * @param endLon
	 *            longitude of destination
	 * @param mcAlgorithm
	 *            implementation of <i>MLC</i> algorithm to use
	 * @return set of edges with suggested width and color for render program
	 */
	@GET
	@Path("/journeys/mc")
	@Produces("application/json")
	public javax.ws.rs.core.Response planMCJourney(@QueryParam("startLat") String startLat,
			@QueryParam("startLon") String startLon, 
			@QueryParam("endLat") String endLat,
			@QueryParam("endLon") String endLon, 
			@DefaultValue("MC_DIJKSTRA_BOUNDED_ELLIPSE_EPSILON") @QueryParam("mcAlgorithm") String mcAlgorithm) {

		double averageCrusingSpeedKMPH = 15d;

		long responseId = -1L;
		String creationTimestamp = dateParser.format(new Date());
		Request request = new Request("mc", new Coordinate((int) Math.round(Double.parseDouble(startLat) * 1E6),
				(int) Math.round(Double.parseDouble(startLon) * 1E6)), null, new Coordinate((int) Math.round(Double
				.parseDouble(endLat) * 1E6), (int) Math.round(Double.parseDouble(endLon) * 1E6)),
				averageCrusingSpeedKMPH, null, null);
		log.debug(request);

		try {
			SupportedMCAlgorithms alg = SupportedMCAlgorithms.valueOf(mcAlgorithm);

			if (!alg.equals(SupportedMCAlgorithms.MC_DIJKSTRA_BOUNDED_ELLIPSE_EPSILON)
					&& !alg.equals(SupportedMCAlgorithms.MC_DIJKSTRA_ELLIPSE_EPSILON)
					&& !alg.equals(SupportedMCAlgorithms.MC_DIJKSTRA_BOUNDED_EPSILON)) {
				return javax.ws.rs.core.Response.status(400).entity("Unsupported multi-criteria algorithm!")
						.type("text/plain").build();
			}

			PlanningInstance2 planningInstance = planningInstanceBuilder.build2(request);

			log.debug(planningInstance);

			Map<CycleNode2, Collection<EdgeId>> contractedNodes = RoutingService3.INSTANCE
					.getContractedNodesForCity(planningInstance.getCityName());
			long timeToGetPlanningGraph = System.currentTimeMillis();
			Graph<CycleNode2, CycleEdge2> firstLastMileGraph = new FirstLastMileGraph(planningInstance.getGraph(),
					planningInstance.getOriginsInGraph(), planningInstance.getDestinationsInGraph(), contractedNodes);
			timeToGetPlanningGraph = System.currentTimeMillis() - timeToGetPlanningGraph;
			log.info("Time to obtain planning graph: " + timeToGetPlanningGraph);

			MultiCriteriaBicyclePlanner mcPlanner = new MultiCriteriaBicyclePlanner(1, alg, null, null, null, null,
					null, new MCCycleCostFunction2());

			Future<ParetoSet<CycleNode2, CycleEdge2>> future = mcPlanner.plan(firstLastMileGraph,
					planningInstance.getOriginsInGraph(), planningInstance.getDestinationsInGraph(), Double.MAX_VALUE,
					null);

			int planId = 0;
			Map<Long, CycleNode2> additionalNodes = new HashMap<Long, CycleNode2>();

			for (CycleNode2 node : planningInstance.getOriginsInGraph()) {
				additionalNodes.put(node.getId(), node);
			}

			for (CycleNode2 node : planningInstance.getDestinationsInGraph()) {
				additionalNodes.put(node.getId(), node);
			}

			Collection<Plan> plans = new ArrayList<Plan>();
			ParetoSet<CycleNode2, CycleEdge2> paretoSet = future.get();

			for (List<CycleEdge2> path : paretoSet.getAllSequenceOfEdges()) {
				PlanBuilder planBuilder = new PlanBuilder(planId++, "", firstLastMileGraph, additionalNodes, path
						.get(0).getFromNodeId(), new HashSet<Long>(), path, planningInstance.getAverageSpeedKmPH());
				Plan plan = planBuilder.build();

				plans.add(plan);
			}

			Response mcResponse = new Response(responseId, creationTimestamp, request, planningInstance.getCityName(),
					ResponseStatus.OK, plans);

			mcPlanner.close();

			return javax.ws.rs.core.Response.ok(JSONUtils.javaObjectToJson(mcResponse), MediaType.APPLICATION_JSON)
					.build();

		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
			
			return javax.ws.rs.core.Response.status(400).entity("Wrong multi-criteria algorithm!").type("text/plain")
					.build();
		} catch (OutOfBoundsException e) {
			log.error(e.getMessage(), e);
			Response response = new Response(responseId, creationTimestamp, request, ResponseStatus.OUT_OF_BOUNDS);

			return javax.ws.rs.core.Response.status(400).entity(JSONUtils.javaObjectToJson(response))
					.type("application/json").build();
		} catch (PlannerException e) {
			log.error(e.getMessage(), e);
			Response response = new Response(responseId, creationTimestamp, request, ResponseStatus.PLANNER_EXCEPTION);

			return javax.ws.rs.core.Response.status(400).entity(JSONUtils.javaObjectToJson(response))
					.type("application/json").build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);

			return javax.ws.rs.core.Response.status(400).build();
		}
	}

	/**
	 * Returns journey with specified id in form of <code>Response</code> class,
	 * defined in package <code>cz.agents.cycleplanner.api.datamodel</code>.
	 * 
	 * @param journeyId
	 *            identifier of journey in database
	 * @return HTTP 200 response with journey in JSON format if journey exists
	 *         in database, otherwise HTTP 404 response with message
	 */
	@GET
	@Path("journeys/{journeyId: [0-9]*}")
	@Produces("application/json")
	public javax.ws.rs.core.Response getJourney(@PathParam("journeyId") long journeyId) {
		log.info("Retrieving journey " + journeyId + " from database!");
		Response response = bicycleJourneyPlanStorage.retrieveCycleplannerResponse(journeyId);

		if (response == null) {
			// Unable to find requested journey
			return javax.ws.rs.core.Response.status(404).entity("No such a journey exists!").type("text/plain").build();
		}

		String responseJson = JSONUtils.javaObjectToJson(response);

		return javax.ws.rs.core.Response.ok(responseJson, MediaType.APPLICATION_JSON).build();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param responseId
	 * @param request
	 * @return
	 */
	private Response createResponse(long responseId, Request request) {
		String creationTimestamp = dateParser.format(new Date());

		try {

			long planningInstanceBuilderTime = System.currentTimeMillis();
			PlanningInstance2 planningInstance = planningInstanceBuilder.build2(request);
			log.info("PlanningInstanceBuilder running time is "
					+ (System.currentTimeMillis() - planningInstanceBuilderTime) + " ms");

			long requestHandlerTime = System.currentTimeMillis();
			Collection<Plan> plans = requestHandler.handle(planningInstance);
			log.info("RequestHandler running time is " + (System.currentTimeMillis() - requestHandlerTime) + " ms");

			return new Response(responseId, creationTimestamp, request, planningInstance.getCityName(),
					ResponseStatus.OK, plans);
		} catch (OutOfBoundsException e) {
			log.error(request, e);

			return new Response(responseId, creationTimestamp, request, ResponseStatus.OUT_OF_BOUNDS);
		} catch (PlanNotFoundException e) {
			log.error(request, e);

			return new Response(responseId, creationTimestamp, request, ResponseStatus.PLAN_NOT_FOUND);
		} catch (PlannerException e) {
			log.error(request, e);

			return new Response(responseId, creationTimestamp, request, ResponseStatus.PLANNER_EXCEPTION);
		}
	}

	/**
	 * 
	 * Saves feedback to database.
	 * 
	 * @param feedback
	 *            JSON format of feedback to bicycle journey plan
	 * @return response with status code 201 when saving runs successfully,
	 *         otherwise returns response with status code 400 or 401
	 */
	@POST
	@Path("/feedback")
	@Consumes(MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response feedback(String feedback) {

		Feedback cycleplannerFeedback = JSONUtils.jsonToJavaObject(feedback, Feedback.class);

		if (cycleplannerFeedback != null) {

			String creationTimestamp = dateParser.format(new Date());
			long feedbackId = bicycleJourneyPlanStorage.getNewId(BicycleJourneyPlanStorage.LAST_FEEDBACK_ID_COLLECTION,
					BicycleJourneyPlanStorage.LAST_FEEDBACK_ID_KEY, BicycleJourneyPlanStorage.FEEDBACK_COLLECTION,
					BicycleJourneyPlanStorage.FEEDBACK_ID_KEY);
			FeedbackStorageObject feedbackStorageObject = new FeedbackStorageObject(feedbackId, creationTimestamp,
					cycleplannerFeedback);
			boolean success = bicycleJourneyPlanStorage.storeCycleplannerFeedback(feedbackStorageObject);

			if (success) {
				URI location = null; // TODO Temporary, we do not provide API to
										// reading feedback from DB
				return javax.ws.rs.core.Response.created(location).build();
			} else {
				log.warn("Problem with saving feedback to database!");
				return javax.ws.rs.core.Response.status(404).entity("Problem with saving feedback to database!")
						.type("text/plain").build();
			}

		} else {
			log.warn("Incompatible JSON feedback format!");

			return javax.ws.rs.core.Response.status(400).entity("Incompatible JSON feedback format!")
					.type("text/plain").build();
		}

	}

}
