package cz.agents.cycleplanner.api;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.jgrapht.GraphPath;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.api.datamodel.v2.Plan;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.exceptions.PlanNotFoundException;
import cz.agents.cycleplanner.exceptions.PlannerException;
import cz.agents.cycleplanner.planner.BicyclePlanner;
import cz.agents.cycleplanner.routingService.FirstLastMileGraph;
import cz.agents.cycleplanner.routingService.RoutingService3;
import eu.superhub.wp5.graphcommon.graph.EdgeId;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedEdge;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class BicycleScalarizedMultiCriteriaRequestHandler implements Closeable,
		RequestHandler<PlanningInstance2, Collection<Plan>> {

	private static Logger log = Logger.getLogger(BicycleScalarizedMultiCriteriaRequestHandler.class);

	private RoutingService3 routingService;
	private BicyclePlanner<Map<Long, GraphPath<TimedNode, TimedEdge>>> bicyclePlanner;

	/**
	 * Represents whole path between origin, waypoints and destination.
	 * 
	 * Contains ordered list of objects of type <code>CycleEdge</code>
	 * representing path in cycle graph. Together with list it contains id of
	 * path origin, ids of path waypoints and cost.
	 * 
	 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
	 *
	 */
	private class FinalPath {
		private long originId;
		private Collection<Long> waypointIds;
		private List<CycleEdge2> path;
		private double pathCost;

		public FinalPath(long originId, Collection<Long> waypointIds, List<CycleEdge2> path, double pathCost) {
			this.originId = originId;
			this.waypointIds = waypointIds;
			this.path = path;
			this.pathCost = pathCost;
		}

		public long getOriginId() {
			return originId;
		}

		public Collection<Long> getWaypointIds() {
			return waypointIds;
		}

		public List<CycleEdge2> getPath() {
			return path;
		}

		public double getPathCost() {
			return pathCost;
		}

		/**
		 * 
		 * TODO javadoc
		 * 
		 * @param originId
		 * @param path
		 * @param pathCost
		 */
		public void concat(long originId, List<CycleEdge2> path, double pathCost) {
			waypointIds.add(this.originId);
			this.originId = originId;
			// Put new path before the previous path
			this.path.addAll(0, path);
			this.pathCost += pathCost;
		}
	}

	public BicycleScalarizedMultiCriteriaRequestHandler(
			BicyclePlanner<Map<Long, GraphPath<TimedNode, TimedEdge>>> bicyclePlanner) {
		this.routingService = RoutingService3.INSTANCE;
		this.bicyclePlanner = bicyclePlanner;
	}

	@Override
	public void close() {
		bicyclePlanner.close();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws PlanNotFoundException
	 * @throws OutOfBoundsException
	 * @throws PlannerException
	 */
	@Override
	public Collection<Plan> handle(PlanningInstance2 planningInstance) throws OutOfBoundsException,
			PlanNotFoundException, PlannerException {
		Collection<Plan> plans = new ArrayList<Plan>();

		try {
			Map<Profiles, List<Future<Map<Long, GraphPath<TimedNode, TimedEdge>>>>> futuresForAllProfiles = new HashMap<Profiles, List<Future<Map<Long, GraphPath<TimedNode, TimedEdge>>>>>();
			Graph<CycleNode2, CycleEdge2> graph = planningInstance.getGraph();
			List<CycleNode2> originNodes = planningInstance.getOriginsInGraph();
			List<List<CycleNode2>> waypointNodes = planningInstance.getWaypointsInGraph();
			List<CycleNode2> destinationNodes = planningInstance.getDestinationsInGraph();
			double averageSpeedKMpH = planningInstance.getAverageSpeedKmPH();
			Map<CycleNode2, Collection<EdgeId>> contractedNodes = routingService
					.getContractedNodesForCity(planningInstance.getCityName());

			synchronized (this) {

				for (Entry<Profiles, CriteriaWeightVector> criteriaWeightVector : planningInstance
						.getCriteriaWeightVectors().entrySet()) {

					List<Future<Map<Long, GraphPath<TimedNode, TimedEdge>>>> futuresForProfile = new ArrayList<Future<Map<Long, GraphPath<TimedNode, TimedEdge>>>>();

					if (waypointNodes.isEmpty()) {
						// TODO tady musim vytvorit planning graph s firstmile
						// lastmile
						Graph<CycleNode2, CycleEdge2> firstLastMileGraph = getFirstLastMileGraph(graph, originNodes,
								destinationNodes, contractedNodes);

						futuresForProfile.add(bicyclePlanner.plan(firstLastMileGraph, originNodes, destinationNodes,
								averageSpeedKMpH, criteriaWeightVector.getValue()));

					} else {
						Iterator<List<CycleNode2>> waypointIterator = waypointNodes.iterator();
						List<CycleNode2> waypoints = waypointIterator.next();
						Graph<CycleNode2, CycleEdge2> firstLastMileGraph = getFirstLastMileGraph(graph, originNodes,
								waypoints, contractedNodes);

						futuresForProfile.add(bicyclePlanner.plan(firstLastMileGraph, originNodes, waypoints, averageSpeedKMpH,
								criteriaWeightVector.getValue()));

						while (waypointIterator.hasNext()) {
							List<CycleNode2> nextWaypoints = waypointIterator.next();
							firstLastMileGraph = getFirstLastMileGraph(graph, waypoints, nextWaypoints, contractedNodes);

							futuresForProfile.add(bicyclePlanner.plan(firstLastMileGraph, waypoints, nextWaypoints,
									averageSpeedKMpH, criteriaWeightVector.getValue()));
							waypoints = nextWaypoints;
						}

						firstLastMileGraph = getFirstLastMileGraph(graph, waypoints, destinationNodes, contractedNodes);

						futuresForProfile.add(bicyclePlanner.plan(firstLastMileGraph, waypoints, destinationNodes,
								averageSpeedKMpH, criteriaWeightVector.getValue()));
					}

					// we want process task in reverse order. If query contains
					// waypoint we want to first process path from waypoint to
					// destination and then path from origin to destination
					Collections.reverse(futuresForProfile);
					futuresForAllProfiles.put(criteriaWeightVector.getKey(), futuresForProfile);
				}
			}
			
			// While waiting for the threads to finish we fill map of checkpoint nodes(origins, destinations and waypoints)
			Map<Long, CycleNode2> additionalNodes = new HashMap<Long, CycleNode2>();
			
			for (CycleNode2 node : originNodes) {
				additionalNodes.put(node.getId(), node);
			}
			for (List<CycleNode2> listOfWaypoints : waypointNodes) {
				for (CycleNode2 node : listOfWaypoints) {
					additionalNodes.put(node.getId(), node);
				}
			}
			for (CycleNode2 node : destinationNodes) {
				additionalNodes.put(node.getId(), node);
			}
			

			for (Entry<Profiles, List<Future<Map<Long, GraphPath<TimedNode, TimedEdge>>>>> entry : futuresForAllProfiles
					.entrySet()) {

				List<Future<Map<Long, GraphPath<TimedNode, TimedEdge>>>> futuresForProfile = entry.getValue();
				Collection<FinalPath> finalPaths = new ArrayList<FinalPath>();
				boolean isFirstFutureProcessed = false;

				for (Future<Map<Long, GraphPath<TimedNode, TimedEdge>>> future : futuresForProfile) {

					Map<Long, GraphPath<TimedNode, TimedEdge>> graphPaths = future.get();
					boolean foundSomeRelevantGraphPath = false;

					if (graphPaths == null || graphPaths.isEmpty()) {
						throw new PlanNotFoundException();
					}

					for (Entry<Long, GraphPath<TimedNode, TimedEdge>> graphPath : graphPaths.entrySet()) {
						try {
							List<CycleEdge2> cycleEdgePath = getCycleEdgePath(graphPath.getValue());
							CycleNode2 origin = additionalNodes.get(cycleEdgePath.get(0).getFromNodeId());

							if (!isFirstFutureProcessed) {
								FinalPath finalPath = new FinalPath(origin.getId(), new HashSet<Long>(), cycleEdgePath,
										graphPath.getValue().getWeight());

								finalPaths.add(finalPath);
								foundSomeRelevantGraphPath = true;

							} else {
								for (FinalPath finalPath : finalPaths) {
									if (finalPath.getOriginId() == graphPath.getKey()) {
										finalPath.concat(origin.getId(), cycleEdgePath, graphPath.getValue()
												.getWeight());
										foundSomeRelevantGraphPath = true;
									}
								}
							}

						} catch (PlanNotFoundException e) {
							log.error(graphPath, e);
						}
					}

					if (!foundSomeRelevantGraphPath) {
						throw new PlanNotFoundException();
					}

					isFirstFutureProcessed = true;
				}

				log.info("# of returned paths: " + finalPaths.size());

				FinalPath optimalFinalPath = getOptimalFinalPath(finalPaths);
				
				log.info("Profile " + entry.getKey().toString() + " optimal path cost is "
						+ optimalFinalPath.getPathCost());

				long planBuilderTime = System.currentTimeMillis();
				PlanBuilder planBuilder = new PlanBuilder(entry.getKey().getValue(), entry.getKey().name()
						.toLowerCase(), graph, additionalNodes, optimalFinalPath.getOriginId(), optimalFinalPath.getWaypointIds(),
						optimalFinalPath.getPath(), averageSpeedKMpH);

				plans.add(planBuilder.build());
				log.info("PlanBuilder running time is " + (System.currentTimeMillis() - planBuilderTime) + "ms");
			}

		} catch (InterruptedException | ExecutionException e) {
			log.error(planningInstance, e);
			throw new PlannerException();
		}

		return plans;
	}

	private Graph<CycleNode2, CycleEdge2> getFirstLastMileGraph(Graph<CycleNode2, CycleEdge2> graph,
			Collection<CycleNode2> firstMileNodes, Collection<CycleNode2> lastMileNodes,
			Map<CycleNode2, Collection<EdgeId>> contractedNodes) {
		long timeToGetPlanningGraph = System.currentTimeMillis();
		Graph<CycleNode2, CycleEdge2> firstLastMileGraph = new FirstLastMileGraph(graph, firstMileNodes, lastMileNodes,
				contractedNodes);

		timeToGetPlanningGraph = System.currentTimeMillis() - timeToGetPlanningGraph;
		log.info("Time to obtain planning graph: " + timeToGetPlanningGraph);

		return firstLastMileGraph;
	}
	
	/**
	 * 
	 * Retrieve path from data structure <code>GraphPath</code> and cast path
	 * segments to type <code>CycleEdge2</code>
	 * 
	 * @param graph
	 * @param graphPath
	 * @return list of graph edges of type <code>CycleEdge2</code>
	 * @throws PlanNotFoundException
	 *             <code>GraphPath</code> is null or contains number of segments
	 *             less than 2
	 */
	private List<CycleEdge2> getCycleEdgePath(GraphPath<TimedNode, TimedEdge> graphPath) throws PlanNotFoundException {
		if (graphPath == null || graphPath.getEdgeList().size() < 2) {
			throw new PlanNotFoundException();
		}

		List<CycleEdge2> edges = new ArrayList<CycleEdge2>();

		for (TimedEdge timedEdge : graphPath.getEdgeList()) {

			edges.add((CycleEdge2) timedEdge.getEdge());
		}

		return edges;
	}

	/**
	 * 
	 * Finds optimal path among collection of paths. The optimal one has the
	 * smallest cost value
	 * 
	 * @param finalPaths
	 *            collection of paths
	 * @return optimal path among collection of paths
	 */
	private FinalPath getOptimalFinalPath(Collection<FinalPath> finalPaths) {
		FinalPath optimalFinalPath = new FinalPath(-1, null, null, Double.MAX_VALUE);

		for (FinalPath path : finalPaths) {
			if (optimalFinalPath.getPathCost() > path.getPathCost()) {
				optimalFinalPath = path;
			}
		}
		return optimalFinalPath;
	}

	/**
	 * Returns current average size of closed set of specified planner
	 * 
	 * @return average closed set size
	 */
	public double getAverageClosedSetSize() {
		return bicyclePlanner.getAverageClosedSetSize();
	}

}
