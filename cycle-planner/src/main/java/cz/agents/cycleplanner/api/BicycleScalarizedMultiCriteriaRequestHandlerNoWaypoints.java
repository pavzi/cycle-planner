package cz.agents.cycleplanner.api;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.jgrapht.GraphPath;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.api.datamodel.v2.Plan;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.exceptions.PlanNotFoundException;
import cz.agents.cycleplanner.exceptions.PlannerException;
import cz.agents.cycleplanner.planner.BicyclePlanner;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedEdge;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class BicycleScalarizedMultiCriteriaRequestHandlerNoWaypoints implements Closeable,
		RequestHandler<PlanningInstance, Collection<Plan>> {

	private static Logger log = Logger.getLogger(BicycleScalarizedMultiCriteriaRequestHandlerNoWaypoints.class);

	private BicyclePlanner<GraphPath<TimedNode, TimedEdge>> bicyclePlanner;

	public BicycleScalarizedMultiCriteriaRequestHandlerNoWaypoints(
			BicyclePlanner<GraphPath<TimedNode, TimedEdge>> bicyclePlanner) {
		this.bicyclePlanner = bicyclePlanner;
	}

	@Override
	public void close() {
		bicyclePlanner.close();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws PlanNotFoundException
	 * @throws OutOfBoundsException
	 * @throws PlannerException
	 */
	@Override
	public Collection<Plan> handle(PlanningInstance planningInstance) throws OutOfBoundsException,
			PlanNotFoundException, PlannerException {
		Collection<Plan> plans = new ArrayList<Plan>();

		try {
			Map<Profiles, Future<GraphPath<TimedNode, TimedEdge>>> futuresForAllProfiles = new HashMap<Profiles, Future<GraphPath<TimedNode, TimedEdge>>>();
			Graph<CycleNode2, CycleEdge2> graph = planningInstance.getCityData().getGraph();
			List<CycleNode2> originNodes = planningInstance.getOriginsInGraph();
			List<CycleNode2> destinationNodes = planningInstance.getDestinationsInGraph();
			double averageSpeedKMpH = planningInstance.getAverageSpeedKmPH();

			synchronized (this) {

				for (Entry<Profiles, CriteriaWeightVector> criteriaWeightVector : planningInstance
						.getCriteriaWeightVectors().entrySet()) {

					futuresForAllProfiles.put(criteriaWeightVector.getKey(), bicyclePlanner.plan(graph, originNodes, destinationNodes,
							averageSpeedKMpH, criteriaWeightVector.getValue()));
				}
			}

			for (Entry<Profiles, Future<GraphPath<TimedNode, TimedEdge>>> entry : futuresForAllProfiles
					.entrySet()) {

				Future<GraphPath<TimedNode, TimedEdge>> future = entry.getValue();
				List<CycleEdge2> cycleEdgePath = getCycleEdgePath(graph, future.get());
								
				long planBuilderTime = System.currentTimeMillis();
				PlanBuilder planBuilder = new PlanBuilder(entry.getKey().getValue(), entry.getKey().name()
						.toLowerCase(), graph, null, cycleEdgePath.get(0).getFromNodeId(), new HashSet<Long>(),
						cycleEdgePath, averageSpeedKMpH);

				plans.add(planBuilder.build());
				log.info("PlanBuilder running time is " + (System.currentTimeMillis() - planBuilderTime) + "ms");
			}

		} catch (InterruptedException | ExecutionException e) {
			log.error(planningInstance, e);
			throw new PlannerException();
		}

		return plans;
	}

	/**
	 * 
	 * Retrieve path from data structure <code>GraphPath</code> and cast path
	 * segments to type <code>CycleEdge2</code>
	 * 
	 * @param graph
	 * @param graphPath
	 * @return list of graph edges of type <code>CycleEdge2</code>
	 * @throws PlanNotFoundException
	 *             <code>GraphPath</code> is null or contains number of segments
	 *             less than 2
	 */
	private List<CycleEdge2> getCycleEdgePath(Graph<? extends Node, ? extends Edge> graph,
			GraphPath<TimedNode, TimedEdge> graphPath) throws PlanNotFoundException {
		if (graphPath == null || graphPath.getEdgeList().size() < 2) {
			throw new PlanNotFoundException();
		}

		List<CycleEdge2> edges = new ArrayList<CycleEdge2>();

		for (TimedEdge timedEdge : graphPath.getEdgeList()) {
			edges.add((CycleEdge2) graph.getEdgeByEdgeId(timedEdge.getEdge().getEdgeId()));
		}

		return edges;
	}

	/**
	 * Returns current average size of closed set of specified planner
	 * 
	 * @return average closed set size
	 */
	public double getAverageClosedSetSize() {
		return bicyclePlanner.getAverageClosedSetSize();
	}

}
