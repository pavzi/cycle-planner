package cz.agents.cycleplanner.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.agents.cycleplanner.api.datamodel.Coordinate;
import cz.agents.cycleplanner.api.datamodel.mlcresponse.EdgeUsage;
import cz.agents.cycleplanner.api.datamodel.mlcresponse.MLCResponse;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.multicriteria.ParetoSet;
import eu.superhub.wp5.graphcommon.graph.EdgeId;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

public class MCResponseBuilder {

	public static MLCResponse buildMCResponse(Graph<CycleNode2, CycleEdge2> graph, CycleNode2 origin, CycleNode2 destination,
			ParetoSet<CycleNode2, CycleEdge2> paretoSet) {

		Map<EdgeId, CycleEdge2> edges = new HashMap<EdgeId, CycleEdge2>();
		Map<EdgeId, Double> widths = new HashMap<EdgeId, Double>();
		Map<EdgeId, String> colours = new HashMap<EdgeId, String>();

		for (Collection<CycleEdge2> journey : paretoSet.getAllSequenceOfEdges()) {

			for (CycleEdge2 edge : journey) {

				EdgeId edgeId = edge.getEdgeId();

				edges.put(edgeId, edge);

				if (widths.containsKey(edgeId)) {
					widths.put(edgeId, widths.get(edgeId) + 1);
				} else {
					widths.put(edgeId, 1d);
				}
			}
		}

		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;

		for (EdgeId id : widths.keySet()) {
			double value = widths.get(id);
			max = Math.max(value, max);
			min = Math.min(value, min);
		}

		for (EdgeId id : widths.keySet()) {
			// spocitam tak hrubku aby pre najmenej prekryvanu hranu bola
			// hrubka nula a pre najviac zase 12 (najvacsia zvolena hrubka
			// ciary)
			double width = widths.get(id);
			widths.put(id, ((width - min) / (max - min)) * 12);

			// spocitam odtien cervenej na stupncic od 150-255, teda
			// najmenej prekryvana bude mat najtmavsiu farbu a najviac
			// prekryvana najbledsiu

			int colour = (int) (150 + ((width - min) / (max - min)) * 105);
			colours.put(id, "#" + Integer.toHexString(colour) + "0000");
			// log.debug("Colour: #" + Integer.toHexString(colour) +
			// "0000");
		}

		List<EdgeUsage> edgesUsage = new ArrayList<EdgeUsage>();

		for (EdgeId id : widths.keySet()) {
			Node from = graph.getNodeByNodeId(id.getFromNodeId());
			Node to = graph.getNodeByNodeId(id.getToNodeId());

			EdgeUsage edgeUsage = new EdgeUsage(new Coordinate(from.getLatitudeE6(), from.getLongitudeE6()),
					new Coordinate(to.getLatitudeE6(), to.getLongitudeE6()), widths.get(id), colours.get(id));

			edgesUsage.add(edgeUsage);
		}

		MLCResponse mlcResponse = new MLCResponse(new Coordinate(origin.getLatitudeE6(), origin.getLongitudeE6()),
				new Coordinate(destination.getLatitudeE6(), destination.getLongitudeE6()), edgesUsage);

		return mlcResponse;
	}
}
