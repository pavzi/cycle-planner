package cz.agents.cycleplanner.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import cz.agents.cycleplanner.api.datamodel.v2.BoundingBox;
import cz.agents.cycleplanner.api.datamodel.v2.Coordinate;
import cz.agents.cycleplanner.api.datamodel.v2.Plan;
import cz.agents.cycleplanner.api.datamodel.v2.PlanStep;
import cz.agents.cycleplanner.api.datamodel.v2.SpecialCoordinateType;
import cz.agents.cycleplanner.core.criteria2.CriteriaBuilder;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.core.datastructures.RoadType;
import cz.agents.cycleplanner.core.datastructures.Surface;
import cz.agents.cycleplanner.core.util.AngleUtil;
import cz.agents.cycleplanner.util.EnergyConsumption;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.wp5common.location.Location;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class PlanBuilder {

	public static double AVERAGE_GRAPH_CRUISING_SPEED_METERS_PER_SECOND = 4.2;

	private final long id;
	private final String description;
	private int minLatE6;
	private int maxLatE6;
	private int minLonE6;
	private int maxLonE6;
	private double length;
	private double duration;
	private double elevationGain;
	private double elevationDrop;
	private double consumedEnergy;
	private List<PlanStep> steps;

	private final Graph<CycleNode2, CycleEdge2> graph;
	private final Map<Long, CycleNode2> additionalNodes;
	private final long originNodeId;
	private final Collection<Long> waypointNodeIds;
	private final List<CycleEdge2> path;
	private final double averageSpeedMetersPerSecond;

	public PlanBuilder(long id, String description, Graph<CycleNode2, CycleEdge2> graph, Map<Long, CycleNode2> additionalNodes, long originNodeId,
			Collection<Long> waypointNodeIds, List<CycleEdge2> path, double averageSpeedKmPH) {

		this.id = id;
		this.description = description;
		this.graph = graph;
		this.additionalNodes = additionalNodes;
		this.originNodeId = originNodeId;
		this.waypointNodeIds = waypointNodeIds;
		this.path = path;
		this.averageSpeedMetersPerSecond = averageSpeedKmPH / 3.6;
	}

	// This method is only called when planner return some plan
	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Plan build() {

		init();

		Location previous = null;

		for (CycleEdge2 cycleEdge : path) {

			Location fromNode = getNode(cycleEdge.getFromNodeId());

			for (Location via : cycleEdge.getVia()) {

				step(previous, fromNode, via, cycleEdge);

				previous = fromNode;
				fromNode = via;
			}

			Location toNode = getNode(cycleEdge.getToNodeId());

			step(previous, fromNode, toNode, cycleEdge);
			consumedEnergy += EnergyConsumption.compute(cycleEdge, averageSpeedMetersPerSecond);

			previous = fromNode;
		}

		CycleEdge2 lastEdge = path.get(path.size() - 1);
		CycleNode2 destinationNode = getNode(lastEdge.getToNodeId());
		Coordinate destinationCoordinate = new Coordinate(destinationNode.getLatitudeE6(),
				destinationNode.getLongitudeE6(), round(destinationNode.getElevation()),
				SpecialCoordinateType.DESTINATION);
		PlanStep lastStep = new PlanStep(destinationCoordinate, 0, 0, 0, null, null, null, null);

		updateMinMaxLatLon(destinationNode.getLatitudeE6(), destinationNode.getLongitudeE6());
		steps.add(lastStep);

		BoundingBox boundingBox = new BoundingBox(new Coordinate(maxLatE6, minLonE6),
				new Coordinate(minLatE6, maxLonE6));
		Plan plan = new Plan(id, description, boundingBox, round(length), round(duration), round(elevationGain),
				round(elevationDrop), round(consumedEnergy), steps);

		return plan;
	}

	private void init() {
		minLatE6 = Integer.MAX_VALUE;
		maxLatE6 = Integer.MIN_VALUE;
		minLonE6 = Integer.MAX_VALUE;
		maxLonE6 = Integer.MIN_VALUE;
		length = 0;
		duration = 0;
		elevationGain = 0;
		elevationDrop = 0;
		consumedEnergy = 0;
		steps = new ArrayList<PlanStep>();
	}
	
	private void step(Location previous, Location from, Location to, CycleEdge2 cycleEdge) {
		Coordinate coordinate = null;

		// TODO Check efficiency, for each node look to two lists
		if (originNodeId == from.getId()) {
			coordinate = new Coordinate(from.getLatitudeE6(), from.getLongitudeE6(), round(from.getElevation()),
					SpecialCoordinateType.ORIGIN);
		} else if (waypointNodeIds.contains(from.getId())) {
			coordinate = new Coordinate(from.getLatitudeE6(), from.getLongitudeE6(), round(from.getElevation()),
					SpecialCoordinateType.WAYPOINT);
		} else {
			coordinate = new Coordinate(from.getLatitudeE6(), from.getLongitudeE6(), round(from.getElevation()));
		}

		CriteriaBuilder criteriaBuilder = new CriteriaBuilder(from, to);
		double lengthInMeters = criteriaBuilder.getLengthInMeters();
		double travelTime = criteriaBuilder.getTravelTime(cycleEdge.getTravelTimeBaseValue(),
				cycleEdge.getTraveTimeConstant(), this.averageSpeedMetersPerSecond);

		// Compute angle between previous and current edge
		int angle = 0;

		if (previous != null) {
			angle = round(AngleUtil.getAngle(previous, from, to));
		}

		Surface surface = cycleEdge.getSurface();
		RoadType roadType = cycleEdge.getRoadType();
		String streetName = cycleEdge.getStreetName();
		String bicycleRouteNumber = cycleEdge.getBicycleRouteNumber();
		PlanStep planStep = new PlanStep(coordinate, round(lengthInMeters), round(travelTime),
				angle, surface, roadType, streetName, bicycleRouteNumber);

		updateMinMaxLatLon(from.getLatitudeE6(), from.getLongitudeE6());
		length += lengthInMeters;
		duration += travelTime;
		elevationGain += criteriaBuilder.getRises();
		elevationDrop += criteriaBuilder.getDrops();
		steps.add(planStep);
	}
	
	private CycleNode2 getNode(long nodeId) {
		if (graph.containsNodeByNodeId(nodeId)) {
			return graph.getNodeByNodeId(nodeId);
		} 
		
		return additionalNodes.get(nodeId);
	}

	private int round(double d) {
		return (int) Math.round(d);
	}

	private void updateMinMaxLatLon(int latE6, int lonE6) {
		minLatE6 = (minLatE6 > latE6) ? latE6 : minLatE6;
		maxLatE6 = (maxLatE6 < latE6) ? latE6 : maxLatE6;
		minLonE6 = (minLonE6 > lonE6) ? lonE6 : minLonE6;
		maxLonE6 = (maxLonE6 < lonE6) ? lonE6 : maxLonE6;
	}

}
