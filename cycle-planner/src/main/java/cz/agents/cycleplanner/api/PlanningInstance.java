package cz.agents.cycleplanner.api;

import java.util.List;
import java.util.Map;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;

/**
 * TODO rename
 * TODO javadoc
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
@Deprecated
public class PlanningInstance {
	
	/**
	 * 
	 */
	private final CityData<CycleNode2, CycleEdge2> cityData;
	
	/**
	 * 
	 */
	private final List<CycleNode2> originsInGraph;
	
	/**
	 * 
	 */
	private final List<List<CycleNode2>> waypointsInGraph;
	
	/**
	 * 
	 */
	private final List<CycleNode2> destinationsInGraph;
	
	/**
	 * 
	 */
	private final Map<Profiles, CriteriaWeightVector> criteriaWeightVectors;
	
	/**
	 * Average cruising speed in kilometers per hour
	 */
	private final double averageSpeedKmPH;

	public PlanningInstance(CityData<CycleNode2, CycleEdge2> cityData, List<CycleNode2> originsInGraph,
			List<List<CycleNode2>> waypointsInGraph, List<CycleNode2> destinationsInGraph,
			Map<Profiles, CriteriaWeightVector> criteriaWeightVectors, double averageSpeedKmPH) {
		super();
		this.cityData = cityData;
		this.originsInGraph = originsInGraph;
		this.waypointsInGraph = waypointsInGraph;
		this.destinationsInGraph = destinationsInGraph;
		this.criteriaWeightVectors = criteriaWeightVectors;
		this.averageSpeedKmPH = averageSpeedKmPH;
	}

	public CityData<CycleNode2, CycleEdge2> getCityData() {
		return cityData;
	}

	public List<CycleNode2> getOriginsInGraph() {
		return originsInGraph;
	}

	public List<List<CycleNode2>> getWaypointsInGraph() {
		return waypointsInGraph;
	}

	public List<CycleNode2> getDestinationsInGraph() {
		return destinationsInGraph;
	}

	public Map<Profiles, CriteriaWeightVector> getCriteriaWeightVectors() {
		return criteriaWeightVectors;
	}

	public double getAverageSpeedKmPH() {
		return averageSpeedKmPH;
	}
	
}
