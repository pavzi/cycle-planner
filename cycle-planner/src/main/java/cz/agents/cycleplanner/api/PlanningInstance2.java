package cz.agents.cycleplanner.api;

import java.util.List;
import java.util.Map;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.Graph;

/**
 * TODO rename
 * TODO javadoc
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class PlanningInstance2 {
	
	/**
	 * 
	 */
	private final String cityName;
	
	/**
	 * 
	 */
	private final Graph<CycleNode2, CycleEdge2> graph;
	
	/**
	 * 
	 */
	private final List<CycleNode2> originsInGraph;
	
	/**
	 * 
	 */
	private final List<List<CycleNode2>> waypointsInGraph;
	
	/**
	 * 
	 */
	private final List<CycleNode2> destinationsInGraph;
	
	/**
	 * 
	 */
	private final Map<Profiles, CriteriaWeightVector> criteriaWeightVectors;
	
	/**
	 * Average cruising speed in kilometers per hour
	 */
	private final double averageSpeedKmPH;

	public PlanningInstance2(String cityName, Graph<CycleNode2, CycleEdge2> graph, List<CycleNode2> originsInGraph,
			List<List<CycleNode2>> waypointsInGraph, List<CycleNode2> destinationsInGraph,
			Map<Profiles, CriteriaWeightVector> criteriaWeightVectors, double averageSpeedKmPH) {
		super();
		this.cityName = cityName;
		this.graph = graph;
		this.originsInGraph = originsInGraph;
		this.waypointsInGraph = waypointsInGraph;
		this.destinationsInGraph = destinationsInGraph;
		this.criteriaWeightVectors = criteriaWeightVectors;
		this.averageSpeedKmPH = averageSpeedKmPH;
	}

	public String getCityName() {
		return cityName;
	}

	public Graph<CycleNode2, CycleEdge2> getGraph() {
		return graph;
	}

	public List<CycleNode2> getOriginsInGraph() {
		return originsInGraph;
	}

	public List<List<CycleNode2>> getWaypointsInGraph() {
		return waypointsInGraph;
	}

	public List<CycleNode2> getDestinationsInGraph() {
		return destinationsInGraph;
	}

	public Map<Profiles, CriteriaWeightVector> getCriteriaWeightVectors() {
		return criteriaWeightVectors;
	}

	public double getAverageSpeedKmPH() {
		return averageSpeedKmPH;
	}
	
}
