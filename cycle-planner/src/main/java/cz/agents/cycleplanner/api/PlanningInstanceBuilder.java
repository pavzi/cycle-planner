package cz.agents.cycleplanner.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.operation.TransformException;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.api.datamodel.v2.Coordinate;
import cz.agents.cycleplanner.api.datamodel.v2.Request;
import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.exceptions.PlannerException;
import cz.agents.cycleplanner.routingService.RoutingService3;

/**
 * TODO rename TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class PlanningInstanceBuilder {

	private static final int MAX_NUMBER_OF_WAYPOINTS = 3;
	
	private static final Logger log = Logger.getLogger(PlanningInstanceBuilder.class);

	private RoutingService3 routingService;

	public PlanningInstanceBuilder() {
		routingService = RoutingService3.INSTANCE;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param request
	 * @return
	 * @throws OutOfBoundsException
	 * @throws PlannerException
	 */
	@Deprecated
	public PlanningInstance build(Request request) throws OutOfBoundsException, PlannerException {

		// log.info("Request: " + request.toString());

		// Get CityCycleData for given origin and destination
		CityData<CycleNode2, CycleEdge2> cityData = routingService.getCityCycleData(request.getOrigin().getLatE6(),
				request.getOrigin().getLonE6(), request.getDestination().getLatE6(), request.getDestination()
						.getLonE6());

		// Obtain closest pair of origin and destination in graph to the given
		// pair
		List<CycleNode2> originsInGraph = null;
		List<List<CycleNode2>> waypointsInGraph = new ArrayList<List<CycleNode2>>();
		List<CycleNode2> destinationsInGraph = null;

		try {
			log.info("Looking for nearest node to origin...");
			originsInGraph = routingService.getNearestNodes(request.getOrigin().getLatE6(), request.getOrigin()
					.getLonE6(), cityData.getCityName());
			
			if (request.getWaypoints() != null) {
				for (Coordinate waypoint : request.getWaypoints()) {
					if (waypointsInGraph.size() < MAX_NUMBER_OF_WAYPOINTS) {
						log.info("Looking for nearest node to waypoint...");
						List<CycleNode2> nearestWaypoints = routingService.getNearestNodes(waypoint.getLatE6(),
								waypoint.getLonE6(), cityData.getCityName());

						waypointsInGraph.add(nearestWaypoints);
					} else {
						break;
					}
				}
			}
			
			log.info("Looking for nearest node to destination...");
			destinationsInGraph = routingService.getNearestNodes(request.getDestination().getLatE6(), request
					.getDestination().getLonE6(), cityData.getCityName());

		} catch (MismatchedDimensionException | TransformException e) {
			log.error(request, e);
			throw new PlannerException();
		}
		log.info("Origins: " + originsInGraph.toString());
		for (List<CycleNode2> waypoints : waypointsInGraph) {
			log.info("Waypoints: " + waypoints.toString());
		}		
		log.info("Destinations: " + destinationsInGraph.toString());

		Map<Profiles, CriteriaWeightVector> criteriaWeightVectors = new HashMap<Profiles, CriteriaWeightVector>();

		if (request.getProfile() == null) {
			criteriaWeightVectors.put(Profiles.COMMUTING, CriteriaWeightVector.COMMUTING);
			criteriaWeightVectors.put(Profiles.BIKE_FRIENDLY, CriteriaWeightVector.BIKE_FRIENDLY);
			criteriaWeightVectors.put(Profiles.FLAT, CriteriaWeightVector.FLAT);
			criteriaWeightVectors.put(Profiles.FAST, CriteriaWeightVector.FAST);
		} else {
			criteriaWeightVectors.put(Profiles.USER_SPECIFIED, new CriteriaWeightVector(request.getProfile()
					.getTravelTimeWeight(), request.getProfile().getComfortWeight(), request.getProfile()
					.getQuietnessWeight(), request.getProfile().getFlatnessWeight()));
		}

		return new PlanningInstance(cityData, originsInGraph, waypointsInGraph, destinationsInGraph, criteriaWeightVectors,
				request.getAverageSpeedKmPH());
	}
	
	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param request
	 * @return
	 * @throws OutOfBoundsException
	 * @throws PlannerException
	 */
	public PlanningInstance2 build2(Request request) throws OutOfBoundsException, PlannerException {

		// log.info("Request: " + request.toString());

		// Get CityCycleData for given origin and destination
		CityData<CycleNode2, CycleEdge2> cityData = routingService.getCityCycleData(request.getOrigin().getLatE6(),
				request.getOrigin().getLonE6(), request.getDestination().getLatE6(), request.getDestination()
						.getLonE6());

		// Obtain closest pair of origin and destination in graph to the given
		// pair
		List<CycleNode2> originsInGraph = null;
		List<List<CycleNode2>> waypointsInGraph = new ArrayList<List<CycleNode2>>();
		List<CycleNode2> destinationsInGraph = null;

		try {
			log.info("Looking for nearest node to origin...");
			originsInGraph = routingService.getNearestNodes(request.getOrigin().getLatE6(), request.getOrigin()
					.getLonE6(), cityData.getCityName());

			if (request.getWaypoints() != null) {
				for (Coordinate waypoint : request.getWaypoints()) {
					if (waypointsInGraph.size() < MAX_NUMBER_OF_WAYPOINTS) {
						log.info("Looking for nearest node to waypoint...");
						List<CycleNode2> nearestWaypoints = routingService.getNearestNodes(waypoint.getLatE6(),
								waypoint.getLonE6(), cityData.getCityName());

						waypointsInGraph.add(nearestWaypoints);
					} else {
						break;
					}
				}
			}

			log.info("Looking for nearest node to destination...");
			destinationsInGraph = routingService.getNearestNodes(request.getDestination().getLatE6(), request
					.getDestination().getLonE6(), cityData.getCityName());

		} catch (MismatchedDimensionException | TransformException e) {
			log.error(request, e);
			throw new PlannerException();
		}
		log.info("Origins: " + originsInGraph.toString());
		for (List<CycleNode2> waypoints : waypointsInGraph) {
			log.info("Waypoints: " + waypoints.toString());
		}
		log.info("Destinations: " + destinationsInGraph.toString());

		Map<Profiles, CriteriaWeightVector> criteriaWeightVectors = new HashMap<Profiles, CriteriaWeightVector>();

		if (request.getProfile() == null) {
			criteriaWeightVectors.put(Profiles.COMMUTING, CriteriaWeightVector.COMMUTING);
			criteriaWeightVectors.put(Profiles.BIKE_FRIENDLY, CriteriaWeightVector.BIKE_FRIENDLY);
			criteriaWeightVectors.put(Profiles.FLAT, CriteriaWeightVector.FLAT);
			criteriaWeightVectors.put(Profiles.FAST, CriteriaWeightVector.FAST);
		} else {
			criteriaWeightVectors.put(Profiles.USER_SPECIFIED, new CriteriaWeightVector(request.getProfile()
					.getTravelTimeWeight(), request.getProfile().getComfortWeight(), request.getProfile()
					.getQuietnessWeight(), request.getProfile().getFlatnessWeight()));
		}

		return new PlanningInstance2(cityData.getCityName(), cityData.getGraph(), originsInGraph, waypointsInGraph,
				destinationsInGraph, criteriaWeightVectors, request.getAverageSpeedKmPH());
	}
	
	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param request
	 * @return
	 * @throws OutOfBoundsException
	 * @throws PlannerException
	 */
	public PlanningInstance2 build2(CityData<CycleNode2, CycleEdge2> cityData, Coordinate origin,
			Coordinate destination, double averageSpeedKmPH) throws OutOfBoundsException, PlannerException {

		// Obtain closest pair of origin and destination in graph to the given
		// pair
		List<CycleNode2> originsInGraph = null;
		List<List<CycleNode2>> waypointsInGraph = new ArrayList<List<CycleNode2>>();
		List<CycleNode2> destinationsInGraph = null;

		try {
			log.info("Looking for nearest node to origin...");
			originsInGraph = routingService.getNearestNodes(origin.getLatE6(), origin.getLonE6(),
					cityData.getCityName());

			log.info("Looking for nearest node to destination...");
			destinationsInGraph = routingService.getNearestNodes(destination.getLatE6(), destination.getLonE6(),
					cityData.getCityName());

		} catch (MismatchedDimensionException | TransformException e) {
			log.error(e);
			throw new PlannerException();
		}

		log.info("Origins: " + originsInGraph.toString());
		log.info("Destinations: " + destinationsInGraph.toString());

		Map<Profiles, CriteriaWeightVector> criteriaWeightVectors = new HashMap<Profiles, CriteriaWeightVector>();

		criteriaWeightVectors.put(Profiles.COMMUTING, CriteriaWeightVector.COMMUTING);
		criteriaWeightVectors.put(Profiles.BIKE_FRIENDLY, CriteriaWeightVector.BIKE_FRIENDLY);
		criteriaWeightVectors.put(Profiles.FLAT, CriteriaWeightVector.FLAT);
		criteriaWeightVectors.put(Profiles.FAST, CriteriaWeightVector.FAST);

		return new PlanningInstance2(cityData.getCityName(), cityData.getGraph(), originsInGraph, waypointsInGraph,
				destinationsInGraph, criteriaWeightVectors, averageSpeedKmPH);
	}
}
