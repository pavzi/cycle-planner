package cz.agents.cycleplanner.api;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum Profiles {
	USER_SPECIFIED(0), COMMUTING(1), BIKE_FRIENDLY(2), FLAT(3), FAST(4);

	private int value;

	private Profiles(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
