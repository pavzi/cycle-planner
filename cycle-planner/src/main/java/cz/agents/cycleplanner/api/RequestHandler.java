package cz.agents.cycleplanner.api;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 * @param <TRequest>
 * @param <TResponse>
 */
public interface RequestHandler<TRequest, TResponse> {

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param request
	 * @return
	 */
	public TResponse handle(TRequest request) throws Exception;
}
