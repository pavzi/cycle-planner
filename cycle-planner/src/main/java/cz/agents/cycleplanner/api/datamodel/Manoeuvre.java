package cz.agents.cycleplanner.api.datamodel;

public enum Manoeuvre {
	KEEP_RIGHT, KEEP_LEFT, TURN_RIGHT, TURN_LEFT, CONTINUE, U_TURN, NONE;
}
