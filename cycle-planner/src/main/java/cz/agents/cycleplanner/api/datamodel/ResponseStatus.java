package cz.agents.cycleplanner.api.datamodel;

public enum ResponseStatus {
	OK, OUT_OF_BOUNDS, PLAN_NOT_FOUND, PLANNER_EXCEPTION;
}
