package cz.agents.cycleplanner.api.datamodel;

public enum RoadType {
	CYCLEWAY, FOOTWAY, STEPS, PRIMARY, SECONDARY, TERTIARY, ROAD;
}
