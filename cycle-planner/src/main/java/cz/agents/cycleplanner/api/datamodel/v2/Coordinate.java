package cz.agents.cycleplanner.api.datamodel.v2;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class Coordinate {
	@JsonProperty(required = true)
	protected final int latE6;

	@JsonProperty(required = true)
	protected final int lonE6;

	protected final Integer elevation;

	/**
	 * TODO javadoc
	 */
	protected final SpecialCoordinateType type;

	protected Coordinate() {
		this.latE6 = Integer.MAX_VALUE;
		this.lonE6 = Integer.MAX_VALUE;
		this.elevation = null;
		this.type = null;
	}
	
	public Coordinate(int latE6, int lonE6) {
		super();
		this.latE6 = latE6;
		this.lonE6 = lonE6;
		this.elevation = null;
		this.type = null;
	}

	public Coordinate(int latE6, int lonE6, int elevation) {
		super();
		this.latE6 = latE6;
		this.lonE6 = lonE6;
		this.elevation = elevation;
		this.type = null;
	}
	
	public Coordinate(int latE6, int lonE6, SpecialCoordinateType type) {
		super();
		this.latE6 = latE6;
		this.lonE6 = lonE6;
		this.elevation = null;
		this.type = type;
	}

	public Coordinate(int latE6, int lonE6, int elevation, SpecialCoordinateType type) {
		super();
		this.latE6 = latE6;
		this.lonE6 = lonE6;
		this.elevation = elevation;
		this.type = type;
	}

	public int getLatE6() {
		return latE6;
	}

	public int getLonE6() {
		return lonE6;
	}

	public Integer getElevation() {
		return elevation;
	}

	public SpecialCoordinateType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elevation == null) ? 0 : elevation.hashCode());
		result = prime * result + latE6;
		result = prime * result + lonE6;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (elevation == null) {
			if (other.elevation != null)
				return false;
		} else if (!elevation.equals(other.elevation))
			return false;
		if (latE6 != other.latE6)
			return false;
		if (lonE6 != other.lonE6)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Coordinate [latE6=" + latE6 + ", lonE6=" + lonE6 + ", elevation=" + elevation + ", type=" + type + "]";
	}
}
