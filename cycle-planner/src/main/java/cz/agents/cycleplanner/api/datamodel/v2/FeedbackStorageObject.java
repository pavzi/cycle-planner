package cz.agents.cycleplanner.api.datamodel.v2;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class FeedbackStorageObject {

	/**
	 * 
	 */
	@JsonProperty(required = true)
	private final long feedbackId;

	/**
	 * Contains timestamp when the feedback was created
	 */
	@JsonProperty(required = true)
	private final String creationTimestamp;

	/**
	 * 
	 */
	@JsonProperty(required = true)
	private final Feedback feedback;

	public FeedbackStorageObject() {
		this.feedbackId = Long.MAX_VALUE;
		this.creationTimestamp = null;
		this.feedback = null;
	}

	public FeedbackStorageObject(long feedbackId, String creationTimestamp, Feedback feedback) {
		super();
		this.feedbackId = feedbackId;
		this.creationTimestamp = creationTimestamp;
		this.feedback = feedback;
	}

	public long getFeedbackId() {
		return feedbackId;
	}

	public String getCreationTimestamp() {
		return creationTimestamp;
	}

	public Feedback getFeedback() {
		return feedback;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationTimestamp == null) ? 0 : creationTimestamp.hashCode());
		result = prime * result + ((feedback == null) ? 0 : feedback.hashCode());
		result = prime * result + (int) (feedbackId ^ (feedbackId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeedbackStorageObject other = (FeedbackStorageObject) obj;
		if (creationTimestamp == null) {
			if (other.creationTimestamp != null)
				return false;
		} else if (!creationTimestamp.equals(other.creationTimestamp))
			return false;
		if (feedback == null) {
			if (other.feedback != null)
				return false;
		} else if (!feedback.equals(other.feedback))
			return false;
		if (feedbackId != other.feedbackId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FeedbackStorageObject [feedbackId=" + feedbackId + ", creationTimestamp=" + creationTimestamp
				+ ", feedback=" + feedback + "]";
	}
}
