package cz.agents.cycleplanner.api.datamodel.v2;

import java.util.List;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class Plan {

	/**
	 * Represents plan id, each predefined profile has its own id
	 */
	private final long planId;

	/**
	 * Plan description: what profile was used,...
	 */
	private final String description;

	/**
	 * Represents geographical bounds, in which plan takes place
	 */
	private final BoundingBox boundingBox;

	/**
	 * Plan length in meters
	 */
	private final int length;

	/**
	 * Plan travel time in seconds
	 */
	private final int duration;

	/**
	 * TODO javadoc
	 */
	private final int elevationGain;

	/**
	 * TODO javadoc
	 */
	private final int elevationDrop;

	/**
	 * planning for e-bikes, maybe in KiloJoules or in Watts, represents how
	 * much energy you have spend to travel along plan
	 */
	private final int consumedEnergy;

	/**
	 * TODO javadoc
	 */
	private final List<PlanStep> steps;

	@SuppressWarnings("unused")
	private Plan() {
		this.planId = Long.MAX_VALUE;
		this.description = null;
		this.boundingBox = null;
		this.length = Integer.MAX_VALUE;
		this.duration = Integer.MAX_VALUE;
		this.elevationGain = Integer.MAX_VALUE;
		this.elevationDrop = Integer.MAX_VALUE;
		this.consumedEnergy = Integer.MAX_VALUE;
		this.steps = null;
	}

	public Plan(long planId, String description, BoundingBox boundingBox, int length, int duration, int elevationGain,
			int elevationDrop, int consumedEnergy, List<PlanStep> steps) {
		super();
		this.planId = planId;
		this.description = description;
		this.boundingBox = boundingBox;
		this.length = length;
		this.duration = duration;
		this.elevationGain = elevationGain;
		this.elevationDrop = elevationDrop;
		this.consumedEnergy = consumedEnergy;
		this.steps = steps;
	}

	public long getPlanId() {
		return planId;
	}

	public String getDescription() {
		return description;
	}

	public BoundingBox getBoundingBox() {
		return boundingBox;
	}

	public int getLength() {
		return length;
	}

	public int getDuration() {
		return duration;
	}

	public int getElevationGain() {
		return elevationGain;
	}

	public int getElevationDrop() {
		return elevationDrop;
	}

	public int getConsumedEnergy() {
		return consumedEnergy;
	}

	public List<PlanStep> getSteps() {
		return steps;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((boundingBox == null) ? 0 : boundingBox.hashCode());
		result = prime * result + consumedEnergy;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + duration;
		result = prime * result + elevationDrop;
		result = prime * result + elevationGain;
		result = prime * result + length;
		result = prime * result + (int) (planId ^ (planId >>> 32));
		result = prime * result + ((steps == null) ? 0 : steps.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plan other = (Plan) obj;
		if (boundingBox == null) {
			if (other.boundingBox != null)
				return false;
		} else if (!boundingBox.equals(other.boundingBox))
			return false;
		if (consumedEnergy != other.consumedEnergy)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (duration != other.duration)
			return false;
		if (elevationDrop != other.elevationDrop)
			return false;
		if (elevationGain != other.elevationGain)
			return false;
		if (length != other.length)
			return false;
		if (planId != other.planId)
			return false;
		if (steps == null) {
			if (other.steps != null)
				return false;
		} else if (!steps.equals(other.steps))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Plan [planId=" + planId + ", description=" + description + ", boundingBox=" + boundingBox + ", length="
				+ length + ", duration=" + duration + ", elevationGain=" + elevationGain + ", elevationDrop="
				+ elevationDrop + ", consumedEnergy=" + consumedEnergy + ", steps=" + steps + "]";
	}

}
