package cz.agents.cycleplanner.api.datamodel.v2;

import com.fasterxml.jackson.annotation.JsonProperty;

import cz.agents.cycleplanner.core.datastructures.RoadType;
import cz.agents.cycleplanner.core.datastructures.Surface;

/**
 * Represents step in the plan, node and its outgoing edge
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class PlanStep {

	/**
	 * 
	 */
	@JsonProperty(required = true)
	private final Coordinate coordinate;

	/**
	 * 
	 */
	@JsonProperty(required = true)
	private final int distanceToNextStep;
	
	/**
	 * 
	 */
	@JsonProperty(required = true)
	private final int travelTimeToNextStep;

	/**
	 * Angle between previous step and this one
	 * 
	 * Future work: maneuver in raw text, html....
	 */
	private final int angle;

	/**
	 * 
	 */
	private final Surface surface;

	/**
	 * 
	 */
	private final RoadType roadType;

	/**
	 * 
	 */
	private final String streetName;

	/**
	 * Bicycle route number
	 */
	private final String bicycleRouteNumber;

	@SuppressWarnings("unused")
	private PlanStep() {
		this.coordinate = null;
		this.distanceToNextStep = Integer.MAX_VALUE;
		this.travelTimeToNextStep = Integer.MAX_VALUE;
		this.angle = Integer.MAX_VALUE;
		this.surface = null;
		this.roadType = null;
		this.streetName = null;
		this.bicycleRouteNumber = null;
	}

	public PlanStep(Coordinate coordinate, int distanceToNextStep, int travelTimeToNextStep, int angle,
			Surface surface, RoadType roadType, String streetName, String bicycleRouteNumber) {
		super();
		this.coordinate = coordinate;
		this.distanceToNextStep = distanceToNextStep;
		this.travelTimeToNextStep = travelTimeToNextStep;
		this.angle = angle;
		this.surface = surface;
		this.roadType = roadType;
		this.streetName = streetName;
		this.bicycleRouteNumber = bicycleRouteNumber;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public int getDistanceToNextStep() {
		return distanceToNextStep;
	}

	public int getTravelTimeToNextStep() {
		return travelTimeToNextStep;
	}

	public int getAngle() {
		return angle;
	}

	public Surface getSurface() {
		return surface;
	}

	public RoadType getRoadType() {
		return roadType;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getBicycleRouteNumber() {
		return bicycleRouteNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + angle;
		result = prime * result + ((bicycleRouteNumber == null) ? 0 : bicycleRouteNumber.hashCode());
		result = prime * result + ((coordinate == null) ? 0 : coordinate.hashCode());
		result = prime * result + distanceToNextStep;
		result = prime * result + ((roadType == null) ? 0 : roadType.hashCode());
		result = prime * result + ((streetName == null) ? 0 : streetName.hashCode());
		result = prime * result + ((surface == null) ? 0 : surface.hashCode());
		result = prime * result + travelTimeToNextStep;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanStep other = (PlanStep) obj;
		if (angle != other.angle)
			return false;
		if (bicycleRouteNumber == null) {
			if (other.bicycleRouteNumber != null)
				return false;
		} else if (!bicycleRouteNumber.equals(other.bicycleRouteNumber))
			return false;
		if (coordinate == null) {
			if (other.coordinate != null)
				return false;
		} else if (!coordinate.equals(other.coordinate))
			return false;
		if (distanceToNextStep != other.distanceToNextStep)
			return false;
		if (roadType != other.roadType)
			return false;
		if (streetName == null) {
			if (other.streetName != null)
				return false;
		} else if (!streetName.equals(other.streetName))
			return false;
		if (surface != other.surface)
			return false;
		if (travelTimeToNextStep != other.travelTimeToNextStep)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PlanStep [coordinate=" + coordinate + ", distanceToNextStep=" + distanceToNextStep
				+ ", travelTimeToNextStep=" + travelTimeToNextStep + ", angle=" + angle + ", surface=" + surface
				+ ", roadType=" + roadType + ", streetName=" + streetName + ", bicycleRouteNumber="
				+ bicycleRouteNumber + "]";
	}

}
