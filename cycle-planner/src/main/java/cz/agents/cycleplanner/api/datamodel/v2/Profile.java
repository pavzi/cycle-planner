package cz.agents.cycleplanner.api.datamodel.v2;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Vector of criteria weights 
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class Profile {

	@JsonProperty(required = true)
	private final double travelTimeWeight;

	@JsonProperty(required = true)
	private final double comfortWeight;

	@JsonProperty(required = true)
	private final double quietnessWeight;

	@JsonProperty(required = true)
	private final double flatnessWeight;

	@SuppressWarnings("unused")
	private Profile() {
		this.travelTimeWeight = Double.MAX_VALUE;
		this.comfortWeight = Double.MAX_VALUE;
		this.quietnessWeight = Double.MAX_VALUE;
		this.flatnessWeight = Double.MAX_VALUE;
	}
	
	public Profile(double travelTimeWeight, double comfortWeight, double quietnessWeight, double flatnessWeight) {
		this.travelTimeWeight = travelTimeWeight;
		this.comfortWeight = comfortWeight;
		this.quietnessWeight = quietnessWeight;
		this.flatnessWeight = flatnessWeight;
	}

	public double getTravelTimeWeight() {
		return travelTimeWeight;
	}

	public double getComfortWeight() {
		return comfortWeight;
	}

	public double getQuietnessWeight() {
		return quietnessWeight;
	}

	public double getFlatnessWeight() {
		return flatnessWeight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(comfortWeight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(flatnessWeight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(quietnessWeight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(travelTimeWeight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profile other = (Profile) obj;
		if (Double.doubleToLongBits(comfortWeight) != Double.doubleToLongBits(other.comfortWeight))
			return false;
		if (Double.doubleToLongBits(flatnessWeight) != Double.doubleToLongBits(other.flatnessWeight))
			return false;
		if (Double.doubleToLongBits(quietnessWeight) != Double.doubleToLongBits(other.quietnessWeight))
			return false;
		if (Double.doubleToLongBits(travelTimeWeight) != Double.doubleToLongBits(other.travelTimeWeight))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Profile [travelTimeWeight=" + travelTimeWeight + ", comfortWeight=" + comfortWeight
				+ ", quietnessWeight=" + quietnessWeight + ", flatnessWeight=" + flatnessWeight + "]";
	}

}
