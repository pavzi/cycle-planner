package cz.agents.cycleplanner.api.datamodel.v2;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum ResponseStatus {
	OK, OUT_OF_BOUNDS, PLAN_NOT_FOUND, PLANNER_EXCEPTION;
}
