package cz.agents.cycleplanner.api.datamodel.v2;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum SpecialCoordinateType {
	ORIGIN, WAYPOINT, DESTINATION;
}
