package cz.agents.cycleplanner.api.v3;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.api.v3.datamodel.Coordinate;
import cz.agents.cycleplanner.api.v3.datamodel.CyclePlannerRequest;
import cz.agents.cycleplanner.api.v3.datamodel.CyclePlannerResponse;
import cz.agents.cycleplanner.api.v3.datamodel.CyclePlannerResponseStatus;
import cz.agents.cycleplanner.api.v3.datamodel.Plan;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.exceptions.PlannerException;
import cz.agents.cycleplanner.mongodb.CyclePlannerStorage;
import cz.agents.cycleplanner.plannernew.MultiCriteriaCyclePlanner;
import cz.agents.cycleplanner.plannernew.PlanFactory;
import cz.agents.cycleplanner.plannernew.PlanningInstance;
import cz.agents.cycleplanner.plannernew.PlanningInstanceFactory;
import cz.agents.cycleplanner.util.JSONUtils;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;
import cz.agents.planningalgorithms.multicriteria.ParetoSet;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
@Path("/planner")
public class CyclePlannerRestService {

	// TODO use HTTP status codes
	// https://en.wikipedia.org/wiki/List_of_HTTP_status_codes

	private static int MULTI_CRITERIA_CYCLE_PLANNER_NUM_OF_THREADS = 1;

	private static Logger log = Logger.getLogger(CyclePlannerRestService.class);
	private static final SimpleDateFormat dateParser = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ssXXX");

	private MultiCriteriaCyclePlanner multiCriteriaCyclePlanner;
	private CyclePlannerStorage cyclePlannerStorage;

	/**
	 * 
	 * Returns bicycle journey plan for predefined query. This method serves to
	 * test whether planner is functional.
	 * 
	 * @return bicycle journey plan for predefined query in JSON format
	 */
	@GET
	@Path("/test")
	@Produces("application/json")
	public javax.ws.rs.core.Response planTestJourney() {
		Coordinate origin = new Coordinate(50072600, 14391800);
		Coordinate destination = new Coordinate(50073200, 14441600);
		double averageSpeedKMpH = 20;
		// Profile profile = new Profile(1, 3, 5, 2);
		// Request request = new Request("test", origin, null, destination,
		// averageSpeedKMpH, "praha", profile);
		// log.info("Request handler in planTestJourney method: " +
		// requestHandler);
		// Response response = createResponse(Long.MAX_VALUE, request);
		//
		// String responseJson = JSONUtils.javaObjectToJson(response);
		//
		// return javax.ws.rs.core.Response.ok(responseJson,
		// MediaType.APPLICATION_JSON).build();

		return null;

	}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "TODO", notes = "TODO")
	@ApiResponses({ @ApiResponse(code = 400, message = "Incompatible JSON request format!"),
			@ApiResponse(code = 404, message = "TODO") })
	public Response plan(String jsonRequest) {

		String creationTimestamp = dateParser.format(new Date());
		CyclePlannerRequest request = JSONUtils.jsonToJavaObject(jsonRequest,
				CyclePlannerRequest.class);

		if (request == null) {
			log.warn("Incompatible JSON request format!");

			return javax.ws.rs.core.Response.status(400)
					.entity("Incompatible JSON request format!").type("text/plain").build();
		}

		PlanningInstance planningInstance = null;

		try {
			planningInstance = PlanningInstanceFactory.getPlanningInstance(request);
		} catch (OutOfBoundsException e) {
			log.error(request);
			log.error(e.getMessage(), e);

			// return javax.ws.rs.core.Response.status(400)
			// .entity("Incompatible JSON request format!").type("text/plain").build();
			// return new Response(responseId, creationTimestamp, request,
			// ResponseStatus.OUT_OF_BOUNDS);
		} catch (PlannerException e) {
			log.error(request);
			log.error(e.getMessage(), e);

			// return javax.ws.rs.core.Response.status(400)
			// .entity("Incompatible JSON request format!").type("text/plain").build();
			// return new Response(responseId, creationTimestamp, request,
			// ResponseStatus.PLANNER_EXCEPTION);
		}

		if (multiCriteriaCyclePlanner == null) {
			multiCriteriaCyclePlanner = new MultiCriteriaCyclePlanner(
					MULTI_CRITERIA_CYCLE_PLANNER_NUM_OF_THREADS);
		}

		Future<ParetoSet<CycleNode, CycleEdge>> multiCriteriaFuture = multiCriteriaCyclePlanner
				.plan(planningInstance);

		Collection<Plan> plans = new ArrayList<Plan>();
		ParetoSet<CycleNode, CycleEdge> paretoSet = null;
		try {
			paretoSet = multiCriteriaFuture.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// CATCH PlanNotFound

		int planId = 0;
		int averageSpeedKmPH = 15;

		for (cz.agents.planningalgorithms.multicriteria.Path<CycleNode, CycleEdge> path : paretoSet
				.getPaths()) {
			PlanFactory planFactory = new PlanFactory();
			Plan plan = planFactory.getPlan(planId++, planningInstance.graph,
					new HashMap<Long, CycleNode>(), path.getSequenceOfNodes().get(0).id,
					new HashSet<Integer>(), path, averageSpeedKmPH);

			plans.add(plan);
		}

		if (cyclePlannerStorage == null) {
			cyclePlannerStorage = CyclePlannerStorage.getStorage();
		}

		// long responseId = cyclePlannerStorage.getNewId(
		// CyclePlannerStorage.LAST_RESPONSE_ID_COLLECTION,
		// CyclePlannerStorage.LAST_RESPONSE_ID_KEY,
		// CyclePlannerStorage.RESPONSES_COLLECTION,
		// CyclePlannerStorage.RESPONSES_ID_KEY);
		long responseId = 1;
		CyclePlannerResponse response = new CyclePlannerResponse(responseId, creationTimestamp,
				request, "todo add region", CyclePlannerResponseStatus.OK, plans);
		String jsonResponse = JSONUtils.javaObjectToJson(response);

		// TODO save to CyclePlanenrStorage

		System.out.println("Response: " + jsonResponse);

		return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@Path("/{responseId: [0-9]*}")
	@ApiOperation(value = "TODO", notes = "TODO", response = CyclePlannerResponse.class)
	@ApiResponses({ @ApiResponse(code = 404, message = "TODO") })
	public Response getCyclePlans() {
		return null;
	}

	@POST
	@Path("/{responseId: [0-9]*}/{planId: [0-9]*}")
	@ApiOperation(value = "TODO", notes = "TODO")
	@ApiResponses({ @ApiResponse(code = 404, message = "TODO") })
	public Response getCyclePlan() {
		return null;
	}

}
