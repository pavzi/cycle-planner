package cz.agents.cycleplanner.api.v3;

import java.text.SimpleDateFormat;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

public class FeedbackRestService {
	
	private static Logger log = Logger.getLogger(FeedbackRestService.class);

	private static final SimpleDateFormat dateParser = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ssXXX");

	/**
	 * 
	 * Saves feedback to database.
	 * 
	 * @param feedback
	 *            JSON format of feedback to bicycle journey plan
	 * @return response with status code 201 when saving runs successfully,
	 *         otherwise returns response with status code 400 or 401
	 */
	@POST
	@Path("/feedback")
	@Consumes(MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response feedback(String feedback) {

		// FeedbackRestService cycleplannerFeedback =
		// JSONUtils.jsonToJavaObject(feedback, FeedbackRestService.class);
		//
		// if (cycleplannerFeedback != null) {
		//
		// String creationTimestamp = dateParser.format(new Date());
		// long feedbackId = bicycleJourneyPlanStorage.getNewId(
		// BicycleJourneyPlanStorage.LAST_FEEDBACK_ID_COLLECTION,
		// BicycleJourneyPlanStorage.LAST_FEEDBACK_ID_KEY,
		// BicycleJourneyPlanStorage.FEEDBACK_COLLECTION,
		// BicycleJourneyPlanStorage.FEEDBACK_ID_KEY);
		// FeedbackStorageObject feedbackStorageObject = new
		// FeedbackStorageObject(feedbackId,
		// creationTimestamp, cycleplannerFeedback);
		// boolean success = bicycleJourneyPlanStorage
		// .storeCycleplannerFeedback(feedbackStorageObject);
		//
		// if (success) {
		// URI location = null; // TODO Temporary, we do not provide API to
		// // reading feedback from DB
		// return javax.ws.rs.core.Response.created(location).build();
		// } else {
		// log.warn("Problem with saving feedback to database!");
		// return javax.ws.rs.core.Response.status(404)
		// .entity("Problem with saving feedback to database!").type("text/plain")
		// .build();
		// }
		//
		// } else {
		// log.warn("Incompatible JSON feedback format!");
		//
		// return javax.ws.rs.core.Response.status(400)
		// .entity("Incompatible JSON feedback format!").type("text/plain").build();
		// }
		return null;

	}
}
