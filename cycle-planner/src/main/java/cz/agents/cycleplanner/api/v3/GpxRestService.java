package cz.agents.cycleplanner.api.v3;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
@Path("/gpx")
@Api(value = "/gpx", description = "Return plan in GPX format")
public class GpxRestService {
	// /**
	// * Returns journey with specified id in form of <code>Response</code>
	// class,
	// * defined in package <code>cz.agents.cycleplanner.api.datamodel</code>.
	// *
	// * @param journeyId
	// * identifier of journey in database
	// * @return HTTP 200 response with journey in JSON format if journey exists
	// * in database, otherwise HTTP 404 response with message
	// */
	// @GET
	// @Path("journeys/{journeyId: [0-9]*}")
	// @Produces("application/json")
	// public javax.ws.rs.core.Response getJourney(@PathParam("journeyId") long
	// journeyId) {
	// log.info("Retrieving journey " + journeyId + " from database!");
	// Response response =
	// bicycleJourneyPlanStorage.retrieveCycleplannerResponse(journeyId);
	//
	// if (response == null) {
	// // Unable to find requested journey
	// return
	// javax.ws.rs.core.Response.status(404).entity("No such a journey exists!").type("text/plain").build();
	// }
	//
	// String responseJson = JSONUtils.javaObjectToJson(response);
	//
	// return javax.ws.rs.core.Response.ok(responseJson,
	// MediaType.APPLICATION_JSON).build();
	// }

	@GET
	@Path("/{responseId: [0-9]*}/{planId: [0-9]*}")
	@ApiOperation(value = "TODO", notes = "TODO")
	@ApiResponses({ @ApiResponse(code = 404, message = "TODO") })
	public Response getGPX(
			@ApiParam(value = "Response ID", required = true) @PathParam("responseId") long responseId,
			@ApiParam(value = "Plan ID", required = true) @PathParam("planId") long planId) {

		System.out.println("Received responseId: " + responseId + ", planId: " + planId);

		// BicycleJourneyPlanStorage bicycleJourneyPlanStorage =
		// BicycleJourneyPlanStorage
		// .getStorage();

		// bicycleJourneyPlanStorage.
		return null;
	}
}
