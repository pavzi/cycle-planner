package cz.agents.cycleplanner.api.v3.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class BoundingBox {

	/**
	 * TODO javadoc
	 */
	@JsonProperty(required = true)
	private final Coordinate topLeft;

	/**
	 * TODO javadoc
	 */
	@JsonProperty(required = true)
	private final Coordinate bottomRight;

	@SuppressWarnings("unused")
	private BoundingBox() {
		this.topLeft = null;
		this.bottomRight = null;
	}

	public BoundingBox(Coordinate topLeft, Coordinate bottomRight) {
		super();
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
	}

	public Coordinate getTopLeft() {
		return topLeft;
	}

	public Coordinate getBottomRight() {
		return bottomRight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bottomRight == null) ? 0 : bottomRight.hashCode());
		result = prime * result + ((topLeft == null) ? 0 : topLeft.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoundingBox other = (BoundingBox) obj;
		if (bottomRight == null) {
			if (other.bottomRight != null)
				return false;
		} else if (!bottomRight.equals(other.bottomRight))
			return false;
		if (topLeft == null) {
			if (other.topLeft != null)
				return false;
		} else if (!topLeft.equals(other.topLeft))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BoundingBox [topLeft=" + topLeft + ", bottomRight=" + bottomRight + "]";
	}

}
