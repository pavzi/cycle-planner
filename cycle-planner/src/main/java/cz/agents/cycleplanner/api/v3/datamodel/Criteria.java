package cz.agents.cycleplanner.api.v3.datamodel;

public class Criteria {

	private final int travelTime;

	private final int physicalEffort;

	private final int stress;

	public Criteria(int travelTime, int physicalEffort, int stress) {
		super();
		this.travelTime = travelTime;
		this.physicalEffort = physicalEffort;
		this.stress = stress;
	}

	public int getTravelTime() {
		return travelTime;
	}

	/**
	 * planning for e-bikes, maybe in KiloJoules or in Watts, represents how
	 * much energy you have spend to travel along plan
	 */
	public int getPhysicalEffort() {
		return physicalEffort;
	}

	public int getStress() {
		return stress;
	}

}
