package cz.agents.cycleplanner.api.v3.datamodel;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class CyclePlannerRequest {

	/**
	 * Type of client aplication requesting a plan: android, web, test
	 */
	private final String client;

	/**
	 * Origin
	 */
	@JsonProperty(required = true)
	private final Coordinate origin;

	/**
	 * Waypoints
	 */
	private final List<Coordinate> waypoints;

	/**
	 * Destination
	 */
	@JsonProperty(required = true)
	private final Coordinate destination;

	/**
	 * Average crusing speed in kilometers per hour
	 */
	@JsonProperty(required = true)
	private final double averageSpeedKmPH;

	/**
	 * A city in which request takes a place.
	 */
	private final String city;

	// /**
	// * A vector of criteria weights. Defines user preferences for each
	// * criterion.
	// */
	// private final Profile profile;

	@SuppressWarnings("unused")
	private CyclePlannerRequest() {
		this.client = null;
		this.origin = null;
		this.waypoints = null;
		this.destination = null;
		this.averageSpeedKmPH = Double.MAX_VALUE;
		this.city = null;
		// this.profile = null;
	}

	public CyclePlannerRequest(String client, Coordinate origin, List<Coordinate> waypoints,
			Coordinate destination, double averageSpeedKmPH, String city) {
		// , Profile profile
		super();
		this.client = client;
		this.origin = origin;
		this.waypoints = waypoints;
		this.destination = destination;
		this.averageSpeedKmPH = averageSpeedKmPH;
		this.city = city;
		// this.profile = profile;
	}

	public String getClient() {
		return client;
	}

	public Coordinate getOrigin() {
		return origin;
	}

	public List<Coordinate> getWaypoints() {
		return waypoints;
	}

	public Coordinate getDestination() {
		return destination;
	}

	public double getAverageSpeedKmPH() {
		return averageSpeedKmPH;
	}

	public String getCity() {
		return city;
	}

	// public Profile getProfile() {
	// return profile;
	// }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(averageSpeedKmPH);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		// result = prime * result + ((profile == null) ? 0 :
		// profile.hashCode());
		result = prime * result + ((waypoints == null) ? 0 : waypoints.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CyclePlannerRequest other = (CyclePlannerRequest) obj;
		if (Double.doubleToLongBits(averageSpeedKmPH) != Double
				.doubleToLongBits(other.averageSpeedKmPH))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		// if (profile == null) {
		// if (other.profile != null)
		// return false;
		// } else if (!profile.equals(other.profile))
		// return false;
		if (waypoints == null) {
			if (other.waypoints != null)
				return false;
		} else if (!waypoints.equals(other.waypoints))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Request [client=" + client + ", origin=" + origin + ", waypoints=" + waypoints
				+ ", destination=" + destination + ", averageSpeedKmPH=" + averageSpeedKmPH
				+ ", city=" + city
				// + ", profile=" + profile
				+ "]";
	}

}
