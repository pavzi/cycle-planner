package cz.agents.cycleplanner.api.v3.datamodel;

import io.swagger.annotations.ApiModel;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
@ApiModel(value = "CyclePlannerResponse", description = "TODO")
public class CyclePlannerResponse {

	/**
	 * 
	 */
	@JsonProperty(required = true)
	private final long responseId;

	/**
	 * Contains timestamp when the feedback was created
	 */
	@JsonProperty(required = true)
	private final String creationTimestamp;

	/**
	 * 
	 */
	@JsonProperty(required = true)
	private final CyclePlannerRequest request;

	/**
	 * 
	 */
	private final String region;

	/**
	 * 
	 */
	@JsonProperty(required = true)
	private final CyclePlannerResponseStatus status;

	/**
	 * 
	 */
	private final Collection<Plan> plans;

	@SuppressWarnings("unused")
	private CyclePlannerResponse() {
		this.responseId = Long.MAX_VALUE;
		this.creationTimestamp = null;
		this.request = null;
		this.region = null;
		this.status = null;
		this.plans = null;
	}

	public CyclePlannerResponse(long responseId, String creationTimestamp,
			CyclePlannerRequest request, CyclePlannerResponseStatus status) {
		super();
		this.responseId = responseId;
		this.creationTimestamp = creationTimestamp;
		this.request = request;
		this.region = null;
		this.status = status;
		this.plans = null;
	}

	public CyclePlannerResponse(long responseId, String creationTimestamp,
			CyclePlannerRequest request, String region, CyclePlannerResponseStatus status,
			Collection<Plan> plans) {
		super();
		this.responseId = responseId;
		this.creationTimestamp = creationTimestamp;
		this.request = request;
		this.region = region;
		this.status = status;
		this.plans = plans;
	}

	public long getResponseId() {
		return responseId;
	}

	public String getCreationTimestamp() {
		return creationTimestamp;
	}

	public CyclePlannerRequest getRequest() {
		return request;
	}

	public String getRegion() {
		return region;
	}

	public CyclePlannerResponseStatus getStatus() {
		return status;
	}

	public Collection<Plan> getPlans() {
		return plans;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationTimestamp == null) ? 0 : creationTimestamp.hashCode());
		result = prime * result + ((plans == null) ? 0 : plans.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		result = prime * result + ((request == null) ? 0 : request.hashCode());
		result = prime * result + (int) (responseId ^ (responseId >>> 32));
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CyclePlannerResponse other = (CyclePlannerResponse) obj;
		if (creationTimestamp == null) {
			if (other.creationTimestamp != null)
				return false;
		} else if (!creationTimestamp.equals(other.creationTimestamp))
			return false;
		if (plans == null) {
			if (other.plans != null)
				return false;
		} else if (!plans.equals(other.plans))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		if (request == null) {
			if (other.request != null)
				return false;
		} else if (!request.equals(other.request))
			return false;
		if (responseId != other.responseId)
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Response [responseId=" + responseId + ", creationTimestamp=" + creationTimestamp
				+ ", request=" + request + ", region=" + region + ", status=" + status + ", plans="
				+ plans + "]";
	}

}
