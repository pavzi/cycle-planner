package cz.agents.cycleplanner.api.v3.datamodel;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum CyclePlannerResponseStatus {
	OK, OUT_OF_BOUNDS, PLAN_NOT_FOUND, PLANNER_EXCEPTION;
}
