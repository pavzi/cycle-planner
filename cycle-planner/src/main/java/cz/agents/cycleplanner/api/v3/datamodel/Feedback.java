package cz.agents.cycleplanner.api.v3.datamodel;

import java.util.List;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class Feedback {

	/**
	 * TODO javadoc
	 */
	private final Long responseId;
	
	/**
	 * Represents plan id, each predefined profile has its own id
	 */
	private final Long planId;

	/**
	 * TODO javadoc
	 */
	private final Integer rating;

	/**
	 * TODO javadoc
	 */
	private final Boolean forbiddenManoeuvers;

	/**
	 * TODO javadoc
	 */
	private final Boolean badProfileCorrespondence;

	/**
	 * TODO javadoc
	 */
	private final Boolean dangerousPlaces;

	/**
	 * TODO javadoc
	 */
	private final Boolean pavementWhenNotNeeded;

	/**
	 * TODO javadoc
	 */
	private final Boolean badMapData;

	/**
	 * TODO javadoc
	 */
	private final String textualFeedback;

	/**
	 * TODO javadoc
	 */
	private final List<TimedCoordinate> trackedJourney;

	@SuppressWarnings("unused")
	private Feedback() {
		this.responseId = null;
		this.planId = null;
		this.rating = null;
		this.forbiddenManoeuvers = null;
		this.badProfileCorrespondence = null;
		this.dangerousPlaces = null;
		this.pavementWhenNotNeeded = null;
		this.badMapData = null;
		this.textualFeedback = null;
		this.trackedJourney = null;
	}

	public Feedback(Long responseId, Long planId, Integer rating, Boolean forbiddenManoeuvers,
			Boolean badProfileCorrespondence, Boolean dangerousPlaces, Boolean pavementWhenNotNeeded,
			Boolean badMapData, String textualFeedback, List<TimedCoordinate> trackedJourney) {
		super();
		this.responseId = responseId;
		this.planId = planId;
		this.rating = rating;
		this.forbiddenManoeuvers = forbiddenManoeuvers;
		this.badProfileCorrespondence = badProfileCorrespondence;
		this.dangerousPlaces = dangerousPlaces;
		this.pavementWhenNotNeeded = pavementWhenNotNeeded;
		this.badMapData = badMapData;
		this.textualFeedback = textualFeedback;
		this.trackedJourney = trackedJourney;
	}

	public Long getResponseId() {
		return responseId;
	}

	public Long getPlanId() {
		return planId;
	}

	public Integer getRating() {
		return rating;
	}

	public Boolean getForbiddenManoeuvers() {
		return forbiddenManoeuvers;
	}

	public Boolean getBadProfileCorrespondence() {
		return badProfileCorrespondence;
	}

	public Boolean getDangerousPlaces() {
		return dangerousPlaces;
	}

	public Boolean getPavementWhenNotNeeded() {
		return pavementWhenNotNeeded;
	}

	public Boolean getBadMapData() {
		return badMapData;
	}

	public String getTextualFeedback() {
		return textualFeedback;
	}

	public List<TimedCoordinate> getTrackedJourney() {
		return trackedJourney;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((badMapData == null) ? 0 : badMapData.hashCode());
		result = prime * result + ((badProfileCorrespondence == null) ? 0 : badProfileCorrespondence.hashCode());
		result = prime * result + ((dangerousPlaces == null) ? 0 : dangerousPlaces.hashCode());
		result = prime * result + ((forbiddenManoeuvers == null) ? 0 : forbiddenManoeuvers.hashCode());
		result = prime * result + ((pavementWhenNotNeeded == null) ? 0 : pavementWhenNotNeeded.hashCode());
		result = prime * result + ((planId == null) ? 0 : planId.hashCode());
		result = prime * result + ((rating == null) ? 0 : rating.hashCode());
		result = prime * result + ((responseId == null) ? 0 : responseId.hashCode());
		result = prime * result + ((textualFeedback == null) ? 0 : textualFeedback.hashCode());
		result = prime * result + ((trackedJourney == null) ? 0 : trackedJourney.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feedback other = (Feedback) obj;
		if (badMapData == null) {
			if (other.badMapData != null)
				return false;
		} else if (!badMapData.equals(other.badMapData))
			return false;
		if (badProfileCorrespondence == null) {
			if (other.badProfileCorrespondence != null)
				return false;
		} else if (!badProfileCorrespondence.equals(other.badProfileCorrespondence))
			return false;
		if (dangerousPlaces == null) {
			if (other.dangerousPlaces != null)
				return false;
		} else if (!dangerousPlaces.equals(other.dangerousPlaces))
			return false;
		if (forbiddenManoeuvers == null) {
			if (other.forbiddenManoeuvers != null)
				return false;
		} else if (!forbiddenManoeuvers.equals(other.forbiddenManoeuvers))
			return false;
		if (pavementWhenNotNeeded == null) {
			if (other.pavementWhenNotNeeded != null)
				return false;
		} else if (!pavementWhenNotNeeded.equals(other.pavementWhenNotNeeded))
			return false;
		if (planId == null) {
			if (other.planId != null)
				return false;
		} else if (!planId.equals(other.planId))
			return false;
		if (rating == null) {
			if (other.rating != null)
				return false;
		} else if (!rating.equals(other.rating))
			return false;
		if (responseId == null) {
			if (other.responseId != null)
				return false;
		} else if (!responseId.equals(other.responseId))
			return false;
		if (textualFeedback == null) {
			if (other.textualFeedback != null)
				return false;
		} else if (!textualFeedback.equals(other.textualFeedback))
			return false;
		if (trackedJourney == null) {
			if (other.trackedJourney != null)
				return false;
		} else if (!trackedJourney.equals(other.trackedJourney))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Feedback [responseId=" + responseId + ", planId=" + planId + ", rating=" + rating
				+ ", forbiddenManoeuvers=" + forbiddenManoeuvers + ", badProfileCorrespondence="
				+ badProfileCorrespondence + ", dangerousPlaces=" + dangerousPlaces + ", pavementWhenNotNeeded="
				+ pavementWhenNotNeeded + ", badMapData=" + badMapData + ", textualFeedback=" + textualFeedback
				+ ", trackedJourney=" + trackedJourney + "]";
	}

}
