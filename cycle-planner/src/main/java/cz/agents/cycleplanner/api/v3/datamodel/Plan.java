package cz.agents.cycleplanner.api.v3.datamodel;

import java.util.List;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class Plan {

	/**
	 * Represents plan id, each predefined profile has its own id
	 */
	private final long planId;

	/**
	 * Represents geographical bounds, in which plan takes place
	 */
	private final BoundingBox boundingBox;

	/**
	 * Plan length in meters
	 */
	private final int length;

	/**
	 * TODO javadoc
	 */
	private final int elevationGain;

	/**
	 * TODO javadoc
	 */
	private final int elevationDrop;

	/**
	 * TODO javadoc
	 */
	private final Criteria criteria;

	/**
	 * TODO javadoc
	 */
	private final List<PlanStep> steps;

	@SuppressWarnings("unused")
	private Plan() {
		this.planId = Long.MAX_VALUE;
		this.boundingBox = null;
		this.length = Integer.MAX_VALUE;
		this.elevationGain = Integer.MAX_VALUE;
		this.elevationDrop = Integer.MAX_VALUE;
		this.criteria = null;
		this.steps = null;
	}

	public Plan(long planId, BoundingBox boundingBox, int length, int elevationGain,
			int elevationDrop, Criteria criteria, List<PlanStep> steps) {
		super();
		this.planId = planId;
		this.boundingBox = boundingBox;
		this.length = length;
		this.elevationGain = elevationGain;
		this.elevationDrop = elevationDrop;
		this.criteria = criteria;
		this.steps = steps;
	}

	public long getPlanId() {
		return planId;
	}

	public BoundingBox getBoundingBox() {
		return boundingBox;
	}

	public int getLength() {
		return length;
	}

	public int getElevationGain() {
		return elevationGain;
	}

	public int getElevationDrop() {
		return elevationDrop;
	}

	public Criteria getCriteria() {
		return criteria;
	}

	public List<PlanStep> getSteps() {
		return steps;
	}

}
