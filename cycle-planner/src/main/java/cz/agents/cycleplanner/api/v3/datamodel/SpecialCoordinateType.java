package cz.agents.cycleplanner.api.v3.datamodel;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum SpecialCoordinateType {
	ORIGIN, WAYPOINT, DESTINATION;
}
