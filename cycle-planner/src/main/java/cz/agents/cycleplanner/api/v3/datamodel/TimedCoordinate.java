package cz.agents.cycleplanner.api.v3.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class TimedCoordinate extends Coordinate {

	@JsonProperty(required = true)
	private final String timestamp;

	@SuppressWarnings("unused")
	private TimedCoordinate() {
		super();
		this.timestamp = null;
	}

	public TimedCoordinate(int latE6, int lonE6, int elevation, String timeStamp) {
		super(latE6, lonE6, elevation);
		this.timestamp = timeStamp;
	}

	public TimedCoordinate(int latE6, int lonE6, int elevation, SpecialCoordinateType type, String timeStamp) {
		super(latE6, lonE6, elevation, type);
		this.timestamp = timeStamp;
	}

	public String getTimestamp() {
		return timestamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimedCoordinate other = (TimedCoordinate) obj;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TimedCoordinate [timestamp=" + timestamp + ", latE6=" + latE6 + ", lonE6=" + lonE6 + ", elevation="
				+ elevation + ", type=" + type + "]";
	}

}
