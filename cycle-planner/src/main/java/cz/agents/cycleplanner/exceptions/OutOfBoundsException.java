package cz.agents.cycleplanner.exceptions;

/**
 * Exception is triggered when, defined point do not lie inside any supported
 * area.
 * 
 * @author Pavol Zilecky <pavol.zilecky@agents.fel.cvut.cz>
 * 
 */
public class OutOfBoundsException extends Exception {

	private static final long serialVersionUID = -7369710063647891851L;

}
