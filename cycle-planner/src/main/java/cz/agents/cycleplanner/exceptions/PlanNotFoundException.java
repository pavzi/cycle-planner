package cz.agents.cycleplanner.exceptions;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class PlanNotFoundException extends Exception {

	private static final long serialVersionUID = 6072270297098108911L;

}
