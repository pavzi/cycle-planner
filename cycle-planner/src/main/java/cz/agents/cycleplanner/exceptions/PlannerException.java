package cz.agents.cycleplanner.exceptions;

/**
 * Exception is triggered when, planner is not able plan a route from unexpected
 * reasons
 * 
 * @author Pavol Zilecky <pavol.zilecky@agents.fel.cvut.cz>
 * 
 */
public class PlannerException extends Exception {

	private static final long serialVersionUID = -4992884089516284470L;

}
