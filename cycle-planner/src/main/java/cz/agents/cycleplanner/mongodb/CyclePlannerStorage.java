package cz.agents.cycleplanner.mongodb;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.bson.BSONObject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.mongodb.util.JSON;

import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;
import cz.agents.cycleplanner.api.datamodel.v2.Response;

/**
 * Implementation of bicycle journey plan storage that is using mongo database at CVUT
 * 
 * @author Jan Hrncir (CVUT)
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 */
public class CyclePlannerStorage {

	private static final Logger log = Logger.getLogger(CyclePlannerStorage.class);
	private static String connectionDetails = "";
	private static CyclePlannerStorage storage = null;

	public static final String RESPONSES_COLLECTION = "responses";
	public static final String FEEDBACK_COLLECTION = "feedback";
	public static final String LAST_RESPONSE_ID_COLLECTION = "lastresponseid";
	public static final String LAST_FEEDBACK_ID_COLLECTION = "lastfeedbackid";
	public static final String RESPONSES_ID_KEY = "responseId";
	public static final String FEEDBACK_ID_KEY = "feedbackId";
	public static final String LAST_RESPONSE_ID_KEY = "lastResponseId";
	public static final String LAST_FEEDBACK_ID_KEY = "lastFeedbackId";

	private DB db = null;
	private ObjectMapper mapper = null;

	private CyclePlannerStorage() {

		try {
			// initialize connection to Mongo DB
			db = StorageMongoDbConnectionProvider.getConnection();

			// remember connection details
			connectionDetails = StorageMongoDbConnectionProvider.getDbConnectionDetails();

			// create index for faster reading responses
			db.getCollection(RESPONSES_COLLECTION).ensureIndex(new BasicDBObject(RESPONSES_ID_KEY, 1),
					"responseId_index", true);
			db.getCollection(FEEDBACK_COLLECTION).ensureIndex(new BasicDBObject(FEEDBACK_ID_KEY, 1),
					"feedbackId_index", true);

			// initialize mapper
			mapper = new ObjectMapper();
			mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

			log.info(String.format("JourneyPlanStorage based on %s successfully initialized.",
					StorageMongoDbConnectionProvider.getDbConnectionDetails()));

		} catch (Exception e) {

			log.error(String.format("JourneyPlanStorage initialization based on %s failed.",
					StorageMongoDbConnectionProvider.getDbConnectionDetails()), e);
		}
	}

	/**
	 * Get a singleton instance of a storage
	 * 
	 * @return Journey plan storage
	 */
	public static CyclePlannerStorage getStorage() {

		if ((storage == null) || (!connectionDetails.equals(StorageMongoDbConnectionProvider.getDbConnectionDetails()))) {

			CyclePlannerStorage newStorage = new CyclePlannerStorage();
			if (newStorage.isConnected()) {
				storage = newStorage;
			} else {
				storage = null;
			}
		}

		return storage;
	}

	/**
	 * 
	 */
	public boolean isConnected() {

		if (db != null) {
			return db.collectionExists(FEEDBACK_COLLECTION) && db.collectionExists(RESPONSES_COLLECTION)
					&& db.collectionExists(LAST_RESPONSE_ID_COLLECTION)
					&& db.collectionExists(LAST_FEEDBACK_ID_COLLECTION);
		}

		return false;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param response
	 * @return
	 */
	public boolean storeCycleplannerResponse(Response response) {
		return MongoUtils.storeObjectInCollection(response, db, RESPONSES_COLLECTION);
	}

	/**
	 * 
	 */
	public Response retrieveCycleplannerResponse(long responseId) {

		BasicDBObject query = new BasicDBObject("responseId", responseId);
		DBCursor cursor = db.getCollection(RESPONSES_COLLECTION).find(query);
		DBObject response = null;
		Response cycleplannerResponse = null;

		try {
			if (cursor.hasNext()) {
				response = cursor.next();
			}
		} finally {
			cursor.close();
		}

		if (response != null) {
			cycleplannerResponse = MongoUtils.convertMongoDbObjectToJava(response, Response.class);
		}

		return cycleplannerResponse;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param feedbackStorageObject
	 * @return
	 */
	public boolean storeCycleplannerFeedback(FeedbackStorageObject feedbackStorageObject) {
		return MongoUtils.storeObjectInCollection(feedbackStorageObject, db, FEEDBACK_COLLECTION);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public List<FeedbackStorageObject> retrieveAllCycleplannerFeedback(long startFeedbackId) {
		List<FeedbackStorageObject> allFeedback = new ArrayList<FeedbackStorageObject>();
		DBCursor cursor = db.getCollection(FEEDBACK_COLLECTION)
				.find(new BasicDBObject("feedbackId", new BasicDBObject("$gte", startFeedbackId)))
				.sort(new BasicDBObject("feedbackId", 1));
		DBObject dbResponse = null;
		FeedbackStorageObject feedback = null;

		try {
			while (cursor.hasNext()) {
				dbResponse = cursor.next();

				if (dbResponse != null) {
					feedback = MongoUtils.convertMongoDbObjectToJava(dbResponse, FeedbackStorageObject.class);

					if (feedback != null) {
						allFeedback.add(feedback);
					}
				}
			}
		} finally {
			cursor.close();
		}

		return allFeedback;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param startResponseId
	 * 
	 * @return
	 */
	public List<Response> retrieveAllResponses(long startResponseId, long endResponseId) {
		List<Response> allResponses = new ArrayList<Response>();
		DBCursor cursor = db
				.getCollection(RESPONSES_COLLECTION)
				.find(new BasicDBObject("responseId", new BasicDBObject("$gte", startResponseId).append("$lte",
						endResponseId))).sort(new BasicDBObject("responseId", 1));
		DBObject dbResponse = null;
		Response response = null;

		try {
			while (cursor.hasNext()) {
				dbResponse = cursor.next();

				if (dbResponse != null) {
					response = MongoUtils.convertMongoDbObjectToJava(dbResponse, Response.class);

					if (response != null) {
						allResponses.add(response);
					}
				}
			}
		} finally {
			cursor.close();
		}

		return allResponses;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * Approach using a special collection to store last id & largest stored id
	 * 
	 * @param dbIdCollection
	 *            collection containing last id
	 * @param dbIdKey
	 *            key to last id
	 * @param dbCollection
	 *            collection for which we want the last id
	 * @param dbKey
	 *            key to id
	 * @return
	 */
	public synchronized long getNewId(String dbIdCollection, String dbIdKey, String dbCollection, String dbKey) {
		long newId = 1;
		DBObject lastIdfound = db.getCollection(dbIdCollection).findOne();
		Integer largestIdInCollection = retrieveLargestIdFromCollection(dbCollection, dbKey);
		WriteResult wr = null;

		if (lastIdfound != null) {
			Integer lastId = (Integer) ((BSONObject) lastIdfound).get(dbIdKey);

			log.debug("Response id retrieved from " + dbIdCollection + " collection: " + lastId);
			log.debug("Largest response id retrieved from  " + dbCollection + " collection: " + largestIdInCollection);

			if (largestIdInCollection != null) {
				lastId = Math.max(largestIdInCollection, lastId);
			}

			newId = lastId + 1;
			DBObject newLastId = (DBObject) JSON.parse("{'" + dbIdKey + "':" + newId + "}");
			wr = db.getCollection(dbIdCollection).update(lastIdfound, newLastId);

		} else {
			if (largestIdInCollection != null) {
				newId = largestIdInCollection + 1;
			}

			DBObject newLastId = (DBObject) JSON.parse("{'" + dbIdKey + "':" + newId + "}");
			wr = db.getCollection(dbIdCollection).insert(newLastId);
		}

		if (!((double) wr.getField("ok") == 1.0)) {
			log.error("Unable to write new id to collection " + dbIdCollection);
		}

		return newId;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param dbCollection
	 * @param dbKey
	 * @return
	 */
	private Integer retrieveLargestIdFromCollection(String dbCollection, String dbKey) {
		Integer largestFeedbackIdFromFeedbackCollection = null;
		// check largest ID in the responses collection
		BasicDBObject sortQuery = new BasicDBObject(dbKey, -1);
		DBCursor cursor = db.getCollection(dbCollection).find().sort(sortQuery);

		if (cursor.hasNext()) {
			BSONObject bsonObject = (BSONObject) cursor.next();

			largestFeedbackIdFromFeedbackCollection = (Integer) bsonObject.get(dbKey);
		}

		return largestFeedbackIdFromFeedbackCollection;
	}

}
