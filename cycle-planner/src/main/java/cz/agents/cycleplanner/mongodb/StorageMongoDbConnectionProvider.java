package cz.agents.cycleplanner.mongodb;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class StorageMongoDbConnectionProvider {

	private static final Logger log = Logger.getLogger(StorageMongoDbConnectionProvider.class);

	/**
	 * DB Connection details
	 */
	private static String host = null;
	private static Integer port = null;
	private static String dbName = null;
	private static String user = null;
	private static String passwd = null;

	/**
	 * Cached connection to database
	 */
	private static MongoClient mongoClient = null;
	private static DB dbConnection = null;

	/**
	 * Get Mongo db connection. If the connection not exists, a new one is
	 * created. If the connection exists, it is returned.
	 * 
	 * @return Mongo db connection
	 */
	public static DB getConnection() {

		if (dbConnection == null) {

			MongoClient client = null;
			DB db = null;
			boolean authenticated = false;
			try {
				mongoClient = new MongoClient(host, port);
				db = mongoClient.getDB(dbName);
				authenticated = db.authenticate(user, passwd.toCharArray());
				if (authenticated) {
					log.info(String.format("Mongo connection %s OK.", getDbConnectionDetails()));
					dbConnection = db;
					mongoClient = client;
				} else {
					log.error(String.format("Mongo authentication %s FAILED.", getDbConnectionDetails()));
					dbConnection = null;
				}
			} catch (Exception e) {
				log.error(String.format("Mongo connection %s FAILED.", getDbConnectionDetails()), e);
				dbConnection = null;
			}
		}

		return dbConnection;
	}

	public static String getDbConnectionDetails() {
		return String.format("%s@%s:%d#%s", user, host, port, dbName);
	}

	public static void setDbConnection(DB db) {

		// set db connection
		dbConnection = db;

		// set connection details
		dbName = db.getName();
		host = db.getMongo().getAddress().getHost();
		port = db.getMongo().getAddress().getPort();
		try {
			CommandResult result = db.command(new BasicDBObject("connectionStatus", 1));
			user = (String) ((DBObject) ((DBObject) ((DBObject) result.get("authInfo")).get("authenticatedUsers"))
					.get("0")).get("user");
		} catch (NullPointerException e) {
			user = "noauth";
		}
	}

	public static void setDbConnectionDetails(String pHost, Integer pPort, String pDbName, String pDbUser,
			String pDbPasswd) {
		host = pHost;
		port = pPort;
		dbName = pDbName;
		user = pDbUser;
		passwd = pDbPasswd;
		dbConnection = null;
		if (mongoClient != null) {
			mongoClient.close();
			mongoClient = null;
		}
	}
	
	public static void setDbConnectionDetailsFromPropertiesFile(String propertiesFileName) throws IOException {
		InputStream propertiesInputStream = StorageMongoDbConnectionProvider.class.getClassLoader().getResourceAsStream(propertiesFileName);
		Properties properties = new Properties();
 
		if (propertiesInputStream != null) {
			properties.load(propertiesInputStream);
		} else {
			throw new FileNotFoundException("property file '" + propertiesFileName + "' not found in the classpath");
		}
		
		String pHost = (String) properties.get("host");
		int pPort = Integer.parseInt((String) properties.get("port"));
		String pDbName = (String) properties.get("database");
		String pDbUser = (String) properties.get("user");
		String pDbPassword = (String) properties.get("password");
		
		setDbConnectionDetails(pHost, pPort, pDbName, pDbUser, pDbPassword);
	}
}
