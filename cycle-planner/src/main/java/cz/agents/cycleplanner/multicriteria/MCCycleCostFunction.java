package cz.agents.cycleplanner.multicriteria;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.EdgeId;

public class MCCycleCostFunction implements MCCostFunction<CycleNode2, CycleEdge2> {

	private static Logger log = Logger.getLogger(MCCycleCostFunction.class);


	/**
	 * Cache costs for edges, because in MLC we can traverse one edge multiple
	 * times.
	 */
	private Map<EdgeId, int[]> costsCache;

	public MCCycleCostFunction() {
		this.costsCache = new HashMap<EdgeId, int[]>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] getCostVector(CycleNode2 current, CycleNode2 next, CycleEdge2 edge) {

		if (costsCache.containsKey(edge.getEdgeId())) {
			return costsCache.get(edge.getEdgeId());
		}

		int travelTime = edge.getCosts()[0];

		double quietComfort = edge.getLengthInMetres() * getCriterionMaxQuietComfort(edge);

		int flatness = edge.getCosts()[3];

		int[] costs = new int[] { travelTime, (int) Math.round(quietComfort), flatness };

		costsCache.put(edge.getEdgeId(), costs);

		return costs;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 * @return
	 */
	private int getCriterionMaxQuietComfort(CycleEdge2 edge) {
		int comfort = (int) Math.round(edge.getComfortBaseValue() * 10d);
		int quietness = (int) Math.round(edge.getQuietnessBaseValue() * 10d);
		
		if (comfort != 10 && quietness != 10) {
			if (comfort >= quietness) {
				return comfort;
			} else {
				return quietness;
			}
		} else if (comfort == 10 && quietness != 10) {
			return quietness;
		}

		return comfort;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 * @return
	 */
	@SuppressWarnings("unused")
	private int getCriterionQuietPlusComfort(CycleEdge2 edge) {
		int comfort = (int) Math.round(edge.getComfortBaseValue() * 10d);
		int quietness = (int) Math.round(edge.getQuietnessBaseValue() * 10d);

		if (comfort == 10 && quietness == 10) {
			return comfort;
		} else if (comfort != 10 && quietness == 10) {
			return comfort;
		} else if (comfort == 10 && quietness != 10) {
			return quietness;
		}

		return comfort + quietness;
	}

}
