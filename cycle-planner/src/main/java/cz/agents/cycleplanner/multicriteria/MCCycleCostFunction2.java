package cz.agents.cycleplanner.multicriteria;

import java.util.HashMap;
import java.util.Map;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.EdgeId;

public class MCCycleCostFunction2 implements MCCostFunction<CycleNode2, CycleEdge2> {
	
	/**
	 * Cache costs for edges, because in MLC we can traverse one edge multiple
	 * times.
	 */
	private Map<EdgeId, int[]> costsCache;
	
	public MCCycleCostFunction2() {
		this.costsCache = new HashMap<EdgeId, int[]>();
	}
	
	@Override
	public int[] getCostVector(CycleNode2 current, CycleNode2 next, CycleEdge2 edge) {
		if (costsCache.containsKey(edge.getEdgeId())) {
			return costsCache.get(edge.getEdgeId());
		}

		double bikeFriendliness = edge.getLengthInMetres() * getCriterionMaxQuietComfort(edge) + edge.getProlongationConstant();

		int[] costs = new int[] { (int) Math.round(edge.getLengthInMetres()), (int) Math.round(bikeFriendliness), edge.getCosts()[3] };

		costsCache.put(edge.getEdgeId(), costs);

		return costs;
	}
	
	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 * @return
	 */
	private int getCriterionMaxQuietComfort(CycleEdge2 edge) {
		int comfort = (int) Math.round(edge.getComfortBaseValue() * 10d);
		int quietness = (int) Math.round(edge.getQuietnessBaseValue() * 10d);
		
		if (comfort != 10 && quietness != 10) {
			if (comfort >= quietness) {
				return comfort;
			} else {
				return quietness;
			}
		} else if (comfort == 10 && quietness != 10) {
			return quietness;
		}

		return comfort;
	}

}
