package cz.agents.cycleplanner.multicriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

public class ParetoSet<TNode extends Node, TEdge extends Edge> implements Serializable {
	
	private static final long serialVersionUID = -77347923710152093L;
	
	private final Map<Label<TNode>, List<TNode>> pathsAsSequenceOfNodes;
	private final Map<Label<TNode>, List<TEdge>> pathsAsSequenceOfEdges;
	
	private final long numberOfNodesExpansions;

	public ParetoSet(Collection<Label<TNode>> destinationBag, Graph<TNode, TEdge> graph, long numberOfNodesExpansions) {
		pathsAsSequenceOfNodes = new HashMap<Label<TNode>, List<TNode>>();
		pathsAsSequenceOfEdges = new HashMap<Label<TNode>, List<TEdge>>();

		for (Iterator<Label<TNode>> it = destinationBag.iterator(); it.hasNext();) {
			Label<TNode> destinationLabel = it.next();

			pathsAsSequenceOfNodes.put(destinationLabel, reconstructPathNodes(destinationLabel));
			pathsAsSequenceOfEdges.put(destinationLabel, reconstructPathEdges(destinationLabel, graph));
		}
		
		this.numberOfNodesExpansions = numberOfNodesExpansions;
	}

	private List<TNode> reconstructPathNodes(Label<TNode> label) {

		List<TNode> path = new ArrayList<TNode>();
		Label<TNode> prevLabel;

		path.add(label.getNode());

		while ((prevLabel = label.getPredecessorLabel()) != null) {

			path.add(prevLabel.getNode());
			label = prevLabel;
		}

		Collections.reverse(path);
		
		return path;
	}

	private List<TEdge> reconstructPathEdges(Label<TNode> label, Graph<TNode, TEdge> graph) {

		List<TEdge> path = new ArrayList<TEdge>();
		Label<TNode> prevLabel = label.getPredecessorLabel();

		while ((prevLabel = label.getPredecessorLabel()) != null) {

			path.add(graph.getEdge(prevLabel.getNode().getId(), label.getNode().getId()));
			label = prevLabel;
		}
		
		Collections.reverse(path);
		
		return path;
	}	
	
	public Set<Entry<Label<TNode>, List<TNode>>> getParetoSetAsSequenceOfNodes() {
		return pathsAsSequenceOfNodes.entrySet();
	}
	
	public Collection<List<TNode>> getAllSequenceOfNodes() {
		return pathsAsSequenceOfNodes.values();
	}
	
	public Set<Entry<Label<TNode>, List<TEdge>>> getParetoSetAsSequenceOfEdges() {
		return pathsAsSequenceOfEdges.entrySet();
	}
	
	public Collection<List<TEdge>> getAllSequenceOfEdges() {
		return pathsAsSequenceOfEdges.values();
	}
	
	public Set<Label<TNode>> getParetoSetLabels() {
		return pathsAsSequenceOfEdges.keySet();
	}
	
	public long getNumberOfNodesExpansions() {
		return numberOfNodesExpansions;
	}
}
