package cz.agents.cycleplanner.multicriteria.alg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.multicriteria.Label;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import cz.agents.cycleplanner.multicriteria.ParetoSet;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

abstract public class AbstractMultiCriteriaDijkstraAlgorithm<TNode extends Node, TEdge extends Edge> implements Callable<ParetoSet<TNode, TEdge>> {

	private static Logger log = Logger.getLogger(AbstractMultiCriteriaDijkstraAlgorithm.class);

	private Graph<TNode, TEdge> graph;

	protected TNode origin, destination;

	private MCCostFunction<TNode, TEdge> costFunction;

	protected PriorityQueue<Label<TNode>> queue;

	protected Map<Long, Collection<Label<TNode>>> bags;

	private long labelIDGenerator = 0;

	protected Collection<Label<TNode>> destinationBag;
	
	private ParetoSet<TNode, TEdge> paretoSet; 

	public AbstractMultiCriteriaDijkstraAlgorithm(Graph<TNode, TEdge> graph, TNode origin, TNode destination,
			MCCostFunction<TNode, TEdge> costFunction) {

		this.graph = graph;
		this.origin = origin;
		log.info("Origin: " + origin);
		this.destination = destination;
		log.info("Destination: " + destination);
		this.costFunction = costFunction;
		this.queue = new PriorityQueue<Label<TNode>>();
		this.bags = new HashMap<Long, Collection<Label<TNode>>>(this.graph.getAllNodes().size());

//		for (TNode node : this.graph.getAllNodes()) {
//			bags.put(node.getId(), new ArrayList<Label<TNode>>());
//		}
		
		this.destinationBag = new ArrayList<Label<TNode>>();
		this.bags.put(destination.getId(), destinationBag);

	}

	@Override
	public ParetoSet<TNode, TEdge> call() {
		long iterations = 0;
		insertOrigin(origin);

		while (!queue.isEmpty()) {

			Label<TNode> currentLabel = queue.poll();

			if (terminationConditon(currentLabel)) {
				break;
			}

			if (skipLabel(currentLabel)) {
				continue;
			}
			
			for (TEdge outcomingEdge : graph.getNodeOutcomingEdges(currentLabel.getNode().getId())) {
				long nextNodeId = outcomingEdge.getToNodeId();
				TNode nextNode = graph.getNodeByNodeId(nextNodeId);

				int[] nextLabelCostVector = new int[3];
				int[] outcomingEdgeCostVector = costFunction.getCostVector(currentLabel.getNode(), nextNode,
						outcomingEdge);

				for (int i = 0; i < nextLabelCostVector.length; i++) {
					nextLabelCostVector[i] = currentLabel.getCostVector()[i] + outcomingEdgeCostVector[i];
				}

				Label<TNode> nextLabel = new Label<TNode>(nextNode, nextLabelCostVector, currentLabel);

				if (skipEdge(nextLabel)) {
					continue;
				}
//				log.info(currentLabel + " -- " + nextLabel);
				
				Collection<Label<TNode>> nextBag = bags.get(nextNode.getId());
				
				if (nextBag == null) {
					nextBag = new ArrayList<Label<TNode>>();
					bags.put(nextNode.getId(), nextBag);
				}
								
				if (checkDominance(nextLabel)) {

					nextBag.add(nextLabel);
//					bags.get(nextNode.getId()).add(nextLabel);
					queue.offer(nextLabel);
				}
			}

			// checkMemoryConsumption();
			iterations++;
		}

		log.info("Number of pareto routes: " + destinationBag.size());
		log.info("Number of iterations: " + iterations);
		
		this.paretoSet = new ParetoSet<TNode, TEdge>(destinationBag, graph, iterations);
		return paretoSet;
	}

	/**
	 * 
	 * @return
	 */
	abstract public boolean terminationConditon(Label<TNode> current);

	/**
	 * 
	 * @return
	 */
	abstract public boolean skipLabel(Label<TNode> current);

	/**
	 * 
	 * @return
	 */
	abstract public boolean skipEdge(Label<TNode> next);

	/**
	 * 
	 * @return
	 */
	abstract public boolean checkDominance(Label<TNode> next);

	/**
	 * Check if first parameter is dominant over second. In other words, check
	 * if second argument is dominated by first.
	 * 
	 * @param dist1
	 * @param dist2
	 * @return
	 */
	// TODO rename example: first dominate second
	protected boolean isDominant(int[] dist1, int[] dist2) {
		assert dist1.length == dist2.length : "Size of paraters do not match!";

		for (int i = 0; i < dist1.length; i++) {
			if (dist1[i] > dist2[i]) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Check if first parameter is Epsilon Dominant over second. In other words,
	 * check if second argument is dominated by first.
	 * 
	 * @param dist1
	 * @param dist2
	 * @param epsilon
	 * @return
	 */
	// TODO rename example: first dominate second
	protected boolean isEpsilonDominant(int[] dist1, int[] dist2, double epsilon) {
		assert dist1.length == dist2.length : "Size of paraters do not match!";
		
		for (int i = 0; i < dist1.length; i++) {
			int epsilonDistance = (int) Math.round((1d + epsilon) * ((double) dist2[i]));
			
			if (dist1[i] > epsilonDistance) {
				return false;
			}
		}
		
		return true;
	}

	private void insertOrigin(TNode origin) {
		Label<TNode> originLabel = new Label<TNode>(origin, new int[] { 0, 0, 0 }, null);
		Collection<Label<TNode>> originBag = new ArrayList<Label<TNode>>();
		
		queue.add(originLabel);
		originBag.add(originLabel);
		bags.put(origin.getId(), originBag);
	}

	protected long getLabelID() {
		return labelIDGenerator++;
	}

	public ParetoSet<TNode, TEdge> getParetoSet() {
		return paretoSet;
	}
	
	

}
