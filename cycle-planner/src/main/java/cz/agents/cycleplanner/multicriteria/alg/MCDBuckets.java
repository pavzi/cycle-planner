package cz.agents.cycleplanner.multicriteria.alg;

import java.util.Iterator;

import cz.agents.cycleplanner.multicriteria.Label;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

public class MCDBuckets<TNode extends Node, TEdge extends Edge> extends MCD<TNode, TEdge> {

	/**
	 * TODO documentation
	 */
	private final int[] buckets;

	public MCDBuckets(Graph<TNode, TEdge> graph, TNode origin, TNode destination, MCCostFunction<TNode, TEdge> costFunction, int[] buckets) {
		super(graph, origin, destination, costFunction);
		this.buckets = buckets;
	}

	@Override
	public boolean checkDominance(Label<TNode> next) {
		boolean isDominant = true;
		long successorsNodeID = next.getNode().getId();
		int[] successorsCriteria = next.getCostVector();
		int[] successorBucketCriteria = new int[] { bucketValue(successorsCriteria[0], buckets[0]),
				bucketValue(successorsCriteria[1], buckets[1]), bucketValue(successorsCriteria[2], buckets[2]) };

		for (Iterator<Label<TNode>> it = bags.get(successorsNodeID).iterator(); it.hasNext();) {
			Label<TNode> lab = it.next();

			int[] bagBucketCriteria = new int[] { bucketValue(lab.getCostVector()[0], buckets[0]),
					bucketValue(lab.getCostVector()[1], buckets[1]), bucketValue(lab.getCostVector()[2], buckets[2]) };

			if (isDominant(bagBucketCriteria, successorBucketCriteria)) {
				isDominant = false;
				break;
			}

			if (isDominant(successorBucketCriteria, bagBucketCriteria)) {
				it.remove();
				queue.remove(lab);
			}
		}

		return isDominant;
	}

	private int bucketValue(int value, int bucketSize) {

		return value - (value % bucketSize);
	}
}
