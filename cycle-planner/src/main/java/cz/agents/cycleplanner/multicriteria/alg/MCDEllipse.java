package cz.agents.cycleplanner.multicriteria.alg;

import cz.agents.cycleplanner.multicriteria.Label;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import cz.agents.cycleplanner.util.Ellipse;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

public class MCDEllipse<TNode extends Node, TEdge extends Edge> extends MCD<TNode, TEdge> {
	
	/**
	 * TODO documentation
	 */
	private final double aOverB;
	
	/**
	 * TODO documentation
	 */
	private Ellipse ellipse;

	public MCDEllipse(Graph<TNode, TEdge> graph, TNode origin, TNode destination, MCCostFunction<TNode, TEdge> costFunction, double aOverB) {
		super(graph, origin, destination, costFunction);
		this.aOverB = aOverB;
	}

	@Override
	public boolean skipEdge(Label<TNode> next) {
		// Lazy initialization
		// TODO consider correctness of this implementation
		// TODO use initialization in constructor when we decide on stable value of aOverB
		if (ellipse == null) {
			ellipse = new Ellipse(origin.getGpsLocation(), destination.getGpsLocation(), this.aOverB);
		}

		return super.skipEdge(next) || !ellipse.isInside(next.getNode().getGpsLocation());
	}

}
