package cz.agents.cycleplanner.multicriteria.alg;

import java.util.Iterator;

import cz.agents.cycleplanner.multicriteria.Label;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

public class MCDEpsilonDominance<TNode extends Node, TEdge extends Edge> extends MCD<TNode, TEdge> {

	/**
	 * TODO javadoc
	 */
	private final double epsilon;

	public MCDEpsilonDominance(Graph<TNode, TEdge> graph, TNode origin, TNode destination, MCCostFunction<TNode, TEdge> costFunction, double epsilon) {
		super(graph, origin, destination, costFunction);
		this.epsilon = epsilon;
	}

	@Override
	public boolean checkDominance(Label<TNode> next) {

		boolean isDominant = true;
		long successorsNodeID = next.getNode().getId();
		int[] successorsCriteria = next.getCostVector();

		for (Iterator<Label<TNode>> it = bags.get(successorsNodeID).iterator(); it.hasNext();) {
			Label<TNode> lab = it.next();

			if (isEpsilonDominant(lab.getCostVector(), successorsCriteria, epsilon)) {
				isDominant = false;
				break;
			}

			if (isEpsilonDominant(successorsCriteria, lab.getCostVector(), epsilon)) {
				it.remove();
				queue.remove(lab);
			}
		}

		return isDominant;
	}

}
