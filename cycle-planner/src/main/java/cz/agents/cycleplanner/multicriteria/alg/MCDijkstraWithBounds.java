package cz.agents.cycleplanner.multicriteria.alg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.jgrapht.GraphPath;
import org.joda.time.DateTime;

import cz.agents.cycleplanner.aStar.JGraphT.BikeAdvancedTimedGraph;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.exceptions.PlanNotFoundException;
import cz.agents.cycleplanner.multicriteria.Label;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.plannercore.algorithms.DijkstraSimpleGraphSingleGoal;
import eu.superhub.wp5.plannercore.algorithms.goalcheckers.DestinationNodeChecker;
import eu.superhub.wp5.plannercore.structures.base.TimeDependentEdge;
import eu.superhub.wp5.plannercore.structures.evaluators.additionalkeysevaluators.AdditionalKeyEvaluator;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.KeyDistanceTimeVE;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.VertexEvaluator;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedEdge;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

public class MCDijkstraWithBounds<TNode extends Node, TEdge extends TimeDependentEdge> extends MCD<TNode, TEdge> {

	private static final Logger log = Logger.getLogger(MCDijkstraWithBounds.class);

	private Collection<int[]> bounds;

	public MCDijkstraWithBounds(Graph<TNode, TEdge> graph, TNode origin, TNode destination,
			MCCostFunction<TNode, TEdge> costFunction) {
		super(graph, origin, destination, costFunction);

		this.bounds = new ArrayList<int[]>();

		GraphPath<TimedNode, TimedEdge> travelTimeOptimizedGraphPath = planSingleCriterion(graph,
				new TravetTimeCriterion(), origin, destination);
		GraphPath<TimedNode, TimedEdge> comfortQuietnessOptimizedGraphPath = planSingleCriterion(graph,
				new ComfortQuitnessCriterion(), origin, destination);
		GraphPath<TimedNode, TimedEdge> flatnessOptimizedGraphPath = planSingleCriterion(graph,
				new FlatnessCriterion(), origin, destination);

		try {
			List<TEdge> travelTimeOptimizedPath = getCycleEdgePath(graph, travelTimeOptimizedGraphPath);
			int[] travelTimeBound = getBound(travelTimeOptimizedPath, costFunction);

			log.info("Have travel time bound: " + Arrays.toString(travelTimeBound));
			bounds.add(travelTimeBound);
		} catch (PlanNotFoundException e) {
			log.error("No travel time bound was found!", e);
		}
		try {
			List<TEdge> comfortQuietnessOptimizedPath = getCycleEdgePath(graph, comfortQuietnessOptimizedGraphPath);
			int[] comfortQuietnessBound = getBound(comfortQuietnessOptimizedPath, costFunction);

			log.info("Have comfortQuietnessBound: " + Arrays.toString(comfortQuietnessBound));
			bounds.add(comfortQuietnessBound);
		} catch (PlanNotFoundException e) {
			log.error("No comfortQuietness bound was found!", e);
		}
		try {
			List<TEdge> flatnessOptimizedPath = getCycleEdgePath(graph, flatnessOptimizedGraphPath);
			int[] flatnessBound = getBound(flatnessOptimizedPath, costFunction);

			log.info("Have flatness bound: " + Arrays.toString(flatnessBound));
			bounds.add(flatnessBound);
		} catch (PlanNotFoundException e) {
			log.error("No elevation gain bound was found!", e);
		}

	}

	@Override
	public boolean skipEdge(Label<TNode> next) {
		boolean skip = super.skipEdge(next);

		if (!skip) {
			for (int[] bound : bounds) {
				if (isDominant(bound, next.getCostVector()) && !isDominant(next.getCostVector(), bound)) {
					// System.out.println("This one is worse: " +
					// Arrays.toString(next.getCostVector()) +" then " +
					// Arrays.toString(bound));
					return true;
				}
			}

			for (Label<TNode> destinationLabel : destinationBag) {
				if (isDominant(destinationLabel.getCostVector(), next.getCostVector())
						&& !isDominant(next.getCostVector(), destinationLabel.getCostVector())) {
					// System.out.println("This one is worse: " +
					// Arrays.toString(next.getCostVector()) + " then "
					// + Arrays.toString(destinationLabel.getCostVector()));
					return true;
				}
			}

		}

		return skip;
	}

	private GraphPath<TimedNode, TimedEdge> planSingleCriterion(Graph<TNode, TEdge> graph,
			AdditionalKeyEvaluator<TimedNode> costFunction, TNode origin, TNode destination) {

		BikeAdvancedTimedGraph<TNode, TEdge> timedGraph = new BikeAdvancedTimedGraph<TNode, TEdge>(graph, costFunction,
				0d);

		TimedNode originVertex = new TimedNode(origin, new DateTime(), 0);

		VertexEvaluator<TimedNode> vertexEvaluator = new KeyDistanceTimeVE<>();

		DestinationNodeChecker<TimedNode> goalChecker = new DestinationNodeChecker<>(destination.getId());

		DijkstraSimpleGraphSingleGoal<TimedNode, TimedEdge> dijkstra = new DijkstraSimpleGraphSingleGoal<TimedNode, TimedEdge>(
				timedGraph, originVertex, vertexEvaluator, goalChecker);

		return dijkstra.call();
	}

	private List<TEdge> getCycleEdgePath(Graph<TNode, TEdge> graph, GraphPath<TimedNode, TimedEdge> graphPath)
			throws PlanNotFoundException {
		if (graphPath == null || graphPath.getEdgeList().size() < 2) {
			throw new PlanNotFoundException();
		}

		List<TEdge> edges = new ArrayList<TEdge>();

		for (TimedEdge timedEdge : graphPath.getEdgeList()) {
			edges.add((TEdge) graph.getEdgeByEdgeId(timedEdge.getEdge().getEdgeId()));
		}

		return edges;
	}

	private int[] getBound(List<TEdge> path, MCCostFunction<TNode, TEdge> costFunction) {
		int[] bound = new int[3];

		for (TEdge edge : path) {
			int[] edgeCost = costFunction.getCostVector(null, null, edge);

			for (int i = 0; i < edgeCost.length; i++) {
				bound[i] += edgeCost[i];
			}
		}

		return bound;
	}

	class TravetTimeCriterion extends AdditionalKeyEvaluator<TimedNode> {

		@Override
		public double computeAdditionalKey(TimedNode current, Node successor, TimeDependentEdge timeDependentEdge) {
			CycleEdge2 edge = (CycleEdge2) timeDependentEdge;

			return Math.round(current.getAdditionalKey() + edge.getCosts()[0]);
		}
	}

	class ComfortQuitnessCriterion extends AdditionalKeyEvaluator<TimedNode> {

		@Override
		public double computeAdditionalKey(TimedNode current, Node successor, TimeDependentEdge timeDependentEdge) {
			CycleEdge2 edge = (CycleEdge2) timeDependentEdge;
			int comfort = (int) Math.round(edge.getComfortBaseValue() * 10d);
			int quietness = (int) Math.round(edge.getQuietnessBaseValue() * 10d);
			int cost;

			if (comfort != 10 && quietness != 10) {
				if (comfort >= quietness) {
					cost = comfort;
				} else {
					cost = quietness;
				}
			} else if (comfort == 10 && quietness != 10) {
				cost = quietness;
			} else {
				cost = comfort;
			}

			return Math.round(current.getAdditionalKey() + edge.getLengthInMetres() * cost);
		}

	}

	class FlatnessCriterion extends AdditionalKeyEvaluator<TimedNode> {

		@Override
		public double computeAdditionalKey(TimedNode current, Node successor, TimeDependentEdge timeDependentEdge) {
			CycleEdge2 edge = (CycleEdge2) timeDependentEdge;

			return Math.round(current.getAdditionalKey() + edge.getCosts()[3]);
		}

	}

}
