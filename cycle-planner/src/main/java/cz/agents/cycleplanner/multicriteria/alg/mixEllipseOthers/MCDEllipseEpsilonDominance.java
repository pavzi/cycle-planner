package cz.agents.cycleplanner.multicriteria.alg.mixEllipseOthers;

import java.util.Iterator;

import cz.agents.cycleplanner.multicriteria.Label;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import cz.agents.cycleplanner.multicriteria.alg.MCDEllipse;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

public class MCDEllipseEpsilonDominance<TNode extends Node, TEdge extends Edge> extends MCDEllipse<TNode, TEdge> {

	/**
	 * TODO documentation
	 */
	private final double epsilon;

	public MCDEllipseEpsilonDominance(Graph<TNode, TEdge> graph, TNode origin, TNode destination,
			MCCostFunction<TNode, TEdge> costFunction, double aOverB, double epsilon) {
		super(graph, origin, destination, costFunction, aOverB);
		this.epsilon = epsilon;
	}

	@Override
	public boolean checkDominance(Label<TNode> next) {

		boolean isDominant = true;
		long successorsNodeID = next.getNode().getId();
		int[] successorsCriteria = next.getCostVector();

		for (Iterator<Label<TNode>> it = bags.get(successorsNodeID).iterator(); it.hasNext();) {
			Label<TNode> lab = it.next();

			if (isEpsilonDominant(lab.getCostVector(), successorsCriteria, epsilon)) {
				isDominant = false;
				break;
			}

			if (isEpsilonDominant(successorsCriteria, lab.getCostVector(), epsilon)) {
				it.remove();
				queue.remove(lab);
			}
		}

		return isDominant;
	}
}
