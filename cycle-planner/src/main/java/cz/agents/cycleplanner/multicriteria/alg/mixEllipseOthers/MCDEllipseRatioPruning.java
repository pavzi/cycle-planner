package cz.agents.cycleplanner.multicriteria.alg.mixEllipseOthers;

import cz.agents.cycleplanner.multicriteria.Label;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import cz.agents.cycleplanner.multicriteria.alg.MCDEllipse;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

public class MCDEllipseRatioPruning<TNode extends Node, TEdge extends Edge> extends MCDEllipse<TNode, TEdge> {

	/**
	 * TODO javadoc
	 */
	private double alpha = 1.6;

	/**
	 * TODO javadoc
	 */
	private double maxTravelTime;

	/**
	 * TODO javadoc
	 */
	private double minTravelTime;

	/**
	 * TODO javadoc
	 */
	private boolean reachedDestination = false;

	public MCDEllipseRatioPruning(Graph<TNode, TEdge> graph, TNode origin, TNode destination,
			MCCostFunction<TNode, TEdge> costFunction, double aOverB) {
		super(graph, origin, destination, costFunction, aOverB);
	}

	@Override
	public boolean terminationConditon(Label<TNode> current) {

		// compute pruning parameter, setting ratio, only when we first time
		// arrived to destination
		if (!reachedDestination && current.getNode().equals(destination)) {
			reachedDestination = true;
			minTravelTime = current.getCostVector()[0];
			maxTravelTime = Math.ceil(minTravelTime * alpha);
		}

		return reachedDestination && current.getCostVector()[0] > maxTravelTime;
	}

}
