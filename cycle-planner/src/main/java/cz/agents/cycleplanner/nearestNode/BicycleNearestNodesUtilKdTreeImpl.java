package cz.agents.cycleplanner.nearestNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.javaml.core.kdtree.KDTree;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.wp5common.location.Location;

/**
 * Class provides service for obtaining the closest node in a graph using
 * KD-tree data structure.
 * 
 * @author Pavol Zilecky <pavol.zilecky@agents.fel.cvut.cz>
 * 
 */
// TODO use generic Node and Edge
public class BicycleNearestNodesUtilKdTreeImpl {

	private final static Logger log = Logger.getLogger(BicycleNearestNodesUtilKdTreeImpl.class);

	private static final double EPSILON = 10d;

	/**
	 * 
	 */
	private final int numberOfNearestNodes;

	/**
	 * 
	 */
	private final double maxDistanceInMeters;

	/**
	 * 
	 */
	private final KDTree tree;

	public BicycleNearestNodesUtilKdTreeImpl(Graph<CycleNode2, CycleEdge2> cycleGraph, Collection<CycleNode2> contractedNodes, double maxDistanceInMeters,
			int numberOfNearestNodes) {
		this.maxDistanceInMeters = maxDistanceInMeters;
		this.numberOfNearestNodes = numberOfNearestNodes;
		this.tree = new KDTree(2);

		fillTree(cycleGraph, contractedNodes);
	}

	/**
	 * 
	 * Fills tree with nodes taken from graph. Keys in KD-tree are projected
	 * latitude and longitude.
	 * 
	 */
	private void fillTree(Graph<CycleNode2, CycleEdge2> cycleGraph, Collection<CycleNode2> contractedNodes) {

		for (CycleNode2 node : cycleGraph.getAllNodes()) {
			if (node.hasProjectedCoordinates()) {
				tree.insert(new double[] { node.getProjectedLatitude(), node.getProjectedLongitude() }, node);
			} else {
				log.warn("Node " + node + " has not projected coordinates.");
			}
		}
		
		for (CycleNode2 node : contractedNodes) {
			if (node.hasProjectedCoordinates()) {
				tree.insert(new double[] { node.getProjectedLatitude(), node.getProjectedLongitude() }, node);
			} else {
				log.warn("Node " + node + " has not projected coordinates.");
			}
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param location
	 * @param isOrigin
	 * @return
	 */
	public CycleNode2 findNearestNode(Location location) {
		List<CycleNode2> nearestNodes = getNearestNodes(location);

		return nearestNodes.get(0);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param projectedLatitude
	 * @param projectedLongitude
	 * @param isOrigin
	 * @return
	 */
	public List<CycleNode2> getNearestNodes(Location location) {

		double[] key = new double[] { location.getProjectedLatitude(), location.getProjectedLongitude() };

		Object[] searchResult = tree.nearest(key, numberOfNearestNodes);

		// Radius is set to maximum possible value, because at this point we
		// want the closest node without any restriction.
		// List<CycleNode2> nearestNodes =
		// filterByRadius(location.getProjectedLatitude(),
		// location.getProjectedLongitude(), searchResult, maxDistanceInMeters);
		//
		// if (nearestNodes.isEmpty()) {
		// nearestNodes.add((CycleNode2) searchResult[0]);
		// }

		List<CycleNode2> nearestNodes = filterByDistanceToNearestNodePlusEpsilon(location.getProjectedLatitude(),
				location.getProjectedLongitude(), searchResult);
		log.debug("# of final nearest nodes " + nearestNodes.size());

		return nearestNodes;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param projectedLatitude
	 * @param projectedLongitude
	 * @param nearestNodes
	 * @param radiusInMeters
	 * @return
	 */
	private List<CycleNode2> filterByRadius(double projectedLatitude, double projectedLongitude, Object[] nearestNodes,
			double radiusInMeters) {
		List<CycleNode2> nearestNodesInRadius = new ArrayList<CycleNode2>(nearestNodes.length);

		for (Object nearestNode : nearestNodes) {
			CycleNode2 nearestCycleNode2 = (CycleNode2) nearestNode;
			double x = projectedLongitude - nearestCycleNode2.getProjectedLongitude();
			double y = projectedLatitude - nearestCycleNode2.getProjectedLatitude();
			double distance = Math.sqrt(x * x + y * y);
			log.debug("Distance between wanted and found node is " + distance);

			// log.debug(projectedLatitude + "," + projectedLongitude + " -> " +
			// t + " distance " + distance);
			if (distance <= radiusInMeters) {
				nearestNodesInRadius.add(nearestCycleNode2);
			}
		}

		return nearestNodesInRadius;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nearestNodes
	 * @param epsilon
	 * @return
	 */
	private List<CycleNode2> filterByDistanceToNearestNodePlusEpsilon(double projectedLatitude, double projectedLongitude, Object[] nearestNodes) {
		CycleNode2 nearestNode = (CycleNode2) nearestNodes[0];
		double x = projectedLongitude - nearestNode.getProjectedLongitude();
		double y = projectedLatitude - nearestNode.getProjectedLatitude();
		double distanceToNearestNodePlusEpsilon = Math.sqrt(x * x + y * y) + EPSILON;
		List<CycleNode2> nodesEpsilonClosedToNearestNode = new ArrayList<CycleNode2>();

		for (Object node : nearestNodes) {
			CycleNode2 cycleNode = (CycleNode2) node;
			x = projectedLongitude - cycleNode.getProjectedLongitude();
			y = projectedLatitude - cycleNode.getProjectedLatitude();
			double distance = Math.sqrt(x * x + y * y);
			log.debug("Distance between the desired node and node from the set of the nearest nodes is " + distance);

			if (distance <= distanceToNearestNodePlusEpsilon) {
				nodesEpsilonClosedToNearestNode.add(cycleNode);
			}
		}

		return nodesEpsilonClosedToNearestNode;
	}
}
