package cz.agents.cycleplanner.planner;

import java.io.Closeable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.Graph;

public abstract class BicyclePlanner<T> implements Closeable {

	private static final int DEFAULT_NUMBER_OF_THREADS = 8;
	protected static final double AVERAGE_SPEED_KILOMETERS_PER_HOUR = 15d;
	
	private final static Logger log = Logger.getLogger(BicyclePlanner.class);

	protected ExecutorService executorService;

	protected Collection<Integer> closedSetSizes;

	public BicyclePlanner() {
		log.info("Initializing bicycle planner...");
		executorService = Executors.newFixedThreadPool(DEFAULT_NUMBER_OF_THREADS);
		closedSetSizes = new HashSet<Integer>();
	}
	
	public BicyclePlanner(int numberOfThreads) {
		log.info("Initializing bicycle planner...");
		executorService = Executors.newFixedThreadPool(numberOfThreads);
		closedSetSizes = new HashSet<Integer>();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param graph
	 * @param originNodes
	 * @param destinationNodes
	 * @param averageSpeedKMpH
	 * @param profile
	 * @return
	 */
	public abstract Future<T> plan(Graph<CycleNode2, CycleEdge2> graph,
			List<CycleNode2> originNodes, List<CycleNode2> destinationNodes, double averageSpeedKMpH,
			CriteriaWeightVector profile);

	/**
	 * TODO javadoc
	 */
	@Override
	public void close() {
		executorService.shutdown();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public double getAverageClosedSetSize() {
		double sumOfClosedSetSizes = 0d;

		for (int closedSetSize : closedSetSizes) {
			sumOfClosedSetSizes += (double) closedSetSize;
		}

		return sumOfClosedSetSizes / closedSetSizes.size();
	}

}
