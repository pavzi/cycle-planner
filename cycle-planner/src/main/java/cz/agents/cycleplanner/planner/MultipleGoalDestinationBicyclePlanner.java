package cz.agents.cycleplanner.planner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.jgrapht.GraphPath;
import org.joda.time.DateTime;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.aStar.JGraphT.BikeAdvancedTimedGraph;
import cz.agents.cycleplanner.aStar.JGraphT.CriterionAKE;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.routingService.DijkstraSimpleGraphMultipleOriginsAndGoals;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.plannercore.algorithms.goalcheckers.MultiGoalChecker;
import eu.superhub.wp5.plannercore.algorithms.goalcheckers.MultiGoalCheckerUsingID;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.KeyDistanceTimeVE;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.VertexEvaluator;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedEdge;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class MultipleGoalDestinationBicyclePlanner extends BicyclePlanner<Map<Long, GraphPath<TimedNode, TimedEdge>>> {

	public MultipleGoalDestinationBicyclePlanner() {
		super();
	}
	
	public MultipleGoalDestinationBicyclePlanner(int numberOfThreads) {
		super(numberOfThreads);
	}

	@Override
	public Future<Map<Long, GraphPath<TimedNode, TimedEdge>>> plan(Graph<CycleNode2, CycleEdge2> graph,
			List<CycleNode2> originNodes, List<CycleNode2> destinationNodes, double averageSpeedKMpH,
			CriteriaWeightVector profile) {

		BikeAdvancedTimedGraph<CycleNode2, CycleEdge2> timedGraph = new BikeAdvancedTimedGraph<CycleNode2, CycleEdge2>(
				graph, new CriterionAKE(profile), averageSpeedKMpH);

		Collection<TimedNode> startVertices = new ArrayList<TimedNode>();

		for (CycleNode2 origin : originNodes) {
			TimedNode originVertex = new TimedNode(origin, new DateTime(), 0);

			startVertices.add(originVertex);
		}

		VertexEvaluator<TimedNode> vertexEvaluator = new KeyDistanceTimeVE<>();

		Collection<Long> destinationNodesIds = new ArrayList<Long>();

		for (CycleNode2 destination : destinationNodes) {
			destinationNodesIds.add(destination.getId());
		}

		MultiGoalChecker<TimedNode> goalChecker = new MultiGoalCheckerUsingID<TimedNode>(destinationNodesIds);
		DijkstraSimpleGraphMultipleOriginsAndGoals<TimedNode, TimedEdge> dijkstra = new DijkstraSimpleGraphMultipleOriginsAndGoals<TimedNode, TimedEdge>(
				timedGraph, startVertices, vertexEvaluator, goalChecker, closedSetSizes);

		return executorService.submit(dijkstra);
	}

	@Override
	public String toString() {
		return "MultipleGoalDestinationBicyclePlanner";
	}

}
