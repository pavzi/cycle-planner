package cz.agents.cycleplanner.planner;

import java.util.List;
import java.util.concurrent.Future;

import org.jgrapht.GraphPath;
import org.joda.time.DateTime;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.aStar.JGraphT.BikeAdvancedTimedGraph;
import cz.agents.cycleplanner.aStar.JGraphT.CriterionAKE;
import cz.agents.cycleplanner.aStar.JGraphT.HeuristicFunction;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.plannercore.algorithms.AStarSimpleGraph;
import eu.superhub.wp5.plannercore.algorithms.goalcheckers.DestinationNodeChecker;
import eu.superhub.wp5.plannercore.algorithms.heuristics.Heuristic;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.HeuristicVE;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.KeyDistanceTimeVE;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.VertexEvaluator;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedEdge;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class SingleGoalDestinationBicyclePlanner extends BicyclePlanner<GraphPath<TimedNode, TimedEdge>> {

	public SingleGoalDestinationBicyclePlanner() {
		super();
	}

	@Override
	public Future<GraphPath<TimedNode, TimedEdge>> plan(Graph<CycleNode2, CycleEdge2> graph,
			List<CycleNode2> originNodes, List<CycleNode2> destinationNodes, double averageSpeedKMpH,
			CriteriaWeightVector profile) {

		BikeAdvancedTimedGraph<CycleNode2, CycleEdge2> timedGraph = new BikeAdvancedTimedGraph<CycleNode2, CycleEdge2>(
				graph, new CriterionAKE(profile), averageSpeedKMpH);

		Heuristic<TimedNode> heuristic = new HeuristicFunction(destinationNodes.get(0), profile);

		TimedNode originVertex = new TimedNode(originNodes.get(0), new DateTime(), 0);
		originVertex = new TimedNode(originNodes.get(0), new DateTime(), heuristic.getCostToGoalEstimate(originVertex));

		VertexEvaluator<TimedNode> vertexEvaluator = new KeyDistanceTimeVE<>();

		HeuristicVE<TimedNode> comparator = new HeuristicVE<TimedNode>(heuristic, vertexEvaluator);

		DestinationNodeChecker<TimedNode> goalChecker = new DestinationNodeChecker<>(destinationNodes.get(0).getId());

		AStarSimpleGraph<TimedNode, TimedEdge> aStarSimpleGraph = new AStarSimpleGraph<TimedNode, TimedEdge>(
				timedGraph, originVertex, comparator, goalChecker);

		Future<GraphPath<TimedNode, TimedEdge>> future = executorService.submit(aStarSimpleGraph);

		return future;
	}

}
