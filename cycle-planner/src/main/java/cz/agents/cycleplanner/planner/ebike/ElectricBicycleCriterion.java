package cz.agents.cycleplanner.planner.ebike;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.RoadType;
import cz.agents.cycleplanner.util.EnergyConsumption;
import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.plannercore.structures.base.TimeDependentEdge;
import eu.superhub.wp5.plannercore.structures.evaluators.additionalkeysevaluators.AdditionalKeyEvaluator;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

/**
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class ElectricBicycleCriterion extends AdditionalKeyEvaluator<TimedNode> {

	private double averageSpeedMetersPerSecond;

	public ElectricBicycleCriterion(double averageSpeedKMpH) {

		this.averageSpeedMetersPerSecond = averageSpeedKMpH / 3.6;
	}

	@Override
	public double computeAdditionalKey(TimedNode current, Node successor, TimeDependentEdge timeDependentEdge) {
		CycleEdge2 edge = (CycleEdge2) timeDependentEdge;

		double isStep = (edge.getRoadType() == RoadType.STEPS) ? Double.POSITIVE_INFINITY : 0d;

		return current.getAdditionalKey() + isStep + EnergyConsumption.compute(edge, averageSpeedMetersPerSecond);
	}

}
