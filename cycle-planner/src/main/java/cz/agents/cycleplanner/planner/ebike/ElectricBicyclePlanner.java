package cz.agents.cycleplanner.planner.ebike;

import java.util.List;
import java.util.concurrent.Future;

import org.jgrapht.GraphPath;
import org.joda.time.DateTime;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.aStar.JGraphT.BikeAdvancedTimedGraph;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.planner.BicyclePlanner;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.plannercore.algorithms.DijkstraSimpleGraphSingleGoal;
import eu.superhub.wp5.plannercore.algorithms.goalcheckers.DestinationNodeChecker;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.KeyDistanceTimeVE;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.VertexEvaluator;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedEdge;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class ElectricBicyclePlanner extends BicyclePlanner<GraphPath<TimedNode, TimedEdge>> {

	public ElectricBicyclePlanner() {
		super();
	}

	@Override
	public Future<GraphPath<TimedNode, TimedEdge>> plan(Graph<CycleNode2, CycleEdge2> graph,
			List<CycleNode2> originNodes, List<CycleNode2> destinationNodes, double averageSpeedKMpH,
			CriteriaWeightVector profile) {

		CycleNode2 originNode = originNodes.get(0);
		CycleNode2 destinationNode = destinationNodes.get(0);
		BikeAdvancedTimedGraph<CycleNode2, CycleEdge2> timedGraph = new BikeAdvancedTimedGraph<CycleNode2, CycleEdge2>(
				graph, new ElectricBicycleCriterion(averageSpeedKMpH), averageSpeedKMpH);

		TimedNode startVertex = new TimedNode(originNode, new DateTime(), 0);

		VertexEvaluator<TimedNode> vertexEvaluator = new KeyDistanceTimeVE<>();

		DestinationNodeChecker<TimedNode> goalChecker = new DestinationNodeChecker<>(destinationNode.getId());

		DijkstraSimpleGraphSingleGoal<TimedNode, TimedEdge> dijkstraSimpleGraph = new DijkstraSimpleGraphSingleGoal<TimedNode, TimedEdge>(
				timedGraph, startVertex, vertexEvaluator, goalChecker);

		dijkstraSimpleGraph.call();

		Future<GraphPath<TimedNode, TimedEdge>> future = executorService.submit(dijkstraSimpleGraph);

		return future;
	}

}
