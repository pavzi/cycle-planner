package cz.agents.cycleplanner.planner.mc;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Future;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import cz.agents.cycleplanner.multicriteria.MCCycleCostFunction;
import cz.agents.cycleplanner.multicriteria.ParetoSet;
import cz.agents.cycleplanner.multicriteria.alg.AbstractMultiCriteriaDijkstraAlgorithm;
import cz.agents.cycleplanner.multicriteria.alg.MCD;
import cz.agents.cycleplanner.multicriteria.alg.MCDBuckets;
import cz.agents.cycleplanner.multicriteria.alg.MCDEllipse;
import cz.agents.cycleplanner.multicriteria.alg.MCDEpsilonDominance;
import cz.agents.cycleplanner.multicriteria.alg.MCDijkstraWithBounds;
import cz.agents.cycleplanner.multicriteria.alg.MCDijkstraWithBoundsBuckets;
import cz.agents.cycleplanner.multicriteria.alg.MCDijkstraWithBoundsEllipse;
import cz.agents.cycleplanner.multicriteria.alg.MCDijkstraWithBoundsEllipseBuckets;
import cz.agents.cycleplanner.multicriteria.alg.MCDijkstraWithBoundsEllipseEpsilonDominance;
import cz.agents.cycleplanner.multicriteria.alg.MCDijkstraWithBoundsEpsilonDominance;
import cz.agents.cycleplanner.multicriteria.alg.mixEllipseOthers.MCDEllipseBuckets;
import cz.agents.cycleplanner.multicriteria.alg.mixEllipseOthers.MCDEllipseEpsilonDominance;
import cz.agents.cycleplanner.multicriteria.alg.mixEllipseRatioPruningOthers.MCDEllipseRatioPruningEpsilonDominance;
import cz.agents.cycleplanner.planner.BicyclePlanner;
import eu.superhub.wp5.graphcommon.graph.Graph;

public class MultiCriteriaBicyclePlanner extends BicyclePlanner<ParetoSet<CycleNode2, CycleEdge2>> {

	// private static final Logger log =
	// Logger.getLogger(MultiCriteriaBicyclePlanner.class);

	private SupportedMCAlgorithms mcAlgorithm;

	// Default values for algorithm parameters
	private int gamma = 5;
	private double aOverB = 1.25;
	private double alpha = 1.6;
	private double epsilon = .05;
	private int[] buckets = new int[] { 15, 2500, 4 };

	private MCCostFunction<CycleNode2, CycleEdge2> costFunction;

	public MultiCriteriaBicyclePlanner() {
		super();
		this.mcAlgorithm = SupportedMCAlgorithms.MC_DIJKSTRA_ELLIPSE_RATIO_EPSILON;
		costFunction = new MCCycleCostFunction();
	}

	public MultiCriteriaBicyclePlanner(int numberOfThreads) {
		super(numberOfThreads);
		this.mcAlgorithm = SupportedMCAlgorithms.MC_DIJKSTRA_ELLIPSE_RATIO_EPSILON;
		costFunction = new MCCycleCostFunction();
	}

	public MultiCriteriaBicyclePlanner(SupportedMCAlgorithms mcAlgorithm) {
		super();
		this.mcAlgorithm = mcAlgorithm;
		costFunction = new MCCycleCostFunction();
	}

	public MultiCriteriaBicyclePlanner(int numberOfThreads, SupportedMCAlgorithms mcAlgorithm) {
		super(numberOfThreads);
		this.mcAlgorithm = mcAlgorithm;
		costFunction = new MCCycleCostFunction();
	}

	public MultiCriteriaBicyclePlanner(int numberOfThreads, SupportedMCAlgorithms mcAlgorithm, Integer gamma,
			Double aOverB, Double alpha, Double epsilon, int[] buckets,
			MCCostFunction<CycleNode2, CycleEdge2> costFunction) {
		super(numberOfThreads);
		this.mcAlgorithm = mcAlgorithm;

		if (gamma != null) {
			this.gamma = gamma;
		}

		if (aOverB != null) {
			this.aOverB = aOverB;
		}

		if (alpha != null) {
			this.alpha = alpha;
		}

		if (epsilon != null) {
			this.epsilon = epsilon;
		}

		if (buckets != null) {
			this.buckets = buckets;
		}

		this.costFunction = costFunction;
	}

	@Override
	public Future<ParetoSet<CycleNode2, CycleEdge2>> plan(Graph<CycleNode2, CycleEdge2> graph,
			List<CycleNode2> originNodes, List<CycleNode2> destinationNodes, double averageSpeedKMpH,
			CriteriaWeightVector profile) {

		CycleNode2 originNode = originNodes.get(0);
		CycleNode2 destinationNode = destinationNodes.get(0);
		AbstractMultiCriteriaDijkstraAlgorithm<CycleNode2, CycleEdge2> mlc = getMCInstance(graph, originNode,
				destinationNode);

		Future<ParetoSet<CycleNode2, CycleEdge2>> future = executorService.submit(mlc);

		return future;
	}

	private AbstractMultiCriteriaDijkstraAlgorithm<CycleNode2, CycleEdge2> getMCInstance(
			Graph<CycleNode2, CycleEdge2> graph, CycleNode2 origin, CycleNode2 destination) {

		AbstractMultiCriteriaDijkstraAlgorithm<CycleNode2, CycleEdge2> mlc;

		switch (mcAlgorithm) {
		case MC_DIJKSTRA:
			mlc = new MCD<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction);
			break;
		case MC_DIJKSTRA_BOUNDED:
			mlc = new MCDijkstraWithBounds<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction);
			break;
		case MC_DIJKSTRA_BOUNDED_EPSILON:
			mlc = new MCDijkstraWithBoundsEpsilonDominance<CycleNode2, CycleEdge2>(graph, origin, destination,
					costFunction, epsilon);
			break;
		case MC_DIJKSTRA_BOUNDED_BUCKETS:
			mlc = new MCDijkstraWithBoundsBuckets<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction,
					buckets);
			break;
		case MC_DIJKSTRA_EPSILON:
			mlc = new MCDEpsilonDominance<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction, epsilon);
			break;
		case MC_DIJKSTRA_BUCKETS:
			mlc = new MCDBuckets<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction, buckets);
			break;
		case MC_DIJKSTRA_ELLIPSE:
			mlc = new MCDEllipse<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction, aOverB);
			break;
		case MC_DIJKSTRA_ELLIPSE_BUCKETS:
			mlc = new MCDEllipseBuckets<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction, aOverB,
					buckets);
			break;
		case MC_DIJKSTRA_ELLIPSE_EPSILON:
			mlc = new MCDEllipseEpsilonDominance<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction,
					aOverB, epsilon);
			break;
		case MC_DIJKSTRA_BOUNDED_ELLIPSE:
			mlc = new MCDijkstraWithBoundsEllipse<CycleNode2, CycleEdge2>(graph, origin, destination, costFunction,
					aOverB);
			break;
		case MC_DIJKSTRA_BOUNDED_ELLIPSE_BUCKETS:
			mlc = new MCDijkstraWithBoundsEllipseBuckets<CycleNode2, CycleEdge2>(graph, origin, destination,
					costFunction, aOverB, buckets);
			break;
		case MC_DIJKSTRA_BOUNDED_ELLIPSE_EPSILON:
			mlc = new MCDijkstraWithBoundsEllipseEpsilonDominance<CycleNode2, CycleEdge2>(graph, origin, destination,
					costFunction, aOverB, epsilon);
			break;
		case MC_DIJKSTRA_ELLIPSE_RATIO_EPSILON:
		default:
			mlc = new MCDEllipseRatioPruningEpsilonDominance<CycleNode2, CycleEdge2>(graph, origin, destination,
					costFunction, aOverB);
			break;
		}

		return mlc;
	}

	public SupportedMCAlgorithms getMCAlgorithm() {
		return mcAlgorithm;
	}

	public MCCostFunction<CycleNode2, CycleEdge2> getCostFunction() {
		return costFunction;
	}

	@Override
	public String toString() {
		return mcAlgorithm + "_gamma=" + gamma + "_aOverB=" + aOverB + "_alpha=" + alpha + "_epsilon=" + epsilon
				+ "_buckets=" + Arrays.toString(buckets).replace(" ", "") + "_costFunction="
				+ costFunction.getClass().getSimpleName();
	}

}
