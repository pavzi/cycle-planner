package cz.agents.cycleplanner.planner.mc;

public enum SupportedMCAlgorithms {
	MC_DIJKSTRA, 
	MC_DIJKSTRA_ELLIPSE, 
	MC_DIJKSTRA_EPSILON, 
	MC_DIJKSTRA_BUCKETS, 
	MC_DIJKSTRA_BOUNDED,
	MC_DIJKSTRA_BOUNDED_BUCKETS,
	MC_DIJKSTRA_BOUNDED_EPSILON,
	MC_DIJKSTRA_ELLIPSE_EPSILON, 
	MC_DIJKSTRA_ELLIPSE_RATIO_EPSILON, 
	MC_DIJKSTRA_ELLIPSE_BUCKETS,
	MC_DIJKSTRA_BOUNDED_ELLIPSE,
	MC_DIJKSTRA_BOUNDED_ELLIPSE_BUCKETS,
	MC_DIJKSTRA_BOUNDED_ELLIPSE_EPSILON
}
