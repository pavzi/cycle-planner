package cz.agents.cycleplanner.plannernew;

import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;
import cz.agents.planningalgorithms.multicriteria.MCCostFunction;

public class CycleMultiCriteriaCostFunction implements MCCostFunction<CycleNode, CycleEdge> {

	@Override
	public int[] getCostVector(CycleNode current, CycleNode next, CycleEdge edge) {
		// TODO use constants instead of numbers
		int[] costVector = { edge.getCostVector()[0], edge.getCostVector()[4],
				edge.getCostVector()[5] };

		return costVector;
	}

}
