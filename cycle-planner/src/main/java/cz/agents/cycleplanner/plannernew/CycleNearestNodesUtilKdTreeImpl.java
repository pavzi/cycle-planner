package cz.agents.cycleplanner.plannernew;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.sf.javaml.core.kdtree.KDTree;

import org.apache.log4j.Logger;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.Graph;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

/**
 * Class provides service for obtaining the closest node in a graph using
 * KD-tree data structure.
 * 
 * @author Pavol Zilecky <pavol.zilecky@agents.fel.cvut.cz>
 * 
 */
// TODO use generic Node and Edge
public class CycleNearestNodesUtilKdTreeImpl {

	private final static Logger log = Logger.getLogger(CycleNearestNodesUtilKdTreeImpl.class);

	private static final double EPSILON = 10d;

	/**
	 * 
	 */
	private final int numberOfNearestNodes;

	/**
	 * 
	 */
	private final double maxDistanceInMeters;

	/**
	 * 
	 */
	private final KDTree tree;

	public CycleNearestNodesUtilKdTreeImpl(Graph<CycleNode, CycleEdge> cycleGraph,
			Collection<CycleNode> contractedNodes, double maxDistanceInMeters,
			int numberOfNearestNodes) {
		this.maxDistanceInMeters = maxDistanceInMeters;
		this.numberOfNearestNodes = numberOfNearestNodes;
		this.tree = new KDTree(2);

		fillTree(cycleGraph, contractedNodes);
	}

	/**
	 * 
	 * Fills tree with nodes taken from graph. Keys in KD-tree are projected
	 * latitude and longitude.
	 * 
	 */
	private void fillTree(Graph<CycleNode, CycleEdge> cycleGraph,
			Collection<CycleNode> contractedNodes) {

		for (CycleNode node : cycleGraph.getAllNodes()) {
			tree.insert(new double[] { node.latProjected, node.lonProjected }, node);
		}

		for (CycleNode node : contractedNodes) {
			tree.insert(new double[] { node.latProjected, node.lonProjected }, node);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param projectedLatitude
	 * @param projectedLongitude
	 * @param isOrigin
	 * @return
	 */
	public List<CycleNode> getNearestNodes(GPSLocation location) {

		double[] key = new double[] { location.latProjected, location.lonProjected };

		Object[] searchResult = tree.nearest(key, numberOfNearestNodes);

		// Radius is set to maximum possible value, because at this point we
		// want the closest node without any restriction.
		// List<CycleNode> nearestNodes =
		// filterByRadius(location.getProjectedLatitude(),
		// location.getProjectedLongitude(), searchResult, maxDistanceInMeters);
		//
		// if (nearestNodes.isEmpty()) {
		// nearestNodes.add((CycleNode) searchResult[0]);
		// }

		List<CycleNode> nearestNodes = filterByDistanceToNearestNodePlusEpsilon(
				location.latProjected, location.lonProjected, searchResult);
		log.debug("# of final nearest nodes " + nearestNodes.size());

		return nearestNodes;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param projectedLatitude
	 * @param projectedLongitude
	 * @param nearestNodes
	 * @param radiusInMeters
	 * @return
	 */
	private List<CycleNode> filterByRadius(double projectedLatitude, double projectedLongitude,
			Object[] nearestNodes, double radiusInMeters) {
		List<CycleNode> nearestNodesInRadius = new ArrayList<CycleNode>(nearestNodes.length);

		for (Object nearestNode : nearestNodes) {
			CycleNode nearestCycleNode2 = (CycleNode) nearestNode;
			double x = projectedLongitude - nearestCycleNode2.lonProjected;
			double y = projectedLatitude - nearestCycleNode2.latProjected;
			double distance = Math.sqrt(x * x + y * y);
			log.debug("Distance between wanted and found node is " + distance);

			// log.debug(projectedLatitude + "," + projectedLongitude + " -> " +
			// t + " distance " + distance);
			if (distance <= radiusInMeters) {
				nearestNodesInRadius.add(nearestCycleNode2);
			}
		}

		return nearestNodesInRadius;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nearestNodes
	 * @param epsilon
	 * @return
	 */
	private List<CycleNode> filterByDistanceToNearestNodePlusEpsilon(double projectedLatitude,
			double projectedLongitude, Object[] nearestNodes) {
		CycleNode nearestNode = (CycleNode) nearestNodes[0];
		double x = projectedLongitude - nearestNode.lonProjected;
		double y = projectedLatitude - nearestNode.latProjected;
		double distanceToNearestNodePlusEpsilon = Math.sqrt(x * x + y * y) + EPSILON;
		List<CycleNode> nodesEpsilonClosedToNearestNode = new ArrayList<CycleNode>();

		for (Object node : nearestNodes) {
			CycleNode cycleNode = (CycleNode) node;
			x = projectedLongitude - cycleNode.lonProjected;
			y = projectedLatitude - cycleNode.latProjected;
			double distance = Math.sqrt(x * x + y * y);
			log.debug("Distance between the desired node and node from the set of the nearest nodes is "
					+ distance);

			if (distance <= distanceToNearestNodePlusEpsilon) {
				nodesEpsilonClosedToNearestNode.add(cycleNode);
			}
		}

		return nodesEpsilonClosedToNearestNode;
	}
}
