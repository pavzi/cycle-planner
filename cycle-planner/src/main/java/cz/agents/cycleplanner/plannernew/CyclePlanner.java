package cz.agents.cycleplanner.plannernew;

import java.io.Closeable;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 * @param <T> 
 */
public abstract class CyclePlanner<T> implements Closeable {

	private static final int DEFAULT_NUMBER_OF_THREADS = 8;
	protected static final double AVERAGE_SPEED_KILOMETERS_PER_HOUR = 15d;

	private final static Logger log = Logger.getLogger(CyclePlanner.class);

	protected ExecutorService executorService;

	protected Collection<Integer> closedSetSizes;

	public CyclePlanner() {
		log.info("Initializing bicycle planner...");
		executorService = Executors.newFixedThreadPool(DEFAULT_NUMBER_OF_THREADS);
		closedSetSizes = new HashSet<Integer>();
	}

	public CyclePlanner(int numberOfThreads) {
		log.info("Initializing bicycle planner...");
		executorService = Executors.newFixedThreadPool(numberOfThreads);
		closedSetSizes = new HashSet<Integer>();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param graph
	 * @param originNodes
	 * @param destinationNodes
	 * @param averageSpeedKMpH
	 * @param profile
	 * @return
	 */
	// public abstract Future<T> plan(Graph<CycleNode, CycleEdge> graph,
	// List<CycleNode> originNodes,
	// List<CycleNode> destinationNodes, double averageSpeedKMpH,
	// CriteriaWeightVector profile);

	public abstract Future<T> plan(PlanningInstance planningInstance);

	/**
	 * TODO javadoc
	 */
	@Override
	public void close() {
		executorService.shutdown();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public double getAverageClosedSetSize() {
		double sumOfClosedSetSizes = 0d;

		for (int closedSetSize : closedSetSizes) {
			sumOfClosedSetSizes += (double) closedSetSize;
		}

		return sumOfClosedSetSizes / closedSetSizes.size();
	}

}
