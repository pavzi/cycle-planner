package cz.agents.cycleplanner.plannernew;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.log4j.Logger;

import cz.agents.basestructures.Zone;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

/**
 * 
 * Provider for <code>Zone</code> object containing cycle graph
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum CycleZoneProvider {
	INSTANCE;

	private final Logger log = Logger.getLogger(CycleZoneProviderTest.class);

	private HashMap<String, Zone<CycleNode, CycleEdge>> zones;

	private CycleZoneProvider() {
		zones = new HashMap<String, Zone<CycleNode, CycleEdge>>();

		try {
			Collection<InputStream> zonesAsStream = getCycleZonesAsStream();

			for (InputStream zoneInputStream : zonesAsStream) {

				ObjectInputStream ois = new ObjectInputStream(zoneInputStream);
				@SuppressWarnings("unchecked")
				Zone<CycleNode, CycleEdge> cycleZone = (Zone<CycleNode, CycleEdge>) ois
						.readObject();

				zones.put(cycleZone.name, cycleZone);

				log.info("City name: " + cycleZone.name);
				log.info("# of nodes: " + cycleZone.graph.getAllNodes().size());
				log.info("# of edges: " + cycleZone.graph.getAllEdges().size());

				zoneInputStream.close();
			}
		} catch (URISyntaxException | IOException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * Returns <code>Zone</code> requested by name if exists in
	 * <code>CycleZoneProvider</code>
	 * 
	 * @param zoneName
	 *            name of the required <code>Zone</code>
	 * @return <code>Zone</code> if exists under specified <code>zoneName</code>
	 *         , otherwise null
	 */
	public Zone<CycleNode, CycleEdge> getZone(String zoneName) {
		if (zones.containsKey(zoneName)) {
			return zones.get(zoneName);
		}

		return null;
	}

	/**
	 *
	 * Looks for city inside which lie specified geographical points (origin and
	 * destination) and returns cycle data for that city
	 *
	 * @param originLatE6
	 *            origin latitude in integer form
	 * @param originLonE6
	 *            origin longitude in integer form
	 * @param destinationLatE6
	 *            destination latitude in integer form
	 * @param destinationLonE6
	 *            destination longitude in integer form
	 * @return cycle data for city in which specified points lie
	 * @throws OutOfBoundsException
	 *             when one of the points does not lie in any city or cities are
	 *             different for origin and destination
	 */
	public Zone<CycleNode, CycleEdge> getZone(int originLatE6, int originLonE6,
			int destinationLatE6, int destinationLonE6) throws OutOfBoundsException {

		Zone<CycleNode, CycleEdge> originZone = getZone(originLatE6, originLonE6);
		Zone<CycleNode, CycleEdge> destinationZone = getZone(destinationLatE6,
				destinationLonE6);

		// if latitude and longitude for origin and destination are not in
		// bounding box of any city, then return status OUT_OF_BOUND
		if (!originZone.name.equals(destinationZone.name)) {
			throw new OutOfBoundsException();
		}

		return originZone;
	}

	/**
	 *
	 * Looks for zone inside which lies a specified geographical location
	 *
	 * @param latE6
	 *            location's latitude in integer form
	 * @param lonE6
	 *            location's longitude in integer form
	 * @return zone in which specified location is situated
	 * @throws OutOfBoundsException
	 *             when location is not situated in any zone
	 */
	public Zone<CycleNode, CycleEdge> getZone(int latE6, int lonE6)
			throws OutOfBoundsException {
		for (Zone<CycleNode, CycleEdge> cycleZone : zones.values()) {
			if (cycleZone.boundingBox.inside(lonE6, latE6)) {
				return cycleZone;
			}
		}

		throw new OutOfBoundsException();
	}

	/**
	 * Loads all <code>ser</code> files from resource folder returns link to
	 * them as <code>InputStream</code> objects. These files contains serialized
	 * <code>Zone</code> objects.
	 * 
	 * @return <code>Collection</code> of <code>InputStream</code> objects to
	 *         <code>ser</code> files
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private Collection<InputStream> getCycleZonesAsStream() throws URISyntaxException, IOException {

		final Collection<InputStream> streams = new ArrayList<>();
		final File jarFile = new File(CycleZoneProviderTest.class.getProtectionDomain()
				.getCodeSource().getLocation().getPath());
		log.info("Resource directory (jar archive): " + jarFile);

		if (jarFile.isFile()) { // Run with JAR file
			final JarFile jar = new JarFile(jarFile);
			// gives ALL entries in jar
			final Enumeration<JarEntry> entries = jar.entries();

			while (entries.hasMoreElements()) {
				final String name = entries.nextElement().getName();

				// filter only serialized city data (files with suffix ser)
				if (name.matches(".*.ser")) {
					log.info("city data file: " + name);
					streams.add(CycleZoneProviderTest.class.getResourceAsStream("/" + name));
				}
			}

			jar.close();

		} else { // Run in IDE
			for (File file : jarFile.listFiles()) {
				// filter only serialized city data (files with suffix ser)
				if (file.getName().matches(".*.ser")) {
					log.info("city data file: " + file.getName());
					streams.add(CycleZoneProviderTest.class.getResourceAsStream("/"
							+ file.getName()));
				}

			}

		}

		return streams;
	}

}
