package cz.agents.cycleplanner.plannernew;

import java.util.concurrent.Future;

import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;
import cz.agents.planningalgorithms.multicriteria.AbstractMls;
import cz.agents.planningalgorithms.multicriteria.MCCostFunction;
import cz.agents.planningalgorithms.multicriteria.MlsEllipseEpsilonDominance;
import cz.agents.planningalgorithms.multicriteria.ParetoSet;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class MultiCriteriaCyclePlanner extends CyclePlanner<ParetoSet<CycleNode, CycleEdge>> {

	// private static final Logger log =
	// Logger.getLogger(MultiCriteriaBicyclePlanner.class);

	// Default values for algorithm parameters
	private int gamma = 5;
	private double aOverB = 1.25;
	private double alpha = 1.6;
	private double epsilon = .5;
	private int[] buckets = new int[] { 15, 2500, 4 };

	private MCCostFunction<CycleNode, CycleEdge> costFunction;

	public MultiCriteriaCyclePlanner() {
		super();
		costFunction = new CycleMultiCriteriaCostFunction();
	}

	public MultiCriteriaCyclePlanner(int numberOfThreads) {
		super(numberOfThreads);
		costFunction = new CycleMultiCriteriaCostFunction();
	}

	public MultiCriteriaCyclePlanner(int numberOfThreads, Integer gamma, Double aOverB,
			Double alpha, Double epsilon, int[] buckets,
			MCCostFunction<CycleNode, CycleEdge> costFunction) {
		super(numberOfThreads);

		if (gamma != null) {
			this.gamma = gamma;
		}

		if (aOverB != null) {
			this.aOverB = aOverB;
		}

		if (alpha != null) {
			this.alpha = alpha;
		}

		if (epsilon != null) {
			this.epsilon = epsilon;
		}

		if (buckets != null) {
			this.buckets = buckets;
		}

		this.costFunction = costFunction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Future<ParetoSet<CycleNode, CycleEdge>> plan(PlanningInstance planningInstance) {

		CycleNode originNode = planningInstance.origins.get(0);
		CycleNode destinationNode = planningInstance.destinations.get(0);
		AbstractMls<CycleNode, CycleEdge> mls = new MlsEllipseEpsilonDominance<CycleNode, CycleEdge>(
				planningInstance.graph, originNode, destinationNode, costFunction, aOverB, epsilon);
		Future<ParetoSet<CycleNode, CycleEdge>> future = executorService.submit(mls);

		return future;
	}

}
