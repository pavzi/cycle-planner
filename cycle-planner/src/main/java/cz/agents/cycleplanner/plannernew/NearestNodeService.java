package cz.agents.cycleplanner.plannernew;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import cz.agents.basestructures.GPSLocation;
import cz.agents.basestructures.Zone;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;
import cz.agents.geotools.EPSGProjection;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public enum NearestNodeService {
	INSTANCE;

	private final static int MAX_DISTANCE_FROM_DESIRED_POINT = 100;
	private final static int MAX_NUMBER_OF_NEAREST_NODES = 5;

	/**/
	private Map<String, CycleNearestNodesUtilKdTreeImpl> kdTrees;

	/**/
	private EPSGProjection projection;

	private NearestNodeService() {

		try {
			projection = new EPSGProjection(2065);
		} catch (FactoryException | TransformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		kdTrees = new HashMap<String, CycleNearestNodesUtilKdTreeImpl>();
	}

	/**
	 * 
	 * Returns nearest node in graph to specified location
	 * 
	 * @param latE6
	 *            location's latitude in integer form
	 * @param lonE6
	 *            location's longitude in integer form
	 * @param zoneName
	 *            <code>Zone</code> where to look for nearest node to specified
	 *            location
	 * @return
	 */
	public CycleNode getNearestNode(String zoneName, int latE6, int lonE6) {
		List<CycleNode> nearestNodes = getNearestNodes(zoneName, latE6, lonE6);

		return nearestNodes.get(0);
	}

	/**
	 * 
	 * Returns nearest nodes in graph to specified location
	 * 
	 * @param latE6
	 *            location's latitude in integer form
	 * @param lonE6
	 *            location's longitude in integer form
	 * @param zoneName
	 *            <code>Zone</code> where to look for nearest nodes to specified
	 *            location
	 * @return
	 */
	public List<CycleNode> getNearestNodes(String zoneName, int latE6, int lonE6) {
		GPSLocation location = projection.getProjectedGPSLocation(latE6, lonE6, 0);

		if (!kdTrees.containsKey(zoneName)) {
			initZoneNearestNodeUtil(zoneName);
		}

		CycleNearestNodesUtilKdTreeImpl cycleNearestNodesUtilKdTreeImpl = kdTrees.get(zoneName);
		List<CycleNode> nearestNodes = cycleNearestNodesUtilKdTreeImpl.getNearestNodes(location);

		return nearestNodes;
	}

	private void initZoneNearestNodeUtil(String zoneName) {
		Zone<CycleNode, CycleEdge> zone = CycleZoneProvider.INSTANCE.getZone(zoneName);
		CycleNearestNodesUtilKdTreeImpl cycleNearestNodesUtilKdTreeImpl = new CycleNearestNodesUtilKdTreeImpl(
				zone.graph, new HashSet<CycleNode>(), MAX_DISTANCE_FROM_DESIRED_POINT,
				MAX_NUMBER_OF_NEAREST_NODES);

		kdTrees.put(zoneName, cycleNearestNodesUtilKdTreeImpl);
	}
}
