package cz.agents.cycleplanner.plannernew;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import cz.agents.basestructures.Graph;
import cz.agents.basestructures.Node;
import cz.agents.cycleplanner.api.v3.datamodel.BoundingBox;
import cz.agents.cycleplanner.api.v3.datamodel.Coordinate;
import cz.agents.cycleplanner.api.v3.datamodel.Criteria;
import cz.agents.cycleplanner.api.v3.datamodel.Plan;
import cz.agents.cycleplanner.api.v3.datamodel.PlanStep;
import cz.agents.cycleplanner.api.v3.datamodel.SpecialCoordinateType;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;
import cz.agents.cyclestructures.RoadType;
import cz.agents.cyclestructures.Surface;
import cz.agents.geotools.AngleUtil;
import cz.agents.geotools.EdgeUtil;
import cz.agents.planningalgorithms.multicriteria.Path;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class PlanFactory {

	public static double AVERAGE_GRAPH_CRUISING_SPEED_METERS_PER_SECOND = 4.2;

	// private final long id;
	private int minLatE6;
	private int maxLatE6;
	private int minLonE6;
	private int maxLonE6;
	private double length;
	private double elevationGain;
	private double elevationDrop;
	private List<PlanStep> steps;

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param id
	 * @param graph
	 * @param additionalNodes
	 * @param originNodeId
	 * @param waypointNodeIds
	 * @param path
	 * @param averageSpeedKmPH
	 * @return
	 */
	public Plan getPlan(long id, Graph<CycleNode, CycleEdge> graph,
			Map<Long, CycleNode> additionalNodes, int originNodeId,
			Collection<Integer> waypointNodeIds, Path<CycleNode, CycleEdge> path,
			double averageSpeedKmPH) {
		// this.averageSpeedMetersPerSecond = averageSpeedKmPH / 3.6;
		init();

		CycleNode previous = null;

		for (CycleEdge cycleEdge : path.getSequenceOfEdges()) {

			CycleNode fromNode = getNode(cycleEdge.fromId, graph, additionalNodes);

			for (CycleNode via : cycleEdge.getViaNodes()) {

				addPlanStep(previous, fromNode, via, cycleEdge, originNodeId, waypointNodeIds);

				previous = fromNode;
				fromNode = via;
			}

			CycleNode toNode = getNode(cycleEdge.toId, graph, additionalNodes);

			addPlanStep(previous, fromNode, toNode, cycleEdge, originNodeId, waypointNodeIds);

			previous = fromNode;
		}

		CycleEdge lastEdge = path.getSequenceOfEdges().get(path.getSequenceOfEdges().size() - 1);
		CycleNode destinationNode = getNode(lastEdge.toId, graph, additionalNodes);
		Coordinate destinationCoordinate = new Coordinate(destinationNode.latE6,
				destinationNode.lonE6, destinationNode.elevation, SpecialCoordinateType.DESTINATION);
		PlanStep lastStep = new PlanStep(destinationCoordinate, 0, 0, 0, null, null, null, null);

		updateMinMaxLatLon(destinationNode.latE6, destinationNode.lonE6);
		steps.add(lastStep);

		BoundingBox boundingBox = new BoundingBox(new Coordinate(maxLatE6, minLonE6),
				new Coordinate(minLatE6, maxLonE6));
		Criteria criteria = new Criteria(path.getCostVector()[0], path.getCostVector()[1],
				path.getCostVector()[2]);

		Plan plan = new Plan(id, boundingBox, round(length), round(elevationGain),
				round(elevationDrop), criteria, steps);

		return plan;
	}

	private void init() {
		minLatE6 = Integer.MAX_VALUE;
		maxLatE6 = Integer.MIN_VALUE;
		minLonE6 = Integer.MAX_VALUE;
		maxLonE6 = Integer.MIN_VALUE;
		length = 0;
		elevationGain = 0;
		elevationDrop = 0;
		steps = new ArrayList<PlanStep>();
	}

	private void addPlanStep(Node previous, Node from, Node to, CycleEdge cycleEdge,
			int originNodeId, Collection<Integer> waypointNodeIds) {
		Coordinate fromCoordinate = getCoordinate(from, originNodeId, waypointNodeIds);

		updateMinMaxLatLon(from.latE6, from.lonE6);
		addElevation(from, to);
		int length = addLength(from, to);
		int angle = getAngle(previous, from, to);
		PlanStep planStep = getPlanStep(fromCoordinate, cycleEdge, length, angle);
		steps.add(planStep);
	}

	private Coordinate getCoordinate(Node node, int originNodeId,
			Collection<Integer> waypointNodeIds) {

		// TODO Check efficiency, for each node look to two lists
		if (originNodeId == node.id) {
			return new Coordinate(node.latE6, node.lonE6, node.elevation,
					SpecialCoordinateType.ORIGIN);
		} else if (waypointNodeIds.contains(node.id)) {
			return new Coordinate(node.latE6, node.lonE6, node.elevation,
					SpecialCoordinateType.WAYPOINT);
		}

		return new Coordinate(node.latE6, node.lonE6, node.elevation);
	}

	private void addElevation(Node from, Node to) {
		double elevation = to.elevation - from.elevation;
		double drops = (elevation > 0) ? 0 : -elevation;
		double rises = (elevation < 0) ? 0 : elevation;
		elevationGain += rises;
		elevationDrop += drops;
	}

	private int addLength(Node from, Node to) {
		double lengthInMeters = EdgeUtil.computeEuclideanDistance(from, to);
		length += lengthInMeters;

		return round(lengthInMeters);
	}

	private int getAngle(Node previous, Node from, Node to) {
		// Compute angle between previous and current edge
		int angle = 0;

		if (previous != null) {
			angle = round(AngleUtil.getAngle(previous, from, to));
		}

		return angle;
	}

	private PlanStep getPlanStep(Coordinate coordinate, CycleEdge cycleEdge, int lengthInMeters,
			int angle) {

		double travelTime = 0; // TODO interpolate it from total travelTime
								// (costVector[0])

		Surface surface = cycleEdge.getSurface();
		RoadType roadType = cycleEdge.getRoadType();
		String streetName = cycleEdge.getStreetName();
		String bicycleRouteNumber = cycleEdge.getBicycleRouteNumber();
		PlanStep planStep = new PlanStep(coordinate, lengthInMeters, round(travelTime), angle,
				surface, roadType, streetName, bicycleRouteNumber);

		return planStep;
	}

	private CycleNode getNode(int nodeId, Graph<CycleNode, CycleEdge> graph,
			Map<Long, CycleNode> additionalNodes) {
		if (graph.containsNodeByNodeId(nodeId)) {
			return graph.getNodeByNodeId(nodeId);
		}

		return additionalNodes.get(nodeId);
	}

	private int round(double d) {
		return (int) Math.round(d);
	}

	private void updateMinMaxLatLon(int latE6, int lonE6) {
		minLatE6 = (minLatE6 > latE6) ? latE6 : minLatE6;
		maxLatE6 = (maxLatE6 < latE6) ? latE6 : maxLatE6;
		minLonE6 = (minLonE6 > lonE6) ? lonE6 : minLonE6;
		maxLonE6 = (maxLonE6 < lonE6) ? lonE6 : maxLonE6;
	}

}
