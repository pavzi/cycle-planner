package cz.agents.cycleplanner.plannernew;

import java.util.List;

import cz.agents.basestructures.Graph;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

/**
 * TODO rename TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class PlanningInstance {

	// /**
	// *
	// */
	// private final String cityName;

	/**
	 * 
	 */
	public final Graph<CycleNode, CycleEdge> graph;

	/**
	 * 
	 */
	public final List<CycleNode> origins;

	/**
	 * 
	 */
	public final List<List<CycleNode>> waypoints;

	/**
	 * 
	 */
	public final List<CycleNode> destinations;

	// /**
	// *
	// */
	// private final Map<Profiles, CriteriaWeightVector> criteriaWeightVectors;

	/**
	 * Average cruising speed in kilometers per hour
	 */
	public final double averageSpeedKmPH;

	public PlanningInstance(Graph<CycleNode, CycleEdge> graph, List<CycleNode> originsInGraph,
			List<List<CycleNode>> waypointsInGraph, List<CycleNode> destinationsInGraph,
			double averageSpeedKmPH) {
		super();

		this.graph = graph;
		this.origins = originsInGraph;
		this.waypoints = waypointsInGraph;
		this.destinations = destinationsInGraph;
		this.averageSpeedKmPH = averageSpeedKmPH;
	}

}
