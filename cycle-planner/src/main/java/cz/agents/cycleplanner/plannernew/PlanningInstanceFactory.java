package cz.agents.cycleplanner.plannernew;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.opengis.geometry.MismatchedDimensionException;

import cz.agents.basestructures.Zone;
import cz.agents.cycleplanner.api.v3.datamodel.Coordinate;
import cz.agents.cycleplanner.api.v3.datamodel.CyclePlannerRequest;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.exceptions.PlannerException;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

/**
 * TODO rename TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class PlanningInstanceFactory {

	private static final int MAX_NUMBER_OF_WAYPOINTS = 3;

	private static final Logger log = Logger.getLogger(PlanningInstanceFactory.class);

	private static NearestNodeService nearestNodeService = NearestNodeService.INSTANCE;
	private static CycleZoneProvider cycleZoneProvider = CycleZoneProvider.INSTANCE;

	private PlanningInstanceFactory() {
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param request
	 * @return
	 * @throws OutOfBoundsException
	 * @throws PlannerException
	 */
	public static PlanningInstance getPlanningInstance(CyclePlannerRequest request)
			throws OutOfBoundsException, PlannerException {

		// log.info("Request: " + request.toString());

		Zone<CycleNode, CycleEdge> zone = cycleZoneProvider.getZone(request.getOrigin().getLatE6(),
				request.getOrigin().getLonE6(), 
				request.getDestination().getLatE6(), 
				request.getDestination().getLonE6());

		// Obtain closest pair of origin and destination in graph to the given
		// pair
		List<CycleNode> originsInGraph = null;
		List<List<CycleNode>> waypointsInGraph = new ArrayList<List<CycleNode>>();
		List<CycleNode> destinationsInGraph = null;

		try {
			log.info("Looking for nearest node to origin...");

			originsInGraph = nearestNodeService.getNearestNodes(zone.name, request.getOrigin()
					.getLatE6(), request.getOrigin().getLonE6());

			if (request.getWaypoints() != null) {
				for (Coordinate waypoint : request.getWaypoints()) {
					if (waypointsInGraph.size() < MAX_NUMBER_OF_WAYPOINTS) {

						log.info("Looking for nearest node to waypoint...");

						List<CycleNode> nearestWaypoints = nearestNodeService.getNearestNodes(
								zone.name, waypoint.getLatE6(), waypoint.getLonE6());

						waypointsInGraph.add(nearestWaypoints);
					} else {
						break;
					}
				}
			}

			log.info("Looking for nearest node to destination...");

			destinationsInGraph = nearestNodeService.getNearestNodes(zone.name, request
					.getDestination().getLatE6(), request.getDestination().getLonE6());

		} catch (MismatchedDimensionException e) {
			// | TransformException e
			log.error(request, e);
			throw new PlannerException();
		}

		log.info("Origins: " + originsInGraph.toString());

		for (List<CycleNode> waypoints : waypointsInGraph) {
			log.info("Waypoints: " + waypoints.toString());
		}

		log.info("Destinations: " + destinationsInGraph.toString());

		// Map<Profiles, CriteriaWeightVector> criteriaWeightVectors = new
		// HashMap<Profiles, CriteriaWeightVector>();
		//
		// if (request.getProfile() == null) {
		// criteriaWeightVectors.put(Profiles.COMMUTING,
		// CriteriaWeightVector.COMMUTING);
		// criteriaWeightVectors.put(Profiles.BIKE_FRIENDLY,
		// CriteriaWeightVector.BIKE_FRIENDLY);
		// criteriaWeightVectors.put(Profiles.FLAT, CriteriaWeightVector.FLAT);
		// criteriaWeightVectors.put(Profiles.FAST, CriteriaWeightVector.FAST);
		// } else {
		// criteriaWeightVectors.put(Profiles.USER_SPECIFIED, new
		// CriteriaWeightVector(request.getProfile()
		// .getTravelTimeWeight(), request.getProfile().getComfortWeight(),
		// request.getProfile()
		// .getQuietnessWeight(), request.getProfile().getFlatnessWeight()));
		// }

		return new PlanningInstance(zone.graph, originsInGraph, waypointsInGraph,
				destinationsInGraph, request.getAverageSpeedKmPH());
	}

}
