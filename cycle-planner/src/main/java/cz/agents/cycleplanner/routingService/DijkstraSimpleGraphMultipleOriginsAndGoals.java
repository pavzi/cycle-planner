package cz.agents.cycleplanner.routingService;

import java.lang.reflect.Field;
import java.util.Collection;

import org.apache.log4j.Logger;

import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.plannercore.algorithms.DijkstraSimpleGraphMultipleGoals;
import eu.superhub.wp5.plannercore.algorithms.goalcheckers.MultiGoalChecker;
import eu.superhub.wp5.plannercore.structures.evaluators.vertexevaluators.VertexEvaluator;
import eu.superhub.wp5.plannercore.structures.search.QueueNode;
import eu.superhub.wp5.plannercore.structures.search.SimpleDirectedGraph;

public class DijkstraSimpleGraphMultipleOriginsAndGoals<V extends Node & QueueNode<V>, E> extends
		DijkstraSimpleGraphMultipleGoals<V, E> {

	private static final Logger log = Logger.getLogger(DijkstraSimpleGraphMultipleOriginsAndGoals.class);

	private Collection<V> startVertices;

	private Collection<Integer> closedSetSizes;

	public DijkstraSimpleGraphMultipleOriginsAndGoals(SimpleDirectedGraph<V, E> graph, Collection<V> startVertices,
			VertexEvaluator<V> comparator, MultiGoalChecker<V> goalChecker, Collection<Integer> closedSetSizes) {
		this(graph, startVertices, comparator, goalChecker, Double.POSITIVE_INFINITY, closedSetSizes);
	}

	public DijkstraSimpleGraphMultipleOriginsAndGoals(SimpleDirectedGraph<V, E> graph, Collection<V> startVertices,
			VertexEvaluator<V> comparator, MultiGoalChecker<V> goalChecker, double radius,
			Collection<Integer> closedSetSizes) {
		super(graph, startVertices.iterator().next(), comparator, goalChecker, radius);

		this.startVertices = startVertices;
		this.closedSetSizes = closedSetSizes;
	}

	@Override
	public void run() {
		for (V vertex : startVertices) {
			encounterVertex(vertex);
		}
		log.debug("Heap size at the start of the algorithm: " + heap.getSize());

		super.run();
		log.debug("Closed set size " + getClosedSetSize());
		closedSetSizes.add(getClosedSetSize());
//		destroy();
	}

	private void destroy() {
		try {
			Field field = this.getClass().getSuperclass().getSuperclass().getDeclaredField("closed");

			field.setAccessible(true);
			field.set(this, null);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			log.error("Closed set could not be emptied!", e);
		}

	}

}
