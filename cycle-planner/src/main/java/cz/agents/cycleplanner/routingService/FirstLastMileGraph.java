package cz.agents.cycleplanner.routingService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleEdgeFactory;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.EdgeId;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Edge;

@SuppressWarnings("serial")
public class FirstLastMileGraph extends Graph<CycleNode2, CycleEdge2> {

	private static final Logger log = Logger.getLogger(FirstLastMileGraph.class);

	Graph<CycleNode2, CycleEdge2> graph;
	private Map<Long, CycleNode2> additionalNodesByNodeId;
	private Map<EdgeId, CycleEdge2> additionalEdgeByFromToNodeIds;
	private Map<Long, List<CycleEdge2>> additionalNodeOutgoingEdges;
	private Map<Long, List<CycleEdge2>> additionalNodeIncomingEdges;

	public FirstLastMileGraph(Graph<CycleNode2, CycleEdge2> graph, Collection<CycleNode2> firstMileNodes,
			Collection<CycleNode2> lastMileNodes, Map<CycleNode2, Collection<EdgeId>> contractedNodes) {
		super(null, null, null, null);
		this.graph = graph;
		this.additionalNodesByNodeId = new HashMap<Long, CycleNode2>();
		this.additionalEdgeByFromToNodeIds = new HashMap<EdgeId, CycleEdge2>();
		this.additionalNodeOutgoingEdges = new HashMap<Long, List<CycleEdge2>>();
		this.additionalNodeIncomingEdges = new HashMap<Long, List<CycleEdge2>>();

		for (CycleNode2 firstMileFromNode : firstMileNodes) {
			if (this.graph.containsNodeByNodeId(firstMileFromNode.getId())) {
				continue;
			}

			addNode(firstMileFromNode);

			for (EdgeId contractedNodeEdgeId : contractedNodes.get(firstMileFromNode)) {
				CycleEdge2 contractedNodeEdge = this.graph.getEdgeByEdgeId(contractedNodeEdgeId);
				CycleNode2 firstMileToNode = this.graph.getNodeByNodeId(contractedNodeEdge.getToNodeId());

				List<CycleNode2> firstMileVia = new ArrayList<CycleNode2>();
				boolean foundFirstMileFromNode = false;

				for (CycleNode2 via : contractedNodeEdge.getVia()) {
					if (!foundFirstMileFromNode && via.equals(firstMileFromNode)) {
						foundFirstMileFromNode = true;

					} else if (foundFirstMileFromNode) {
						firstMileVia.add(via);
					}
				}

				CycleEdge2 firstMile = CycleEdgeFactory.getCycleEdge2(firstMileFromNode, firstMileToNode, firstMileVia,
						new HashSet<>(contractedNodeEdge.getTags()));
				log.info("First mile: " + firstMile);

				if (!additionalNodesByNodeId.containsKey(firstMileToNode.getId())) {
					addNode(firstMileToNode);
				}

				addEdge(firstMile);
			}
		}

		for (CycleNode2 lastMileToNode : lastMileNodes) {
			if (this.graph.containsNodeByNodeId(lastMileToNode.getId())) {
				continue;
			}

			addNode(lastMileToNode);

			for (EdgeId contractedNodeEdgeId : contractedNodes.get(lastMileToNode)) {
				CycleEdge2 contractedNodeEdge = this.graph.getEdgeByEdgeId(contractedNodeEdgeId);
				CycleNode2 lastMileFromNode = this.graph.getNodeByNodeId(contractedNodeEdge.getFromNodeId());

				List<CycleNode2> lastMileVia = new ArrayList<CycleNode2>();

				for (CycleNode2 via : contractedNodeEdge.getVia()) {
					if (via.equals(lastMileToNode)) {
						break;
					}

					lastMileVia.add(via);
				}

				CycleEdge2 lastMile = CycleEdgeFactory.getCycleEdge2(lastMileFromNode, lastMileToNode, lastMileVia,
						new HashSet<>(contractedNodeEdge.getTags()));
				log.info("Last mile: " + lastMile);

				if (!additionalNodesByNodeId.containsKey(lastMileFromNode.getId())) {
					addNode(lastMileFromNode);
				}

				addEdge(lastMile);
			}
		}

	}

	private void addNode(CycleNode2 node) {
		additionalNodesByNodeId.put(node.getId(), node);
		additionalNodeOutgoingEdges.put(node.getId(), new ArrayList<CycleEdge2>());
		additionalNodeIncomingEdges.put(node.getId(), new ArrayList<CycleEdge2>());
	}

	private void addEdge(CycleEdge2 edge) {
		EdgeId edgeId = new EdgeId(edge.getFromNodeId(), edge.getToNodeId());
		List<CycleEdge2> outcomingEdgesFromNode = additionalNodeOutgoingEdges.get(edge.getFromNodeId());
		List<CycleEdge2> incomingEdgesToNode = additionalNodeIncomingEdges.get(edge.getToNodeId());

		outcomingEdgesFromNode.add(edge);
		incomingEdgesToNode.add(edge);

		additionalEdgeByFromToNodeIds.put(edgeId, edge);
	}

	@Override
	public CycleNode2 getNodeByNodeId(long nodeId) {
		if (additionalNodesByNodeId.containsKey(nodeId)) {
			return additionalNodesByNodeId.get(nodeId);
		}

		return this.graph.getNodeByNodeId(nodeId);
	}

	@Override
	public CycleEdge2 getEdge(long fromNodeId, long toNodeId) {
		EdgeId edgeId = new EdgeId(fromNodeId, toNodeId);

		if (additionalEdgeByFromToNodeIds.containsKey(edgeId)) {
			return additionalEdgeByFromToNodeIds.get(edgeId);
		}

		return this.graph.getEdge(fromNodeId, toNodeId);
	}

	@Override
	public CycleEdge2 getEdgeByEdgeId(EdgeId edgeId) {
		if (additionalEdgeByFromToNodeIds.containsKey(edgeId)) {
			return additionalEdgeByFromToNodeIds.get(edgeId);
		}

		return this.graph.getEdgeByEdgeId(edgeId);
	}

	@Override
	public Collection<CycleNode2> getAllNodes() {
		// TODO
		return this.graph.getAllNodes();
	}

	@Override
	public List<CycleEdge2> getNodeIncomingEdges(long nodeId) {
		if (additionalNodesByNodeId.containsKey(nodeId)) {
			List<CycleEdge2> incomingEdges = new ArrayList<CycleEdge2>(additionalNodeIncomingEdges.get(nodeId));

			if (this.graph.containsNodeByNodeId(nodeId)) {
				incomingEdges.addAll(this.graph.getNodeIncomingEdges(nodeId));
			}
			
			return incomingEdges;
		}

		return this.graph.getNodeIncomingEdges(nodeId);
	}

	@Override
	public List<CycleEdge2> getNodeOutcomingEdges(long nodeId) {
		if (additionalNodesByNodeId.containsKey(nodeId)) {
			List<CycleEdge2> outgoingEdges = new ArrayList<CycleEdge2>(additionalNodeOutgoingEdges.get(nodeId));

			if (this.graph.containsNodeByNodeId(nodeId)) {
				outgoingEdges.addAll(this.graph.getNodeOutcomingEdges(nodeId));
			}
			
			return outgoingEdges;
		}

		return this.graph.getNodeOutcomingEdges(nodeId);
	}

	@Override
	public Collection<CycleEdge2> getAllEdges() {
		// TODO
		return this.graph.getAllEdges();
	}

	@Override
	public Set<Long> getAllSuccessors(long node) {

		final Set<Long> successors;

		successors = new HashSet<Long>();
		for (Edge edge : this.getNodeOutcomingEdges(node)) {
			successors.add(edge.getToNodeId());
		}

		return successors;
	}

	@Override
	public Set<Long> getAllPredecessors(long node) {
		final Set<Long> predecessors;

		predecessors = new HashSet<Long>();
		for (Edge edge : this.getNodeIncomingEdges(node)) {
			predecessors.add(edge.getFromNodeId());
		}

		return predecessors;
	}

	@Override
	public Set<Long> getAllNeighbors(long node) {
		final Set<Long> neighbors;

		neighbors = new HashSet<Long>();
		neighbors.addAll(this.getAllSuccessors(node));
		neighbors.addAll(this.getAllPredecessors(node));

		return neighbors;
	}

	@Override
	public boolean containsNodeByNodeId(long nodeId) {
		return additionalNodesByNodeId.containsKey(nodeId) || this.graph.containsNodeByNodeId(nodeId);
	}

	@Override
	public String toString() {
		return "FirstLastMileGraph [graph=" + graph + ", additionalNodesByNodeId=" + additionalNodesByNodeId
				+ ", additionalEdgeByFromToNodeIds=" + additionalEdgeByFromToNodeIds + ", additionalNodeOutgoingEdges="
				+ additionalNodeOutgoingEdges + ", additionalNodeIncomingEdges=" + additionalNodeIncomingEdges + "]";
	}

}
