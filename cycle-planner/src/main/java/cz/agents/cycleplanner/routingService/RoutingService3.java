package cz.agents.cycleplanner.routingService;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import cz.agents.cycleplanner.citydatastorage.CycleCityDataLoader;
import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.nearestNode.BicycleNearestNodesUtilKdTreeImpl;
import eu.superhub.wp5.graphcommon.graph.EdgeId;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.plannerdataimporter.util.EPSGProjection;
import eu.superhub.wp5.wp5common.location.GPSLocation;
import eu.superhub.wp5.wp5common.location.Location;

/**
 * RoutingService holds reference to a planning graph, is able to provide
 * nearest node in graph to specified geographical point, is able to return
 * street name for given osm way id and recognize city for specified
 * geographical point
 * 
 * Use singleton design pattern, because we do not want to have more than one
 * instances of graphs in memory
 * 
 * @author Marcel Német <marcel.nemet@gmail.com>
 * @author Pavol Zilecky <pavol.zilecky@gmail.com>
 */
public enum RoutingService3 {

	INSTANCE;

	private final static int MAX_DISTANCE_FROM_DESIRED_POINT = 100;
	private final static int MAX_NUMBER_OF_NEAREST_NODES = 5;

	private final Logger log = Logger.getLogger(RoutingService3.class);
	
	// TODO lazy initialization
	private Map<String, CityData<CycleNode2, CycleEdge2>> cycleCitiesData;
	
	private Map<String, BicycleNearestNodesUtilKdTreeImpl> kdTrees;
	
	private Map<String, Map<CycleNode2, Collection<EdgeId>>> contractedNodes;

	public EPSGProjection projection;

	private RoutingService3() {

		try {

			log.info("RoutingService3 constructor called, initializing...");

			cycleCitiesData = new HashMap<String, CityData<CycleNode2, CycleEdge2>>();
			
			kdTrees = new HashMap<String, BicycleNearestNodesUtilKdTreeImpl>();
			contractedNodes = new HashMap<String, Map<CycleNode2, Collection<EdgeId>>>();
			Collection<InputStream> cityDataInputStreams = CycleCityDataLoader.getCycleCitiesDataAsStream();

			for (InputStream cityDataInputStream : cityDataInputStreams) {
				ObjectInputStream ois = new ObjectInputStream(cityDataInputStream);
				@SuppressWarnings("unchecked")
				CityData<CycleNode2, CycleEdge2> cycleCityData = (CityData<CycleNode2, CycleEdge2>) ois.readObject();
				Graph<CycleNode2, CycleEdge2> cycleGraph = cycleCityData.getGraph();

				log.info("# of nodes: " + cycleGraph.getAllNodes().size());
				log.info("# of edges: " + cycleGraph.getAllEdges().size());
				log.info("City name: " + cycleCityData.getCityName());
				log.info("topE6: " + cycleCityData.getTopE6() + " leftE6: " + cycleCityData.getLeftE6() + " bottomE6: "
						+ cycleCityData.getBottomE6() + " rightE6: " + cycleCityData.getRightE6());

				cycleCitiesData.put(cycleCityData.getCityName(), cycleCityData);
				
				Map<CycleNode2, Collection<EdgeId>> cityContractedNodes = new HashMap<CycleNode2, Collection<EdgeId>>();
				
				for (CycleEdge2 edge : cycleGraph.getAllEdges()) {
					for (CycleNode2 via : edge.getVia()) {
						Collection<EdgeId> contractedNodeEdgeIds = cityContractedNodes.get(via);
						
						if (contractedNodeEdgeIds == null) {
							contractedNodeEdgeIds = new HashSet<EdgeId>();
							cityContractedNodes.put(via, contractedNodeEdgeIds);
						}
						
						contractedNodeEdgeIds.add(edge.getEdgeId());						
					}
				}

				kdTrees.put(cycleCityData.getCityName(), new BicycleNearestNodesUtilKdTreeImpl(cycleGraph,
						cityContractedNodes.keySet(), MAX_DISTANCE_FROM_DESIRED_POINT, MAX_NUMBER_OF_NEAREST_NODES));

				contractedNodes.put(cycleCityData.getCityName(), cityContractedNodes);

				projection = new EPSGProjection(2065);

				ois.close();
			}
			
			System.gc();
			log.info("Cycleplanner initialization was successful!");

		} catch (URISyntaxException | IOException | ClassNotFoundException | FactoryException | TransformException e) {
			log.error("Initializing failed", e);
			System.exit(-1);
		}

	}

	public Iterator<CityData<CycleNode2, CycleEdge2>> getSupportedCities() {
		return cycleCitiesData.values().iterator();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param cityName
	 * @return
	 */
	public CityData<CycleNode2, CycleEdge2> getCityCycleData(String cityName) {
		return cycleCitiesData.get(cityName);
	}


	/**
	 *
	 * Looks for city inside which lie specified geographical points (origin and
	 * destination) and returns cycle data for that city
	 *
	 * @param originLatE6
	 *            origin latitude in integer form
	 * @param originLonE6
	 *            origin longitude in integer form
	 * @param destinationLatE6
	 *            destination latitude in integer form
	 * @param destinationLonE6
	 *            destination longitude in integer form
	 * @return cycle data for city in which specified points lie
	 * @throws OutOfBoundsException
	 *             when one of the points does not lie in any city or cities are
	 *             different for origin and destination
	 */
	public CityData<CycleNode2, CycleEdge2> getCityCycleData(int originLatE6, int originLonE6, int destinationLatE6,
			int destinationLonE6) throws OutOfBoundsException {

		CityData<CycleNode2, CycleEdge2> startData = getCycleDataForLocation(originLatE6, originLonE6);
		CityData<CycleNode2, CycleEdge2> endData = getCycleDataForLocation(destinationLatE6, destinationLonE6);

		// if latitude and longitude for origin and destination are not in
		// bounding box of any city, then return status OUT_OF_BOUND
		if (startData == null || endData == null || !startData.getCityName().equals(endData.getCityName())) {
			throw new OutOfBoundsException();
		}
		log.debug("Choosed city: " + endData.getCityName());

		return startData;
	}

	/**
	 *
	 * Looks for city inside which lies a specified geographical point and
	 * returns cycle data for that city
	 *
	 * @param latE6
	 * @param lonE6
	 * @return cycle data for city in which specified point lie
	 */
	public CityData<CycleNode2, CycleEdge2> getCycleDataForLocation(int latE6, int lonE6) {
		for (CityData<CycleNode2, CycleEdge2> cityData : cycleCitiesData.values()) {
			if (cityData.getLeftE6() <= lonE6 && lonE6 <= cityData.getRightE6() && cityData.getBottomE6() <= latE6
					&& latE6 <= cityData.getTopE6()) {
				return cityData;
			}
		}
		// TODO throw out of bound exception if it doeas not found any city
		return null;
	}

	/**
	 *
	 * Returns nearest node in graph to specified location
	 *
	 * @param latE6
	 * @param lonE6
	 * @param city
	 * @return
	 * @throws MismatchedDimensionException
	 * @throws TransformException
	 */
	public CycleNode2 getNearestNode(int latE6, int lonE6, String cityName) throws MismatchedDimensionException,
			TransformException {
		Location location = projection.getProjectedLocation(new GPSLocation(latE6, lonE6));
		CycleNode2 nearestNode = kdTrees.get(cityName).findNearestNode(location);

		return nearestNode;
	}

	/**
	 *
	 * Returns nearest nodes in graph to specified location
	 *
	 * @param latE6
	 * @param lonE6
	 * @param city
	 * @return
	 * @throws MismatchedDimensionException
	 * @throws TransformException
	 */
	public List<CycleNode2> getNearestNodes(int latE6, int lonE6, String cityName) throws MismatchedDimensionException,
			TransformException {
		Location location = projection.getProjectedLocation(new GPSLocation(latE6, lonE6));
		List<CycleNode2> nearestNodes = kdTrees.get(cityName).getNearestNodes(location);

		return nearestNodes;
	}
	
	/**
	 * 
	 * TODO javadoc
	 * @param cityName
	 * @return
	 */
	public Map<CycleNode2, Collection<EdgeId>> getContractedNodesForCity(String cityName) {
		return contractedNodes.get(cityName);
	}

}
