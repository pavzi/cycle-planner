package cz.agents.cycleplanner;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.citydatastorage.CycleCityDataLoader;
import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.Graph;

public class ExportPragueCycleGraph {

	public static void main(String[] args) {

		Logger log = Logger.getLogger(ExportPragueCycleGraph.class);

		try {

			log.info("RoutingService3 constructor called, initializing...");
			Collection<InputStream> cityDataInputStreams = CycleCityDataLoader.getCycleCitiesDataAsStream();

			for (InputStream cityDataInputStream : cityDataInputStreams) {
				ObjectInputStream ois = new ObjectInputStream(cityDataInputStream);

				@SuppressWarnings("unchecked")
				CityData<CycleNode2, CycleEdge2> cycleCityData = (CityData<CycleNode2, CycleEdge2>) ois.readObject();
				Graph<CycleNode2, CycleEdge2> cycleGraph = cycleCityData.getGraph();

				if (cycleCityData.getCityName().equals("praha")) {

					// OUTPUT to file
					BufferedWriter writer = null;
					try {
						writer = new BufferedWriter(new FileWriter("prague_graph.txt"));

						// nodes
						int nodesCount = 0;
						for (CycleNode2 node : cycleGraph.getAllNodes()) {
							writer.write(String.format("%d,%d,%d\n", node.getId(), node.getLatitudeE6(),
									node.getLongitudeE6()));
							nodesCount++;
						}

						// via nodes
						for (CycleEdge2 edge : cycleGraph.getAllEdges()) {
							for (CycleNode2 node : edge.getVia()) {
								writer.write(String.format("%d,%d,%d\n", node.getId(), node.getLatitudeE6(),
										node.getLongitudeE6()));
								nodesCount++;
							}
						}

						// edges
						int edgesCount = 0;
						for (CycleEdge2 edge : cycleGraph.getAllEdges()) {
							List<Long> edgeNodes = new ArrayList<>();
							edgeNodes.add(edge.getFromNodeId());
							if (!edge.getVia().isEmpty()) {
								for (CycleNode2 node : edge.getVia()) {
									edgeNodes.add(node.getId());
								}
							}
							edgeNodes.add(edge.getToNodeId());
							for (int i = 1; i < edgeNodes.size(); i++) {
								writer.write(String.format("%d,%d\n", edgeNodes.get(i - 1), edgeNodes.get(i)));
								edgesCount++;
							}
						}

						log.info("# of nodes: " + cycleGraph.getAllNodes().size());
						log.info("# of edges: " + cycleGraph.getAllEdges().size());
						log.info("City name: " + cycleCityData.getCityName());
						log.info("topE6: " + cycleCityData.getTopE6() + " leftE6: " + cycleCityData.getLeftE6()
								+ " bottomE6: " + cycleCityData.getBottomE6() + " rightE6: "
								+ cycleCityData.getRightE6());
						log.info("Total # of nodes: " + nodesCount);
						log.info("Total # of edges: " + edgesCount);

					} catch (IOException e) {
					} finally {
						try {
							if (writer != null)
								writer.close();
						} catch (IOException e) {
						}
					}
				}
				ois.close();
			}

			System.gc();
			log.info("Prague graph export was successful!");

		} catch (URISyntaxException | IOException | ClassNotFoundException e) {
			log.error("Initializing failed", e);
			System.exit(-1);
		}
	}

}
