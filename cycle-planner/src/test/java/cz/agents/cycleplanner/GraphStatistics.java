package cz.agents.cycleplanner;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.routingService.RoutingService3;
import eu.superhub.wp5.graphcommon.graph.Graph;

public class GraphStatistics {
	private static final Logger log = Logger.getLogger(GraphStatistics.class);

	private static RoutingService3 routingService;
	private static Graph<CycleNode2, CycleEdge2> cycleGraph;

	public static void main(String[] args) {

		routingService = RoutingService3.INSTANCE;
		cycleGraph = routingService.getCityCycleData("prague").getGraph();

		// TODO move to statistics on GeneralGraph
//		double countTags = 0;
//		HashMap<String, Integer> tagsStatistic = new HashMap<String, Integer>();
//
//		for (CycleEdge2 edge : cycleGraph.getAllEdges()) {
//
//			countTags += edge.getOSMtags().size();
//			for (Iterator<String> it = edge.getOSMtags().iterator(); it.hasNext();) {
//				String tag = it.next();
//				if (tagsStatistic.containsKey(tag)) {
//					tagsStatistic.put(tag, tagsStatistic.get(tag) + 1);
//				} else {
//					tagsStatistic.put(tag, 1);
//				}
//			}
//		}
//		log.info("Tags statistic");
//		for (Iterator<String> it = tagsStatistic.keySet().iterator(); it.hasNext();) {
//			String key = it.next();
//			log.info(key + ", " + tagsStatistic.get(key));
//		}
//
//		log.info("Priemerny pocet tagov na hranu " + (countTags / (double) cycleGraph.getAllEdges().size()));

		int countNodesWithZeroOutDegree = 0;
		for (CycleNode2 node : cycleGraph.getAllNodes()) {
			if (cycleGraph.getNodeOutcomingEdges(node.getId()).size() == 0)
				countNodesWithZeroOutDegree++;
		}
		log.info("Pocet vrchlov ktore nemaju vystupnu hranu: " + countNodesWithZeroOutDegree);

		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		double avg = 0;

		for (CycleEdge2 edge : cycleGraph.getAllEdges()) {

			min = (min > edge.getLengthInMetres()) ? edge.getLengthInMetres() : min;
			max = (max < edge.getLengthInMetres()) ? edge.getLengthInMetres() : max;

			avg += edge.getLengthInMetres();
		}
		
		log.info("Min edge length in meters: " + min);
		log.info("Max edge length in meters: " + max);
		log.info("Average edge length in meters: " + (avg / (double) cycleGraph.getAllEdges().size()));
	}

}
