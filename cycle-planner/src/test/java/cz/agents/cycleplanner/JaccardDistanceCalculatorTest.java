package cz.agents.cycleplanner;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Sets;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.core.datastructures.RoadType;
import cz.agents.cycleplanner.core.datastructures.Surface;
import cz.agents.cycleplanner.util.JaccardDistanceCalculator;
import eu.superhub.wp5.wp5common.location.GPSLocation;

public class JaccardDistanceCalculatorTest {
	
	Set<String> yourFriends = Sets.newHashSet(
	        "Desiree Jagger",
	        "Benedict Casteel",
	        "Evon Saddler",
	        "Toby Greenland", 
	        "Norine Caruana",
	        "Felecia Houghton",
	        "Lanelle Franzoni",
	        "Armandina Everitt",
	        "Inger Honea", 
	        "Autumn Hendriks");

	Set<String> myFriends = Sets.newHashSet(
	        "Karrie Rutan",
	        "Desiree Jagger", 
	        "Armandina Everitt",
	        "Arlen Nowacki",
	        "Ward Siciliano",
	        "Mira Yonts",
	        "Marcelo Arab",
	        "Autumn Hendriks",
	        "Mazie Hemstreet",
	        "Toby Greenland");
	
	CycleNode2 node1 = new CycleNode2(1, new GPSLocation(1, 1), "test", new HashSet<String>());
	CycleNode2 node2 = new CycleNode2(2, new GPSLocation(2, 2), "test", new HashSet<String>());
	CycleNode2 node3 = new CycleNode2(3, new GPSLocation(3, 3), "test", new HashSet<String>());
	CycleNode2 node4 = new CycleNode2(4, new GPSLocation(4, 4), "test", new HashSet<String>());
	CycleNode2 node5 = new CycleNode2(5, new GPSLocation(5, 5), "test", new HashSet<String>());
	CycleNode2 node6 = new CycleNode2(6, new GPSLocation(6, 6), "test", new HashSet<String>());

	CycleEdge2 edge1 = new CycleEdge2(node1.getId(), node2.getId(), 0, new ArrayList<CycleNode2>(), new int[0], 0d, 0d,
			0d, 0d, 0d, 0, 0, Surface.UNPAVED, RoadType.CYCLEWAY, "street", "A1", new HashSet<String>());
	CycleEdge2 edge2 = new CycleEdge2(node2.getId(), node3.getId(), 0, new ArrayList<CycleNode2>(), new int[0], 0d, 0d,
			0d, 0d, 0d, 0, 0, Surface.UNPAVED, RoadType.CYCLEWAY, "street", "A1", new HashSet<String>());
	CycleEdge2 edge3 = new CycleEdge2(node3.getId(), node4.getId(), 0, new ArrayList<CycleNode2>(), new int[0], 0d, 0d,
			0d, 0d, 0d, 0, 0, Surface.UNPAVED, RoadType.CYCLEWAY, "street", "A1", new HashSet<String>());
	CycleEdge2 edge4 = new CycleEdge2(node4.getId(), node5.getId(), 0, new ArrayList<CycleNode2>(), new int[0], 0d, 0d,
			0d, 0d, 0d, 0, 0, Surface.UNPAVED, RoadType.CYCLEWAY, "street", "A1", new HashSet<String>());
	CycleEdge2 edge5 = new CycleEdge2(node5.getId(), node6.getId(), 0, new ArrayList<CycleNode2>(), new int[0], 0d, 0d,
			0d, 0d, 0d, 0, 0, Surface.UNPAVED, RoadType.CYCLEWAY, "street", "A1", new HashSet<String>());

	Set<CycleEdge2> path1 = Sets.newHashSet(edge1, edge3, edge5);
	Set<CycleEdge2> path2 = Sets.newHashSet(edge2, edge4);
	
	Set<CycleEdge2> path3 = Sets.newHashSet(edge1, edge2, edge3);
	Set<CycleEdge2> path4 = Sets.newHashSet(edge3, edge4, edge5);
	
	Set<CycleEdge2> path5 = Sets.newHashSet(edge1, edge2, edge3, edge4, edge5);
	
	@Test
	public void test() {
		double jd = JaccardDistanceCalculator.calculate(yourFriends, myFriends);

		assertEquals(0.75, jd, 0d);
	}
	
	@Test
	public void testPaths(){
		double jd = JaccardDistanceCalculator.calculate(path1, path2);
		
		assertEquals(1d, jd, 0d);
		
		jd = JaccardDistanceCalculator.calculate(path3, path4);
		
		assertEquals(0.8, jd, 0d);
		
		jd = JaccardDistanceCalculator.calculate(path5, path5);
		
		assertEquals(0d, jd, 0d);
	}

}
