package cz.agents.cycleplanner.aStar;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

public class CriteriaWeightVectorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		assertArrayEquals(CriteriaWeightVector.FAST.getWeights(), new double[]{1d, 0d, 0d, 0d}, 0d);
		assertArrayEquals(CriteriaWeightVector.BIKE_FRIENDLY.getWeights(), new double[]{0d, 0.2d, 0.6d, 0.2d}, 0d);
		assertArrayEquals(CriteriaWeightVector.COMMUTING.getWeights(), new double[]{0.3d, 0.5d, 0.1d, 0.1d}, 0d);
		assertArrayEquals(CriteriaWeightVector.FLAT.getWeights(), new double[]{0d, 0.1d, 0.1d, 0.8d}, 0d);
	}

}
