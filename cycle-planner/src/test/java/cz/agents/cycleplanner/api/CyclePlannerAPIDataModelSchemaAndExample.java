package cz.agents.cycleplanner.api;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import cz.agents.cycleplanner.api.datamodel.v2.Coordinate;
import cz.agents.cycleplanner.api.datamodel.v2.Feedback;
import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;
import cz.agents.cycleplanner.api.datamodel.v2.Plan;
import cz.agents.cycleplanner.api.datamodel.v2.Profile;
import cz.agents.cycleplanner.api.datamodel.v2.Request;
import cz.agents.cycleplanner.api.datamodel.v2.Response;
import cz.agents.cycleplanner.api.datamodel.v2.ResponseStatus;
import cz.agents.cycleplanner.api.datamodel.v2.SpecialCoordinateType;
import cz.agents.cycleplanner.api.datamodel.v2.TimedCoordinate;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.exceptions.PlanNotFoundException;
import cz.agents.cycleplanner.exceptions.PlannerException;
import cz.agents.cycleplanner.planner.MultipleGoalDestinationBicyclePlanner;
import cz.agents.cycleplanner.util.JSONSchemaUtils;
import cz.agents.cycleplanner.util.JSONUtils;

public class CyclePlannerAPIDataModelSchemaAndExample {

	private static final Logger log = Logger.getLogger(CyclePlannerAPIDataModelSchemaAndExample.class);
	private static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

	public static void main(String[] args) throws JsonProcessingException, FileNotFoundException {

		BicycleScalarizedMultiCriteriaRequestHandler bicycleRequestHandler = new BicycleScalarizedMultiCriteriaRequestHandler(
				new MultipleGoalDestinationBicyclePlanner());
		PlanningInstanceBuilder planningInstanceBuilder = new PlanningInstanceBuilder();

		Coordinate origin = new Coordinate(50072600, 14391800, SpecialCoordinateType.ORIGIN);
		List<Coordinate> waypoints = new ArrayList<Coordinate>();

		waypoints.add(new Coordinate(50099441, 14400716));

		Coordinate destination = new Coordinate(50073200, 14441600, 28, SpecialCoordinateType.DESTINATION);
		double averageSpeedKMpH = 20;
		Profile profile = new Profile(1, 0, 0, 0);
		Request request = new Request("test", origin, waypoints, destination, averageSpeedKMpH, "praha", profile);

		log.info("Request: " + JSONUtils.javaObjectToJson(request));
		log.info("Request scheme: ");
		JSONSchemaUtils.generateSchemaAndSaveToFile(Request.class, "CyclePlannerRequestSchema.json");

		Response response;

		try {
			long planningInstanceBuilderTime = System.currentTimeMillis();
			PlanningInstance2 planningInstance = planningInstanceBuilder.build2(request);
			log.info("PlanningInstanceBuilder running time is "
					+ (System.currentTimeMillis() - planningInstanceBuilderTime) + " ms");

			long requestHandlerTime = System.currentTimeMillis();
			Collection<Plan> plans = bicycleRequestHandler.handle(planningInstance);
			log.info("RequestHandler running time is " + (System.currentTimeMillis() - requestHandlerTime) + " ms");

			response = new Response(0L, dateParser.format(new Date()), request, planningInstance.getCityName(),
					ResponseStatus.OK, plans);
		} catch (OutOfBoundsException e) {
			log.error(e);
			response = new Response(0L, dateParser.format(new Date()), request, ResponseStatus.OUT_OF_BOUNDS);
		} catch (PlanNotFoundException e) {
			log.error(e);
			response = new Response(0L, dateParser.format(new Date()), request, ResponseStatus.PLAN_NOT_FOUND);
		} catch (PlannerException e) {
			log.error(e);
			response = new Response(0L, dateParser.format(new Date()), request, ResponseStatus.PLANNER_EXCEPTION);
		}

		log.info("Response: " + JSONUtils.javaObjectToJson(response));
		log.info("Response scheme: ");
		JSONSchemaUtils.generateSchemaAndSaveToFile(Response.class, "CyclePlannerResponseSchema.json");

		bicycleRequestHandler.close();

		TimedCoordinate tc1 = new TimedCoordinate(14245100, 50180400, 333, "2012-11-16T11:00:00.000+01:00");
		TimedCoordinate tc2 = new TimedCoordinate(14712100, 49937100, 333, "2012-11-16T12:00:00.000+01:00");
		List<TimedCoordinate> tcList = new ArrayList<>();

		tcList.add(tc1);
		tcList.add(tc2);

		Feedback feedback = new Feedback(1234L, 2L, 3, false, true, false, false, true, "test feedback message", tcList);

		log.info("Feedback example: " + JSONUtils.javaObjectToJson(feedback));
		log.info("Feedback scheme: ");
		JSONSchemaUtils.generateSchemaAndSaveToFile(Feedback.class, "CyclePlannerFeedbackSchema.json");
		JSONSchemaUtils.generateSchemaAndSaveToFile(FeedbackStorageObject.class,
				"CyclePlannerFeedbackStorageObjectSchema.json");
	}

}
