package cz.agents.cycleplanner.api;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

public class CyclePlannerMCAPI {

	private static Logger log = Logger.getLogger(CyclePlannerMCAPI.class);

	public static void main(String[] args) {
		BicycleJourneyPlanning api = new BicycleJourneyPlanning();
		api.contextInitialized(null);

		Response response = api.planMCJourney("50.072600", "14.391800", "50.073200", "14.441600",
				"MC_DIJKSTRA_BOUNDED_ELLIPSE_EPSILON");
		
		log.info(response.getEntity().toString());

	}

}
