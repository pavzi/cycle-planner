package cz.agents.cycleplanner.api;

import java.util.ArrayList;
import java.util.List;

import cz.agents.cycleplanner.api.v3.CyclePlannerRestService;
import cz.agents.cycleplanner.api.v3.datamodel.Coordinate;
import cz.agents.cycleplanner.api.v3.datamodel.CyclePlannerRequest;
import cz.agents.cycleplanner.api.v3.datamodel.SpecialCoordinateType;
import cz.agents.cycleplanner.util.JSONUtils;

public class CyclePlannerRestServiceTest {

	public static void main(String[] args) {
		CyclePlannerRestService cyclePlannerRestService = new CyclePlannerRestService();

		Coordinate origin = new Coordinate(50072600, 14391800, SpecialCoordinateType.ORIGIN);
		List<Coordinate> waypoints = new ArrayList<Coordinate>();

		// waypoints.add(new Coordinate(50099441, 14400716));

		Coordinate destination = new Coordinate(50073200, 14441600, 28,
				SpecialCoordinateType.DESTINATION);

		CyclePlannerRequest request = new CyclePlannerRequest("test", origin, waypoints,
				destination, 15d, "test");

		cyclePlannerRestService.plan(JSONUtils.javaObjectToJson(request));

	}

}
