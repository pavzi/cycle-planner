package cz.agents.cycleplanner.mongodb;

import static org.junit.Assert.assertTrue;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;

import aima.core.util.datastructure.Pair;
import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;
import cz.agents.cycleplanner.api.datamodel.v2.Response;
import cz.agents.cycleplanner.util.JSONUtils;

public class BackupResponsesAndFeedbacks {

	private static final Logger log = Logger.getLogger(BackupResponsesAndFeedbacks.class);
	private static final SimpleDateFormat fileDateParser = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

	public static void main(String[] args) throws IOException {

		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");
		CyclePlannerStorage bicycleJourneyPlanStorage = CyclePlannerStorage.getStorage();
		assertTrue(bicycleJourneyPlanStorage.isConnected());

		List<Pair<Long, Long>> responseIds = new ArrayList<>();
		// responseIds.add(new Pair<Long, Long>(7221L, 8000L));
		// responseIds.add(new Pair<Long, Long>(8001L, 9000L));
		// responseIds.add(new Pair<Long, Long>(9001L, 10000L));
		// responseIds.add(new Pair<Long, Long>(10001L, 11000L));
		// responseIds.add(new Pair<Long, Long>(11001L, 12000L));
		// responseIds.add(new Pair<Long, Long>(12001L, 13000L));
		// responseIds.add(new Pair<Long, Long>(13001L, 14000L));
		// responseIds.add(new Pair<Long, Long>(14001L, 15000L));
		// responseIds.add(new Pair<Long, Long>(15001L, 16000L));
		// responseIds.add(new Pair<Long, Long>(16001L, 17000L));
		responseIds.add(new Pair<Long, Long>(17001L, 18000L));
		responseIds.add(new Pair<Long, Long>(18001L, 19000L));
		responseIds.add(new Pair<Long, Long>(19001L, 20000L));
		responseIds.add(new Pair<Long, Long>(20001L, 21000L));
		responseIds.add(new Pair<Long, Long>(21001L, 22000L));
		responseIds.add(new Pair<Long, Long>(22001L, 23000L));
		responseIds.add(new Pair<Long, Long>(23001L, 24000L));
		responseIds.add(new Pair<Long, Long>(24001L, 25000L));

		// responses
		for (Pair<Long, Long> pair : responseIds) {
			List<Response> responses = bicycleJourneyPlanStorage
					.retrieveAllResponses(pair.getFirst(), pair.getSecond());
			if (responses.size() > 0) {
				String filename = fileDateParser.format(new Date(System.currentTimeMillis())) + "_responses"
						+ pair.getFirst() + "-" + pair.getSecond() + ".json.gz";
				saveJSONAsGzip(JSONUtils.javaObjectToJson(responses), filename);
				System.out.println("Total number of responses for " + filename + ": " + responses.size());
			}
		}

		// feedbacks
		List<FeedbackStorageObject> feedbacks = bicycleJourneyPlanStorage.retrieveAllCycleplannerFeedback(146);
		saveJSONAsGzip(JSONUtils.javaObjectToJson(feedbacks),
				fileDateParser.format(new Date(System.currentTimeMillis())) + "_feedbacks.json.gz");
		System.out.println("Total number of feedbacks: " + feedbacks.size());
	}

	public static void saveJSONAsGzip(String json, String outputFileName) {
		File file = new File(outputFileName);
		try {
			file.createNewFile();
		} catch (IOException e) {
			log.error("The output file couldn't be created or accessed!", e);
		}
		try (GZIPOutputStream gos = new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
			gos.write(json.getBytes(Charset.forName("UTF-8")));
		} catch (IOException e) {
			log.error("An unexpected IOException has occurred!", e);
		}
	}
}
