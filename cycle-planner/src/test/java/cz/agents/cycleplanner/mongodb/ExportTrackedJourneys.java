package cz.agents.cycleplanner.mongodb;

import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;
import cz.agents.cycleplanner.api.datamodel.v2.TimedCoordinate;

public class ExportTrackedJourneys {

	public static void main(String[] args) throws IOException {

		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");
		CyclePlannerStorage bicycleJourneyPlanStorage = CyclePlannerStorage.getStorage();
		assertTrue(bicycleJourneyPlanStorage.isConnected());
		List<FeedbackStorageObject> feedbacks = bicycleJourneyPlanStorage.retrieveAllCycleplannerFeedback(146);
		int trackId = 1;
		new File("tracked").mkdir();

		for (FeedbackStorageObject f : feedbacks) {

			if ((f.getFeedback().getTrackedJourney() != null) && (f.getFeedback().getTrackedJourney().size() >= 10)) {

				// use Prague bounding box cz,praha,14.0261,14.9050,50.2801,49.8747
				TimedCoordinate t = f.getFeedback().getTrackedJourney().get(0);
				if ((49874700 <= t.getLatE6()) && (t.getLatE6() <= 50280100) && (14026100 <= t.getLonE6())
						&& (t.getLonE6() <= 14905000)) {

					// OUTPUT to file
					BufferedWriter writer = null;
					try {
						writer = new BufferedWriter(new FileWriter(String.format("tracked/%06d.tra", trackId)));
						for (TimedCoordinate c : f.getFeedback().getTrackedJourney()) {
							writer.write(String.format("%d,%d,%s\n", c.getLatE6(), c.getLonE6(), c.getTimestamp()));
						}
					} catch (IOException e) {
					} finally {
						try {
							if (writer != null)
								writer.close();
						} catch (IOException e) {
						}
					}
					trackId++;
				}
			}
		}
	}
}
