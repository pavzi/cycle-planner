package cz.agents.cycleplanner.mongodb;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;
import cz.agents.cycleplanner.api.datamodel.v2.TimedCoordinate;
import cz.agents.cycleplanner.util.GeoCalculationsHelper;

public class KmTrackedInMay {

	public static void main(String[] args) throws IOException {

		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");
		CyclePlannerStorage bicycleJourneyPlanStorage = CyclePlannerStorage.getStorage();
		assertTrue(bicycleJourneyPlanStorage.isConnected());
		List<FeedbackStorageObject> feedbacks = bicycleJourneyPlanStorage.retrieveAllCycleplannerFeedback(146);
		int trackId = 0;
		int totalDistance = 0;
		int MAX_EDGE_DISTANCE = 60;

		for (FeedbackStorageObject f : feedbacks) {

			if ((f.getFeedback().getTrackedJourney() != null) && (f.getFeedback().getTrackedJourney().size() >= 10)) {

				// limit to May 2015
				if (f.getFeedbackId() <= 2418) {

					// use Prague bounding box cz,praha,14.0261,14.9050,50.2801,49.8747
					// TimedCoordinate t = f.getFeedback().getTrackedJourney().get(0);
					// if ((49874700 <= t.getLatE6()) && (t.getLatE6() <= 50280100) && (14026100 <= t.getLonE6())
					// && (t.getLonE6() <= 14905000)) {

					// calculate distance of track
					int distance = 0;
					TimedCoordinate previous = null;
					boolean first = true;
					for (TimedCoordinate c : f.getFeedback().getTrackedJourney()) {
						if (first) {
							first = false;
						} else {
							int edgeDistance = (int) GeoCalculationsHelper.distanceHaversine(
									(double) previous.getLatE6() / 1E6, (double) previous.getLonE6() / 1E6,
									(double) c.getLatE6() / 1E6, (double) c.getLonE6() / 1E6);
							if (edgeDistance <= MAX_EDGE_DISTANCE) {
								distance += edgeDistance;
							}
						}
						previous = c;
					}
					trackId++;
					// System.out.println(String.format("%d - %d m", trackId, distance));
					System.out.println(distance);
					totalDistance += distance;
					// }
				}
			}
		}

		System.out.println("Tracked count: " + trackId);
		System.out.println("Tracked m:     " + totalDistance);
	}
}
