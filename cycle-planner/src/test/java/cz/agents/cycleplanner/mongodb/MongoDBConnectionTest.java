package cz.agents.cycleplanner.mongodb;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class MongoDBConnectionTest {

	@Test
	public void test() throws IOException {
		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");

		CyclePlannerStorage bicycleJourneyPlanStorage = CyclePlannerStorage.getStorage();

		assertTrue(bicycleJourneyPlanStorage.isConnected());
	}

}
