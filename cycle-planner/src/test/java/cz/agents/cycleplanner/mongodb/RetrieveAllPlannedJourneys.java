package cz.agents.cycleplanner.mongodb;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.api.datamodel.v2.Plan;
import cz.agents.cycleplanner.api.datamodel.v2.Response;
import cz.agents.cycleplanner.api.datamodel.v2.ResponseStatus;

public class RetrieveAllPlannedJourneys {

	private static final Logger log = Logger.getLogger(RetrieveAllPlannedJourneys.class);
	private static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy,M,d,H,m,s");
	private static final SimpleDateFormat isoDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

	public static void main(String[] args) throws IOException {

		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");
		CyclePlannerStorage bicycleJourneyPlanStorage = CyclePlannerStorage.getStorage();
		assertTrue(bicycleJourneyPlanStorage.isConnected());
		List<Response> responses = bicycleJourneyPlanStorage.retrieveAllResponses(14602L, 100000L);

		System.out.println("ResponseId,Status,Region,Client,Year,Month,Day,Hour,Minute,Second,AvgDistance");

		for (Response r : responses) {

			// avg distance from the four plans
			int avgDistance = -1;
			if (r.getPlans() != null && r.getPlans().size() > 0 && r.getStatus().equals(ResponseStatus.OK)) {
				avgDistance = 0;
				for (Plan plan : r.getPlans()) {
					avgDistance += plan.getLength();
				}
				avgDistance = avgDistance / r.getPlans().size();
			}

			System.out.println(r.getResponseId() + "," + r.getStatus() + "," + r.getRegion() + ","
					+ r.getRequest().getClient() + "," + toCSVDate(parseDate(r.getCreationTimestamp())) + ","
					+ ((avgDistance == -1) ? "NOPLAN" : avgDistance));
		}
		System.out.println("Total number of responses: " + responses.size());
	}

	public static String toCSVDate(Date date) {
		return dateParser.format(date);
	}

	public static Date parseDate(String date) {
		try {
			return isoDateParser.parse(date);
		} catch (ParseException e) {
			log.warn("cannot convert '" + date + "' to java.util.Date", e);
			return null;
		}
	}
}
