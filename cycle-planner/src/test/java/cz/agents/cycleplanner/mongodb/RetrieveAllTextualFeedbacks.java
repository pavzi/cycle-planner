package cz.agents.cycleplanner.mongodb;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;

public class RetrieveAllTextualFeedbacks {

	private static final Logger log = Logger.getLogger(RetrieveAllTextualFeedbacks.class);

	public static void main(String[] args) throws IOException {

		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");
		CyclePlannerStorage bicycleJourneyPlanStorage = CyclePlannerStorage.getStorage();
		assertTrue(bicycleJourneyPlanStorage.isConnected());
		List<FeedbackStorageObject> feedbacks = bicycleJourneyPlanStorage.retrieveAllCycleplannerFeedback(99);

		System.out.println("ID,Timestamp,ResponseID,Rating,Priority,Description,Resolution,URL");
		for (FeedbackStorageObject f : feedbacks) {
			if (f.getFeedback().getTextualFeedback() != null && f.getFeedback().getTextualFeedback().length() > 0
					&& !f.getFeedback().getTextualFeedback().equals("AUTOMATIC FEEDBACK")) {

				long responseId = -1;
				if (f.getFeedback().getResponseId() != null && (1 <= f.getFeedback().getResponseId())
						&& (f.getFeedback().getResponseId() < 9223372036854775807L)) {
					responseId = f.getFeedback().getResponseId();
				}
				int rating = -1;
				if (f.getFeedback().getRating() != null && (1 <= f.getFeedback().getRating())
						&& (f.getFeedback().getRating() <= 5)) {
					rating = f.getFeedback().getRating();
				}

				// System.out.println(JSONUtils.javaObjectToJson(f.getFeedback().getTrackedJourney()));
				// System.out.println(f);
				// System.out.println(f.getFeedback().getResponseId());

				System.out.println(f.getFeedbackId()
						+ ","
						+ f.getCreationTimestamp()
						+ ","
						+ responseId
						+ ","
						+ rating
						+ ",' ',"
						+ "'"
						+ f.getFeedback().getTextualFeedback().replaceAll("^\\s|\n\\s|\\s$", "").replace("'", "\"")
						+ "'"
						+ ((responseId != -1) ? ",' ',http://transport.felk.cvut.cz/cykloplanovac/#?responseId="
								+ f.getFeedback().getResponseId() : ""));
			}

		}
		System.out.println("Total number of feedbacks: " + feedbacks.size());
	}

}
