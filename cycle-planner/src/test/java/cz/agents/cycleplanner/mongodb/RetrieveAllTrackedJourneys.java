package cz.agents.cycleplanner.mongodb;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;

public class RetrieveAllTrackedJourneys {

	private static final Logger log = Logger.getLogger(RetrieveAllTrackedJourneys.class);

	public static void main(String[] args) throws IOException {

		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");
		CyclePlannerStorage bicycleJourneyPlanStorage = CyclePlannerStorage.getStorage();
		assertTrue(bicycleJourneyPlanStorage.isConnected());
		List<FeedbackStorageObject> feedbacks = bicycleJourneyPlanStorage.retrieveAllCycleplannerFeedback(2643);

		System.out
				.println("FeedbackId,Year,Month,Day,Hour,Minute,Second,ResponseID,Rating,Description,CoordinatesCount");

		for (FeedbackStorageObject f : feedbacks) {

			if (f.getFeedback().getTrackedJourney() != null) {

				long responseId = -1;
				if (f.getFeedback().getResponseId() != null && (1 <= f.getFeedback().getResponseId())
						&& (f.getFeedback().getResponseId() < 9223372036854775807L)) {
					responseId = f.getFeedback().getResponseId();
				}
				int rating = -1;
				if (f.getFeedback().getRating() != null && (1 <= f.getFeedback().getRating())
						&& (f.getFeedback().getRating() <= 5)) {
					rating = f.getFeedback().getRating();
				}
				String text = "NOFEEDBACK";
				if (f.getFeedback().getTextualFeedback() != null && f.getFeedback().getTextualFeedback().length() > 0) {
					text = f.getFeedback().getTextualFeedback().replaceAll("^\\s|\n\\s|\\s$", "").replace("'", "\"");
				}

				System.out.println(f.getFeedbackId()
						+ ","
						+ RetrieveAllPlannedJourneys.toCSVDate(RetrieveAllPlannedJourneys.parseDate(f
								.getCreationTimestamp())) + "," + responseId + "," + rating + ",'" + text + "',"
						+ f.getFeedback().getTrackedJourney().size());

				// if (f.getFeedbackId() == 931) {
				// System.out.println(JSONUtils.javaObjectToJson(f.getFeedback().getTrackedJourney()));
				// }
			}
		}
		System.out.println("Total number of tracks: " + feedbacks.size());
	}

}
