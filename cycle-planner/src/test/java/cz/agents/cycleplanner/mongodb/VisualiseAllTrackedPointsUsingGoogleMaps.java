package cz.agents.cycleplanner.mongodb;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;
import cz.agents.cycleplanner.api.datamodel.v2.TimedCoordinate;

@Deprecated
public class VisualiseAllTrackedPointsUsingGoogleMaps {

	private static final Logger log = Logger.getLogger(VisualiseAllTrackedPointsUsingGoogleMaps.class);

	public static void main(String[] args) throws IOException {

		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");
		CyclePlannerStorage bicycleJourneyPlanStorage = CyclePlannerStorage.getStorage();
		assertTrue(bicycleJourneyPlanStorage.isConnected());
		List<FeedbackStorageObject> feedbacks = bicycleJourneyPlanStorage.retrieveAllCycleplannerFeedback(146);
		int points = 0;

		for (FeedbackStorageObject f : feedbacks) {

			if (f.getFeedback().getTrackedJourney() != null) {

				for (TimedCoordinate c : f.getFeedback().getTrackedJourney()) {
					points++;
					System.out
							.println(String
									.format(Locale.UK,
											"new google.maps.Marker({position: new google.maps.LatLng(%.6f, %.6f), map: map, icon: image});",
											(double) c.getLatE6() / 1e6, (double) c.getLonE6() / 1.e6));
				}

			}
		}
		System.out.println("Total number of points: " + points);
	}

}
