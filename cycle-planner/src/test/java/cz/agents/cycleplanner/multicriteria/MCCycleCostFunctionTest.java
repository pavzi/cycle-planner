package cz.agents.cycleplanner.multicriteria;

import static org.junit.Assert.assertArrayEquals;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.multicriteria.MCCycleCostFunction;
import cz.agents.cycleplanner.multicriteria.MCCycleCostFunction2;
import eu.superhub.wp5.wp5common.location.GPSLocation;

public class MCCycleCostFunctionTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
				
		MCCycleCostFunction costFunction = new MCCycleCostFunction();
		CycleNode2 origin = new CycleNode2(0, new GPSLocation(1, 1, 1, 1, 300d), 
				"mc cycle cost function test", new HashSet<String>());
		CycleNode2 destination = new CycleNode2(1, new GPSLocation(1, 2, 1, 2, 300d), 
				"mc cycle cost function test", new HashSet<String>());

		int[] costs = {25, 50, 40, 90};
		CycleEdge2 edge = new CycleEdge2(origin.getId(), destination.getId(), 1d, null, costs, 0, 0, 1, 5.5, 0, 0, 0, null, null, null, null, null);
		int[] mcCosts = costFunction.getCostVector(origin, destination, edge);
		
		assertArrayEquals(new int[]{25, 55, 90}, mcCosts);
	}
	
	@Test
	public void test2() {
				
		MCCycleCostFunction2 costFunction = new MCCycleCostFunction2();
		CycleNode2 origin = new CycleNode2(0, new GPSLocation(1, 1, 1, 1, 300d), 
				"mc cycle cost function test", new HashSet<String>());
		CycleNode2 destination = new CycleNode2(1, new GPSLocation(1, 2, 1, 2, 300d), 
				"mc cycle cost function test", new HashSet<String>());

		int[] costs = {25, 50, 40, 90};
		CycleEdge2 edge = new CycleEdge2(origin.getId(), destination.getId(), 1d, null, costs, 0, 0, 1, 5.5, 0, 0, 0, null, null, null, null, null);
		int[] mcCosts = costFunction.getCostVector(origin, destination, edge);
		
		assertArrayEquals(new int[]{1, 55, 90}, mcCosts);
	}
}
