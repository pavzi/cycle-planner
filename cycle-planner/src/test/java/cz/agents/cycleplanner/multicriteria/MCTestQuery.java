package cz.agents.cycleplanner.multicriteria;

public class MCTestQuery {
	
		private long origin;
		private long destination;

		public MCTestQuery(long origin, long destination) {
			super();
			this.origin = origin;
			this.destination = destination;
		}

		public long getOrigin() {
			return origin;
		}

		public long getDestination() {
			return destination;
		}
	
}
