package cz.agents.cycleplanner.plannernew;

import java.util.Scanner;

public class CycleZoneProviderTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Type something to run a program!");
		scanner.next();

		CycleZoneProvider cycleZoneProvider = CycleZoneProvider.INSTANCE;

		System.out.println("Type something to shutdown a program!");
		scanner.next();
	}

}
