package cz.agents.cycleplanner.plannernew;

import java.util.List;
import java.util.Scanner;

import cz.agents.cyclestructures.CycleNode;

public class NearestNodeServiceTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Type something to run a program!");
		scanner.next();

		NearestNodeService nearestNodeService = NearestNodeService.INSTANCE;

		List<CycleNode> nearestNodes = nearestNodeService.getNearestNodes("prague", 50076617,
				14417587);
		System.out.println(nearestNodes);

		System.out.println("Type something to shutdown a program!");
		scanner.next();

	}

}
