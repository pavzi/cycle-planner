package cz.agents.cycleplanner.arguments;

import java.util.Arrays;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import cz.agents.cycleplanner.planner.mc.SupportedMCAlgorithms;

public class CycleplannerExperimentArgumentParser extends CommandLineArgumentsParser {

	public CycleplannerExperimentArgumentParser(String[] args) {
		super(args);
	}

	@Override
	protected Options getOptions() {
		Options options = new Options();
		
		options.addOption("reg", "region", true, "Currently supported regions: prague, praguejunction, praguemediuma, praguemediumajunction, praguemediumbjunction, praguemediumcjunction");
		
		options.addOption("alg", "algorithm", true,
				"Currently supported implementations: dijkstra,"
						+ Arrays.toString(SupportedMCAlgorithms.values()).toLowerCase().replace('[', ' ').replace("]", ""));
		
		options.addOption("w", "warmup", true, "specify number of runs of algorithm for warm up period");
		options.addOption("r", "requests", true, "specify number of requests per experiment");
		options.addOption("ct", "cutting-time", true, "maximal running time in milliseconds for one request");
		
		options.addOption("seed", true, "");
		options.addOption("mind", "min-direct-distance", true, "");
		options.addOption("maxd", "max-direct-distance", true, "");
		
		options.addOption("s", "speed-kmph", true, "");
		options.addOption("wtt", "", true, "travel time criterion weight");
		options.addOption("wco", "", true, "comfort criterion weight");
		options.addOption("wq", "", true, "quietness criterion weight");
		options.addOption("weg", "", true, "elevation gain criterion weight");
		
		options.addOption("gamma", true, "parameter for cost pruning speedup technique");
		options.addOption("aOverB", true,
				"parameter for ellipse speedup technique, represents the ratio between ellipse's parameters a and b");
		options.addOption("alpha", true, "parameter for ratio pruning speedup technique");
		options.addOption("epsilon", true, "parameter for epsilon dominance speedup technique");

		@SuppressWarnings("static-access")
		Option buckets = OptionBuilder.withArgName("buckets").hasArgs()
				.withDescription("parameters for buckets speedup technique, specify one bucket value per criterion")
				.create("buckets");
		options.addOption(buckets);
		
		options.addOption("d", "dir", true, "path to directory with results");
				
		return options;
	}
	
	public String getRegion() {
		return cmd.getOptionValue("region", null);
	}
	
	public long getCuttingTime() {
		return (cmd.hasOption("ct")) ? Long.parseLong(cmd.getOptionValue("ct")) : Long.MAX_VALUE;
	}
	
	public String getAlgorithm() {
		return cmd.getOptionValue("alg", null);
	}
	
	public int getNumberOfWarmUpRuns() {
		return Integer.parseInt(cmd.getOptionValue('w', "0"));
	}
	
	public int getNumberOfRequests() {
		return Integer.parseInt(cmd.getOptionValue('r', "0"));
	}
	
	public long getRandomSeed() {
		return Long.parseLong(cmd.getOptionValue("seed", "103"));
	}
	
	public int getMinDirectDistance() {
		return Integer.parseInt(cmd.getOptionValue("mind", "500"));
	}
	
	public int getMaxDirectDistance() {
		return Integer.parseInt(cmd.getOptionValue("maxd", "12000"));
	}
	
	public Integer getGamma() {
		return cmd.hasOption("gamma") ? Integer.parseInt(cmd.getOptionValue("gamma")) : null;
	}
	
	public Double getAOverB() {
		return cmd.hasOption("aOverB") ? Double.parseDouble(cmd.getOptionValue("aOverB")) : null;
	}
	
	public Double getAlpha() {
		return cmd.hasOption("alpha") ? Double.parseDouble(cmd.getOptionValue("alpha")) : null;
	}
	
	public Double getEpsilon() {
		return cmd.hasOption("epsilon") ? Double.parseDouble(cmd.getOptionValue("epsilon")) : null;
	}
	
	public int[] getBuckets() {
		if (cmd.hasOption("buckets")) {

			String[] bucketsAsString = cmd.getOptionValues("buckets");
			int[] buckets = new int[bucketsAsString.length];

			for (int i = 0; i < buckets.length; i++) {
				buckets[i] = Integer.parseInt(bucketsAsString[i]);
			}

			return buckets;
		}
		return null;
	}
	
	public String getResultDirectory() {
		return cmd.getOptionValue('d', "");
	}
	
	public double getSpeedKMpH() {
		return Double.parseDouble(cmd.getOptionValue('s', "15"));
	}
	
	public double getTravelTimeWeight() {
		return Double.parseDouble(cmd.getOptionValue("wtt", "3"));
	}
	
	public double getComfortWeight() {
		return Double.parseDouble(cmd.getOptionValue("wco", "5"));
	}
	
	public double getQuietnessWeight() {
		return Double.parseDouble(cmd.getOptionValue("wq", "1"));
	}
	
	public double getElevationGainWeight() {
		return Double.parseDouble(cmd.getOptionValue("weg", "1"));
	}
}
