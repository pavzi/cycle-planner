package cz.agents.cycleplanner.experiment;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.jgrapht.GraphPath;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.planner.MultipleGoalDestinationBicyclePlanner;
import cz.agents.cycleplanner.statistics.ExperimentStatistics;
import cz.agents.cycleplanner.statistics.MLCLineStatistic;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.utils.EdgeUtil;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedEdge;
import eu.superhub.wp5.plannercore.structures.timedstructures.TimedNode;

public class CycleplannerExperiment implements Experiment2 {

	private static final Logger log = Logger.getLogger(CycleplannerExperiment.class);

	private MultipleGoalDestinationBicyclePlanner planner;
	private ExperimentStatistics statistics;

	public CycleplannerExperiment(MultipleGoalDestinationBicyclePlanner planner, ExperimentStatistics statistics) {
		this.planner = planner;
		this.statistics = statistics;
	}

	@Override
	public void run(String cityName, Graph<CycleNode2, CycleEdge2> graph, int i, List<CycleNode2> originNodes,
			List<CycleNode2> destinationNodes, double averageSpeedKMpH, CriteriaWeightVector profile) {
		try {
			long planningTime = System.currentTimeMillis();
			Future<Map<Long, GraphPath<TimedNode, TimedEdge>>> future = planner.plan(graph, originNodes,
					destinationNodes, averageSpeedKMpH, profile);

			future.get();

			planningTime = System.currentTimeMillis() - planningTime;

			statistics.add(new MLCLineStatistic("", planner.toString(), cityName, i, originNodes.get(0).getId(),
					destinationNodes.get(0).getId(), (int) EdgeUtil.computeDirectDistanceInM(originNodes.get(0),
							destinationNodes.get(0)), 0, 0, (long) planner.getAverageClosedSetSize(), planningTime, 0,
					0d, 0d, 0d, 0, 0, 0, 0, 0, 0));

		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
