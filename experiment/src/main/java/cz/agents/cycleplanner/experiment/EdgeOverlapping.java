package cz.agents.cycleplanner.experiment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.EdgeId;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.elements.Node;

public class EdgeOverlapping {
	
	private static Logger log = Logger.getLogger(EdgeOverlapping.class);
	
	private final Graph<CycleNode2, CycleEdge2> graph;
	private Map<EdgeId, CycleEdge2> edges;
	private Map<EdgeId, Double> widths;
	private Map<EdgeId, String> colours;

	private CycleNode2 origin = null;
	private CycleNode2 destination = null;

	public EdgeOverlapping(Graph<CycleNode2, CycleEdge2> graph) {
		this.graph = graph;
	}
	
	public void create(CycleNode2 origin, CycleNode2 destination, Collection<List<CycleEdge2>> paretoSet, File file) {

		this.origin = origin;
		this.destination = destination;
		
		edges = new HashMap<EdgeId, CycleEdge2>();
		widths = new HashMap<EdgeId, Double>();
		colours = new HashMap<EdgeId, String>();
		
		for (Collection<CycleEdge2> journey : paretoSet) {
			
			for (CycleEdge2 edge : journey) {
				
				EdgeId edgeId = edge.getEdgeId();
				
				edges.put(edgeId, edge);
				
				if (widths.containsKey(edgeId)) {
					widths.put(edgeId, widths.get(edgeId) + 1);
				} else {
					widths.put(edgeId, 1d);
				}
			}			
		}
		
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;

		for (EdgeId id : widths.keySet()) {
			double value = widths.get(id);
			max = Math.max(value, max);
			min = Math.min(value, min);
		}

		for (EdgeId id : widths.keySet()) {
			// spocitam tak hrubku aby pre najmenej prekryvanu hranu bola
			// hrubka nula a pre najviac zase 12 (najvacsia zvolena hrubka
			// ciary)
			double width = widths.get(id);
			widths.put(id, ((width - min) / (max - min)) * 12);

			// spocitam odtien cervenej na stupncic od 150-255, teda
			// najmenej prekryvana bude mat najtmavsiu farbu a najviac
			// prekryvana najbledsiu

			int colour = (int) (150 + ((width - min) / (max - min)) * 105);
			colours.put(id, "#" + Integer.toHexString(colour) + "0000");
			// log.debug("Colour: #" + Integer.toHexString(colour) +
			// "0000");
		}
		
		save(file);
	}

	private void save(File file) {
		log.info("Saving...");
		
		try {
			
			PrintStream c = new PrintStream(file);
			
			c.print("{ \"markers\":{\"origin\":[");
			c.print(origin.getLongitude());
			c.print(", ");
			c.print(origin.getLatitude());
			c.print("], \"destination\":[");
			c.print(destination.getLongitude());
			c.print(", ");
			c.print(destination.getLatitude());
			c.print("]}, \"edges\":[");
			
			int j = 0;
			
			for (EdgeId id : widths.keySet()) {
				Node from = graph.getNodeByNodeId(id.getFromNodeId());
				Node to = graph.getNodeByNodeId(id.getToNodeId());
				
				c.print("{\"from\":[");
				c.print(from.getLongitude());
				c.print(", ");
				c.print(from.getLatitude());
				c.print("], \"to\":[");
				c.print(to.getLongitude());
				c.print(", ");
				c.print(to.getLatitude());
				c.print("], \"overlaps\":");
				c.print(widths.get(id));
				c.print(", \"colour\":\"");
				c.print(colours.get(id));
				c.print("\"}");

				if (++j < widths.size()) {
					c.print(", ");
				}
			}
			c.print("]}");
			c.flush();
			c.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
