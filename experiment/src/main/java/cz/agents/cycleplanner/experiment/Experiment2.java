package cz.agents.cycleplanner.experiment;

import java.util.List;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.Graph;

/**
 * An experiment.
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public interface Experiment2 {

	/**
	 * Runs experiment.
	 */
	public void run(String cityName, Graph<CycleNode2, CycleEdge2> graph, int i, List<CycleNode2> originNodes,
			List<CycleNode2> destinationNodes, double averageSpeedKMpH, CriteriaWeightVector profile);
}
