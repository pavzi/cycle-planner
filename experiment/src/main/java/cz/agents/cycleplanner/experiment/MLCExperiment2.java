package cz.agents.cycleplanner.experiment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.multicriteria.Label;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import cz.agents.cycleplanner.multicriteria.ParetoSet;
import cz.agents.cycleplanner.planner.mc.MultiCriteriaBicyclePlanner;
import cz.agents.cycleplanner.statistics.ExperimentStatistics;
import cz.agents.cycleplanner.statistics.MLCLineStatistic;
import cz.agents.cycleplanner.util.EuclideanDistanceCalculator;
import cz.agents.cycleplanner.util.JaccardDistanceCalculator;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.utils.EdgeUtil;

/**
 * An implementation of an experiment on multi-label correcting algorithm.
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class MLCExperiment2 implements Experiment2 {

	private final static Logger log = Logger.getLogger(MLCExperiment2.class);

	private String resultsDirectoryName;

	private MultiCriteriaBicyclePlanner planner;
	private long cuttingTime;
	private ExperimentStatistics statistics;
	
	
	public MLCExperiment2(MultiCriteriaBicyclePlanner planner, String outputDir, ExperimentStatistics statistics, long cuttingTime) {
		this.planner = planner;
		this.cuttingTime = cuttingTime;
		
		this.resultsDirectoryName = outputDir;
				
		this.statistics = statistics;
	}
	
	@Override
	public void run(String cityName, Graph<CycleNode2, CycleEdge2> graph, int i, List<CycleNode2> originNodes,
			List<CycleNode2> destinationNodes, double averageSpeedKMpH, CriteriaWeightVector profile) {
				
		try {

			log.info("Loading optimal Pareto set...");
			Collection<List<CycleEdge2>> optimalParetoSetOfPaths = loadParetoSet(cityName, i);
			Set<CycleEdge2> optimalParetoSetOfEdges = flattenToSetOfEdges(optimalParetoSetOfPaths);

			long planningTime = System.currentTimeMillis();
			Future<ParetoSet<CycleNode2, CycleEdge2>> future = planner.plan(graph, originNodes,
					destinationNodes, averageSpeedKMpH, profile);
			ParetoSet<CycleNode2, CycleEdge2> paretoSet = future.get(cuttingTime, TimeUnit.MILLISECONDS);
			
			planningTime = System.currentTimeMillis() - planningTime;
			
			Collection<List<CycleEdge2>> paretoSetOfPaths = paretoSet.getAllSequenceOfEdges();
			Set<CycleEdge2> paretoSetOfEdges = flattenToSetOfEdges(paretoSetOfPaths);
			
			EdgeOverlapping edgeOverlapping = new EdgeOverlapping(graph);
			edgeOverlapping.create(originNodes.get(0),
					destinationNodes.get(0), paretoSetOfPaths, 
					new File(resultsDirectoryName + "/edges_"	+ planner.toString() + "_" + i + ".json"));

			log.info("Serializing Pareto set of cycle plans...");
			serializeParetoSet(paretoSet, new File(resultsDirectoryName + "pareto_set_" + i
					+ ".javaobject"));

			int numOfJointPlans = getNumberOfJointPlans(optimalParetoSetOfPaths, paretoSetOfPaths);
			log.info("Number of joint plans: " + numOfJointPlans);

			double averageMinJaccardDistance = computeAverageMinJaccardDistance(optimalParetoSetOfPaths,
					paretoSetOfPaths);
			log.info("Average minimal Jaccard Distance from optimal Pareto set to heuristic Pareto set is "
					+ averageMinJaccardDistance);

			double averageMinCostSpaceDistance = computeAverageMinCostSpaceDistance(optimalParetoSetOfPaths,
					paretoSetOfPaths, graph, planner.getCostFunction());
			log.info("Average minimal distance in cost space from optimal Pareto set to heuristic Pareto set is "
					+ averageMinCostSpaceDistance);

			double jdEdges = JaccardDistanceCalculator.calculate(optimalParetoSetOfEdges, paretoSetOfEdges);
			log.info("Jaccard distance of sets of edges of all plans: " + jdEdges);

			Collection<Label<CycleNode2>> destinationBag = paretoSet.getParetoSetLabels();
			int[][] maxMinOfLabels = getMaximumAndMinimumOfLabels(destinationBag);
			
			CycleNode2 origin = originNodes.get(0);
			CycleNode2 destination = destinationNodes.get(0);

			statistics.add(new MLCLineStatistic(planner.getMCAlgorithm().toString(), planner.toString(), cityName, i,
					origin.getId(), destination.getId(), (int) EdgeUtil.computeDirectDistanceInM(origin, destination), optimalParetoSetOfPaths.size(),
					destinationBag.size(), paretoSet.getNumberOfNodesExpansions(), planningTime, numOfJointPlans,
					averageMinJaccardDistance, averageMinCostSpaceDistance, jdEdges, maxMinOfLabels[0][0],
					maxMinOfLabels[0][1], maxMinOfLabels[1][0], maxMinOfLabels[1][1], maxMinOfLabels[2][0],
					maxMinOfLabels[2][1]));
			
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			log.error(e.getMessage(), e);
		}
		
	}

	/**
	 * Loads serialized Pareto set of cycle plans.
	 * 
	 * @param paretoSetIndex
	 *            Pareto set identifier
	 * @return collection of cycle plans
	 */
	@SuppressWarnings("unchecked")
	private Collection<List<CycleEdge2>> loadParetoSet(String cityName, int paretoSetIndex) {
		String pathToParetoSet = "/" + cityName + "/pareto_set_" + paretoSetIndex + ".javaobject";
		ParetoSet<CycleNode2, CycleEdge2> loadedParetoSet = null;
		log.info("Path to pareto set: " + pathToParetoSet);
		try {
			ObjectInputStream ois = new ObjectInputStream(this.getClass().getResourceAsStream(pathToParetoSet));

			loadedParetoSet = (ParetoSet<CycleNode2, CycleEdge2>) ois.readObject();

			ois.close();

		} catch (FileNotFoundException e) {
			log.error("Could not find a file! " + e.getMessage(), e);
		} catch (IOException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (NullPointerException e) {
			log.error("No such a resource! " + e.getMessage(), e);
			return new ArrayList<>();
		}

		return loadedParetoSet.getAllSequenceOfEdges();
	}

	/**
	 * Flatten collection of cycle plans to set of <code>CycleEdge2</code>.
	 * 
	 * @param paretoSetOfPaths
	 *            collection of cycle plans
	 * @return set of <code>CycleEdge2</code>
	 */
	private Set<CycleEdge2> flattenToSetOfEdges(Collection<List<CycleEdge2>> paretoSetOfPaths) {
		Set<CycleEdge2> paretoSetOfEdges = new HashSet<>();

		for (Collection<CycleEdge2> paths : paretoSetOfPaths) {
			for (CycleEdge2 cycleEdge : paths) {
				paretoSetOfEdges.add(cycleEdge);
			}
		}

		return paretoSetOfEdges;
	}

	/**
	 * Serialize Pareto set of cycle plans.
	 * 
	 * @param paretoSet
	 *            collection of cycle plans
	 * @param f
	 *            file where Pareto set is serialized in
	 */
	private void serializeParetoSet(ParetoSet<CycleNode2, CycleEdge2> paretoSet, File f) {
		try {

			FileOutputStream fos = new FileOutputStream(f);

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(paretoSet);
			oos.close();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * Calculate number of cycle plans that are present in sub-optimal Pareto
	 * set as well as in optimal Pareto set
	 * 
	 * @param optimal
	 *            optimal pareto set of cycle plans
	 * @param subOptimal
	 *            sub-optimal pareto set of cycle plans
	 * @return number of joint cycle plans among two sets
	 */
	private int getNumberOfJointPlans(Collection<List<CycleEdge2>> optimal,
			Collection<List<CycleEdge2>> subOptimal) {
		int count = 0;

		for (Collection<CycleEdge2> subOptimalPlan : subOptimal) {

			for (Collection<CycleEdge2> optimalPlan : optimal) {
				boolean areEqual = isFirstPlanEqualsSecond(subOptimalPlan, optimalPlan);

				if (areEqual) {
					count++;
					break;
				}
			}
		}

		return count;
	}

	/**
	 * Compares first specified cycle plan to second.
	 * 
	 * @param first
	 *            cycle plan
	 * @param second
	 *            cycle plan
	 * @return <code>true</code> if first plan is equal to second, otherwise
	 *         <code>false</code>
	 */
	private boolean isFirstPlanEqualsSecond(Collection<CycleEdge2> first, Collection<CycleEdge2> second) {

		if (first.size() != second.size()) {
			return false;
		}

		Iterator<CycleEdge2> firstIterator = first.iterator();
		Iterator<CycleEdge2> secondIterator = second.iterator();

		while (firstIterator.hasNext() && secondIterator.hasNext()) {

			if (!firstIterator.next().equals(secondIterator.next())) {
				return false;
			}

		}

		return true;
	}

	/**
	 * 
	 * Calculates average minimal Jaccard distance between plans in optimal
	 * Pareto set and sub-optimal.
	 * 
	 * @param optimalParetoSetOfPaths
	 *            optimal pareto set of cycle plans
	 * @param subOptimalParetoSetOfPaths
	 *            sub-optimal pareto set of cycle plans
	 * @return average minimal distance Jaccard distance
	 */
	private double computeAverageMinJaccardDistance(Collection<List<CycleEdge2>> optimalParetoSetOfPaths,
			Collection<List<CycleEdge2>> subOptimalParetoSetOfPaths) {

		Set<Set<CycleEdge2>> optimalParetoSetOfSetsOfEdges = new HashSet<>();

		for (Collection<CycleEdge2> collection : optimalParetoSetOfPaths) {
			Set<CycleEdge2> optimal = new HashSet<>(collection);

			optimalParetoSetOfSetsOfEdges.add(optimal);
		}

		Set<Set<CycleEdge2>> heuristicParetoSetOfSetsOfEdges = new HashSet<>();

		for (Collection<CycleEdge2> collection : subOptimalParetoSetOfPaths) {
			Set<CycleEdge2> heuristic = new HashSet<>(collection);

			heuristicParetoSetOfSetsOfEdges.add(heuristic);
		}

		double averageMin = 0;

		for (Set<CycleEdge2> optimal : optimalParetoSetOfSetsOfEdges) {

			double min = Double.MAX_VALUE;

			for (Set<CycleEdge2> heuristic : heuristicParetoSetOfSetsOfEdges) {
				double jaccardDistance = JaccardDistanceCalculator.calculate(optimal, heuristic);

				min = (min > jaccardDistance) ? jaccardDistance : min;
				// log.debug("min JD: "+min +" JD: "+jaccardDistance);
			}

			averageMin += min;
		}

		return averageMin / optimalParetoSetOfSetsOfEdges.size();
	}

	/**
	 * Calculates average minimal distance in cost space between plans in
	 * optimal Pareto set and sub-optimal.
	 * 
	 * @param optimalParetoSetOfPaths
	 *            optimal pareto set of cycle plans
	 * @param subOptimalParetoSetOfPaths
	 *            sub-optimal pareto set of cycle plans
	 * @return average minimal distance in cost space
	 */
	private double computeAverageMinCostSpaceDistance(Collection<List<CycleEdge2>> optimalParetoSetOfPaths,
			Collection<List<CycleEdge2>> subOptimalParetoSetOfPaths, Graph<CycleNode2, CycleEdge2> graph, MCCostFunction<CycleNode2, CycleEdge2> costFunction) {

		double[] minCostVector = new double[3];
		double[] maxCostVector = new double[3];

		Arrays.fill(minCostVector, Double.MAX_VALUE);
		Arrays.fill(maxCostVector, Double.MIN_VALUE);

		Set<double[]> optimalCostVectors = new HashSet<>();

		for (Collection<CycleEdge2> collection : optimalParetoSetOfPaths) {

			double[] costVector = new double[3];

			for (CycleEdge2 cycleEdge : collection) {
				CycleNode2 fromNode = graph.getNodeByNodeId(cycleEdge.getFromNodeId());
				CycleNode2 toNode = graph.getNodeByNodeId(cycleEdge.getToNodeId());
				int[] cycleEdgeCostVector = costFunction.getCostVector(fromNode, toNode, cycleEdge);

				for (int i = 0; i < costVector.length; i++) {
					costVector[i] += cycleEdgeCostVector[i];
				}
			}

			for (int i = 0; i < costVector.length; i++) {
				minCostVector[i] = (minCostVector[i] > costVector[i]) ? costVector[i] : minCostVector[i];
				maxCostVector[i] = (maxCostVector[i] < costVector[i]) ? costVector[i] : maxCostVector[i];
			}

			optimalCostVectors.add(costVector);
		}

		// log.info("mins: " + Arrays.toString(minCostVector));
		// log.info("max: " + Arrays.toString(maxCostVector));

		Set<double[]> heuristicCostVectors = new HashSet<>();

		for (Collection<CycleEdge2> collection : subOptimalParetoSetOfPaths) {

			double[] costVector = new double[3];

			for (CycleEdge2 cycleEdge : collection) {
				CycleNode2 fromNode = graph.getNodeByNodeId(cycleEdge.getFromNodeId());
				CycleNode2 toNode = graph.getNodeByNodeId(cycleEdge.getToNodeId());
				int[] cycleEdgeCostVector = costFunction.getCostVector(fromNode, toNode, cycleEdge);

				for (int i = 0; i < costVector.length; i++) {
					costVector[i] += cycleEdgeCostVector[i];
				}
			}

			for (int i = 0; i < costVector.length; i++) {
				minCostVector[i] = (minCostVector[i] > costVector[i]) ? costVector[i] : minCostVector[i];
				maxCostVector[i] = (maxCostVector[i] < costVector[i]) ? costVector[i] : maxCostVector[i];
			}

			heuristicCostVectors.add(costVector);
		}

		for (double[] optimalCostVector : optimalCostVectors) {

			// log.debug("optimal before: "+Arrays.toString(optimalCostVector));
			for (int i = 0; i < optimalCostVector.length; i++) {
				optimalCostVector[i] = (optimalCostVector[i] - minCostVector[i])
						/ (maxCostVector[i] - minCostVector[i]);
			}

			// log.debug("optimal after: "+Arrays.toString(optimalCostVector));
		}

		for (double[] heuristicCostVector : heuristicCostVectors) {

			// log.debug("heuristic before: " +
			// Arrays.toString(heuristicCostVector));
			for (int i = 0; i < heuristicCostVector.length; i++) {
				heuristicCostVector[i] = (heuristicCostVector[i] - minCostVector[i])
						/ (maxCostVector[i] - minCostVector[i]);
			}

			// log.debug("heuristic after: " +
			// Arrays.toString(heuristicCostVector));
		}

		double averageMin = 0;

		for (double[] optimalCostVector : optimalCostVectors) {

			double min = Double.MAX_VALUE;

			for (double[] heuristicCostVector : heuristicCostVectors) {

				double euclideanDistance = EuclideanDistanceCalculator
						.calculate(optimalCostVector, heuristicCostVector);

				min = (min > euclideanDistance) ? euclideanDistance : min;
			}

			averageMin += min;
		}

		return averageMin / optimalCostVectors.size();
	}

	/**
	 * Collect maximal and minimal value of criteria for final plans.
	 * 
	 * @param destinationBag
	 *            collection of labels from destination node
	 * @return maximal and minimal value for each criteria
	 */
	private int[][] getMaximumAndMinimumOfLabels(Collection<Label<CycleNode2>> destinationBag) {

		int[][] maxMinOfLabels = new int[3][2];

		for (int i = 0; i < maxMinOfLabels.length; i++) {
			maxMinOfLabels[i][0] = Integer.MIN_VALUE;
			maxMinOfLabels[i][1] = Integer.MAX_VALUE;
		}

		for (Label<CycleNode2> label : destinationBag) {
			for (int i = 0; i < maxMinOfLabels.length; i++) {

				if (maxMinOfLabels[i][0] <= label.getCostVector()[i]) {
					maxMinOfLabels[i][0] = label.getCostVector()[i];
				}

				if (maxMinOfLabels[i][1] > label.getCostVector()[i]) {
					maxMinOfLabels[i][1] = label.getCostVector()[i];
				}
			}
		}

		return maxMinOfLabels;
	}

	

}
