package cz.agents.cycleplanner.experiment;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.apache.log4j.Logger;

import cz.agents.cycleplanner.aStar.CriteriaWeightVector;
import cz.agents.cycleplanner.api.PlanningInstance2;
import cz.agents.cycleplanner.api.PlanningInstanceBuilder;
import cz.agents.cycleplanner.api.datamodel.BoundingBox;
import cz.agents.cycleplanner.api.datamodel.v2.Coordinate;
import cz.agents.cycleplanner.api.datamodel.v2.SpecialCoordinateType;
import cz.agents.cycleplanner.arguments.CycleplannerExperimentArgumentParser;
import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.exceptions.OutOfBoundsException;
import cz.agents.cycleplanner.exceptions.PlannerException;
import cz.agents.cycleplanner.multicriteria.MCCostFunction;
import cz.agents.cycleplanner.multicriteria.MCCycleCostFunction2;
import cz.agents.cycleplanner.originDestination.OriginDestinationLocationGenerator;
import cz.agents.cycleplanner.originDestination.OriginDestinationPair;
import cz.agents.cycleplanner.planner.BicyclePlanner;
import cz.agents.cycleplanner.planner.MultipleGoalDestinationBicyclePlanner;
import cz.agents.cycleplanner.planner.mc.MultiCriteriaBicyclePlanner;
import cz.agents.cycleplanner.planner.mc.SupportedMCAlgorithms;
import cz.agents.cycleplanner.routingService.FirstLastMileGraph;
import cz.agents.cycleplanner.routingService.RoutingService3;
import cz.agents.cycleplanner.statistics.ExperimentStatistics;
import cz.agents.cycleplanner.statistics.MLCHeaderStatistics;
import eu.superhub.wp5.graphcommon.graph.EdgeId;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.wp5common.location.Location;

public class RunExperiment2 {

	private static final String RESULTS_DIR = "results/";

	private static final Logger log = Logger.getLogger(RunExperiment2.class);

	public static void main(String[] args) throws OutOfBoundsException, PlannerException, IOException {
		CycleplannerExperimentArgumentParser cli = new CycleplannerExperimentArgumentParser(args);
		String region = cli.getRegion();

		if (region == null) {
			log.error("No region specified! " + Arrays.toString(args));
			System.exit(-1);
		}
		if (cli.getAlgorithm() == null) {
			log.error("No algorithm specified! " + Arrays.toString(args));
			System.exit(-1);
		}

		RoutingService3 routingService = RoutingService3.INSTANCE;
		CityData<CycleNode2, CycleEdge2> cityData = routingService.getCityCycleData(region);
		PlanningInstanceBuilder planningInstanceBuilder = new PlanningInstanceBuilder();
		BoundingBox boundingBox = new BoundingBox(cityData.getLeftE6(), cityData.getTopE6(), cityData.getRightE6(),
				cityData.getBottomE6());
		OriginDestinationLocationGenerator generator = new OriginDestinationLocationGenerator(cli.getRandomSeed(),
				boundingBox, cli.getMaxDirectDistance(), cli.getMinDirectDistance());

		Experiment2 exp;
		ExperimentStatistics statistics;
		BicyclePlanner planner = null;
		String resultsDirectoryName;

		if (cli.getResultDirectory() == "") {
			resultsDirectoryName = "results/" + region + "/" + cli.getAlgorithm() + "/";
		} else {
			resultsDirectoryName = cli.getResultDirectory() + "/results/" + region + "/" + cli.getAlgorithm() + "/";
		}

		new File(resultsDirectoryName).mkdirs();

		if (cli.getAlgorithm().equals("dijkstra")) {

			planner = new MultipleGoalDestinationBicyclePlanner(1);
			statistics = new ExperimentStatistics(new MLCHeaderStatistics(), new File(resultsDirectoryName + "/"
					+ region + "_" + planner.toString() + ".csv"));
			exp = new CycleplannerExperiment((MultipleGoalDestinationBicyclePlanner) planner, statistics);

		} else {

			MCCostFunction<CycleNode2, CycleEdge2> costFunction = new MCCycleCostFunction2();

			planner = new MultiCriteriaBicyclePlanner(1,
					SupportedMCAlgorithms.valueOf(cli.getAlgorithm().toUpperCase()), cli.getGamma(), cli.getAOverB(),
					cli.getAlpha(), cli.getEpsilon(), cli.getBuckets(), costFunction);

			statistics = new ExperimentStatistics(new MLCHeaderStatistics(), new File(resultsDirectoryName + "/"
					+ region + "_" + planner.toString() + ".csv"));

			exp = new MLCExperiment2((MultiCriteriaBicyclePlanner) planner, resultsDirectoryName, statistics,
					cli.getCuttingTime());
		}

		for (int i = 0; i < cli.getNumberOfWarmUpRuns(); i++) {
			log.info("Warm-up run: " + i);

			OriginDestinationPair<Location> originDestinationPair = generator.getNextOriginDestination();
			Coordinate origin = new Coordinate(originDestinationPair.getOrigin().getLatitudeE6(), originDestinationPair
					.getOrigin().getLongitudeE6(), SpecialCoordinateType.ORIGIN);
			Coordinate destination = new Coordinate(originDestinationPair.getDestination().getLatitudeE6(),
					originDestinationPair.getDestination().getLongitudeE6(), 28, SpecialCoordinateType.DESTINATION);
			PlanningInstance2 planningInstance = planningInstanceBuilder.build2(cityData, origin, destination,
					cli.getSpeedKMpH());
			Map<CycleNode2, Collection<EdgeId>> contractedNodes = routingService
					.getContractedNodesForCity(planningInstance.getCityName());
			long timeToGetPlanningGraph = System.currentTimeMillis();
			Graph<CycleNode2, CycleEdge2> firstLastMileGraph = new FirstLastMileGraph(planningInstance.getGraph(),
					planningInstance.getOriginsInGraph(), planningInstance.getDestinationsInGraph(), contractedNodes);

			timeToGetPlanningGraph = System.currentTimeMillis() - timeToGetPlanningGraph;
			log.info("Time to obtain planning graph: " + timeToGetPlanningGraph);

			exp.run(planningInstance.getCityName(),
					firstLastMileGraph,
					i,
					planningInstance.getOriginsInGraph(),
					planningInstance.getDestinationsInGraph(),
					planningInstance.getAverageSpeedKmPH(),
					new CriteriaWeightVector(cli.getTravelTimeWeight(), cli.getComfortWeight(), cli
							.getQuietnessWeight(), cli.getTravelTimeWeight()));
		}

		generator.restart();

		for (int i = 0; i < cli.getNumberOfRequests(); i++) {
			log.debug("Request: " + i);

			OriginDestinationPair<Location> originDestinationPair = generator.getNextOriginDestination();
			Coordinate origin = new Coordinate(originDestinationPair.getOrigin().getLatitudeE6(), originDestinationPair
					.getOrigin().getLongitudeE6(), SpecialCoordinateType.ORIGIN);
			Coordinate destination = new Coordinate(originDestinationPair.getDestination().getLatitudeE6(),
					originDestinationPair.getDestination().getLongitudeE6(), 28, SpecialCoordinateType.DESTINATION);
			PlanningInstance2 planningInstance = planningInstanceBuilder.build2(cityData, origin, destination,
					cli.getSpeedKMpH());
			Map<CycleNode2, Collection<EdgeId>> contractedNodes = routingService
					.getContractedNodesForCity(planningInstance.getCityName());

			long timeToGetPlanningGraph = System.currentTimeMillis();
			Graph<CycleNode2, CycleEdge2> firstLastMileGraph = new FirstLastMileGraph(planningInstance.getGraph(),
					planningInstance.getOriginsInGraph(), planningInstance.getDestinationsInGraph(), contractedNodes);

			timeToGetPlanningGraph = System.currentTimeMillis() - timeToGetPlanningGraph;
			log.info("Time to obtain planning graph: " + timeToGetPlanningGraph);

			exp.run(planningInstance.getCityName(),
					firstLastMileGraph,
					i,
					planningInstance.getOriginsInGraph(),
					planningInstance.getDestinationsInGraph(),
					planningInstance.getAverageSpeedKmPH(),
					new CriteriaWeightVector(cli.getTravelTimeWeight(), cli.getComfortWeight(), cli
							.getQuietnessWeight(), cli.getTravelTimeWeight()));
		}

		statistics.close();
		planner.close();
	}

}
