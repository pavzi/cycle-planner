package cz.agents.cycleplanner.originDestination;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.routingService.RoutingService3;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.utils.EdgeUtil;

/**
 * A random generator of origin and destination.
 * 
 * Origin and destination are instances of <code>CycleNode2</code>.
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class OriginDestinationNodeGenerator implements OriginDestinationGenerator<CycleNode2> {

	private long seed;
	private Random random;
	private CycleNode2[] nodes;
	private int maxDirectDistance;
	private int minDirectDistance;

	public OriginDestinationNodeGenerator(long seed, String city, int maxDirectDistance, int minDirectDistance) {
		this.seed = seed;
		this.random = new Random(seed);
		this.maxDirectDistance = maxDirectDistance;
		this.minDirectDistance = minDirectDistance;

		RoutingService3 routingService = RoutingService3.INSTANCE;
		Graph<CycleNode2, CycleEdge2> graph = routingService.getCityCycleData(city).getGraph();
		this.nodes = graph.getAllNodes().toArray(new CycleNode2[0]);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OriginDestinationPair<CycleNode2> getNextOriginDestination() {
		return generate(random);
	}

	/**
	 * Method create new instance of random generator and generate list of
	 * originDestination pairs.
	 * 
	 * This method does not have effect on <code>getNextOriginDestion</code>
	 * method.
	 * 
	 * @param seed
	 * @param size
	 * @return
	 */
	public List<OriginDestinationPair<CycleNode2>> getListOfOriginDestinationPairs(long seed, int size) {
		Random generator = new Random(seed);
		List<OriginDestinationPair<CycleNode2>> originDestinationPairs = new ArrayList<>(size);

		for (int i = 0; i < size; i++) {

			originDestinationPairs.add(generate(generator));
		}

		return originDestinationPairs;
	}

	/**
	 * Using random generator specified as argument randomly choose origin and
	 * destination from set of nodes which meets direct distance restrictions
	 * 
	 * @param generator
	 * @return OriginDestinationPair<CycleNode2> - origin and destination as
	 *         instance of CycleNode2
	 */
	private OriginDestinationPair<CycleNode2> generate(Random generator) {
		// Randomly pick origin from set of nodes
		CycleNode2 origin = nodes[generator.nextInt(nodes.length)];
		CycleNode2 destination;
		int directDistance;

		do {
			// Randomly pick destination from set of nodes
			destination = nodes[generator.nextInt(nodes.length)];

			// Compute direct between origin and current destination
			directDistance = (int) Math.round(EdgeUtil.computeDirectDistanceInM(origin.getGpsLocation(),
					destination.getGpsLocation()));

			// Check whether origin equals destination or whether direct
			// distance is less then maximum allowed or whether direct distance
			// is more then allowed minimum
		} while (origin.equals(destination) || directDistance > maxDirectDistance || directDistance < minDirectDistance);

		OriginDestinationPair<CycleNode2> originDestinationPair = new OriginDestinationPair<CycleNode2>(origin,
				destination, directDistance);

		return originDestinationPair;
	}

	@Override
	public void restart() {
		random = new Random(seed);
	}

}
