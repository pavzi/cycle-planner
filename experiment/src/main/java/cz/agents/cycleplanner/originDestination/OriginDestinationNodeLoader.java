package cz.agents.cycleplanner.originDestination;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.core.util.ResourceToFile;
import cz.agents.cycleplanner.routingService.RoutingService3;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.utils.EdgeUtil;

/**
 * A generator of origin and destination from file.
 * 
 * Origin and destination are instances of <code>CycleNode2</code>.
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class OriginDestinationNodeLoader implements OriginDestinationGenerator<CycleNode2> {

	private static Logger log = Logger.getLogger(OriginDestinationNodeLoader.class);

	private List<OriginDestinationPair<CycleNode2>> originDestinationPairs;
	private Iterator<OriginDestinationPair<CycleNode2>> iterator = null;

	public OriginDestinationNodeLoader(String city) {
		originDestinationPairs = new ArrayList<>();

		try {
			File f = ResourceToFile.getFileFromResource(this.getClass().getResourceAsStream(
					"/" + city + "/requests.csv"));
			RoutingService3 routingService = RoutingService3.INSTANCE;
			Graph<CycleNode2, CycleEdge2> graph = routingService.getCityCycleData(city).getGraph();

			CSVReader reader = new CSVReader(new FileReader(f));
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {

				CycleNode2 origin = graph.getNodeByNodeId(Long.parseLong(nextLine[1]));
				CycleNode2 destination = graph.getNodeByNodeId(Long.parseLong(nextLine[2]));
				int directDistance = (int) Math.round(EdgeUtil.computeDirectDistanceInM(origin.getGpsLocation(),
						destination.getGpsLocation()));

				originDestinationPairs.add(new OriginDestinationPair<CycleNode2>(origin, destination, directDistance));
			}
			reader.close();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OriginDestinationPair<CycleNode2> getNextOriginDestination() {
		if (iterator == null) {
			iterator = originDestinationPairs.iterator();
		}

		return iterator.next();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void restart() {
		iterator = originDestinationPairs.iterator();
	}

}
