package cz.agents.cycleplanner.statistics;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Statistics of experiment.
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class ExperimentStatistics implements Closeable {

	private static Logger log = Logger.getLogger(ExperimentStatistics.class);

	private List<String[]> statistics;
	private CSVWriter csvWriter;

	public ExperimentStatistics(HeaderStatistics header, File file) {
		statistics = new ArrayList<>();
		statistics.add(header.getHeader());
		
		try {
			csvWriter = new CSVWriter(new FileWriter(file));
			
			csvWriter.writeNext(header.getHeader());
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * Add specified line to the statistics.
	 * 
	 * @param line
	 *            line to be appended to statistics
	 */
	public void add(LineStatistic line) {
		statistics.add(line.getLine());
		csvWriter.writeNext(line.getLine());
	}

	/**
	 * Write statistics to different <code>csv</code> file, then declared in constructor.
	 * 
	 * @param file
	 *            output file for writing statistics
	 */
	public void write(File file) {
		try {
			CSVWriter csvWriter = new CSVWriter(new FileWriter(file));
			csvWriter.writeAll(statistics);
			csvWriter.close();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void close() throws IOException {
		csvWriter.close();
	}
}
