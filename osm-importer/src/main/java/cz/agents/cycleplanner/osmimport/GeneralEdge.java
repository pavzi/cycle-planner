package cz.agents.cycleplanner.osmimport;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import eu.superhub.wp5.graphcommon.graph.elements.Edge;

@SuppressWarnings("serial")
public class GeneralEdge extends Edge {

	private final List<GeneralNode> via;
	
	/**
	 * TODO javadoc
	 */
	private Collection<Long> wayIds;

	/**
	 * TODO javadoc
	 */
	private Collection<String> tags;
	
	public GeneralEdge(long fromNodeId, List<GeneralNode> via, long toNodeId, Collection<Long> wayIds,
			Collection<String> tags) {
		super(fromNodeId, toNodeId, 0d);

		this.via = via;
		this.wayIds = wayIds;
		this.tags = tags;
	}
	

	public List<GeneralNode> getVia() {
		return via;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Collection<Long> getWayIds() {
		return wayIds;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * One edge can be part of more ways, mostly because there is a lot of
	 * contributors
	 * 
	 * @param wayId
	 * @return
	 */
	public boolean addWayId(long wayId) {
		return wayIds.add(wayId);
	}

	public boolean addWayIds(Collection<Long> wayIds) {
		return wayIds.addAll(wayIds);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tag
	 * @return
	 */
	public boolean addTag(String tag) {
		return tags.add(tag);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param additionalTags
	 * @return
	 */
	public boolean addTags(Collection<String> additionalTags) {
		return tags.addAll(additionalTags);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Collection<String> getTags() {
		return Collections.unmodifiableCollection(tags);
	}

}
