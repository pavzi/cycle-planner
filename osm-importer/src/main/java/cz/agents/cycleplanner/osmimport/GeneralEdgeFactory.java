package cz.agents.cycleplanner.osmimport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.openstreetmap.osmosis.core.domain.v0_6.Way;

import eu.superhub.wp5.graphcommon.graph.elements.Node;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class GeneralEdgeFactory {

	private GeneralEdgeFactory() {
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param fromNode
	 * @param toNode
	 * @param wayId
	 * @param tags
	 * @return
	 */
	public static GeneralEdge getGeneralEdge(Node fromNode, Node toNode, Way way) {
		List<GeneralNode> via = Collections.emptyList();
		Collection<String> tags = TagUtil.tagsToStrings("way", way.getTags());
		Collection<Long> wayIds = new HashSet<Long>();

		wayIds.add(way.getId());

		return new GeneralEdge(fromNode.getId(), via, toNode.getId(), wayIds, tags);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param fromNode
	 * @param toNode
	 * @param tags
	 * @return
	 */
	public static GeneralEdge getGeneralEdge(Node fromNode, Node toNode, Collection<String> tags) {
		List<GeneralNode> via = Collections.emptyList();
		Collection<String> newTags = new HashSet<String>(tags);
		Collection<Long> wayIds = new HashSet<Long>();

		return new GeneralEdge(fromNode.getId(), via, toNode.getId(), wayIds, newTags);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalEdge
	 * @return
	 */
	public static GeneralEdge getOppositeGeneralEdge(GeneralEdge generalEdge) {
		List<GeneralNode> via = new ArrayList<GeneralNode>(generalEdge.getVia());

		Collections.reverse(via);

		Collection<String> tags = new HashSet<String>(generalEdge.getTags());
		Collection<Long> wayIds = new HashSet<Long>(generalEdge.getWayIds());

		return new GeneralEdge(generalEdge.getToNodeId(), via, generalEdge.getFromNodeId(), wayIds,
				tags);
	}
}
