package cz.agents.cycleplanner.osmimport;

import java.util.Collection;
import java.util.Collections;

import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.wp5common.location.GPSLocation;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
@SuppressWarnings("serial")
public class GeneralNode extends Node {

	/**
	 * TODO javadoc
	 */
	private final Collection<String> tags;

	public GeneralNode(long id, GPSLocation gpsLocation, String description, Collection<String> tags) {
		super(id, gpsLocation, description);

		this.tags = tags;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tag
	 */
	public void addTag(String tag) {
		tags.add(tag);
	}
	
	public void addTags(Collection<String> additionalTags) {
		tags.addAll(additionalTags);
	}
	
	public void removeTag(String tag) {
		tags.remove(tag);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Collection<String> getTags() {
		return Collections.unmodifiableCollection(tags);
	}

	

}
