package cz.agents.cycleplanner.osmimport;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;
import org.openstreetmap.osmosis.core.domain.v0_6.Node;
import org.openstreetmap.osmosis.core.domain.v0_6.Tag;

import eu.superhub.wp5.plannerdataimporter.util.EPSGProjection;
import eu.superhub.wp5.wp5common.location.GPSLocation;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class GeneralNodeFactory {

	private static final Logger log = Logger.getLogger(GeneralNodeFactory.class);

	private static EPSGProjection projection;

	private GeneralNodeFactory() {
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param osmNode
	 * @return
	 */
	public static GeneralNode getGeneralNode(Node osmNode) {
		String description = "GeneralNode";
		double elevation = 0;

		for (Tag tag : osmNode.getTags()) {
			if (tag.getKey().equals("height")) {
				elevation = Double.parseDouble(tag.getValue());
				break;
			}
		}

		Collection<String> tags = TagUtil.tagsToStrings("node", osmNode.getTags());

		GPSLocation gpsLocation = new GPSLocation(osmNode.getLatitude(), osmNode.getLongitude(), elevation);
		gpsLocation = getProjectedGPSLocation(gpsLocation);

		return new GeneralNode(osmNode.getId(), gpsLocation, description, tags);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * If initialization of projection tool fails, we return the same GPS
	 * coordinate
	 * 
	 * @param gpsLocation
	 * @return
	 */
	private static GPSLocation getProjectedGPSLocation(GPSLocation gpsLocation) {
		if (projection == null) {

			try {
				projection = new EPSGProjection(2065);
			} catch (FactoryException | TransformException e) {
				log.error(e.getMessage(), e);
				return gpsLocation;
			}
		}

		gpsLocation = projection.getProjectedGPSLocation(gpsLocation);
		return gpsLocation;

	}
}
