package cz.agents.cycleplanner.osmimport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import eu.superhub.wp5.graphcommon.graph.EdgeId;

// TODO rename to something with special data structure
public class GraphBuilder2 {

	private Map<Long, GeneralNode> nodesByNodeId;

	private Map<EdgeId, GeneralEdge> edgeByFromToNodeIds;
	private Map<Long, Collection<GeneralEdge>> nodeOutgoingEdges;
	private Map<Long, Collection<GeneralEdge>> nodeIncomingEdges;

	private Map<Long, Collection<GeneralEdge>> waysByWayId;

	private Map<Long, RelationWraper> relations;

	public GraphBuilder2() {
		nodesByNodeId = new LinkedHashMap<Long, GeneralNode>();

		edgeByFromToNodeIds = new LinkedHashMap<EdgeId, GeneralEdge>();
		nodeOutgoingEdges = new HashMap<Long, Collection<GeneralEdge>>();
		nodeIncomingEdges = new HashMap<Long, Collection<GeneralEdge>>();

		waysByWayId = new HashMap<Long, Collection<GeneralEdge>>();

		relations = new HashMap<Long, RelationWraper>();
	}

	/**
	 * Add a collection of nodes to the graph
	 * 
	 * @param nodes
	 *            Nodes
	 */
	public void addNodes(Collection<GeneralNode> nodes) {
		for (GeneralNode node : nodes) {
			addNode(node);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param node
	 */
	public void addNode(GeneralNode node) {
		nodesByNodeId.put(node.getId(), node);
		nodeOutgoingEdges.put(node.getId(), new ArrayList<GeneralEdge>());
		nodeIncomingEdges.put(node.getId(), new ArrayList<GeneralEdge>());
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 * @return
	 */
	public boolean containsNode(long nodeId) {
		return nodesByNodeId.containsKey(nodeId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Collection<GeneralNode> getAllNodes() {
		return nodesByNodeId.values();
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 * @return
	 */
	public GeneralNode getNode(long nodeId) {
		return nodesByNodeId.get(nodeId);
	}
	
	public void removeNode(long nodeId) {
		removeEdges(new HashSet<>(nodeOutgoingEdges.get(nodeId)));
		removeEdges(new HashSet<>(nodeIncomingEdges.get(nodeId)));	
		nodesByNodeId.remove(nodeId);
	}

	/**
	 * Add a collection of edges to the graph
	 * 
	 * @param edges
	 *            Edges
	 */
	public void addEdges(Collection<GeneralEdge> edges) {
		for (GeneralEdge edge : edges) {
			addEdge(edge);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edge
	 */
	public void addEdge(GeneralEdge edge) {

		assert nodesByNodeId.get(edge.getFromNodeId()) != null && nodesByNodeId.get(edge.getToNodeId()) != null : "Node has to be in graph builder before inserting edge";

		EdgeId edgeId = edge.getEdgeId();

		if (!containsEdge(edgeId)) {

			addNewEdge(edge);

		} else {

			mergeToExistingEdge(edge);
		}

		addEdgeToWays(edge);
	}

	private void addNewEdge(GeneralEdge edge) {
		Collection<GeneralEdge> outgoingEdgesFromNode = nodeOutgoingEdges.get(edge.getFromNodeId());
		Collection<GeneralEdge> incomingEdgesToNode = nodeIncomingEdges.get(edge.getToNodeId());

		outgoingEdgesFromNode.add(edge);
		incomingEdgesToNode.add(edge);

		edgeByFromToNodeIds.put(edge.getEdgeId(), edge);
	}

	private void mergeToExistingEdge(GeneralEdge edge) {
		GeneralEdge existingEdge = edgeByFromToNodeIds.get(edge.getEdgeId());

		existingEdge.addTags(edge.getTags());
		existingEdge.addWayIds(edge.getWayIds());
	}

	private void addEdgeToWays(GeneralEdge edge) {
		for (long wayId : edge.getWayIds()) {
			Collection<GeneralEdge> way = waysByWayId.get(wayId);

			if (way == null) {
				way = new HashSet<GeneralEdge>();

				waysByWayId.put(wayId, way);
			}

			way.add(edge);
		}
	}

	/**
	 * 
	 * Returns true when the graph contains edge with given edgeId
	 * 
	 * @param edgeId
	 * @return
	 */
	public boolean containsEdge(EdgeId edgeId) {
		return edgeByFromToNodeIds.containsKey(edgeId);
	}

	/**
	 * 
	 * Provide current Collection of edges
	 * 
	 * @return
	 */
	public Collection<GeneralEdge> getAllEdges() {
		return Collections.unmodifiableCollection(edgeByFromToNodeIds.values());
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edgeId
	 * @return
	 */
	public GeneralEdge getEdge(EdgeId edgeId) {
		return edgeByFromToNodeIds.get(edgeId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 * @return
	 */
	public Collection<GeneralEdge> getAllNodeOutgoingEdges(long nodeId) {
		return Collections.unmodifiableCollection(nodeOutgoingEdges.get(nodeId));
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param nodeId
	 * @return
	 */
	public Collection<GeneralEdge> getAllNodeIncomingEdges(long nodeId) {
		return Collections.unmodifiableCollection(nodeIncomingEdges.get(nodeId));
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edgeId
	 * @return
	 */
	public void removeEdges(Collection<GeneralEdge> edges) {
		for (GeneralEdge generalEdge : edges) {
			removeEdge(generalEdge);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param edgeId
	 */
	public void removeEdge(GeneralEdge edge) {
		Collection<GeneralEdge> outgoingEdgesFromNode = nodeOutgoingEdges.get(edge.getFromNodeId());
		Collection<GeneralEdge> incomingEdgesToNode = nodeIncomingEdges.get(edge.getToNodeId());

		removeEdgeFromCollectionOfEdges(outgoingEdgesFromNode, edge.getEdgeId());
		removeEdgeFromCollectionOfEdges(incomingEdgesToNode, edge.getEdgeId());

		edgeByFromToNodeIds.remove(edge.getEdgeId());

		removeEdgeFromWays(edge);
	}

	private void removeEdgeFromWays(GeneralEdge edge) {
		for (long wayId : edge.getWayIds()) {
			removeEdgeFromCollectionOfEdges(waysByWayId.get(wayId), edge.getEdgeId());
		}
	}

	private void removeEdgeFromCollectionOfEdges(Collection<GeneralEdge> edges, EdgeId edgeId) {
		for (Iterator<GeneralEdge> iterator = edges.iterator(); iterator.hasNext();) {
			GeneralEdge generalEdge = iterator.next();

			if (generalEdge.getEdgeId().equals(edgeId)) {
				iterator.remove();
			}
		}
	}
	
	/**
	 * 
	 * TODO javadoc
	 * @param edge
	 */
	public void shrinkEdgeToNode(GeneralEdge edge) {
		removeEdge(edge);

		GeneralNode from = getNode(edge.getFromNodeId());
		GeneralNode to = getNode(edge.getToNodeId());

		Collection<GeneralEdge> outgoingEdgesToNode = nodeOutgoingEdges.get(to.getId());
		
		for (GeneralEdge outgoingEdge : outgoingEdgesToNode) {
			GeneralEdge newOutgoingEdge = GeneralEdgeFactory.getGeneralEdge(from, getNode(outgoingEdge.getToNodeId()),
					outgoingEdge.getTags());
			
			newOutgoingEdge.addWayIds(edge.getWayIds());
			addEdge(newOutgoingEdge);
		}
		
		Collection<GeneralEdge> incomingEdgesToNode = nodeIncomingEdges.get(to.getId());

		for (GeneralEdge incomingEdge : incomingEdgesToNode) {
			GeneralEdge newIncomingEdge = GeneralEdgeFactory.getGeneralEdge(getNode(incomingEdge.getFromNodeId()), from,
					incomingEdge.getTags());
			
			newIncomingEdge.addWayIds(edge.getWayIds());
			addEdge(newIncomingEdge);
		}
		
		removeNode(to.getId());
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param wayId
	 * @return
	 */
	public boolean containsWay(long wayId) {
		return waysByWayId.containsKey(wayId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param wayId
	 * @return collection of general edges that together creates specified way
	 */
	public Collection<GeneralEdge> getAllEdgesOfWay(long wayId) {
		return Collections.unmodifiableCollection(waysByWayId.get(wayId));
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param relationId
	 * @param relation
	 */
	public void addRelation(long relationId, RelationWraper relation) {
		relations.put(relationId, relation);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param relationId
	 * @return
	 */
	public boolean containsRelation(long relationId) {
		return relations.containsKey(relationId);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @return
	 */
	public Collection<RelationWraper> getAllRelations() {
		return Collections.unmodifiableCollection(relations.values());
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param relationId
	 * @return
	 */
	public RelationWraper getRelation(long relationId) {
		return relations.get(relationId);
	}

}
