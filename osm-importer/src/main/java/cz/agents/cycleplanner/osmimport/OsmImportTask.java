package cz.agents.cycleplanner.osmimport;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.openstreetmap.osm.data.MemoryDataSet;
import org.openstreetmap.osm.data.coordinates.Bounds;
import org.openstreetmap.osmosis.core.domain.v0_6.Node;
import org.openstreetmap.osmosis.core.domain.v0_6.Relation;
import org.openstreetmap.osmosis.core.domain.v0_6.Way;
import org.openstreetmap.osmosis.core.domain.v0_6.WayNode;

import eu.superhub.wp5.plannerdataimporter.graphimporter.OsmDataGetter;

/**
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public abstract class OsmImportTask {

	private static final Logger log = Logger.getLogger(OsmImportTask.class);

	private final MemoryDataSet osmData;
	protected GraphBuilder2 graphBuilder2;

	public OsmImportTask(File osm) {
		
		log.debug("Exist osm file? " + osm.exists());
		log.debug("Osm file path: " + osm.getAbsolutePath());
		log.debug("Can read osm file? " + osm.canRead());
		log.debug("Can write osm file? " + osm.canWrite());
		log.debug("Osm file size " + (osm.length() / 1024) + " KB");
		
		OsmDataGetter osmDataGetter = OsmDataGetter.createOsmDataGetter(osm);
		
		osmData = osmDataGetter.getAllOsmData();
		graphBuilder2 = new GraphBuilder2();
	}

	/**
	 * TODO javadoc
	 * 
	 * @param osmData
	 * @return
	 */
	public GraphBuilder2 execute() {

		importNodes(osmData.getNodes(Bounds.WORLD));
		importWays(osmData.getWays(Bounds.WORLD));
		importRelations(osmData.getRelations(Bounds.WORLD));

		return graphBuilder2;
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param iteratorOfNodes
	 */
	private void importNodes(Iterator<Node> iteratorOfNodes) {

		while (iteratorOfNodes.hasNext()) {
			Node osmNode = iteratorOfNodes.next();
			GeneralNode generalNode = GeneralNodeFactory.getGeneralNode(osmNode);

			graphBuilder2.addNode(generalNode);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param iteratorOfWays
	 */
	private void importWays(Iterator<Way> iteratorOfWays) {

		while (iteratorOfWays.hasNext()) {
			Way way = iteratorOfWays.next();

			Iterator<WayNode> wayNodesIterator = way.getWayNodes().iterator();

			WayNode fromWayNodeOsm = wayNodesIterator.next();
			Collection<GeneralEdge> generalEdges = new ArrayList<GeneralEdge>();

			while (wayNodesIterator.hasNext()) {

				WayNode toWayNodeOsm = wayNodesIterator.next();

				GeneralNode fromNode = graphBuilder2.getNode(fromWayNodeOsm.getNodeId());
				GeneralNode toNode = graphBuilder2.getNode(toWayNodeOsm.getNodeId());
				
				if (fromNode != null && toNode != null) {
					GeneralEdge generalEdge = GeneralEdgeFactory.getGeneralEdge(fromNode, toNode, way);
					
					generalEdges.add(generalEdge);
				}
				
				fromWayNodeOsm = toWayNodeOsm;
			}
			
			graphBuilder2.addEdges(generalEdges);
		}
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param iterationOfRelations
	 */
	protected abstract void importRelations(Iterator<Relation> iterationOfRelations);

}
