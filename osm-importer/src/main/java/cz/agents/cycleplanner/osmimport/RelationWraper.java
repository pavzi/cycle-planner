package cz.agents.cycleplanner.osmimport;

import java.util.Collection;

import org.openstreetmap.osmosis.core.domain.v0_6.Relation;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public abstract class RelationWraper {

	protected Relation relation;
	protected Collection<String> tags;

	public RelationWraper(Relation relation) {
		this.relation = relation;
		tags = TagUtil.tagsToStrings("relation", relation.getTags());
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param generalGraphBuilder
	 */
	public abstract void apply(GraphBuilder2 generalGraphBuilder);

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param additionalTags
	 */
	public void addTags(Collection<String> additionalTags) {
		tags.addAll(additionalTags);
	}

	public Collection<String> getTags() {
		return tags;
	}

}
