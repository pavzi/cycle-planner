package cz.agents.cycleplanner.osmimport;

import java.util.Collection;
import java.util.HashSet;

import org.openstreetmap.osmosis.core.domain.v0_6.Tag;

/**
 * 
 * TODO javadoc
 * 
 * @author Pavol Zilecky (pavol.zilecky@agents.fel.cvut.cz)
 *
 */
public class TagUtil {

	private TagUtil() {
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param tags
	 * @return
	 */
	public static Collection<String> tagsToStrings(Collection<Tag> tags) {

		return tagsToStrings("", tags);
	}

	/**
	 * 
	 * TODO javadoc
	 * 
	 * @param entity
	 * @param tags
	 * @return
	 */
	public static Collection<String> tagsToStrings(String entity, Collection<Tag> tags) {

		Collection<String> strings = new HashSet<String>();

		for (Tag tag : tags) {
			String stringTag = entity + "::" + tag.getKey() + "::" + tag.getValue();
			strings.add(stringTag);
		}

		return strings;
	}
}
