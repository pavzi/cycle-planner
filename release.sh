#!/bin/bash

# ./release.sh 0.1.11 0.1.12-SNAPSHOT

function die_with() {
	echo "$*" >&2
	exit 1
}

function rollback_and_die_with() {
	echo "$*" >&2
	
	echo "Resetting release commit to return you to the same working state as before attempting a deploy"
	echo "> git reset --hard HEAD^1"
	$GIT reset --hard HEAD^1 || echo "Git reset command failed!"
	
	exit 1
}

###############################################
# MAKE SURE SCRIPT DEPENDENCIES ARE INSTALLED #
###############################################

MVN=mvn
GIT=git
#"/cygdrive/c/Program\ Files\ \(x86\)/Git/bin/git.exe"
RELEASE_VERSION=$1
NEXT_VERSION=$2

echo "RELEASE OF CYCLEPLANNER"
echo ""

#########################################
# BAIL IF THERE ARE UNCOMMITTED CHANGES #
#########################################

# If there are any uncommitted changes we must abort immediately
if [ $($GIT status -s | wc -l) != "0" ] ; then
	$GIT status -s
	die_with "There are uncommitted changes, please commit or stash them to continue with the release:"
else
	echo "Good, no uncommitted changes found"
fi

echo ""
echo "Using $RELEASE_VERSION for release"
echo "Using $NEXT_VERSION for next development version"

#############################
# START THE RELEASE PROCESS #
#############################

VCS_RELEASE_TAG="v${RELEASE_VERSION}"

# if a release tag of this version already exists then abort immediately
if [ $($GIT tag -l "${VCS_RELEASE_TAG}" | wc -l) != "0" ] ; then
	die_with "A tag already exists ${VCS_RELEASE_TAG} for the release version ${RELEASE_VERSION}"
fi

# Update the pom.xml versions
$MVN versions:set -DgenerateBackupPoms=false -DnewVersion=$RELEASE_VERSION || die_with "Failed to set release version on pom.xml files"

# Commit the updated pom.xml files
echo "$GIT commit -a -m \"Release version ${RELEASE_VERSION}\""
$GIT commit -a -m "Release version ${RELEASE_VERSION}" || die_with "Failed to commit updated pom.xml versions for release!"

echo ""
echo "Starting build and deploy"
echo ""

# build and deploy the release
$MVN -Pcycleplanner clean install deploy -Dmaven.test.skip=true || rollback_and_die_with "Build/Deploy failure. Release failed."

# tag the release
$GIT tag "${VCS_RELEASE_TAG}" || die_with "Failed to create tag ${VCS_RELEASE_TAG}! Release has been deployed, however"

######################################
# START THE NEXT DEVELOPMENT PROCESS #
######################################

$MVN versions:set -DgenerateBackupPoms=false "-DnewVersion=${NEXT_VERSION}" || die_with "Failed to set next dev version on pom.xml files, please do this manually"

$GIT commit -a -m "Development version ${NEXT_VERSION}" || die_with "Failed to commit updated pom.xml versions for next dev version! Please do this manually"

$GIT push || die_with "Failed to push commits. Please do this manually"

$GIT push --tags || die_with "Failed to push tags. Please do this manually" 
