#!/bin/bash

# Generate all osm files based on citiesbb.csv
# city,left,right,top,bottom

# generate osm files
PBF="czech-republic-latest.osm.pbf"
BB="citiesbb.csv"
count=0
total=`cat ${BB} | wc -l`
while read line; do 
  count=$(($count+1));

  echo "REM City ${count} of ${total}: ${line}"
  CITY=`echo ${line} | cut -d"," -f1`
  LEFT=`echo ${line} | cut -d"," -f2`
  RIGHT=`echo ${line} | cut -d"," -f3`
  TOP=`echo ${line} | cut -d"," -f4`
  BOTTOM=`echo ${line} | cut -d"," -f5`
  
  OSMFROMPBF="$CITY.nosrtm.osm"
  OSMFILTERED="$CITY.filtered.osm"
  OSMSRTMFINAL="$CITY.osm"
  SERIALIZEDGRAPH="$CITY.ser"

  # echo "EXTRACTING OSM FROM PBF"
  echo "osmosis --read-pbf file=$PBF --bounding-box left=$LEFT right=$RIGHT top=$TOP bottom=$BOTTOM completeWays=yes --used-node --wx $OSMFROMPBF"

  # echo "FILTERING OSM USING OSM FILTER"
  echo "osmfilter $OSMFROMPBF --parameter-file=cycle_osm.params -o=$OSMFILTERED"

  # echo "ADDING SRTM INFORMATION"
  echo "osmosis --rx file=$OSMFILTERED --write-srtm --wx $OSMSRTMFINAL"
 
  # echo "BUILDING GRAPH"  
  echo "java -Xmx16000m -jar cycle-city-data-builder-jar-with-dependencies.jar -cityName $CITY -osmFile $OSMSRTMFINAL -outputFile $SERIALIZEDGRAPH -left $LEFT -right $RIGHT -top $TOP -bottom $BOTTOM"

done < ${BB}
