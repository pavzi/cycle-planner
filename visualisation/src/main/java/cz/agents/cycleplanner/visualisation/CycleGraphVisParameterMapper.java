package cz.agents.cycleplanner.visualisation;

import java.awt.Color;
import java.util.Iterator;
import java.util.regex.Pattern;

import cz.agents.agentpolis.tools.geovisio.layer.visparameter.VisParameterMapper;
import cz.agents.agentpolis.tools.geovisio.layer.visparameter.VisParameters;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import eu.superhub.wp5.graphcommon.graph.elements.Node;
import eu.superhub.wp5.plannercore.structures.base.TimeDependentEdge;

public class CycleGraphVisParameterMapper implements VisParameterMapper {

	private static final Pattern EXCLUDED_TAGS = Pattern
			.compile("way::bicycle::no|way::access::customers|way::access::delivery|way::access::private|way::access::no");
	private final int nodeSize;

	public CycleGraphVisParameterMapper() {
		this(7);
	}

	public CycleGraphVisParameterMapper(int nodeSize) {
		super();
		this.nodeSize = nodeSize;
	}

	@Override
	public VisParameters getVisParameter(Object object) {
		Color color = null;
		double styleParameter = 0;
		if (object instanceof Node) {
			color = getColor((Node) object);
			styleParameter = nodeSize;
		}
		if (object instanceof TimeDependentEdge) {
			color = getColor((TimeDependentEdge) object);
			styleParameter = 1;
		}

		return new VisParameters(color, styleParameter);
	}

	private Color getColor(Node node) {
		
		return Color.BLACK;
	}

	private Color getColor(TimeDependentEdge edge) {
		

		return Color.RED;
	}

}
