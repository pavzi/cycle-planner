package cz.agents.cycleplanner.visualisation;

import java.sql.SQLException;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.opengis.referencing.operation.TransformException;

import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.routingService.RoutingService3;
import eu.superhub.wp5.graphcommon.graph.Graph;

public class VisualiseCycleGraph {

	private static final Logger log = Logger.getLogger(VisualiseCycleGraph.class);

	public static void main(String[] args) {

		// init cycle planner routing service
		RoutingService3 service = RoutingService3.INSTANCE;

//		Graph<CycleNode2, CycleEdge2> graph;
//		CycleGraphVisualisation vis = null;
//		CityData<CycleNode2, CycleEdge2> cycleCityData;
//
//		try {
//			// Iterate over all supported cities
//			for (Iterator<CityData<CycleNode2, CycleEdge2>> iterator = service.getSupportedCities(); iterator.hasNext();) {
//
//				cycleCityData = iterator.next();
//				log.info("Cycle data city: " + cycleCityData.getCityName());
//				graph = cycleCityData.getGraph();
//				
//				log.info(cycleCityData.getCityName() + " cycle graph: " + graph.getAllNodes().size() + " nodes, "
//						+ graph.getAllEdges().size() + " edges");
//
//				vis = VisualizationUtil.initVisualisation(cycleCityData, "_cycleways");
//				vis.visualizeGraph(graph, "");
//				
//				if (vis != null) {
//					vis.saveAndCreateIndexesAndClose();
//				}
//			}			
//
//		} catch (SQLException | TransformException e) {
//			System.out.println(e.getMessage());
//		}

	}
}
