package cz.agents.cycleplanner.visualisation;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.opengis.referencing.operation.TransformException;

import cz.agents.cycleplanner.api.datamodel.v2.FeedbackStorageObject;
import cz.agents.cycleplanner.api.datamodel.v2.TimedCoordinate;
import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cycleplanner.core.datastructures.CycleEdge2;
import cz.agents.cycleplanner.core.datastructures.CycleNode2;
import cz.agents.cycleplanner.mongodb.BicycleJourneyPlanStorage;
import cz.agents.cycleplanner.mongodb.StorageMongoDbConnectionProvider;
import eu.superhub.wp5.graphcommon.graph.Graph;
import eu.superhub.wp5.graphcommon.graph.GraphBuilder;
import eu.superhub.wp5.wp5common.location.GPSLocation;

public class VisualiseTrackedJourneys {

	private static final Logger log = Logger.getLogger(VisualiseTrackedJourneys.class);

	public static void main(String[] args) throws IOException, SQLException, TransformException {

		// AUTOMAT: http://prahounakole.cz/wp-content/trackmap/

//		Graph<CycleNode2, CycleEdge2> graph;
//		CycleGraphVisualisation vis = null;
//		CityData<CycleNode2, CycleEdge2> cycleCityData;
//		GraphBuilder<CycleNode2, CycleEdge2> graphBuilder = new GraphBuilder<>();
//
//		StorageMongoDbConnectionProvider.setDbConnectionDetailsFromPropertiesFile("mongodb.properties");
//		BicycleJourneyPlanStorage bicycleJourneyPlanStorage = BicycleJourneyPlanStorage.getStorage();
//		assertTrue(bicycleJourneyPlanStorage.isConnected());
//		List<FeedbackStorageObject> feedbacks = bicycleJourneyPlanStorage.retrieveAllCycleplannerFeedback(146);
//		long pointId = 1;
//
//		for (FeedbackStorageObject f : feedbacks) {
//
//			if (f.getFeedback().getTrackedJourney() != null) {
//
//				// add nodes and edges
//				boolean firstCoordinate = true;
//				for (TimedCoordinate c : f.getFeedback().getTrackedJourney()) {
//
//					// node
//					CycleNode2 node = new CycleNode2(pointId, new GPSLocation(c.getLatE6(), c.getLonE6()), null, null);
//					graphBuilder.addNode(node);
//
//					// edge
//					if (firstCoordinate) {
//						firstCoordinate = false;
//					} else {
//						CycleEdge2 edge = new CycleEdge2(pointId - 1, pointId, 0, null, null, 0, 0, 0, 0, 0, 0, 0,
//								null, null, null, null, null);
//						graphBuilder.addEdge(edge);
//					}
//					pointId++;
//				}
//
//			}
//		}
//		graph = graphBuilder.createGraph();
//		cycleCityData = new CityData<>("czechrepublic", 51104000, 12074000, 18885000, 48509000, graph);
//		log.info("Cycle data city: " + cycleCityData.getCityName());
//		log.info(cycleCityData.getCityName() + " cycle graph: " + graph.getAllNodes().size() + " nodes, "
//				+ graph.getAllEdges().size() + " edges");
//
//		vis = VisualizationUtil.initVisualisation(cycleCityData, "_tracked", 1);
//		vis.visualizeGraph(graph, "tracked");
//
//		if (vis != null) {
//			vis.saveAndCreateIndexesAndClose();
//		}
	}
}
