package cz.agents.cycleplanner.visualisation.newstructures;

import java.awt.Color;

import cz.agents.agentpolis.tools.geovisio.layer.visparameter.VisParameterMapper;
import cz.agents.agentpolis.tools.geovisio.layer.visparameter.VisParameters;
import cz.agents.basestructures.Edge;
import cz.agents.basestructures.Node;

public class CycleGraphVisParameterMapper implements VisParameterMapper {

	private final int nodeSize;

	public CycleGraphVisParameterMapper() {
		this(7);
	}

	public CycleGraphVisParameterMapper(int nodeSize) {
		super();
		this.nodeSize = nodeSize;
	}

	@Override
	public VisParameters getVisParameter(Object object) {
		Color color = null;
		double styleParameter = 0;
		if (object instanceof Node) {
			color = getColor((Node) object);
			styleParameter = nodeSize;
		}
		if (object instanceof Edge) {
			color = getColor((Edge) object);
			styleParameter = 1;
		}

		return new VisParameters(color, styleParameter);
	}

	private Color getColor(Node node) {

		return Color.BLACK;
	}

	private Color getColor(Edge edge) {

		return Color.RED;
	}

}
