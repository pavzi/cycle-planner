package cz.agents.cycleplanner.visualisation.newstructures;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.operation.TransformException;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

import cz.agents.agentpolis.tools.geovisio.database.table.column.Column;
import cz.agents.agentpolis.tools.geovisio.database.table.column.ColumnType;
import cz.agents.agentpolis.tools.geovisio.layer.BoundingBox;
import cz.agents.agentpolis.tools.geovisio.layer.GeometryRecord;
import cz.agents.agentpolis.tools.geovisio.layer.Layer;
import cz.agents.agentpolis.tools.geovisio.layer.LayerSettings;
import cz.agents.agentpolis.tools.geovisio.layer.visparameter.VisParameterMapper;
import cz.agents.agentpolis.tools.geovisio.layer.visparameter.VisParameters;
import cz.agents.agentpolis.tools.geovisio.settings.NameSettings;
import cz.agents.agentpolis.tools.geovisio.visualisation.Visualisation;
import cz.agents.agentpolis.tools.geovisio.visualisation.VisualisationSettings;
import cz.agents.basestructures.Graph;
import cz.agents.basestructures.Node;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

public class CycleGraphVisualisation extends Visualisation {

	private static final Logger log = Logger.getLogger(CycleGraphVisualisation.class);

	private List<Layer<LineString>> responseLayers = new ArrayList<>();
	private List<Layer<Point>> visitedNodesLayers = new ArrayList<>();
	private List<Layer<Point>> nodeLayers = new ArrayList<>();;
	private List<Layer<LineString>> edgeLayers = new ArrayList<>();;

	public CycleGraphVisualisation(String visualisationName, int inputDataSrid, int dbDataSrid,
			VisualisationSettings settings, BoundingBox boundingBox,
			VisParameterMapper visParameterMapper, boolean deleteIfAlreadyExist)
			throws MalformedURLException, NoSuchAuthorityCodeException, FactoryException,
			TransformException, ClassNotFoundException, IllegalArgumentException, SQLException,
			TransformerException {

		super(visualisationName, inputDataSrid, dbDataSrid, settings, boundingBox,
				visParameterMapper, deleteIfAlreadyExist);
	}

	/**
	 * Create two layers named {@code layerNamesPrefix+"base_nodes/base_edges"}.
	 * It only setup the layers, the data are NOT uploaded to the database. To
	 * save the data to the database call {@code saveToDatabase()} or
	 * {@code saveAndCreateIndexesAndClose()}.
	 * 
	 * @param nodes
	 * @param edges
	 * @param layerNamesPrefix
	 * @throws SQLException
	 * @throws TransformException
	 */
	public void visualizeGraph(Graph<CycleNode, CycleEdge> graph, String layerNamesPrefix)
			throws SQLException, TransformException {

		List<Column> nodeColumns = new ArrayList<Column>();
		nodeColumns.add(new Column(ColumnType.INT, "latitude"));
		nodeColumns.add(new Column(ColumnType.INT, "longitude"));
		nodeColumns.add(new Column(ColumnType.INT, "elevation"));

		LayerSettings nodeLayerSettings = new LayerSettings(ColumnType.POINT,
				layerNamesPrefix.concat("tns_nodes"), NameSettings.POINT_PARAMETER_STYLE_NAME);
		Layer<Point> nodeLayer = createAndPublishLayerIncludingTable(nodeColumns, nodeLayerSettings);

		List<Column> edgeColumns = new ArrayList<Column>();

		edgeColumns.add(new Column(ColumnType.LONG, "node_from"));
		edgeColumns.add(new Column(ColumnType.LONG, "node_to"));
		edgeColumns.add(new Column(ColumnType.STRING, "cost_vector"));
		edgeColumns.add(new Column(ColumnType.INT, "number_of_via"));
		edgeColumns.add(new Column(ColumnType.STRING, "street_name"));
		edgeColumns.add(new Column(ColumnType.STRING, "bicycle_route_number"));

		LayerSettings edgeLayerSettings = new LayerSettings(ColumnType.LINE,
				layerNamesPrefix.concat("tns_edges"), NameSettings.LINE_PARAMETER_STYLE_NAME);
		Layer<LineString> edgeLayer = createAndPublishLayerIncludingTable(edgeColumns,
				edgeLayerSettings);

		GeometryFactory geometryFactory = getGeomFactory();
		System.out.println("Geometry factory: " + geometryFactory.getSRID());
		VisParameterMapper visParameterMapper = getVisParameterMapper();

		for (CycleNode node : graph.getAllNodes()) {

			// Coordinate c = new Coordinate((double) node.lonE6 / 1E6, (double)
			// node.latE6 / 1E6);
			Coordinate c = new Coordinate((double) node.latE6 / 1E6, (double) node.lonE6 / 1E6);
			Point p = geometryFactory.createPoint(c);
			Map<String, Object> map = new HashMap<String, Object>();

			map.put("latitude", node.latE6);
			map.put("longitude", node.lonE6);
			map.put("elevation", node.elevation);

			VisParameters vp = visParameterMapper.getVisParameter(node);
			addGeometryToLayer(nodeLayer, node.id, p, null, null, vp, map);
		}
		nodeLayers.add(nodeLayer);
		log.info(graph.getAllNodes().size() + " nodes added to queue");

		for (CycleEdge edge : graph.getAllEdges()) {

			Node from = graph.getNodeByNodeId(edge.fromId);
			Node to = graph.getNodeByNodeId(edge.toId);

			// Coordinate p = new Coordinate((double) from.lonE6 / 1E6, (double)
			// from.latE6 / 1E6);
			// Coordinate p2 = new Coordinate((double) to.lonE6 / 1E6, (double)
			// to.latE6 / 1E6);
			Coordinate p = new Coordinate((double) from.latE6 / 1E6, (double) from.lonE6 / 1E6);
			Coordinate p2 = new Coordinate((double) to.latE6 / 1E6, (double) to.lonE6 / 1E6);

			LineString line = geometryFactory.createLineString(new Coordinate[] { p, p2 });

			Map<String, Object> map = new HashMap<String, Object>();

			map.put("node_from", from.id);
			map.put("node_to", to.id);
			map.put("cost_vector", Arrays.toString(edge.getCostVector()));
			map.put("number_of_via", edge.getViaNodes().size());
			map.put("street_name", edge.getStreetName());
			map.put("bicycle_route_number", edge.getBicycleRouteNumber());

			VisParameters vp = visParameterMapper.getVisParameter(edge);
			addGeometryToLayer(edgeLayer, line, null, null, vp, map);
		}
		edgeLayers.add(edgeLayer);
		log.info(graph.getAllEdges().size() + " edges added to queue");
	}

	public void saveToDatabase() throws SQLException {

		long start = System.currentTimeMillis();
		// response layers
		if (responseLayers.size() > 0) {
			log.info("Saving response layers to database started");
			for (Layer<LineString> layer : responseLayers) {
				layer.saveToDatabase();
				log.info("Response layer saved: " + layer.getName());
			}
			log.info("Saving response layers to database finished");
		}

		// visited nodes layers
		if (visitedNodesLayers.size() > 0) {
			log.info("Saving visited nodes layers to database started");
			for (Layer<Point> layer : visitedNodesLayers) {
				layer.saveToDatabase();
				log.info("Visited nodes layer saved: " + layer.getName());
			}
			log.info("Saving visited nodes layers to database finished");
		}

		// base nodes
		if (nodeLayers.size() > 0) {
			log.info("Saving nodes to database started");
			for (Layer<Point> nodeLayer : nodeLayers) {
				nodeLayer.saveToDatabase();
				log.info("Visited nodes layer saved: " + nodeLayer.getName());
			}
			log.info(" saving nodes to database finished");
		}

		// base edges
		if (edgeLayers.size() > 0) {
			log.info(" saving edges to database started");
			for (Layer<LineString> edgeLayer : edgeLayers) {
				edgeLayer.saveToDatabase();
				log.info("Visited nodes layer saved: " + edgeLayer.getName());
			}
			log.info(" saving edges to database finished");
		}
		log.info(String.format("All data saved in %.1f seconds",
				(double) (System.currentTimeMillis() - start) / 1000.0));
	}

	public void createIndexes() throws SQLException {
		for (Layer<LineString> layer : responseLayers) {
			layer.createIndexes();
		}
		for (Layer<Point> layer : visitedNodesLayers) {
			layer.createIndexes();
		}
		for (Layer<Point> layer : nodeLayers) {
			layer.createIndexes();
		}
		for (Layer<LineString> layer : edgeLayers) {
			layer.createIndexes();
		}
	}

	public void closeConnectionToDatabase() throws SQLException {
		closeConnectionToDb();
	}

	public void saveAndCreateIndexesAndClose() throws SQLException {
		saveToDatabase();
		createIndexes();
		closeConnectionToDatabase();
	}

	private <T extends Geometry> void addGeometryToLayer(Layer<T> layer, long id, T geometry,
			Timestamp fromTime, Timestamp toTime, VisParameters vp, Map<String, Object> description)
			throws TransformException {
		geometry = (T) getProjectionTransformer().transform(geometry);
		layer.addRecord(new GeometryRecord<T>(id, geometry, fromTime, toTime, vp, description));
	}

	private <T extends Geometry> void addGeometryToLayer(Layer<T> layer, T geometry,
			Timestamp fromTime, Timestamp toTime, VisParameters vp, Map<String, Object> description)
			throws TransformException {
		geometry = (T) getProjectionTransformer().transform(geometry);
		layer.addRecord(new GeometryRecord<T>(geometry, fromTime, toTime, vp, description));
	}

}
