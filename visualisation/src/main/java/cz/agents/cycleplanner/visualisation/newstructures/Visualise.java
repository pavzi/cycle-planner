package cz.agents.cycleplanner.visualisation.newstructures;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.opengis.referencing.operation.TransformException;

import cz.agents.basestructures.Graph;
import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

public class Visualise {

	private static final Logger log = Logger.getLogger(Visualise.class);

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {

		log.info("Loading graph...");
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("prague2.ser")));
		CityData<CycleNode, CycleEdge> cityData = (CityData<CycleNode, CycleEdge>) ois.readObject();
		Graph<CycleNode, CycleEdge> graph = cityData.getGraph();

		log.info("# of nodes: " + graph.getAllNodes().size());
		log.info("# of edges: " + graph.getAllEdges().size());

		CycleGraphVisualisation vis = null;

		try {

			vis = VisualizationUtil.initVisualisation(cityData, "_cycleways");
			vis.visualizeGraph(graph, "");

			if (vis != null) {
				vis.saveAndCreateIndexesAndClose();
			}

		} catch (SQLException | TransformException e) {
			System.out.println(e.getMessage());
		}

		ois.close();

	}
}
