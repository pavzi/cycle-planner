package cz.agents.cycleplanner.visualisation.newstructures;

import java.net.MalformedURLException;
import java.sql.SQLException;

import javax.xml.transform.TransformerException;

import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.TransformException;

import cz.agents.agentpolis.tools.geovisio.layer.BoundingBox;
import cz.agents.agentpolis.tools.geovisio.visualisation.VisualisationSettings;
import cz.agents.cycleplanner.core.datastructures.CityData;
import cz.agents.cyclestructures.CycleEdge;
import cz.agents.cyclestructures.CycleNode;

public class VisualizationUtil {

	/**
	 * Visualisation settings
	 */
	public static final VisualisationSettings HONZA_NTB = new VisualisationSettings("localhost", 54321, "postgres", "",
			"medford", "http://localhost:8080/geoserver", "admin", "geovisio");
	public final static VisualisationSettings AGENT4ITS = new VisualisationSettings("its.felk.cvut.cz", 5432, "visio",
			"geovisio", "visio", "http://its.felk.cvut.cz:8080/geoserver", "admin", "geovisio");

	public static CycleGraphVisualisation initVisualisation(CityData<CycleNode, CycleEdge> data,
			String datastoreNameSuffix) {

		final String datastoreName = "prague_tns";
		// minlon=&minlat=&maxlon=&maxlat=&box=yes#map=8/49.821/15.480
//		final BoundingBox boundingBox = new BoundingBox((double) data.getLeftE6() / 1E6,
//				(double) data.getBottomE6() / 1E6, (double) data.getRightE6() / 1E6, (double) data.getTopE6() / 1E6,
//				4326);
		
		// for jar file
		final BoundingBox boundingBox = new BoundingBox((double) data.getBottomE6() / 1E6,
				(double) data.getLeftE6() / 1E6, 
				(double) data.getTopE6() / 1E6, 
				(double) data.getRightE6() / 1E6, 
				4326);
		
		// final BoundingBox boundingBox = new BoundingBox(12.09, 48.55, 18.87,
		// 51.06, 4326);

		try {
			CycleGraphVisualisation vis = null;

			vis = new CycleGraphVisualisation(datastoreName, 4326, 900913, AGENT4ITS, boundingBox,
					new CycleGraphVisParameterMapper(), true);
			return vis;
		} catch (MalformedURLException | ClassNotFoundException | IllegalArgumentException | SQLException
				| FactoryException | TransformException | TransformerException e) {
			System.err.println("Visualization wasn't inited. " + e.getLocalizedMessage());
			e.printStackTrace(System.err);
			return null;
		}
	}

}
